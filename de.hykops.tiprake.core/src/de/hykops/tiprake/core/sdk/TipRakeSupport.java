/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.tiprake.core.sdk;

import de.hykops.lpd.core.Activator;
import de.hykops.lpd.core.factory.IHykopsFactory;
import de.hykops.lpd.core.nomenclature.EMapKeys;
import de.hykops.lpd.core.sdk.CoordinateSystem3DSupport;
import de.hykops.lpd.core.sdk.Point3DSupport;
import de.hykops.lpd.datamodel.HComponent;
import de.hykops.lpd.datamodel.HComposition;
import de.hykops.lpd.datamodel.HCoordinateSystem3D;
import de.hykops.lpd.datamodel.HLinearLoftPath;
import de.hykops.lpd.datamodel.HLoft;
import de.hykops.lpd.datamodel.HPoint3D;
import de.hykops.tiprake.core.TypeID;

/**
 * 
 *
 * @author stoye
 * @since Nov 22, 2016
 */
public class TipRakeSupport {
	/**
	 * Create empty HComponent for a component of type propeller
	 * 
	 * @param name
	 * @param description
	 * @param parent
	 *            the corresponding composition
	 * @param xref
	 *            origin of propeller in composition
	 * @param yref
	 *            origin of propeller in composition
	 * @param zref
	 *            origin of propeller in composition
	 * @param bladeRadius
	 *            the blade radius of the propeller
	 * @return empty propeller data setup
	 *
	 * @author stoye
	 * @since Jul 18, 2016
	 */
	@SuppressWarnings({ "boxing" })
	public static HComponent createEmptyTipRakePropeller(String description, HComposition parent, double xref, double yref, double zref, double radius, int bladeCount, double rakeAngle) {
		// create empty component

		IHykopsFactory factory = Activator.getFactoryService().getFactory();

		HComponent prop = factory.createComponent();
		prop.setHComposition(parent);
		// set composition, name, description
		prop.setHParent(parent);

		prop.setName("Tip-Rake");
		prop.setType(TypeID.TYPE_ID_CONECONSTANT);

		prop.setDescription(description);

		// Coordinate system of the whole propeller component: cartesian
		HCoordinateSystem3D cos = CoordinateSystem3DSupport.createCoordinateSystem3DAffine(prop, /*-*/
				xref, yref, zref, /*-origin*/
				1., 0., 0., /*-x1 axis*/
				0., 1., 0., /*-x2 axis*/
				0., 0., 1.);/*-x3 axis*/
		prop.setHCoordinateSystem3D(cos);
		// create blade loft
		HLoft blade = factory.createLoft();
		blade.setHComponent(prop);
		blade.setHParent(prop);
		blade.setName("blade profile loft");
		blade.setDescription("Default blade loft");

		// determine raked radial axis
		double rx = Math.cos(rakeAngle);
		double rz = Math.sin(rakeAngle);
		
		// Set blade loft coordinate system: CONE
		HCoordinateSystem3D cos2 = CoordinateSystem3DSupport.createCoordinatesSystem3DCone(blade,
				0., 0., 0., /*-origin*/
				1., 0., 0., /*-axial*/
				rx, 0., rz /*-radial*/);
		blade.getHCoordinateSystems3D().add(cos2);
		prop.getHLofts().add(blade);

		// Set blade loft axis: Linear, z upwards
		HLinearLoftPath path = factory.createLinearLoftPath();
		path.setHParent(blade);
		path.setHLoft(blade);
		path.setDenominator("BLADE_LOFT");
		// Coordinate system of the whole propeller component: cartesian
		HCoordinateSystem3D cos3 = CoordinateSystem3DSupport.createCoordinateSystem3DAffine(path, /*-*/
				xref, yref, zref, /*-origin*/
				1., 0., 0., /*-x1 axis*/
				0., 1., 0., /*-x2 axis*/
				0., 0., 1.);/*-x3 axis*/
		path.setHLoftCoordinateSystem(cos3);
		
		double scale = radius;
		// origin of blade loft axis
		HPoint3D orig = Point3DSupport.createPoint3D(path, 0., 0., 0.);
		path.setOrigin(orig);
		// blade loft axis vector
		HPoint3D axis = Point3DSupport.createPoint3D(path, 0., 0., scale);
		path.setLoftPathVector(axis);
		// blade loft perpendicular coordinate 1
		HPoint3D xi = Point3DSupport.createPoint3D(path, scale, 0., 0.);
		path.setLoftPathXi(xi);
		// blade loft perpendicular coordinate 2
		HPoint3D eta = Point3DSupport.createPoint3D(path, 0., 1., 0.); /*- 1.0, da umfangskoordinate in radiant (skaliert NICHT mit)*/
		path.setLoftPathEta(eta);

		blade.setHLoftPath(path);

//		blade.getParameters().put(EMapKeys.ROTFREQ.toString(), String.format("1. 0. 0. %03d", bladeCount));

		return prop;
	}

	// TODO: CHECK!
	public static double[] convertPointToPolar3D(double relProfileRadius, double relProfileRake, double pitchAngleRadians, double relProfileChi, double relChordLength, double relX, double relY,
			boolean clockwise) {
		// John Carlton, Marine Propellers and Propulsions: Eq. 3.17:
		double r = relProfileRadius;
		double iG = -relProfileRake;
		double thetaNt = pitchAngleRadians; /// Math.atan2(pitch, (2. * Math.PI
											/// *r));
		double thetaS = Math.cos(thetaNt) * (relProfileChi - (relChordLength / 2.)) / (r);
		double c = relChordLength;
		return convertPointToPolar3DCarlton(iG, r, thetaS, thetaNt, c, relX, relY, clockwise);
	}

	/**
	 * Transform to polar coordinate system:
	 * 
	 * John Carlton, Marine Propellers and Propulsions: Eq. 3.17:
	 * 
	 * @param iG
	 * @param r
	 * @param thetaS
	 * @param thetaNt
	 * @param c
	 * @param relX
	 * @param relY
	 * @param clockwise
	 * @return point in polar coordinates
	 *
	 * @author stoye
	 * @since Jul 21, 2016
	 */
	// TODO: CHECK!

	public static double[] convertPointToPolar3DCarlton(double iG, double r, double thetaS,
			double thetaNt, double c, double relX, double relY, boolean clockwise) {
		// 1. transform to cartesian coordinates
		double xCart = -(iG + r * thetaS * Math.tan(thetaNt)) + (-0.5 * c + relX) *
				Math.sin(thetaNt) + relY * Math.cos(thetaNt);
		double yCart = r * Math.sin(thetaS - ((-0.5 * c + relX) * Math.cos(thetaNt)
				- relY * Math.sin(thetaNt)) / (r));
		yCart *= clockwise ? 1. : -1.;

		double zCart = r * Math.cos(thetaS - ((-0.5 * c + relX) * Math.cos(thetaNt)
				- relY * Math.sin(thetaNt)) / (r));
		// 2. transform to polar values
		double polarX = -xCart;
		double polarTheta = Math.atan2(yCart, zCart);
		double polarRadius = Math.sqrt(yCart * yCart + zCart * zCart);

		return new double[] { polarX, polarTheta, polarRadius };
	}

}
