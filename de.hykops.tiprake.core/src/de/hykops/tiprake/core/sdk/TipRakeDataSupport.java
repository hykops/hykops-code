/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.tiprake.core.sdk;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.StringTokenizer;

import de.fsg.tk.math.constants.Epsilon;
import de.fsg.tk.math.maps.Pair;
import de.fsg.tk.math.maps.SegmentIterator;
import de.fsg.tk.math.vec.VecOp;
import de.hykops.lpd.core.nomenclature.EMapKeys;
import de.hykops.lpd.core.sdk.CoordinateSystem3DSupport;
import de.hykops.lpd.datamodel.HComponent;
import de.hykops.lpd.datamodel.HCoordinateSystem3D;
import de.hykops.lpd.datamodel.HCoordinateSystem3DAffine;
import de.hykops.lpd.datamodel.HCoordinateSystem3DCone;
import de.hykops.lpd.datamodel.HLinearLoftPath;
import de.hykops.lpd.datamodel.HLoft;
import de.hykops.lpd.datamodel.HLoftElement;
import de.hykops.lpd.datamodel.HLoftPath;
import de.hykops.lpd.datamodel.HPoint3D;

/**
 * 
 *
 * @author stoye
 * @since Nov 22, 2016
 */
public class TipRakeDataSupport {
	public static double getBladeRadius(HComponent component) {
		if (component.getHLofts().size() == 1) {
			HLoftPath blade = component.getHLofts().get(0).getHLoftPath();
			double coneAngle = TipRakeDataSupport.getSpindleRakeAngle(component);
			if (blade instanceof HLinearLoftPath) {
				HPoint3D axis = ((HLinearLoftPath) blade).getLoftPathVector();
				double r = axis.getX3() * Math.sin(coneAngle);
				return r;
			}
		}
		return 0.;
	}

	public static void setBladeRadius(HComponent component, double radius) {
		if (component.getHLofts().size() == 1) {
			HLoftPath blade = component.getHLofts().get(0).getHLoftPath();
			if (blade instanceof HLinearLoftPath) {
				HPoint3D axis = ((HLinearLoftPath) blade).getLoftPathVector();
				HPoint3D xi = ((HLinearLoftPath) blade).getLoftPathXi();

				double coneAngle = TipRakeDataSupport.getSpindleRakeAngle(component);
				if (Math.abs(coneAngle) > Epsilon.NANO) {
					double rSpindle = radius / Math.sin(coneAngle);
					axis.setX3(rSpindle);
					xi.setX1(rSpindle);
					/*-eta nicht skalieren: Siehe PropellerSupport.createEmptyPropeller*/
				}
			}
		}
	}

	public static double getSpindleRakeAngle(HComponent component) {
		if (component.getHLofts().size() == 1) {
			HCoordinateSystem3D cos = component.getHLofts().get(0).getHCoordinateSystems3D().get(0);
			if (cos instanceof HCoordinateSystem3DCone) {
				HPoint3D axial = ((HCoordinateSystem3DCone) cos).getAxialVector();
				HPoint3D radial = ((HCoordinateSystem3DCone) cos).getRadialVector();
				double angle = VecOp.getAngle(new double[] { axial.getX1(), axial.getX2(), axial.getX3() }, new double[] { radial.getX1(), radial.getX2(), radial.getX3() });
				return angle;
			}
		}
		return Math.PI / 2.;
	}

	public static void setSpindleRakeAngle(HComponent component, double angleRadians) {
		if (component.getHLofts().size() == 1) {
			HCoordinateSystem3D cos = component.getHLofts().get(0).getHCoordinateSystems3D().get(0);
			if (cos instanceof HCoordinateSystem3DCone) {
				HPoint3D axial = ((HCoordinateSystem3DCone) cos).getAxialVector();
				HPoint3D radial = ((HCoordinateSystem3DCone) cos).getRadialVector();
				double lxz = VecOp.getLength(new double[] { radial.getX1(), radial.getX3() });
				double rx = lxz * Math.cos(angleRadians);
				double rz = lxz * Math.sin(angleRadians);
				radial.setX1(rx);
				radial.setX3(rz);
			}
		}
	}

	@SuppressWarnings("boxing")
	public static double getPitchAngle(HComponent component, double relRadius) {
		if (component.getHLofts().size() == 1) {
			HLoftPath blade = component.getHLofts().get(0).getHLoftPath();
			if (blade instanceof HLinearLoftPath) {
				{
					int np = component.getHLofts().get(0).getHLoftElements().size();

					List<Pair<Double, Double>> curve = new ArrayList<Pair<Double, Double>>();

					double x[] = new double[np];
					double y[] = new double[np];
					for (int i = 0; i < np; i++) {
						HLoftElement element = component.getHLofts().get(0).getHLoftElements().get(i);
						x[i] = getRelRadius(element);
						y[i] = getPitchAngle(element);
						curve.add(new Pair<Double, Double>(x[i], y[i]));
					}
					Collections.sort(curve, new Comparator<Pair<Double, Double>>() {
						@Override
						public int compare(Pair<Double, Double> o1, Pair<Double, Double> o2) {
							if (o1.getT1() < o2.getT1())
								return -1;
							if (o1.getT1() > o2.getT1())
								return 1;
							return 0;
						}
					});
					// piecewise linear interpolation
					for (SegmentIterator<Pair<Double, Double>> it = new SegmentIterator<Pair<Double, Double>>(curve, false); it.hasNext();) {
						Pair<Pair<Double, Double>, Pair<Double, Double>> p = it.next();
						double r1 = p.getT1().getT1();
						double p1 = p.getT1().getT2();
						double r2 = p.getT2().getT1();
						double p2 = p.getT2().getT2();

						if (relRadius >= r1 && relRadius <= r2) {
							if (Math.abs(r2 - r1) > Epsilon.NANO) {
								double c = (relRadius - r1) / (r2 - r1);
								return (p1 * (1. - c)) + (p2 * c);
							}
							return (p1 + p2) / 2.;

						}
					}

				}
			}
		}
		return 0.;
	}

	public static void setPitchAngle(HComponent component, double relRadius, double newPitch) {
		double oldPitch = getPitchAngle(component, relRadius);
		double scale = newPitch / oldPitch;
		for (HLoftElement element : component.getHLofts().get(0).getHLoftElements()) {
			double profilePitch = getPitchAngle(element);
			setPitchAngle(element, (profilePitch * scale));
		}
	}

	public static double getPitchRatio(HComponent component, double relRadius) {
		double phi = getPitchAngle(component, relRadius);
		return 0.7 * Math.PI * Math.tan(phi);
	}

	public static void setPitchRatio(HComponent component, double relRadius, double pdRatio) {
		double d = 2. * getBladeRadius(component);
		double p = pdRatio * d;

		double phi = Math.atan2(p, (0.7 * Math.PI * d));
		setPitchAngle(component, relRadius, phi);
	}

	@SuppressWarnings("boxing")
	public static double getChordLength(HComponent component, double relRadius) {
		if (component.getHLofts().size() == 1) {
			HLoftPath blade = component.getHLofts().get(0).getHLoftPath();
			if (blade instanceof HLinearLoftPath) {
				{
					int np = component.getHLofts().get(0).getHLoftElements().size();

					List<Pair<Double, Double>> curve = new ArrayList<Pair<Double, Double>>();

					double x[] = new double[np];
					double y[] = new double[np];
					for (int i = 0; i < np; i++) {
						HLoftElement element = component.getHLofts().get(0).getHLoftElements().get(i);
						x[i] = getRelRadius(element);
						y[i] = getChordLength(element);
						curve.add(new Pair<Double, Double>(x[i], y[i]));
					}
					Collections.sort(curve, new Comparator<Pair<Double, Double>>() {
						@Override
						public int compare(Pair<Double, Double> o1, Pair<Double, Double> o2) {
							if (o1.getT1() < o2.getT1())
								return -1;
							if (o1.getT1() > o2.getT1())
								return 1;
							return 0;
						}
					});
					// piecewise linear interpolation
					for (SegmentIterator<Pair<Double, Double>> it = new SegmentIterator<Pair<Double, Double>>(curve, false); it.hasNext();) {
						Pair<Pair<Double, Double>, Pair<Double, Double>> p = it.next();
						double r1 = p.getT1().getT1();
						double c1 = p.getT1().getT2();
						double r2 = p.getT2().getT1();
						double c2 = p.getT2().getT2();
						if (relRadius >= r1 && relRadius <= r2) {
							if (Math.abs(r2 - r1) > Epsilon.NANO) {
								double c = (relRadius - r1) / (r2 - r1);
								return (c1 * (1. - c)) + (c2 * c);
							}
							return (c1 + c2) / 2.;

						}
					}

				}
			}
		}
		return 0.;
	}

	@SuppressWarnings("boxing")
	public static double getRake(HComponent component, double relRadius) {
		if (component.getHLofts().size() == 1) {
			HLoftPath blade = component.getHLofts().get(0).getHLoftPath();
			if (blade instanceof HLinearLoftPath) {
				{
					int np = component.getHLofts().get(0).getHLoftElements().size();

					List<Pair<Double, Double>> curve = new ArrayList<Pair<Double, Double>>();

					double x[] = new double[np];
					double y[] = new double[np];
					for (int i = 0; i < np; i++) {
						HLoftElement element = component.getHLofts().get(0).getHLoftElements().get(i);
						x[i] = getRelRadius(element);
						y[i] = getRake(element);
						curve.add(new Pair<Double, Double>(x[i], y[i]));
					}
					Collections.sort(curve, new Comparator<Pair<Double, Double>>() {
						@Override
						public int compare(Pair<Double, Double> o1, Pair<Double, Double> o2) {
							if (o1.getT1() < o2.getT1())
								return -1;
							if (o1.getT1() > o2.getT1())
								return 1;
							return 0;
						}
					});

					// piecewise linear interpolation
					for (SegmentIterator<Pair<Double, Double>> it = new SegmentIterator<Pair<Double, Double>>(curve, false); it.hasNext();) {
						Pair<Pair<Double, Double>, Pair<Double, Double>> p = it.next();
						double r1 = p.getT1().getT1();
						double rake1 = p.getT1().getT2();
						double r2 = p.getT2().getT1();
						double rake2 = p.getT2().getT2();

						if (relRadius >= r1 && relRadius <= r2) {
							if (Math.abs(r2 - r1) > Epsilon.NANO) {
								double c = (relRadius - r1) / (r2 - r1);
								return (rake1 * (1. - c)) + (rake2 * c);
							}
							return (rake1 + rake2) / 2.;

						}
					}

				}
			}
		}
		return 0.;
	}

	@SuppressWarnings("boxing")
	public static double getChi(HComponent component, double relRadius) {
		if (component.getHLofts().size() == 1) {
			HLoftPath blade = component.getHLofts().get(0).getHLoftPath();
			if (blade instanceof HLinearLoftPath) {
				{
					int np = component.getHLofts().get(0).getHLoftElements().size();

					List<Pair<Double, Double>> curve = new ArrayList<Pair<Double, Double>>();

					double x[] = new double[np];
					double y[] = new double[np];
					for (int i = 0; i < np; i++) {
						HLoftElement element = component.getHLofts().get(0).getHLoftElements().get(i);
						x[i] = getRelRadius(element);
						y[i] = getChi(element);
						curve.add(new Pair<Double, Double>(x[i], y[i]));
					}
					Collections.sort(curve, new Comparator<Pair<Double, Double>>() {
						@Override
						public int compare(Pair<Double, Double> o1, Pair<Double, Double> o2) {
							if (o1.getT1() < o2.getT1())
								return -1;
							if (o1.getT1() > o2.getT1())
								return 1;
							return 0;
						}
					});

					// piecewise linear interpolation
					for (SegmentIterator<Pair<Double, Double>> it = new SegmentIterator<Pair<Double, Double>>(curve, false); it.hasNext();) {
						Pair<Pair<Double, Double>, Pair<Double, Double>> p = it.next();
						double r1 = p.getT1().getT1();
						double chi1 = p.getT1().getT2();
						double r2 = p.getT2().getT1();
						double chi2 = p.getT2().getT2();

						if (relRadius >= r1 && relRadius <= r2) {
							if (Math.abs(r2 - r1) > Epsilon.NANO) {
								double c = (relRadius - r1) / (r2 - r1);
								return (chi1 * (1. - c)) + (chi2 * c);
							}
							return (chi1 + chi2) / 2.;
						}
					}

				}
			}
		}
		return 0.;
	}

	@SuppressWarnings("unused")
	public static double getSkew(HComponent component, double relRadius) {
		if (component.getHLofts().size() == 1) {
			HLoftPath blade = component.getHLofts().get(0).getHLoftPath();
			if (blade instanceof HLinearLoftPath) {
				{
					int np = component.getHLofts().get(0).getHLoftElements().size();

					double min = Double.MAX_VALUE;
					double max = -Double.MAX_VALUE;

					for (int i = 0; i < np; i++) {
						HLoftElement element = component.getHLofts().get(0).getHLoftElements().get(i);
						double s = getSkewAngle(element);

						if (s > max) {
							max = s;
						}
						if (s < min) {
							min = s;
						}
					}
					return max - min;
				}
			}
		}
		return 0.;
	}

	public static double getRelRadius(HLoftElement element) {
		HCoordinateSystem3D cos = element.getLoftPlaneDefiningCoordinateSystem();
		if (cos instanceof HCoordinateSystem3DAffine) {
			HPoint3D orig = ((HCoordinateSystem3DAffine) cos).getOrigin();
			return orig.getX3();
		}
		return 0.;
	}

	public static double getPitchAngle(HLoftElement element) {
		HCoordinateSystem3D cos = element.getLoftPlaneDefiningCoordinateSystem();
		if (cos instanceof HCoordinateSystem3DAffine) {
			HPoint3D xi = ((HCoordinateSystem3DAffine) cos).getX1();
			double dx = xi.getX1();
			double du = xi.getX2() * getRelRadius(element);
			return Math.atan2(-dx, -du);
		}
		return 0.;
	}

	public static void setPitchAngle(HLoftElement element, double newPitchAngle) {
		double chord = TipRakeDataSupport.getRelChordLength(element);
		double pitch = TipRakeDataSupport.getPitchAngle(element);
		double rake = TipRakeDataSupport.getRelProfileRake(element);
		double chi = TipRakeDataSupport.getRelChi(element);
		double relRadius = TipRakeDataSupport.getRelRadius(element);
		// TODO: CLOCKWISE identifizieren/persistieren
		boolean clockwise = true;

		pitch = newPitchAngle;

		double[] polarOrig = TipRakeSupport.convertPointToPolar3D(/*-*/
				relRadius, /*-*/
				rake, /*-*/
				pitch, /*-*/
				chi, /*-*/
				chord, /*-*/
				0., /*-*/
				0., /*-*/
				clockwise);

		double[] polarXi = TipRakeSupport.convertPointToPolar3D(/*-*/
				relRadius, /*-*/
				rake, /*-*/
				pitch, /*-*/
				chi, /*-*/
				chord, /*-*/
				chord, /*-*/
				0., /*-*/
				clockwise);

		double[] polarEta = TipRakeSupport.convertPointToPolar3D(/*-*/
				relRadius, /*-*/
				rake, /*-*/
				pitch, /*-*/
				chi, /*-*/
				chord, /*-*/
				0., /*-*/
				-chord, /*-*/
				clockwise);

		// xi = chordline
		double xi[] = de.fsg.tk.math.vec.VecOp.sub(polarXi, polarOrig);
		// eta
		double eta[] = de.fsg.tk.math.vec.VecOp.sub(polarEta, polarOrig);
		// perpendicular vector
		double z1 = 0.;
		double z2 = 0.;
		double z3 = 1.;

		// exchange loft element (2D-)coordinate system
		HCoordinateSystem3D planeCos = CoordinateSystem3DSupport.createCoordinateSystem3DAffine(null, /*-*/
				polarOrig[0], polarOrig[1], polarOrig[2], /*-*/
				xi[0], xi[1], xi[2], /*-*/ //
				eta[0], eta[1], eta[2], /*-*/
				z1, z2, z3);/*-*/
		element.setLoftPlaneDefiningCoordinateSystem(planeCos);
		planeCos.setHParent(element);
	}

	public static double getRelChordLength(HLoftElement element) {
		HCoordinateSystem3D cos = element.getLoftPlaneDefiningCoordinateSystem();
		if (cos instanceof HCoordinateSystem3DAffine) {
			HPoint3D xi = ((HCoordinateSystem3DAffine) cos).getX1();
			double dx = xi.getX1();
			double du = xi.getX2() * getRelRadius(element);
			double chord = Math.sqrt(Math.pow(dx, 2.) + Math.pow(du, 2.));
			return chord;
		}
		return 0.;
	}

	public static double getChordLength(HLoftElement element) {

		HComponent component = element.getHLoft().getHComponent();
		double bladeRadius = getBladeRadius(component);
		return bladeRadius * getRelChordLength(element);
	}

	public static double getRelProfileRake(HLoftElement element) {
		HCoordinateSystem3D cos = element.getLoftPlaneDefiningCoordinateSystem();
		if (cos instanceof HCoordinateSystem3DAffine) {
			HPoint3D orig = ((HCoordinateSystem3DAffine) cos).getOrigin();
			double chi = getRelChi(element);
			double pitch = getPitchAngle(element);
			double xorig = orig.getX1();
			double rake = -xorig + chi * Math.sin(pitch);

			return rake;
		}
		return 0.;
	}

	public static double getRake(HLoftElement element) {
		HComponent component = element.getHLoft().getHComponent();
		double bladeRadius = getBladeRadius(component);
		return bladeRadius * getRelProfileRake(element);
	}

	public static double getRelChi(HLoftElement element) {
		HCoordinateSystem3D cos = element.getLoftPlaneDefiningCoordinateSystem();
		if (cos instanceof HCoordinateSystem3DAffine) {
			HPoint3D orig = ((HCoordinateSystem3DAffine) cos).getOrigin();
			double pitch = getPitchAngle(element);
			double relRadius = getRelRadius(element);
			double chi = relRadius * (orig.getX2()) / Math.cos(pitch);
			return chi;

		}
		return 0.;
	}

	public static double getChi(HLoftElement element) {

		HComponent component = element.getHLoft().getHComponent();
		double bladeRadius = getBladeRadius(component);
		return bladeRadius * getRelChi(element);
	}

	public static double getSkewAngle(HLoftElement element) {
		HCoordinateSystem3D cos = element.getLoftPlaneDefiningCoordinateSystem();
		if (cos instanceof HCoordinateSystem3DAffine) {

			double pitch = getPitchAngle(element);
			double relRadius = getRelRadius(element);
			double chi = getRelChi(element);
			double chord = getRelChordLength(element);
			double rake = getRelProfileRake(element);

			double p[] = TipRakeSupport.convertPointToPolar3D(relRadius, rake, pitch, chi, chord, 0.5, 0., true);
			// TODO: Dies stimmt nicht mit der PFF-Ansicht überein!
			return p[1];

		}
		return 0.;
	}

	public static int getBladeCount(HComponent component) {
		if (component.getHLofts().size() > 0) {
			HLoft loft = component.getHLofts().get(0);
			if (loft.getParameters().containsKey(EMapKeys.ROTFREQ.toString())) {
				double rotAxis[] = new double[3];
				int z = 1;
				String value = (loft.getParameters().get(EMapKeys.ROTFREQ.toString()));
				if (value != null && value.length() > 0) {
					StringTokenizer st = new StringTokenizer(value);
					if (st.countTokens() == 4) {
						rotAxis[0] = Double.parseDouble(st.nextToken());
						rotAxis[1] = Double.parseDouble(st.nextToken());
						rotAxis[2] = Double.parseDouble(st.nextToken());
						z = Integer.parseInt(st.nextToken());
					}
				}
				return z;
			}
		}
		return 1;
	}

	@SuppressWarnings("boxing")
	public static void setBladeCount(HComponent component, int bladeCount) {
		if (component.getHLofts().size() > 0) {
			HLoft loft = component.getHLofts().get(0);
			String value = String.format("%3.1f %3.1f %3.1f %03d", 1., 0., 0., bladeCount);
			loft.getParameters().put(EMapKeys.ROTFREQ.toString(), value);
		}
	}

}
