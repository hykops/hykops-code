package de.hykops.lpd.datamodel;

/**
 * @model
 * @author Thomas Stoye
 *
 */
public interface HCurvePoint2D extends HPoint2D {
	
	static final String CLASS_NAME = HCurvePoint2D.class.getSimpleName();

	static final String PROPERTY_CURVE_COORDINATE = "curveCoordinate";
	
	/**
	* @model
	*/
	public Double getCurveCoordinate();

	/**
	 * Sets the value of the '{@link de.hykops.lpd.datamodel.HCurvePoint2D#getCurveCoordinate <em>Curve Coordinate</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Curve Coordinate</em>' attribute.
	 * @see #getCurveCoordinate()
	 * @generated
	 */
	void setCurveCoordinate(Double value);

}
