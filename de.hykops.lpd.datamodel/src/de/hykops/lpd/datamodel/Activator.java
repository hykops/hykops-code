package de.hykops.lpd.datamodel;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

public class Activator implements BundleActivator {

	private static BundleContext context;
	private static IEMFService emfService;

	@Override
	public void start(BundleContext context) throws Exception {
		Activator.context = context;
		ServiceReference<IEMFService> service = context.getServiceReference(IEMFService.class);
		Activator.emfService = context.getService(service);
	}

	@Override
	public void stop(BundleContext context) throws Exception {
		Activator.context = null;
	}

	public static IEMFService getEmfService() {
		return Activator.emfService;
	}
	
	public static BundleContext getBundleContext() {
		return Activator.context;
	}

}
