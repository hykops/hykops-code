/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.lpd.datamodel;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;

/**
 * Coordinate system containing different coordinate systems dependent on the
 * loft coordinate. The coordinate systems contained in the list are relative to
 * the origin of this coordinate system: There is only an origin, no further
 * coordinate axes in this class. The coordinate axes are to be calculated by
 * interpolation of the coordinate axes of the list. The coordinate system
 * dependent on the loft coordinate can be identified by interpolation between
 * the coordinate systems in the list. The corresponding
 * "loft coordinate of the coordinate system" depends on the component type:
 * E.g. for a tip-fin propeller with different cone coordinate systems for each
 * profile, the coordinate-systems in the list are related to a loft coordinate
 * by calculating the angle of the origin of each coordinate system relative to
 * this.origin in the axial-radial plane.
 * 
 * @model
 * @author stoye
 * @since Nov 11, 2016
 */
public interface HCoordinateSystem3DLofted extends HCoordinateSystem3D {

	/**
	 * 
	 * @model containment = "true"
	 *
	 */
	EMap<String, String> getParameters();

	/**
	 * @model containment = "true"
	 * @return
	 *
	 * @author stoye
	 * @since Nov 11, 2016
	 */
	EList<HCoordinateSystem3D> getLoftedCoordinateSystemElements();
}
