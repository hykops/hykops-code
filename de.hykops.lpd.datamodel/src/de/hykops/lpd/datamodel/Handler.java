package de.hykops.lpd.datamodel;

import com.sun.jna.NativeLong;

public interface Handler {

	public NativeLong getHandle();

}
