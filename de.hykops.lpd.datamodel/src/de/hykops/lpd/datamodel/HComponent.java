package de.hykops.lpd.datamodel;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;

/**
 * @model opposite = "hComposition"
 */
public interface HComponent extends HParent {

	static final String CLASS_NAME = HComponent.class.getSimpleName();

	static final String PROPERTY_NAME = "name";
	static final String PROPERTY_DESCRIPTION = "description";
	static final String PROPERTY_TYPE = "type";
	static final String PROPERTY_COORDINATESYSTEM = "hCoordinateSystem3D";
	static final String PROPERTY_COMPOSITION = "hComposition";
	static final String PROPERTY_LOFTS = "hLofts";

	static final String PROPERTY_PARAMTER = "parameters";
	
	/**
	 * @model
	 */
	String getName();

	/**
	 * Sets the value of the '{@link de.hykops.lpd.datamodel.HComponent#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * @model
	 */
	String getType();

	/**
	 * Sets the value of the '{@link de.hykops.lpd.datamodel.HComponent#getType <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' attribute.
	 * @see #getType()
	 * @generated
	 */
	void setType(String value);

	/**
	 * @model
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link de.hykops.lpd.datamodel.HComponent#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * @model containment = "true"
	 */
	EMap<String, String> getParameters();


	/**
	 * @model containment = "true"
	 */
	HCoordinateSystem3D getHCoordinateSystem3D();

	/**
	 * Sets the value of the '{@link de.hykops.lpd.datamodel.HComponent#getHCoordinateSystem3D <em>HCoordinate System3 D</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>HCoordinate System3 D</em>' containment reference.
	 * @see #getHCoordinateSystem3D()
	 * @generated
	 */
	void setHCoordinateSystem3D(HCoordinateSystem3D value);

	/**
	 * @model containment = "true" opposite = component
	 */
	EList<HLoft> getHLofts();

	/**
	 * @model opposite = "hComponents"
	 */
	HComposition getHComposition();

	/**
	 * Sets the value of the '{@link de.hykops.lpd.datamodel.HComponent#getHComposition <em>HComposition</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>HComposition</em>' container reference.
	 * @see #getHComposition()
	 * @generated
	 */
	void setHComposition(HComposition value);
}
