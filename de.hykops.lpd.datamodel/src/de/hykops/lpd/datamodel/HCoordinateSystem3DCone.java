/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.lpd.datamodel;

/**
 * @model
 *
 * @author stoye
 * @since Jun 30, 2016
 */
public interface HCoordinateSystem3DCone extends HCoordinateSystem3D {
	static final String CLASS_NAME = HCoordinateSystem3DCone.class.getSimpleName();

	static final String PROPERTY_AXIAL_VECTOR = "axialVector";
	static final String PROPERTY_RADIAL_VECTOR = "radialVector";
	static final String PROPERTY_ANGLE = "angle";
	
	/**
	 * @model containment = "true"
	 * @return the axial vector
	 */
	public HPoint3D getAxialVector();

	/**
	 * Sets the value of the '{@link de.hykops.lpd.datamodel.HCoordinateSystem3DCone#getAxialVector <em>Axial Vector</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Axial Vector</em>' containment reference.
	 * @see #getAxialVector()
	 * @generated
	 */
	void setAxialVector(HPoint3D value);

	/**
	 * @model containment = "true"
	 * @return the radial vector of the coordinate system
	 */
	public HPoint3D getRadialVector();

	/**
	 * Sets the value of the '{@link de.hykops.lpd.datamodel.HCoordinateSystem3DCone#getRadialVector <em>Radial Vector</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Radial Vector</em>' containment reference.
	 * @see #getRadialVector()
	 * @generated
	 */
	void setRadialVector(HPoint3D value);

	/**
	 * @model containment = "true"
	 * @return the angular value in radians
	 */
	public HPoint3D getAngle();

	/**
	 * Sets the value of the '{@link de.hykops.lpd.datamodel.HCoordinateSystem3DCone#getAngle <em>Angle</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Angle</em>' containment reference.
	 * @see #getAngle()
	 * @generated
	 */
	void setAngle(HPoint3D value);

}
