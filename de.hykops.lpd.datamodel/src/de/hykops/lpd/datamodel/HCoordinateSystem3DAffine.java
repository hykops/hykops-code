package de.hykops.lpd.datamodel;

/**
 * @model
 * @author Thomas Stoye
 *
 */
public interface HCoordinateSystem3DAffine extends HCoordinateSystem3D {

	
	static final String CLASS_NAME = HCoordinateSystem3DAffine.class.getSimpleName();

	static final String PROPERTY_X1 = "x1";
	static final String PROPERTY_X2 = "x2";
	static final String PROPERTY_X3 = "x3";
	
	/**
	 * @model containment = "true"
	 * @return the 1st coordinate axis vector
	 */
	public HPoint3D getX1();

	/**
	 * Sets the value of the '{@link de.hykops.lpd.datamodel.HCoordinateSystem3DAffine#getX1 <em>X1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>X1</em>' containment reference.
	 * @see #getX1()
	 * @generated
	 */
	void setX1(HPoint3D value);

	/**
	 * @model containment = "true"
	 * @return the 2nd coordinate axis vector
	 */
	public HPoint3D getX2();

	/**
	 * Sets the value of the '{@link de.hykops.lpd.datamodel.HCoordinateSystem3DAffine#getX2 <em>X2</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>X2</em>' containment reference.
	 * @see #getX2()
	 * @generated
	 */
	void setX2(HPoint3D value);

	/**
	 * @model containment = "true"
	 * @return the 3rd coordinate axis vector
	 */
	public HPoint3D getX3();

	/**
	 * Sets the value of the '{@link de.hykops.lpd.datamodel.HCoordinateSystem3DAffine#getX3 <em>X3</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>X3</em>' containment reference.
	 * @see #getX3()
	 * @generated
	 */
	void setX3(HPoint3D value);

}
