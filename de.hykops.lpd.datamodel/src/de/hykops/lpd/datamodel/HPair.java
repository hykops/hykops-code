package de.hykops.lpd.datamodel;

/**
 * @model
 * @author Thomas Stoye
 *
 */
public interface HPair<A, B> extends HParent {
	/**
	 * @model containment = "true"
	 * @return the first part of the pair
	 */
	public A getA();

	/**
	 * Sets the value of the '{@link de.hykops.lpd.datamodel.HPair#getA <em>A</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>A</em>' containment reference.
	 * @see #getA()
	 * @generated
	 */
	void setA(A value);

	/**
	 * @model containment = "true"
	 * @return the second part of the pair
	 */
	public B getB();

	/**
	 * Sets the value of the '{@link de.hykops.lpd.datamodel.HPair#getB <em>B</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>B</em>' containment reference.
	 * @see #getB()
	 * @generated
	 */
	void setB(B value);

}
