/**
 */
package de.hykops.lpd.datamodel.provider;


import de.hykops.lpd.datamodel.DatamodelFactory;
import de.hykops.lpd.datamodel.DatamodelPackage;
import de.hykops.lpd.datamodel.HLoft;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.ComposeableAdapterFactory;
import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link de.hykops.lpd.datamodel.HLoft} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class HLoftItemProvider extends HParentItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HLoftItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

			addNamePropertyDescriptor(object);
			addDescriptionPropertyDescriptor(object);
		}
		return itemPropertyDescriptors;
	}

	/**
	 * This adds a property descriptor for the Name feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addNamePropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_HLoft_name_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_HLoft_name_feature", "_UI_HLoft_type"),
				 DatamodelPackage.Literals.HLOFT__NAME,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This adds a property descriptor for the Description feature.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void addDescriptionPropertyDescriptor(Object object) {
		itemPropertyDescriptors.add
			(createItemPropertyDescriptor
				(((ComposeableAdapterFactory)adapterFactory).getRootAdapterFactory(),
				 getResourceLocator(),
				 getString("_UI_HLoft_description_feature"),
				 getString("_UI_PropertyDescriptor_description", "_UI_HLoft_description_feature", "_UI_HLoft_type"),
				 DatamodelPackage.Literals.HLOFT__DESCRIPTION,
				 true,
				 false,
				 false,
				 ItemPropertyDescriptor.GENERIC_VALUE_IMAGE,
				 null,
				 null));
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(DatamodelPackage.Literals.HLOFT__PARAMETERS);
			childrenFeatures.add(DatamodelPackage.Literals.HLOFT__HCOORDINATE_SYSTEMS3_D);
			childrenFeatures.add(DatamodelPackage.Literals.HLOFT__HLOFT_PATH);
			childrenFeatures.add(DatamodelPackage.Literals.HLOFT__HLOFT_ELEMENTS);
			childrenFeatures.add(DatamodelPackage.Literals.HLOFT__HSURFACE_IDENTIFICATIONS);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns HLoft.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/HLoft"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		String label = ((HLoft)object).getName();
		return label == null || label.length() == 0 ?
			getString("_UI_HLoft_type") :
			getString("_UI_HLoft_type") + " " + label;
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(HLoft.class)) {
			case DatamodelPackage.HLOFT__NAME:
			case DatamodelPackage.HLOFT__DESCRIPTION:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), false, true));
				return;
			case DatamodelPackage.HLOFT__PARAMETERS:
			case DatamodelPackage.HLOFT__HCOORDINATE_SYSTEMS3_D:
			case DatamodelPackage.HLOFT__HLOFT_PATH:
			case DatamodelPackage.HLOFT__HLOFT_ELEMENTS:
			case DatamodelPackage.HLOFT__HSURFACE_IDENTIFICATIONS:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(DatamodelPackage.Literals.HLOFT__HCOORDINATE_SYSTEMS3_D,
				 DatamodelFactory.eINSTANCE.createHCoordinateSystem3DAffine()));

		newChildDescriptors.add
			(createChildParameter
				(DatamodelPackage.Literals.HLOFT__HCOORDINATE_SYSTEMS3_D,
				 DatamodelFactory.eINSTANCE.createHCoordinateSystem3DCartesian()));

		newChildDescriptors.add
			(createChildParameter
				(DatamodelPackage.Literals.HLOFT__HCOORDINATE_SYSTEMS3_D,
				 DatamodelFactory.eINSTANCE.createHCoordinateSystem3DPolar()));

		newChildDescriptors.add
			(createChildParameter
				(DatamodelPackage.Literals.HLOFT__HCOORDINATE_SYSTEMS3_D,
				 DatamodelFactory.eINSTANCE.createHCoordinateSystem3DCone()));

		newChildDescriptors.add
			(createChildParameter
				(DatamodelPackage.Literals.HLOFT__HLOFT_PATH,
				 DatamodelFactory.eINSTANCE.createHLoftPath()));

		newChildDescriptors.add
			(createChildParameter
				(DatamodelPackage.Literals.HLOFT__HLOFT_PATH,
				 DatamodelFactory.eINSTANCE.createHDiscreteLoftPath()));

		newChildDescriptors.add
			(createChildParameter
				(DatamodelPackage.Literals.HLOFT__HLOFT_PATH,
				 DatamodelFactory.eINSTANCE.createHLinearLoftPath()));

		newChildDescriptors.add
			(createChildParameter
				(DatamodelPackage.Literals.HLOFT__HLOFT_ELEMENTS,
				 DatamodelFactory.eINSTANCE.createHLoftElement()));

		newChildDescriptors.add
			(createChildParameter
				(DatamodelPackage.Literals.HLOFT__HSURFACE_IDENTIFICATIONS,
				 DatamodelFactory.eINSTANCE.createHSurfaceIdentifier()));
	}

}
