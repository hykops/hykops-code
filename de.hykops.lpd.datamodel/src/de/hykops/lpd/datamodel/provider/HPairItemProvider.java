/**
 */
package de.hykops.lpd.datamodel.provider;


import de.hykops.lpd.datamodel.DatamodelFactory;
import de.hykops.lpd.datamodel.DatamodelPackage;
import de.hykops.lpd.datamodel.HPair;

import java.util.Collection;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EStructuralFeature;

import org.eclipse.emf.edit.provider.IItemPropertyDescriptor;
import org.eclipse.emf.edit.provider.ViewerNotification;

/**
 * This is the item provider adapter for a {@link de.hykops.lpd.datamodel.HPair} object.
 * <!-- begin-user-doc -->
 * <!-- end-user-doc -->
 * @generated
 */
public class HPairItemProvider extends HParentItemProvider {
	/**
	 * This constructs an instance from a factory and a notifier.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HPairItemProvider(AdapterFactory adapterFactory) {
		super(adapterFactory);
	}

	/**
	 * This returns the property descriptors for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public List<IItemPropertyDescriptor> getPropertyDescriptors(Object object) {
		if (itemPropertyDescriptors == null) {
			super.getPropertyDescriptors(object);

		}
		return itemPropertyDescriptors;
	}

	/**
	 * This specifies how to implement {@link #getChildren} and is used to deduce an appropriate feature for an
	 * {@link org.eclipse.emf.edit.command.AddCommand}, {@link org.eclipse.emf.edit.command.RemoveCommand} or
	 * {@link org.eclipse.emf.edit.command.MoveCommand} in {@link #createCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Collection<? extends EStructuralFeature> getChildrenFeatures(Object object) {
		if (childrenFeatures == null) {
			super.getChildrenFeatures(object);
			childrenFeatures.add(DatamodelPackage.Literals.HPAIR__A);
			childrenFeatures.add(DatamodelPackage.Literals.HPAIR__B);
		}
		return childrenFeatures;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EStructuralFeature getChildFeature(Object object, Object child) {
		// Check the type of the specified child object and return the proper feature to use for
		// adding (see {@link AddCommand}) it as a child.

		return super.getChildFeature(object, child);
	}

	/**
	 * This returns HPair.gif.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object getImage(Object object) {
		return overlayImage(object, getResourceLocator().getImage("full/obj16/HPair"));
	}

	/**
	 * This returns the label text for the adapted class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getText(Object object) {
		return getString("_UI_HPair_type");
	}
	

	/**
	 * This handles model notifications by calling {@link #updateChildren} to update any cached
	 * children and by creating a viewer notification, which it passes to {@link #fireNotifyChanged}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void notifyChanged(Notification notification) {
		updateChildren(notification);

		switch (notification.getFeatureID(HPair.class)) {
			case DatamodelPackage.HPAIR__A:
			case DatamodelPackage.HPAIR__B:
				fireNotifyChanged(new ViewerNotification(notification, notification.getNotifier(), true, false));
				return;
		}
		super.notifyChanged(notification);
	}

	/**
	 * This adds {@link org.eclipse.emf.edit.command.CommandParameter}s describing the children
	 * that can be created under this object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected void collectNewChildDescriptors(Collection<Object> newChildDescriptors, Object object) {
		super.collectNewChildDescriptors(newChildDescriptors, object);

		newChildDescriptors.add
			(createChildParameter
				(DatamodelPackage.Literals.HPAIR__A,
				 DatamodelFactory.eINSTANCE.createHComponent()));

		newChildDescriptors.add
			(createChildParameter
				(DatamodelPackage.Literals.HPAIR__A,
				 DatamodelFactory.eINSTANCE.createHComposition()));

		newChildDescriptors.add
			(createChildParameter
				(DatamodelPackage.Literals.HPAIR__A,
				 DatamodelFactory.eINSTANCE.createHCoordinateSystem3DAffine()));

		newChildDescriptors.add
			(createChildParameter
				(DatamodelPackage.Literals.HPAIR__A,
				 DatamodelFactory.eINSTANCE.createHCoordinateSystem3DCartesian()));

		newChildDescriptors.add
			(createChildParameter
				(DatamodelPackage.Literals.HPAIR__A,
				 DatamodelFactory.eINSTANCE.createHCoordinateSystem3DPolar()));

		newChildDescriptors.add
			(createChildParameter
				(DatamodelPackage.Literals.HPAIR__A,
				 DatamodelFactory.eINSTANCE.createHCoordinateSystem3DCone()));

		newChildDescriptors.add
			(createChildParameter
				(DatamodelPackage.Literals.HPAIR__A,
				 DatamodelFactory.eINSTANCE.createHPoint2D()));

		newChildDescriptors.add
			(createChildParameter
				(DatamodelPackage.Literals.HPAIR__A,
				 DatamodelFactory.eINSTANCE.createHCurvePoint2D()));

		newChildDescriptors.add
			(createChildParameter
				(DatamodelPackage.Literals.HPAIR__A,
				 DatamodelFactory.eINSTANCE.createHDiscreteCurve2D()));

		newChildDescriptors.add
			(createChildParameter
				(DatamodelPackage.Literals.HPAIR__A,
				 DatamodelFactory.eINSTANCE.createHLoftPath()));

		newChildDescriptors.add
			(createChildParameter
				(DatamodelPackage.Literals.HPAIR__A,
				 DatamodelFactory.eINSTANCE.createHDiscreteLoftPath()));

		newChildDescriptors.add
			(createChildParameter
				(DatamodelPackage.Literals.HPAIR__A,
				 DatamodelFactory.eINSTANCE.createHPoint3D()));

		newChildDescriptors.add
			(createChildParameter
				(DatamodelPackage.Literals.HPAIR__A,
				 DatamodelFactory.eINSTANCE.createHDiscreteLoftPathPoint3D()));

		newChildDescriptors.add
			(createChildParameter
				(DatamodelPackage.Literals.HPAIR__A,
				 DatamodelFactory.eINSTANCE.createHHinge()));

		newChildDescriptors.add
			(createChildParameter
				(DatamodelPackage.Literals.HPAIR__A,
				 DatamodelFactory.eINSTANCE.createHLinearLoftPath()));

		newChildDescriptors.add
			(createChildParameter
				(DatamodelPackage.Literals.HPAIR__A,
				 DatamodelFactory.eINSTANCE.createHLoft()));

		newChildDescriptors.add
			(createChildParameter
				(DatamodelPackage.Literals.HPAIR__A,
				 DatamodelFactory.eINSTANCE.createHLoftElement()));

		newChildDescriptors.add
			(createChildParameter
				(DatamodelPackage.Literals.HPAIR__A,
				 DatamodelFactory.eINSTANCE.createHPair()));

		newChildDescriptors.add
			(createChildParameter
				(DatamodelPackage.Literals.HPAIR__A,
				 DatamodelFactory.eINSTANCE.createHSeriesProfileCurve2D()));

		newChildDescriptors.add
			(createChildParameter
				(DatamodelPackage.Literals.HPAIR__A,
				 DatamodelFactory.eINSTANCE.createHSurfaceIdentifier()));

		newChildDescriptors.add
			(createChildParameter
				(DatamodelPackage.Literals.HPAIR__B,
				 DatamodelFactory.eINSTANCE.createHComponent()));

		newChildDescriptors.add
			(createChildParameter
				(DatamodelPackage.Literals.HPAIR__B,
				 DatamodelFactory.eINSTANCE.createHComposition()));

		newChildDescriptors.add
			(createChildParameter
				(DatamodelPackage.Literals.HPAIR__B,
				 DatamodelFactory.eINSTANCE.createHCoordinateSystem3DAffine()));

		newChildDescriptors.add
			(createChildParameter
				(DatamodelPackage.Literals.HPAIR__B,
				 DatamodelFactory.eINSTANCE.createHCoordinateSystem3DCartesian()));

		newChildDescriptors.add
			(createChildParameter
				(DatamodelPackage.Literals.HPAIR__B,
				 DatamodelFactory.eINSTANCE.createHCoordinateSystem3DPolar()));

		newChildDescriptors.add
			(createChildParameter
				(DatamodelPackage.Literals.HPAIR__B,
				 DatamodelFactory.eINSTANCE.createHCoordinateSystem3DCone()));

		newChildDescriptors.add
			(createChildParameter
				(DatamodelPackage.Literals.HPAIR__B,
				 DatamodelFactory.eINSTANCE.createHPoint2D()));

		newChildDescriptors.add
			(createChildParameter
				(DatamodelPackage.Literals.HPAIR__B,
				 DatamodelFactory.eINSTANCE.createHCurvePoint2D()));

		newChildDescriptors.add
			(createChildParameter
				(DatamodelPackage.Literals.HPAIR__B,
				 DatamodelFactory.eINSTANCE.createHDiscreteCurve2D()));

		newChildDescriptors.add
			(createChildParameter
				(DatamodelPackage.Literals.HPAIR__B,
				 DatamodelFactory.eINSTANCE.createHLoftPath()));

		newChildDescriptors.add
			(createChildParameter
				(DatamodelPackage.Literals.HPAIR__B,
				 DatamodelFactory.eINSTANCE.createHDiscreteLoftPath()));

		newChildDescriptors.add
			(createChildParameter
				(DatamodelPackage.Literals.HPAIR__B,
				 DatamodelFactory.eINSTANCE.createHPoint3D()));

		newChildDescriptors.add
			(createChildParameter
				(DatamodelPackage.Literals.HPAIR__B,
				 DatamodelFactory.eINSTANCE.createHDiscreteLoftPathPoint3D()));

		newChildDescriptors.add
			(createChildParameter
				(DatamodelPackage.Literals.HPAIR__B,
				 DatamodelFactory.eINSTANCE.createHHinge()));

		newChildDescriptors.add
			(createChildParameter
				(DatamodelPackage.Literals.HPAIR__B,
				 DatamodelFactory.eINSTANCE.createHLinearLoftPath()));

		newChildDescriptors.add
			(createChildParameter
				(DatamodelPackage.Literals.HPAIR__B,
				 DatamodelFactory.eINSTANCE.createHLoft()));

		newChildDescriptors.add
			(createChildParameter
				(DatamodelPackage.Literals.HPAIR__B,
				 DatamodelFactory.eINSTANCE.createHLoftElement()));

		newChildDescriptors.add
			(createChildParameter
				(DatamodelPackage.Literals.HPAIR__B,
				 DatamodelFactory.eINSTANCE.createHPair()));

		newChildDescriptors.add
			(createChildParameter
				(DatamodelPackage.Literals.HPAIR__B,
				 DatamodelFactory.eINSTANCE.createHSeriesProfileCurve2D()));

		newChildDescriptors.add
			(createChildParameter
				(DatamodelPackage.Literals.HPAIR__B,
				 DatamodelFactory.eINSTANCE.createHSurfaceIdentifier()));
	}

	/**
	 * This returns the label text for {@link org.eclipse.emf.edit.command.CreateChildCommand}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getCreateChildText(Object owner, Object feature, Object child, Collection<?> selection) {
		Object childFeature = feature;
		Object childObject = child;

		boolean qualify =
			childFeature == DatamodelPackage.Literals.HPAIR__A ||
			childFeature == DatamodelPackage.Literals.HPAIR__B;

		if (qualify) {
			return getString
				("_UI_CreateChild_text2",
				 new Object[] { getTypeText(childObject), getFeatureText(childFeature), getTypeText(owner) });
		}
		return super.getCreateChildText(owner, feature, child, selection);
	}

}
