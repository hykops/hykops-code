package de.hykops.lpd.datamodel;

/**
 * @model
 * @author Thomas Stoye
 *
 */
public interface HCoordinateSystem3DPolar extends HCoordinateSystem3D {

	static final String CLASS_NAME = HCoordinateSystem3DPolar.class.getSimpleName();

	static final String PROPERTY_AXIAL_VECTOR = "axialVector";
	static final String PROPERTY_RADIAL_VECTOR = "radialVector";
	static final String PROPERTY_ANGLE = "angle";
	
	/**
	 * @model containment = "true"
	 * @return the axial vector
	 */
	public HPoint3D getAxialVector();

	/**
	 * Sets the value of the '{@link de.hykops.lpd.datamodel.HCoordinateSystem3DPolar#getAxialVector <em>Axial Vector</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Axial Vector</em>' containment reference.
	 * @see #getAxialVector()
	 * @generated
	 */
	void setAxialVector(HPoint3D value);

	/**
	 * @model containment = "true"
	 * @return the radial vector of the coordinate system
	 */
	public HPoint3D getRadialVector();

	/**
	 * Sets the value of the '{@link de.hykops.lpd.datamodel.HCoordinateSystem3DPolar#getRadialVector <em>Radial Vector</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Radial Vector</em>' containment reference.
	 * @see #getRadialVector()
	 * @generated
	 */
	void setRadialVector(HPoint3D value);

	/**
	 * @model containment = "true"
	 * @return the angular value in radians
	 */
	public HPoint3D getAngle();

	/**
	 * Sets the value of the '{@link de.hykops.lpd.datamodel.HCoordinateSystem3DPolar#getAngle <em>Angle</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Angle</em>' containment reference.
	 * @see #getAngle()
	 * @generated
	 */
	void setAngle(HPoint3D value);
}
