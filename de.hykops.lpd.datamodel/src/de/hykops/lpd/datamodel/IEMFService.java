package de.hykops.lpd.datamodel;

public interface IEMFService {

	public DatamodelFactory getEMFDataModelFactory();
	
	public DatamodelPackage getEMFDatamodelPackage();
	
}
