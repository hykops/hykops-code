package de.hykops.lpd.datamodel;

/**
 * @model
 * @author Thomas Stoye
 *
 */
public interface HLoftPath extends HParent {
	
	static final String CLASS_NAME = HLoftPath.class.getSimpleName();

	static final String PROPERTY_DENOMINATOR = "denominator";
	static final String PROPERTY_LOFT_COORDINATE_SYSTEM = "hLoftCoordinateSystem";
	static final String PROPERTY_LOFT_ELEMENTS = "hLoftElements";
	static final String PROPERTY_LOFT = "hLoft";
	
	/**
	 * @model
	 * @return the denomination of the axis
	 */
	public String getDenominator();

	/**
	 * Sets the value of the '{@link de.hykops.lpd.datamodel.HLoftPath#getDenominator <em>Denominator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Denominator</em>' attribute.
	 * @see #getDenominator()
	 * @generated
	 */
	void setDenominator(String value);

	/**
	 * @model containment = "true"
	 * @return the loft path coordinate system
	 *
	 * @author stoye
	 * @since Jul 4, 2016
	 */
	public HCoordinateSystem3D getHLoftCoordinateSystem();
	
	/**
	 * Sets the value of the '{@link de.hykops.lpd.datamodel.HLoftPath#getHLoftCoordinateSystem <em>HLoft Coordinate System</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>HLoft Coordinate System</em>' containment reference.
	 * @see #getHLoftCoordinateSystem()
	 * @generated
	 */
	void setHLoftCoordinateSystem(HCoordinateSystem3D value);

	/**
	 * @model opposite = "hLoftPath"
	 */
	public HLoft getHLoft();

	/**
	 * Sets the value of the '{@link de.hykops.lpd.datamodel.HLoftPath#getHLoft <em>HLoft</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>HLoft</em>' container reference.
	 * @see #getHLoft()
	 * @generated
	 */
	void setHLoft(HLoft value);
}
