/**
 */
package de.hykops.lpd.datamodel;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see de.hykops.lpd.datamodel.DatamodelFactory
 * @model kind="package"
 * @generated
 */
public interface DatamodelPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "datamodel";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http:///de/hykops/lpd/datamodel.ecore";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "de.hykops.lpd.datamodel";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	DatamodelPackage eINSTANCE = Activator.getEmfService().getEMFDatamodelPackage();

	/**
	 * The meta object id for the '{@link de.hykops.lpd.datamodel.impl.HParentImpl <em>HParent</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hykops.lpd.datamodel.impl.HParentImpl
	 * @see de.hykops.lpd.datamodel.impl.DatamodelPackageImpl#getHParent()
	 * @generated
	 */
	int HPARENT = 18;

	/**
	 * The feature id for the '<em><b>HParent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HPARENT__HPARENT = 0;

	/**
	 * The number of structural features of the '<em>HParent</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HPARENT_FEATURE_COUNT = 1;

	/**
	 * The number of operations of the '<em>HParent</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HPARENT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link de.hykops.lpd.datamodel.impl.HComponentImpl <em>HComponent</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hykops.lpd.datamodel.impl.HComponentImpl
	 * @see de.hykops.lpd.datamodel.impl.DatamodelPackageImpl#getHComponent()
	 * @generated
	 */
	int HCOMPONENT = 0;

	/**
	 * The feature id for the '<em><b>HParent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCOMPONENT__HPARENT = HPARENT__HPARENT;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCOMPONENT__NAME = HPARENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCOMPONENT__TYPE = HPARENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCOMPONENT__DESCRIPTION = HPARENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCOMPONENT__PARAMETERS = HPARENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>HCoordinate System3 D</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCOMPONENT__HCOORDINATE_SYSTEM3_D = HPARENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>HLofts</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCOMPONENT__HLOFTS = HPARENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>HComposition</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCOMPONENT__HCOMPOSITION = HPARENT_FEATURE_COUNT + 6;

	/**
	 * The number of structural features of the '<em>HComponent</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCOMPONENT_FEATURE_COUNT = HPARENT_FEATURE_COUNT + 7;

	/**
	 * The number of operations of the '<em>HComponent</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCOMPONENT_OPERATION_COUNT = HPARENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.hykops.lpd.datamodel.impl.HCompositionImpl <em>HComposition</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hykops.lpd.datamodel.impl.HCompositionImpl
	 * @see de.hykops.lpd.datamodel.impl.DatamodelPackageImpl#getHComposition()
	 * @generated
	 */
	int HCOMPOSITION = 1;

	/**
	 * The feature id for the '<em><b>HParent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCOMPOSITION__HPARENT = HPARENT__HPARENT;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCOMPOSITION__VERSION = HPARENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCOMPOSITION__NAME = HPARENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCOMPOSITION__DESCRIPTION = HPARENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Unit System</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCOMPOSITION__UNIT_SYSTEM = HPARENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>HCoordinate System3 D</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCOMPOSITION__HCOORDINATE_SYSTEM3_D = HPARENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>HComponents</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCOMPOSITION__HCOMPONENTS = HPARENT_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>HComposition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCOMPOSITION_FEATURE_COUNT = HPARENT_FEATURE_COUNT + 6;

	/**
	 * The number of operations of the '<em>HComposition</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCOMPOSITION_OPERATION_COUNT = HPARENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.hykops.lpd.datamodel.impl.HCoordinateSystem3DImpl <em>HCoordinate System3 D</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hykops.lpd.datamodel.impl.HCoordinateSystem3DImpl
	 * @see de.hykops.lpd.datamodel.impl.DatamodelPackageImpl#getHCoordinateSystem3D()
	 * @generated
	 */
	int HCOORDINATE_SYSTEM3_D = 2;

	/**
	 * The feature id for the '<em><b>HParent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCOORDINATE_SYSTEM3_D__HPARENT = HPARENT__HPARENT;

	/**
	 * The feature id for the '<em><b>Origin</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCOORDINATE_SYSTEM3_D__ORIGIN = HPARENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>HHinges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCOORDINATE_SYSTEM3_D__HHINGES = HPARENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>HCoordinate System3 D</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCOORDINATE_SYSTEM3_D_FEATURE_COUNT = HPARENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>HCoordinate System3 D</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCOORDINATE_SYSTEM3_D_OPERATION_COUNT = HPARENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.hykops.lpd.datamodel.impl.HCoordinateSystem3DAffineImpl <em>HCoordinate System3 DAffine</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hykops.lpd.datamodel.impl.HCoordinateSystem3DAffineImpl
	 * @see de.hykops.lpd.datamodel.impl.DatamodelPackageImpl#getHCoordinateSystem3DAffine()
	 * @generated
	 */
	int HCOORDINATE_SYSTEM3_DAFFINE = 3;

	/**
	 * The feature id for the '<em><b>HParent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCOORDINATE_SYSTEM3_DAFFINE__HPARENT = HCOORDINATE_SYSTEM3_D__HPARENT;

	/**
	 * The feature id for the '<em><b>Origin</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCOORDINATE_SYSTEM3_DAFFINE__ORIGIN = HCOORDINATE_SYSTEM3_D__ORIGIN;

	/**
	 * The feature id for the '<em><b>HHinges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCOORDINATE_SYSTEM3_DAFFINE__HHINGES = HCOORDINATE_SYSTEM3_D__HHINGES;

	/**
	 * The feature id for the '<em><b>X1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCOORDINATE_SYSTEM3_DAFFINE__X1 = HCOORDINATE_SYSTEM3_D_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>X2</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCOORDINATE_SYSTEM3_DAFFINE__X2 = HCOORDINATE_SYSTEM3_D_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>X3</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCOORDINATE_SYSTEM3_DAFFINE__X3 = HCOORDINATE_SYSTEM3_D_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>HCoordinate System3 DAffine</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCOORDINATE_SYSTEM3_DAFFINE_FEATURE_COUNT = HCOORDINATE_SYSTEM3_D_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>HCoordinate System3 DAffine</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCOORDINATE_SYSTEM3_DAFFINE_OPERATION_COUNT = HCOORDINATE_SYSTEM3_D_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.hykops.lpd.datamodel.impl.HCoordinateSystem3DCartesianImpl <em>HCoordinate System3 DCartesian</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hykops.lpd.datamodel.impl.HCoordinateSystem3DCartesianImpl
	 * @see de.hykops.lpd.datamodel.impl.DatamodelPackageImpl#getHCoordinateSystem3DCartesian()
	 * @generated
	 */
	int HCOORDINATE_SYSTEM3_DCARTESIAN = 4;

	/**
	 * The feature id for the '<em><b>HParent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCOORDINATE_SYSTEM3_DCARTESIAN__HPARENT = HCOORDINATE_SYSTEM3_D__HPARENT;

	/**
	 * The feature id for the '<em><b>Origin</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCOORDINATE_SYSTEM3_DCARTESIAN__ORIGIN = HCOORDINATE_SYSTEM3_D__ORIGIN;

	/**
	 * The feature id for the '<em><b>HHinges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCOORDINATE_SYSTEM3_DCARTESIAN__HHINGES = HCOORDINATE_SYSTEM3_D__HHINGES;

	/**
	 * The feature id for the '<em><b>X1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCOORDINATE_SYSTEM3_DCARTESIAN__X1 = HCOORDINATE_SYSTEM3_D_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>X1</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCOORDINATE_SYSTEM3_DCARTESIAN__E1 = HCOORDINATE_SYSTEM3_D_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>HCoordinate System3 DCartesian</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCOORDINATE_SYSTEM3_DCARTESIAN_FEATURE_COUNT = HCOORDINATE_SYSTEM3_D_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>HCoordinate System3 DCartesian</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCOORDINATE_SYSTEM3_DCARTESIAN_OPERATION_COUNT = HCOORDINATE_SYSTEM3_D_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.hykops.lpd.datamodel.impl.HCoordinateSystem3DPolarImpl <em>HCoordinate System3 DPolar</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hykops.lpd.datamodel.impl.HCoordinateSystem3DPolarImpl
	 * @see de.hykops.lpd.datamodel.impl.DatamodelPackageImpl#getHCoordinateSystem3DPolar()
	 * @generated
	 */
	int HCOORDINATE_SYSTEM3_DPOLAR = 6;

	/**
	 * The feature id for the '<em><b>HParent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCOORDINATE_SYSTEM3_DPOLAR__HPARENT = HCOORDINATE_SYSTEM3_D__HPARENT;

	/**
	 * The feature id for the '<em><b>Origin</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCOORDINATE_SYSTEM3_DPOLAR__ORIGIN = HCOORDINATE_SYSTEM3_D__ORIGIN;

	/**
	 * The feature id for the '<em><b>HHinges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCOORDINATE_SYSTEM3_DPOLAR__HHINGES = HCOORDINATE_SYSTEM3_D__HHINGES;

	/**
	 * The feature id for the '<em><b>Axial Vector</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCOORDINATE_SYSTEM3_DPOLAR__AXIAL_VECTOR = HCOORDINATE_SYSTEM3_D_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Radial Vector</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCOORDINATE_SYSTEM3_DPOLAR__RADIAL_VECTOR = HCOORDINATE_SYSTEM3_D_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Angle</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCOORDINATE_SYSTEM3_DPOLAR__ANGLE = HCOORDINATE_SYSTEM3_D_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>HCoordinate System3 DPolar</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCOORDINATE_SYSTEM3_DPOLAR_FEATURE_COUNT = HCOORDINATE_SYSTEM3_D_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>HCoordinate System3 DPolar</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCOORDINATE_SYSTEM3_DPOLAR_OPERATION_COUNT = HCOORDINATE_SYSTEM3_D_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.hykops.lpd.datamodel.impl.HCoordinateSystem3DConeImpl <em>HCoordinate System3 DCone</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hykops.lpd.datamodel.impl.HCoordinateSystem3DConeImpl
	 * @see de.hykops.lpd.datamodel.impl.DatamodelPackageImpl#getHCoordinateSystem3DCone()
	 * @generated
	 */
	int HCOORDINATE_SYSTEM3_DCONE = 5;

	/**
	 * The feature id for the '<em><b>HParent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCOORDINATE_SYSTEM3_DCONE__HPARENT = HCOORDINATE_SYSTEM3_D__HPARENT;

	/**
	 * The feature id for the '<em><b>Origin</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCOORDINATE_SYSTEM3_DCONE__ORIGIN = HCOORDINATE_SYSTEM3_D__ORIGIN;

	/**
	 * The feature id for the '<em><b>HHinges</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCOORDINATE_SYSTEM3_DCONE__HHINGES = HCOORDINATE_SYSTEM3_D__HHINGES;

	/**
	 * The feature id for the '<em><b>Axial Vector</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCOORDINATE_SYSTEM3_DCONE__AXIAL_VECTOR = HCOORDINATE_SYSTEM3_D_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Radial Vector</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCOORDINATE_SYSTEM3_DCONE__RADIAL_VECTOR = HCOORDINATE_SYSTEM3_D_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Angle</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCOORDINATE_SYSTEM3_DCONE__ANGLE =  HCOORDINATE_SYSTEM3_D_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>HCoordinate System3 DCone</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCOORDINATE_SYSTEM3_DCONE_FEATURE_COUNT = HCOORDINATE_SYSTEM3_D_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>HCoordinate System3 DCone</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCOORDINATE_SYSTEM3_DCONE_OPERATION_COUNT = HCOORDINATE_SYSTEM3_D_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.hykops.lpd.datamodel.impl.HCurve2DImpl <em>HCurve2 D</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hykops.lpd.datamodel.impl.HCurve2DImpl
	 * @see de.hykops.lpd.datamodel.impl.DatamodelPackageImpl#getHCurve2D()
	 * @generated
	 */
	int HCURVE2_D = 7;

	/**
	 * The feature id for the '<em><b>HParent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCURVE2_D__HPARENT = HPARENT__HPARENT;

	/**
	 * The feature id for the '<em><b>HSurface Identifiers</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCURVE2_D__HSURFACE_IDENTIFIERS = HPARENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>HLoft Element</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCURVE2_D__HLOFT_ELEMENT = HPARENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>HCurve2 D</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCURVE2_D_FEATURE_COUNT = HPARENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>HCurve2 D</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCURVE2_D_OPERATION_COUNT = HPARENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.hykops.lpd.datamodel.impl.HPoint2DImpl <em>HPoint2 D</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hykops.lpd.datamodel.impl.HPoint2DImpl
	 * @see de.hykops.lpd.datamodel.impl.DatamodelPackageImpl#getHPoint2D()
	 * @generated
	 */
	int HPOINT2_D = 19;

	/**
	 * The feature id for the '<em><b>HParent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HPOINT2_D__HPARENT = HPARENT__HPARENT;

	/**
	 * The feature id for the '<em><b>X1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HPOINT2_D__X1 = HPARENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>X2</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HPOINT2_D__X2 = HPARENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>HPoint2 D</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HPOINT2_D_FEATURE_COUNT = HPARENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>HPoint2 D</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HPOINT2_D_OPERATION_COUNT = HPARENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.hykops.lpd.datamodel.impl.HCurvePoint2DImpl <em>HCurve Point2 D</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hykops.lpd.datamodel.impl.HCurvePoint2DImpl
	 * @see de.hykops.lpd.datamodel.impl.DatamodelPackageImpl#getHCurvePoint2D()
	 * @generated
	 */
	int HCURVE_POINT2_D = 8;

	/**
	 * The feature id for the '<em><b>HParent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCURVE_POINT2_D__HPARENT = HPOINT2_D__HPARENT;

	/**
	 * The feature id for the '<em><b>X1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCURVE_POINT2_D__X1 = HPOINT2_D__X1;

	/**
	 * The feature id for the '<em><b>X2</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCURVE_POINT2_D__X2 = HPOINT2_D__X2;

	/**
	 * The feature id for the '<em><b>Curve Coordinate</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCURVE_POINT2_D__CURVE_COORDINATE = HPOINT2_D_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>HCurve Point2 D</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCURVE_POINT2_D_FEATURE_COUNT = HPOINT2_D_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>HCurve Point2 D</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HCURVE_POINT2_D_OPERATION_COUNT = HPOINT2_D_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.hykops.lpd.datamodel.impl.HDiscreteCurve2DImpl <em>HDiscrete Curve2 D</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hykops.lpd.datamodel.impl.HDiscreteCurve2DImpl
	 * @see de.hykops.lpd.datamodel.impl.DatamodelPackageImpl#getHDiscreteCurve2D()
	 * @generated
	 */
	int HDISCRETE_CURVE2_D = 9;

	/**
	 * The feature id for the '<em><b>HParent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HDISCRETE_CURVE2_D__HPARENT = HCURVE2_D__HPARENT;

	/**
	 * The feature id for the '<em><b>HSurface Identifiers</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HDISCRETE_CURVE2_D__HSURFACE_IDENTIFIERS = HCURVE2_D__HSURFACE_IDENTIFIERS;

	/**
	 * The feature id for the '<em><b>HLoft Element</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HDISCRETE_CURVE2_D__HLOFT_ELEMENT = HCURVE2_D__HLOFT_ELEMENT;

	/**
	 * The feature id for the '<em><b>Interpolation Scheme</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HDISCRETE_CURVE2_D__INTERPOLATION_SCHEME = HCURVE2_D_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Curve Points</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HDISCRETE_CURVE2_D__CURVE_POINTS = HCURVE2_D_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>HDiscrete Curve2 D</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HDISCRETE_CURVE2_D_FEATURE_COUNT = HCURVE2_D_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>HDiscrete Curve2 D</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HDISCRETE_CURVE2_D_OPERATION_COUNT = HCURVE2_D_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.hykops.lpd.datamodel.impl.HLoftPathImpl <em>HLoft Path</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hykops.lpd.datamodel.impl.HLoftPathImpl
	 * @see de.hykops.lpd.datamodel.impl.DatamodelPackageImpl#getHLoftPath()
	 * @generated
	 */
	int HLOFT_PATH = 16;

	/**
	 * The feature id for the '<em><b>HParent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HLOFT_PATH__HPARENT = HPARENT__HPARENT;

	/**
	 * The feature id for the '<em><b>Denominator</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HLOFT_PATH__DENOMINATOR = HPARENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>HLoft Coordinate System</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HLOFT_PATH__HLOFT_COORDINATE_SYSTEM = HPARENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>HLoft</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HLOFT_PATH__HLOFT = HPARENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>HLoft Path</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HLOFT_PATH_FEATURE_COUNT = HPARENT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>HLoft Path</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HLOFT_PATH_OPERATION_COUNT = HPARENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.hykops.lpd.datamodel.impl.HDiscreteLoftPathImpl <em>HDiscrete Loft Path</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hykops.lpd.datamodel.impl.HDiscreteLoftPathImpl
	 * @see de.hykops.lpd.datamodel.impl.DatamodelPackageImpl#getHDiscreteLoftPath()
	 * @generated
	 */
	int HDISCRETE_LOFT_PATH = 10;

	/**
	 * The feature id for the '<em><b>HParent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HDISCRETE_LOFT_PATH__HPARENT = HLOFT_PATH__HPARENT;

	/**
	 * The feature id for the '<em><b>Denominator</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HDISCRETE_LOFT_PATH__DENOMINATOR = HLOFT_PATH__DENOMINATOR;

	/**
	 * The feature id for the '<em><b>HLoft Coordinate System</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HDISCRETE_LOFT_PATH__HLOFT_COORDINATE_SYSTEM = HLOFT_PATH__HLOFT_COORDINATE_SYSTEM;

	/**
	 * The feature id for the '<em><b>HLoft</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HDISCRETE_LOFT_PATH__HLOFT = HLOFT_PATH__HLOFT;

	/**
	 * The feature id for the '<em><b>Loft Curve Points</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HDISCRETE_LOFT_PATH__LOFT_CURVE_POINTS = HLOFT_PATH_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Interpolation Scheme</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HDISCRETE_LOFT_PATH__INTERPOLATION_SCHEME = HLOFT_PATH_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>HDiscrete Loft Path</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HDISCRETE_LOFT_PATH_FEATURE_COUNT = HLOFT_PATH_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>HDiscrete Loft Path</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HDISCRETE_LOFT_PATH_OPERATION_COUNT = HLOFT_PATH_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.hykops.lpd.datamodel.impl.HPoint3DImpl <em>HPoint3 D</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hykops.lpd.datamodel.impl.HPoint3DImpl
	 * @see de.hykops.lpd.datamodel.impl.DatamodelPackageImpl#getHPoint3D()
	 * @generated
	 */
	int HPOINT3_D = 20;

	/**
	 * The feature id for the '<em><b>HParent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HPOINT3_D__HPARENT = HPARENT__HPARENT;

	/**
	 * The feature id for the '<em><b>X1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HPOINT3_D__X1 = HPARENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>X2</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HPOINT3_D__X2 = HPARENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>X3</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HPOINT3_D__X3 = HPARENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>HPoint3 D</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HPOINT3_D_FEATURE_COUNT = HPARENT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>HPoint3 D</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HPOINT3_D_OPERATION_COUNT = HPARENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.hykops.lpd.datamodel.impl.HDiscreteLoftPathPoint3DImpl <em>HDiscrete Loft Path Point3 D</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hykops.lpd.datamodel.impl.HDiscreteLoftPathPoint3DImpl
	 * @see de.hykops.lpd.datamodel.impl.DatamodelPackageImpl#getHDiscreteLoftPathPoint3D()
	 * @generated
	 */
	int HDISCRETE_LOFT_PATH_POINT3_D = 11;

	/**
	 * The feature id for the '<em><b>HParent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HDISCRETE_LOFT_PATH_POINT3_D__HPARENT = HPOINT3_D__HPARENT;

	/**
	 * The feature id for the '<em><b>X1</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HDISCRETE_LOFT_PATH_POINT3_D__X1 = HPOINT3_D__X1;

	/**
	 * The feature id for the '<em><b>X2</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HDISCRETE_LOFT_PATH_POINT3_D__X2 = HPOINT3_D__X2;

	/**
	 * The feature id for the '<em><b>X3</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HDISCRETE_LOFT_PATH_POINT3_D__X3 = HPOINT3_D__X3;

	/**
	 * The feature id for the '<em><b>Loft Curve Point</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HDISCRETE_LOFT_PATH_POINT3_D__LOFT_CURVE_POINT = HPOINT3_D_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Loft Coordinate</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HDISCRETE_LOFT_PATH_POINT3_D__LOFT_COORDINATE = HPOINT3_D_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Loft Path Vector</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HDISCRETE_LOFT_PATH_POINT3_D__LOFT_PATH_VECTOR = HPOINT3_D_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Loft Path Xi</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HDISCRETE_LOFT_PATH_POINT3_D__LOFT_PATH_XI = HPOINT3_D_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Loft Path Eta</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HDISCRETE_LOFT_PATH_POINT3_D__LOFT_PATH_ETA = HPOINT3_D_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>HDiscrete Loft Path Point3 D</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HDISCRETE_LOFT_PATH_POINT3_D_FEATURE_COUNT = HPOINT3_D_FEATURE_COUNT + 5;

	/**
	 * The number of operations of the '<em>HDiscrete Loft Path Point3 D</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HDISCRETE_LOFT_PATH_POINT3_D_OPERATION_COUNT = HPOINT3_D_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.hykops.lpd.datamodel.impl.HHingeImpl <em>HHinge</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hykops.lpd.datamodel.impl.HHingeImpl
	 * @see de.hykops.lpd.datamodel.impl.DatamodelPackageImpl#getHHinge()
	 * @generated
	 */
	int HHINGE = 12;

	/**
	 * The feature id for the '<em><b>HParent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HHINGE__HPARENT = HPARENT__HPARENT;

	/**
	 * The feature id for the '<em><b>Hinge Axis</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HHINGE__HINGE_AXIS = HPARENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Hinge Positions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HHINGE__HINGE_POSITIONS = HPARENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Hinge Limited</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HHINGE__HINGE_LIMITED = HPARENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>HCoordinate System3 D</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HHINGE__HCOORDINATE_SYSTEM3_D = HPARENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>HHinge</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HHINGE_FEATURE_COUNT = HPARENT_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>HHinge</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HHINGE_OPERATION_COUNT = HPARENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.hykops.lpd.datamodel.impl.HLinearLoftPathImpl <em>HLinear Loft Path</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hykops.lpd.datamodel.impl.HLinearLoftPathImpl
	 * @see de.hykops.lpd.datamodel.impl.DatamodelPackageImpl#getHLinearLoftPath()
	 * @generated
	 */
	int HLINEAR_LOFT_PATH = 13;

	/**
	 * The feature id for the '<em><b>HParent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HLINEAR_LOFT_PATH__HPARENT = HLOFT_PATH__HPARENT;

	/**
	 * The feature id for the '<em><b>Denominator</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HLINEAR_LOFT_PATH__DENOMINATOR = HLOFT_PATH__DENOMINATOR;

	/**
	 * The feature id for the '<em><b>HLoft Coordinate System</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HLINEAR_LOFT_PATH__HLOFT_COORDINATE_SYSTEM = HLOFT_PATH__HLOFT_COORDINATE_SYSTEM;

	/**
	 * The feature id for the '<em><b>HLoft</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HLINEAR_LOFT_PATH__HLOFT = HLOFT_PATH__HLOFT;

	/**
	 * The feature id for the '<em><b>Origin</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HLINEAR_LOFT_PATH__ORIGIN = HLOFT_PATH_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Loft Path Vector</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HLINEAR_LOFT_PATH__LOFT_PATH_VECTOR = HLOFT_PATH_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Loft Path Xi</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HLINEAR_LOFT_PATH__LOFT_PATH_XI = HLOFT_PATH_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Loft Path Eta</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HLINEAR_LOFT_PATH__LOFT_PATH_ETA = HLOFT_PATH_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>HLinear Loft Path</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HLINEAR_LOFT_PATH_FEATURE_COUNT = HLOFT_PATH_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>HLinear Loft Path</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HLINEAR_LOFT_PATH_OPERATION_COUNT = HLOFT_PATH_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.hykops.lpd.datamodel.impl.HLoftImpl <em>HLoft</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hykops.lpd.datamodel.impl.HLoftImpl
	 * @see de.hykops.lpd.datamodel.impl.DatamodelPackageImpl#getHLoft()
	 * @generated
	 */
	int HLOFT = 14;

	/**
	 * The feature id for the '<em><b>HParent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HLOFT__HPARENT = HPARENT__HPARENT;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HLOFT__NAME = HPARENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HLOFT__DESCRIPTION = HPARENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Parameters</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HLOFT__PARAMETERS = HPARENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>HCoordinate Systems3 D</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HLOFT__HCOORDINATE_SYSTEMS3_D = HPARENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>HLoft Path</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HLOFT__HLOFT_PATH = HPARENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>HLoft Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HLOFT__HLOFT_ELEMENTS = HPARENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>HSurface Identifications</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HLOFT__HSURFACE_IDENTIFICATIONS = HPARENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>HComponent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HLOFT__HCOMPONENT = HPARENT_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>HLoft</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HLOFT_FEATURE_COUNT = HPARENT_FEATURE_COUNT + 8;

	/**
	 * The number of operations of the '<em>HLoft</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HLOFT_OPERATION_COUNT = HPARENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.hykops.lpd.datamodel.impl.HLoftElementImpl <em>HLoft Element</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hykops.lpd.datamodel.impl.HLoftElementImpl
	 * @see de.hykops.lpd.datamodel.impl.DatamodelPackageImpl#getHLoftElement()
	 * @generated
	 */
	int HLOFT_ELEMENT = 15;

	/**
	 * The feature id for the '<em><b>HParent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HLOFT_ELEMENT__HPARENT = HPARENT__HPARENT;

	/**
	 * The feature id for the '<em><b>Loft Plane Defining Coordinate System</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HLOFT_ELEMENT__LOFT_PLANE_DEFINING_COORDINATE_SYSTEM = HPARENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>HCurve2 Ds</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HLOFT_ELEMENT__HCURVE2_DS = HPARENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>HLoft</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HLOFT_ELEMENT__HLOFT = HPARENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>HLoft Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HLOFT_ELEMENT_FEATURE_COUNT = HPARENT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>HLoft Element</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HLOFT_ELEMENT_OPERATION_COUNT = HPARENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.hykops.lpd.datamodel.impl.HPairImpl <em>HPair</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hykops.lpd.datamodel.impl.HPairImpl
	 * @see de.hykops.lpd.datamodel.impl.DatamodelPackageImpl#getHPair()
	 * @generated
	 */
	int HPAIR = 17;

	/**
	 * The feature id for the '<em><b>HParent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HPAIR__HPARENT = HPARENT__HPARENT;

	/**
	 * The feature id for the '<em><b>A</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HPAIR__A = HPARENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>B</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HPAIR__B = HPARENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>HPair</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HPAIR_FEATURE_COUNT = HPARENT_FEATURE_COUNT + 2;

	/**
	 * The number of operations of the '<em>HPair</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HPAIR_OPERATION_COUNT = HPARENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.hykops.lpd.datamodel.impl.HSeriesProfileCurve2DImpl <em>HSeries Profile Curve2 D</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hykops.lpd.datamodel.impl.HSeriesProfileCurve2DImpl
	 * @see de.hykops.lpd.datamodel.impl.DatamodelPackageImpl#getHSeriesProfileCurve2D()
	 * @generated
	 */
	int HSERIES_PROFILE_CURVE2_D = 21;

	/**
	 * The feature id for the '<em><b>HParent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HSERIES_PROFILE_CURVE2_D__HPARENT = HCURVE2_D__HPARENT;

	/**
	 * The feature id for the '<em><b>HSurface Identifiers</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HSERIES_PROFILE_CURVE2_D__HSURFACE_IDENTIFIERS = HCURVE2_D__HSURFACE_IDENTIFIERS;

	/**
	 * The feature id for the '<em><b>HLoft Element</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HSERIES_PROFILE_CURVE2_D__HLOFT_ELEMENT = HCURVE2_D__HLOFT_ELEMENT;

	/**
	 * The feature id for the '<em><b>Denomination</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HSERIES_PROFILE_CURVE2_D__DENOMINATION = HCURVE2_D_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Side</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HSERIES_PROFILE_CURVE2_D__SIDE = HCURVE2_D_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Side</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HSERIES_PROFILE_CURVE2_D__SERIES = HCURVE2_D_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>HSeries Profile Curve2 D</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HSERIES_PROFILE_CURVE2_D_FEATURE_COUNT = HCURVE2_D_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>HSeries Profile Curve2 D</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HSERIES_PROFILE_CURVE2_D_OPERATION_COUNT = HCURVE2_D_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.hykops.lpd.datamodel.impl.HSurfaceIdentifierImpl <em>HSurface Identifier</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hykops.lpd.datamodel.impl.HSurfaceIdentifierImpl
	 * @see de.hykops.lpd.datamodel.impl.DatamodelPackageImpl#getHSurfaceIdentifier()
	 * @generated
	 */
	int HSURFACE_IDENTIFIER = 22;

	/**
	 * The feature id for the '<em><b>HParent</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HSURFACE_IDENTIFIER__HPARENT = HPARENT__HPARENT;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HSURFACE_IDENTIFIER__NAME = HPARENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>HSurface Identifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HSURFACE_IDENTIFIER_FEATURE_COUNT = HPARENT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>HSurface Identifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HSURFACE_IDENTIFIER_OPERATION_COUNT = HPARENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link java.util.Map <em>Map</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see java.util.Map
	 * @see de.hykops.lpd.datamodel.impl.DatamodelPackageImpl#getMap()
	 * @generated
	 */
	int MAP = 23;

	/**
	 * The number of structural features of the '<em>Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAP_FEATURE_COUNT = 0;

	/**
	 * The number of operations of the '<em>Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MAP_OPERATION_COUNT = 0;
	
	/**
	 * The meta object id for the '{@link de.hykops.lpd.datamodel.impl.StringToStringMapImpl <em>String To String Map</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.hykops.lpd.datamodel.impl.StringToStringMapImpl
	 * @see de.hykops.lpd.datamodel.impl.DatamodelPackageImpl#getStringToStringMap()
	 * @generated
	 */
	int STRING_TO_STRING_MAP = 24;

	/**
	 * The feature id for the '<em><b>Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_TO_STRING_MAP__KEY = 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_TO_STRING_MAP__VALUE = 1;

	/**
	 * The number of structural features of the '<em>String To String Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_TO_STRING_MAP_FEATURE_COUNT = 2;

	/**
	 * The number of operations of the '<em>String To String Map</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int STRING_TO_STRING_MAP_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link de.hykops.lpd.datamodel.HComponent <em>HComponent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>HComponent</em>'.
	 * @see de.hykops.lpd.datamodel.HComponent
	 * @generated
	 */
	EClass getHComponent();

	/**
	 * Returns the meta object for the attribute '{@link de.hykops.lpd.datamodel.HComponent#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see de.hykops.lpd.datamodel.HComponent#getName()
	 * @see #getHComponent()
	 * @generated
	 */
	EAttribute getHComponent_Name();

	/**
	 * Returns the meta object for the attribute '{@link de.hykops.lpd.datamodel.HComponent#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see de.hykops.lpd.datamodel.HComponent#getType()
	 * @see #getHComponent()
	 * @generated
	 */
	EAttribute getHComponent_Type();

	/**
	 * Returns the meta object for the attribute '{@link de.hykops.lpd.datamodel.HComponent#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see de.hykops.lpd.datamodel.HComponent#getDescription()
	 * @see #getHComponent()
	 * @generated
	 */
	EAttribute getHComponent_Description();

	/**
	 * Returns the meta object for the containment reference '{@link de.hykops.lpd.datamodel.HComponent#getParameters <em>Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Parameters</em>'.
	 * @see de.hykops.lpd.datamodel.HComponent#getParameters()
	 * @see #getHComponent()
	 * @generated
	 */
	EReference getHComponent_Parameters();

	/**
	 * Returns the meta object for the containment reference '{@link de.hykops.lpd.datamodel.HComponent#getHCoordinateSystem3D <em>HCoordinate System3 D</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>HCoordinate System3 D</em>'.
	 * @see de.hykops.lpd.datamodel.HComponent#getHCoordinateSystem3D()
	 * @see #getHComponent()
	 * @generated
	 */
	EReference getHComponent_HCoordinateSystem3D();

	/**
	 * Returns the meta object for the containment reference list '{@link de.hykops.lpd.datamodel.HComponent#getHLofts <em>HLofts</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>HLofts</em>'.
	 * @see de.hykops.lpd.datamodel.HComponent#getHLofts()
	 * @see #getHComponent()
	 * @generated
	 */
	EReference getHComponent_HLofts();

	/**
	 * Returns the meta object for the container reference '{@link de.hykops.lpd.datamodel.HComponent#getHComposition <em>HComposition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>HComposition</em>'.
	 * @see de.hykops.lpd.datamodel.HComponent#getHComposition()
	 * @see #getHComponent()
	 * @generated
	 */
	EReference getHComponent_HComposition();

	/**
	 * Returns the meta object for class '{@link de.hykops.lpd.datamodel.HComposition <em>HComposition</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>HComposition</em>'.
	 * @see de.hykops.lpd.datamodel.HComposition
	 * @generated
	 */
	EClass getHComposition();

	/**
	 * Returns the meta object for the attribute '{@link de.hykops.lpd.datamodel.HComposition#getVersion <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Version</em>'.
	 * @see de.hykops.lpd.datamodel.HComposition#getVersion()
	 * @see #getHComposition()
	 * @generated
	 */
	EAttribute getHComposition_Version();

	/**
	 * Returns the meta object for the attribute '{@link de.hykops.lpd.datamodel.HComposition#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see de.hykops.lpd.datamodel.HComposition#getName()
	 * @see #getHComposition()
	 * @generated
	 */
	EAttribute getHComposition_Name();

	/**
	 * Returns the meta object for the attribute '{@link de.hykops.lpd.datamodel.HComposition#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see de.hykops.lpd.datamodel.HComposition#getDescription()
	 * @see #getHComposition()
	 * @generated
	 */
	EAttribute getHComposition_Description();

	/**
	 * Returns the meta object for the attribute '{@link de.hykops.lpd.datamodel.HComposition#getUnitSystem <em>Unit System</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Unit System</em>'.
	 * @see de.hykops.lpd.datamodel.HComposition#getUnitSystem()
	 * @see #getHComposition()
	 * @generated
	 */
	EAttribute getHComposition_UnitSystem();

	/**
	 * Returns the meta object for the containment reference '{@link de.hykops.lpd.datamodel.HComposition#getHCoordinateSystem3D <em>HCoordinate System3 D</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>HCoordinate System3 D</em>'.
	 * @see de.hykops.lpd.datamodel.HComposition#getHCoordinateSystem3D()
	 * @see #getHComposition()
	 * @generated
	 */
	EReference getHComposition_HCoordinateSystem3D();

	/**
	 * Returns the meta object for the containment reference list '{@link de.hykops.lpd.datamodel.HComposition#getHComponents <em>HComponents</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>HComponents</em>'.
	 * @see de.hykops.lpd.datamodel.HComposition#getHComponents()
	 * @see #getHComposition()
	 * @generated
	 */
	EReference getHComposition_HComponents();

	/**
	 * Returns the meta object for class '{@link de.hykops.lpd.datamodel.HCoordinateSystem3D <em>HCoordinate System3 D</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>HCoordinate System3 D</em>'.
	 * @see de.hykops.lpd.datamodel.HCoordinateSystem3D
	 * @generated
	 */
	EClass getHCoordinateSystem3D();

	/**
	 * Returns the meta object for the containment reference '{@link de.hykops.lpd.datamodel.HCoordinateSystem3D#getOrigin <em>Origin</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Origin</em>'.
	 * @see de.hykops.lpd.datamodel.HCoordinateSystem3D#getOrigin()
	 * @see #getHCoordinateSystem3D()
	 * @generated
	 */
	EReference getHCoordinateSystem3D_Origin();

	/**
	 * Returns the meta object for the containment reference list '{@link de.hykops.lpd.datamodel.HCoordinateSystem3D#getHHinges <em>HHinges</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>HHinges</em>'.
	 * @see de.hykops.lpd.datamodel.HCoordinateSystem3D#getHHinges()
	 * @see #getHCoordinateSystem3D()
	 * @generated
	 */
	EReference getHCoordinateSystem3D_HHinges();

	/**
	 * Returns the meta object for class '{@link de.hykops.lpd.datamodel.HCoordinateSystem3DAffine <em>HCoordinate System3 DAffine</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>HCoordinate System3 DAffine</em>'.
	 * @see de.hykops.lpd.datamodel.HCoordinateSystem3DAffine
	 * @generated
	 */
	EClass getHCoordinateSystem3DAffine();

	/**
	 * Returns the meta object for the containment reference '{@link de.hykops.lpd.datamodel.HCoordinateSystem3DAffine#getX1 <em>X1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>X1</em>'.
	 * @see de.hykops.lpd.datamodel.HCoordinateSystem3DAffine#getX1()
	 * @see #getHCoordinateSystem3DAffine()
	 * @generated
	 */
	EReference getHCoordinateSystem3DAffine_X1();

	/**
	 * Returns the meta object for the containment reference '{@link de.hykops.lpd.datamodel.HCoordinateSystem3DAffine#getX2 <em>X2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>X2</em>'.
	 * @see de.hykops.lpd.datamodel.HCoordinateSystem3DAffine#getX2()
	 * @see #getHCoordinateSystem3DAffine()
	 * @generated
	 */
	EReference getHCoordinateSystem3DAffine_X2();

	/**
	 * Returns the meta object for the containment reference '{@link de.hykops.lpd.datamodel.HCoordinateSystem3DAffine#getX3 <em>X3</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>X3</em>'.
	 * @see de.hykops.lpd.datamodel.HCoordinateSystem3DAffine#getX3()
	 * @see #getHCoordinateSystem3DAffine()
	 * @generated
	 */
	EReference getHCoordinateSystem3DAffine_X3();

	/**
	 * Returns the meta object for class '{@link de.hykops.lpd.datamodel.HCoordinateSystem3DCartesian <em>HCoordinate System3 DCartesian</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>HCoordinate System3 DCartesian</em>'.
	 * @see de.hykops.lpd.datamodel.HCoordinateSystem3DCartesian
	 * @generated
	 */
	EClass getHCoordinateSystem3DCartesian();

	/**
	 * Returns the meta object for the containment reference '{@link de.hykops.lpd.datamodel.HCoordinateSystem3DCartesian#getX1 <em>X1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>X1</em>'.
	 * @see de.hykops.lpd.datamodel.HCoordinateSystem3DCartesian#getX1()
	 * @see #getHCoordinateSystem3DCartesian()
	 * @generated
	 */
	EReference getHCoordinateSystem3DCartesian_X1();

	/**
	 * Returns the meta object for the containment reference '{@link de.hykops.lpd.datamodel.HCoordinateSystem3DCartesian#getE1 <em>E1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>E1</em>'.
	 * @see de.hykops.lpd.datamodel.HCoordinateSystem3DCartesian#getE1()
	 * @see #getHCoordinateSystem3DCartesian()
	 * @generated
	 */
	EReference getHCoordinateSystem3DCartesian_E1();

	/**
	 * Returns the meta object for class '{@link de.hykops.lpd.datamodel.HCoordinateSystem3DCone <em>HCoordinate System3 DCone</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>HCoordinate System3 DCone</em>'.
	 * @see de.hykops.lpd.datamodel.HCoordinateSystem3DCone
	 * @generated
	 */
	EClass getHCoordinateSystem3DCone();

	/**
	 * Returns the meta object for the containment reference '{@link de.hykops.lpd.datamodel.HCoordinateSystem3DCone#getAxialVector <em>Axial Vector</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Axial Vector</em>'.
	 * @see de.hykops.lpd.datamodel.HCoordinateSystem3DCone#getAxialVector()
	 * @see #getHCoordinateSystem3DCone()
	 * @generated
	 */
	EReference getHCoordinateSystem3DCone_AxialVector();

	/**
	 * Returns the meta object for the containment reference '{@link de.hykops.lpd.datamodel.HCoordinateSystem3DCone#getRadialVector <em>Radial Vector</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Radial Vector</em>'.
	 * @see de.hykops.lpd.datamodel.HCoordinateSystem3DCone#getRadialVector()
	 * @see #getHCoordinateSystem3DCone()
	 * @generated
	 */
	EReference getHCoordinateSystem3DCone_RadialVector();

	/**
	 * Returns the meta object for the containment reference '{@link de.hykops.lpd.datamodel.HCoordinateSystem3DCone#getAngle <em>Angle</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Angle</em>'.
	 * @see de.hykops.lpd.datamodel.HCoordinateSystem3DCone#getAngle()
	 * @see #getHCoordinateSystem3DCone()
	 * @generated
	 */
	EReference getHCoordinateSystem3DCone_Angle();
	
	/**
	 * Returns the meta object for class '{@link de.hykops.lpd.datamodel.HCoordinateSystem3DPolar <em>HCoordinate System3 DPolar</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>HCoordinate System3 DPolar</em>'.
	 * @see de.hykops.lpd.datamodel.HCoordinateSystem3DPolar
	 * @generated
	 */
	EClass getHCoordinateSystem3DPolar();

	/**
	 * Returns the meta object for the containment reference '{@link de.hykops.lpd.datamodel.HCoordinateSystem3DPolar#getAxialVector <em>Axial Vector</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Axial Vector</em>'.
	 * @see de.hykops.lpd.datamodel.HCoordinateSystem3DPolar#getAxialVector()
	 * @see #getHCoordinateSystem3DPolar()
	 * @generated
	 */
	EReference getHCoordinateSystem3DPolar_AxialVector();

	/**
	 * Returns the meta object for the containment reference '{@link de.hykops.lpd.datamodel.HCoordinateSystem3DPolar#getRadialVector <em>Radial Vector</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Radial Vector</em>'.
	 * @see de.hykops.lpd.datamodel.HCoordinateSystem3DPolar#getRadialVector()
	 * @see #getHCoordinateSystem3DPolar()
	 * @generated
	 */
	EReference getHCoordinateSystem3DPolar_RadialVector();

	/**
	 * Returns the meta object for the containment reference '{@link de.hykops.lpd.datamodel.HCoordinateSystem3DPolar#getAngle <em>Angle</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Angle</em>'.
	 * @see de.hykops.lpd.datamodel.HCoordinateSystem3DPolar#getAngle()
	 * @see #getHCoordinateSystem3DPolar()
	 * @generated
	 */
	EReference getHCoordinateSystem3DPolar_Angle();

	/**
	 * Returns the meta object for class '{@link de.hykops.lpd.datamodel.HCurve2D <em>HCurve2 D</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>HCurve2 D</em>'.
	 * @see de.hykops.lpd.datamodel.HCurve2D
	 * @generated
	 */
	EClass getHCurve2D();

	/**
	 * Returns the meta object for the reference list '{@link de.hykops.lpd.datamodel.HCurve2D#getHSurfaceIdentifiers <em>HSurface Identifiers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>HSurface Identifiers</em>'.
	 * @see de.hykops.lpd.datamodel.HCurve2D#getHSurfaceIdentifiers()
	 * @see #getHCurve2D()
	 * @generated
	 */
	EReference getHCurve2D_HSurfaceIdentifiers();

	/**
	 * Returns the meta object for the container reference '{@link de.hykops.lpd.datamodel.HCurve2D#getHLoftElement <em>HLoft Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>HLoft Element</em>'.
	 * @see de.hykops.lpd.datamodel.HCurve2D#getHLoftElement()
	 * @see #getHCurve2D()
	 * @generated
	 */
	EReference getHCurve2D_HLoftElement();

	/**
	 * Returns the meta object for class '{@link de.hykops.lpd.datamodel.HCurvePoint2D <em>HCurve Point2 D</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>HCurve Point2 D</em>'.
	 * @see de.hykops.lpd.datamodel.HCurvePoint2D
	 * @generated
	 */
	EClass getHCurvePoint2D();

	/**
	 * Returns the meta object for the attribute '{@link de.hykops.lpd.datamodel.HCurvePoint2D#getCurveCoordinate <em>Curve Coordinate</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Curve Coordinate</em>'.
	 * @see de.hykops.lpd.datamodel.HCurvePoint2D#getCurveCoordinate()
	 * @see #getHCurvePoint2D()
	 * @generated
	 */
	EAttribute getHCurvePoint2D_CurveCoordinate();

	/**
	 * Returns the meta object for class '{@link de.hykops.lpd.datamodel.HDiscreteCurve2D <em>HDiscrete Curve2 D</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>HDiscrete Curve2 D</em>'.
	 * @see de.hykops.lpd.datamodel.HDiscreteCurve2D
	 * @generated
	 */
	EClass getHDiscreteCurve2D();

	/**
	 * Returns the meta object for the attribute '{@link de.hykops.lpd.datamodel.HDiscreteCurve2D#getInterpolationScheme <em>Interpolation Scheme</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Interpolation Scheme</em>'.
	 * @see de.hykops.lpd.datamodel.HDiscreteCurve2D#getInterpolationScheme()
	 * @see #getHDiscreteCurve2D()
	 * @generated
	 */
	EAttribute getHDiscreteCurve2D_InterpolationScheme();

	/**
	 * Returns the meta object for the containment reference list '{@link de.hykops.lpd.datamodel.HDiscreteCurve2D#getCurvePoints <em>Curve Points</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Curve Points</em>'.
	 * @see de.hykops.lpd.datamodel.HDiscreteCurve2D#getCurvePoints()
	 * @see #getHDiscreteCurve2D()
	 * @generated
	 */
	EReference getHDiscreteCurve2D_CurvePoints();

	/**
	 * Returns the meta object for class '{@link de.hykops.lpd.datamodel.HDiscreteLoftPath <em>HDiscrete Loft Path</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>HDiscrete Loft Path</em>'.
	 * @see de.hykops.lpd.datamodel.HDiscreteLoftPath
	 * @generated
	 */
	EClass getHDiscreteLoftPath();

	/**
	 * Returns the meta object for the containment reference list '{@link de.hykops.lpd.datamodel.HDiscreteLoftPath#getLoftCurvePoints <em>Loft Curve Points</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Loft Curve Points</em>'.
	 * @see de.hykops.lpd.datamodel.HDiscreteLoftPath#getLoftCurvePoints()
	 * @see #getHDiscreteLoftPath()
	 * @generated
	 */
	EReference getHDiscreteLoftPath_LoftCurvePoints();

	/**
	 * Returns the meta object for the attribute '{@link de.hykops.lpd.datamodel.HDiscreteLoftPath#getInterpolationScheme <em>Interpolation Scheme</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Interpolation Scheme</em>'.
	 * @see de.hykops.lpd.datamodel.HDiscreteLoftPath#getInterpolationScheme()
	 * @see #getHDiscreteLoftPath()
	 * @generated
	 */
	EAttribute getHDiscreteLoftPath_InterpolationScheme();

	/**
	 * Returns the meta object for class '{@link de.hykops.lpd.datamodel.HDiscreteLoftPathPoint3D <em>HDiscrete Loft Path Point3 D</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>HDiscrete Loft Path Point3 D</em>'.
	 * @see de.hykops.lpd.datamodel.HDiscreteLoftPathPoint3D
	 * @generated
	 */
	EClass getHDiscreteLoftPathPoint3D();

	/**
	 * Returns the meta object for the containment reference '{@link de.hykops.lpd.datamodel.HDiscreteLoftPathPoint3D#getLoftCurvePoint <em>Loft Curve Point</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Loft Curve Point</em>'.
	 * @see de.hykops.lpd.datamodel.HDiscreteLoftPathPoint3D#getLoftCurvePoint()
	 * @see #getHDiscreteLoftPathPoint3D()
	 * @generated
	 */
	EReference getHDiscreteLoftPathPoint3D_LoftCurvePoint();

	/**
	 * Returns the meta object for the attribute '{@link de.hykops.lpd.datamodel.HDiscreteLoftPathPoint3D#getLoftCoordinate <em>Loft Coordinate</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Loft Coordinate</em>'.
	 * @see de.hykops.lpd.datamodel.HDiscreteLoftPathPoint3D#getLoftCoordinate()
	 * @see #getHDiscreteLoftPathPoint3D()
	 * @generated
	 */
	EAttribute getHDiscreteLoftPathPoint3D_LoftCoordinate();

	/**
	 * Returns the meta object for the containment reference '{@link de.hykops.lpd.datamodel.HDiscreteLoftPathPoint3D#getLoftPathVector <em>Loft Path Vector</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Loft Path Vector</em>'.
	 * @see de.hykops.lpd.datamodel.HDiscreteLoftPathPoint3D#getLoftPathVector()
	 * @see #getHDiscreteLoftPathPoint3D()
	 * @generated
	 */
	EReference getHDiscreteLoftPathPoint3D_LoftPathVector();

	/**
	 * Returns the meta object for the containment reference '{@link de.hykops.lpd.datamodel.HDiscreteLoftPathPoint3D#getLoftPathXi <em>Loft Path Xi</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Loft Path Xi</em>'.
	 * @see de.hykops.lpd.datamodel.HDiscreteLoftPathPoint3D#getLoftPathXi()
	 * @see #getHDiscreteLoftPathPoint3D()
	 * @generated
	 */
	EReference getHDiscreteLoftPathPoint3D_LoftPathXi();

	/**
	 * Returns the meta object for the containment reference '{@link de.hykops.lpd.datamodel.HDiscreteLoftPathPoint3D#getLoftPathEta <em>Loft Path Eta</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Loft Path Eta</em>'.
	 * @see de.hykops.lpd.datamodel.HDiscreteLoftPathPoint3D#getLoftPathEta()
	 * @see #getHDiscreteLoftPathPoint3D()
	 * @generated
	 */
	EReference getHDiscreteLoftPathPoint3D_LoftPathEta();

	/**
	 * Returns the meta object for class '{@link de.hykops.lpd.datamodel.HHinge <em>HHinge</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>HHinge</em>'.
	 * @see de.hykops.lpd.datamodel.HHinge
	 * @generated
	 */
	EClass getHHinge();

	/**
	 * Returns the meta object for the containment reference '{@link de.hykops.lpd.datamodel.HHinge#getHingeAxis <em>Hinge Axis</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Hinge Axis</em>'.
	 * @see de.hykops.lpd.datamodel.HHinge#getHingeAxis()
	 * @see #getHHinge()
	 * @generated
	 */
	EReference getHHinge_HingeAxis();

	/**
	 * Returns the meta object for the containment reference list '{@link de.hykops.lpd.datamodel.HHinge#getHingePositions <em>Hinge Positions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Hinge Positions</em>'.
	 * @see de.hykops.lpd.datamodel.HHinge#getHingePositions()
	 * @see #getHHinge()
	 * @generated
	 */
	EReference getHHinge_HingePositions();

	/**
	 * Returns the meta object for the attribute '{@link de.hykops.lpd.datamodel.HHinge#isHingeLimited <em>Hinge Limited</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Hinge Limited</em>'.
	 * @see de.hykops.lpd.datamodel.HHinge#isHingeLimited()
	 * @see #getHHinge()
	 * @generated
	 */
	EAttribute getHHinge_HingeLimited();

	/**
	 * Returns the meta object for the container reference '{@link de.hykops.lpd.datamodel.HHinge#getHCoordinateSystem3D <em>HCoordinate System3 D</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>HCoordinate System3 D</em>'.
	 * @see de.hykops.lpd.datamodel.HHinge#getHCoordinateSystem3D()
	 * @see #getHHinge()
	 * @generated
	 */
	EReference getHHinge_HCoordinateSystem3D();

	/**
	 * Returns the meta object for class '{@link de.hykops.lpd.datamodel.HLinearLoftPath <em>HLinear Loft Path</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>HLinear Loft Path</em>'.
	 * @see de.hykops.lpd.datamodel.HLinearLoftPath
	 * @generated
	 */
	EClass getHLinearLoftPath();

	/**
	 * Returns the meta object for the containment reference '{@link de.hykops.lpd.datamodel.HLinearLoftPath#getOrigin <em>Origin</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Origin</em>'.
	 * @see de.hykops.lpd.datamodel.HLinearLoftPath#getOrigin()
	 * @see #getHLinearLoftPath()
	 * @generated
	 */
	EReference getHLinearLoftPath_Origin();

	/**
	 * Returns the meta object for the containment reference '{@link de.hykops.lpd.datamodel.HLinearLoftPath#getLoftPathVector <em>Loft Path Vector</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Loft Path Vector</em>'.
	 * @see de.hykops.lpd.datamodel.HLinearLoftPath#getLoftPathVector()
	 * @see #getHLinearLoftPath()
	 * @generated
	 */
	EReference getHLinearLoftPath_LoftPathVector();

	/**
	 * Returns the meta object for the containment reference '{@link de.hykops.lpd.datamodel.HLinearLoftPath#getLoftPathXi <em>Loft Path Xi</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Loft Path Xi</em>'.
	 * @see de.hykops.lpd.datamodel.HLinearLoftPath#getLoftPathXi()
	 * @see #getHLinearLoftPath()
	 * @generated
	 */
	EReference getHLinearLoftPath_LoftPathXi();

	/**
	 * Returns the meta object for the containment reference '{@link de.hykops.lpd.datamodel.HLinearLoftPath#getLoftPathEta <em>Loft Path Eta</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Loft Path Eta</em>'.
	 * @see de.hykops.lpd.datamodel.HLinearLoftPath#getLoftPathEta()
	 * @see #getHLinearLoftPath()
	 * @generated
	 */
	EReference getHLinearLoftPath_LoftPathEta();

	/**
	 * Returns the meta object for class '{@link de.hykops.lpd.datamodel.HLoft <em>HLoft</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>HLoft</em>'.
	 * @see de.hykops.lpd.datamodel.HLoft
	 * @generated
	 */
	EClass getHLoft();

	/**
	 * Returns the meta object for the attribute '{@link de.hykops.lpd.datamodel.HLoft#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see de.hykops.lpd.datamodel.HLoft#getName()
	 * @see #getHLoft()
	 * @generated
	 */
	EAttribute getHLoft_Name();

	/**
	 * Returns the meta object for the attribute '{@link de.hykops.lpd.datamodel.HLoft#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see de.hykops.lpd.datamodel.HLoft#getDescription()
	 * @see #getHLoft()
	 * @generated
	 */
	EAttribute getHLoft_Description();

	/**
	 * Returns the meta object for the containment reference '{@link de.hykops.lpd.datamodel.HLoft#getParameters <em>Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Parameters</em>'.
	 * @see de.hykops.lpd.datamodel.HLoft#getParameters()
	 * @see #getHLoft()
	 * @generated
	 */
	EReference getHLoft_Parameters();

	/**
	 * Returns the meta object for the containment reference list '{@link de.hykops.lpd.datamodel.HLoft#getHCoordinateSystems3D <em>HCoordinate Systems3 D</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>HCoordinate Systems3 D</em>'.
	 * @see de.hykops.lpd.datamodel.HLoft#getHCoordinateSystems3D()
	 * @see #getHLoft()
	 * @generated
	 */
	EReference getHLoft_HCoordinateSystems3D();

	/**
	 * Returns the meta object for the containment reference '{@link de.hykops.lpd.datamodel.HLoft#getHLoftPath <em>HLoft Path</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>HLoft Path</em>'.
	 * @see de.hykops.lpd.datamodel.HLoft#getHLoftPath()
	 * @see #getHLoft()
	 * @generated
	 */
	EReference getHLoft_HLoftPath();

	/**
	 * Returns the meta object for the containment reference list '{@link de.hykops.lpd.datamodel.HLoft#getHLoftElements <em>HLoft Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>HLoft Elements</em>'.
	 * @see de.hykops.lpd.datamodel.HLoft#getHLoftElements()
	 * @see #getHLoft()
	 * @generated
	 */
	EReference getHLoft_HLoftElements();

	/**
	 * Returns the meta object for the containment reference list '{@link de.hykops.lpd.datamodel.HLoft#getHSurfaceIdentifications <em>HSurface Identifications</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>HSurface Identifications</em>'.
	 * @see de.hykops.lpd.datamodel.HLoft#getHSurfaceIdentifications()
	 * @see #getHLoft()
	 * @generated
	 */
	EReference getHLoft_HSurfaceIdentifications();

	/**
	 * Returns the meta object for the container reference '{@link de.hykops.lpd.datamodel.HLoft#getHComponent <em>HComponent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>HComponent</em>'.
	 * @see de.hykops.lpd.datamodel.HLoft#getHComponent()
	 * @see #getHLoft()
	 * @generated
	 */
	EReference getHLoft_HComponent();

	/**
	 * Returns the meta object for class '{@link de.hykops.lpd.datamodel.HLoftElement <em>HLoft Element</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>HLoft Element</em>'.
	 * @see de.hykops.lpd.datamodel.HLoftElement
	 * @generated
	 */
	EClass getHLoftElement();

	/**
	 * Returns the meta object for the containment reference '{@link de.hykops.lpd.datamodel.HLoftElement#getLoftPlaneDefiningCoordinateSystem <em>Loft Plane Defining Coordinate System</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Loft Plane Defining Coordinate System</em>'.
	 * @see de.hykops.lpd.datamodel.HLoftElement#getLoftPlaneDefiningCoordinateSystem()
	 * @see #getHLoftElement()
	 * @generated
	 */
	EReference getHLoftElement_LoftPlaneDefiningCoordinateSystem();

	/**
	 * Returns the meta object for the containment reference list '{@link de.hykops.lpd.datamodel.HLoftElement#getHCurve2Ds <em>HCurve2 Ds</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>HCurve2 Ds</em>'.
	 * @see de.hykops.lpd.datamodel.HLoftElement#getHCurve2Ds()
	 * @see #getHLoftElement()
	 * @generated
	 */
	EReference getHLoftElement_HCurve2Ds();

	/**
	 * Returns the meta object for the container reference '{@link de.hykops.lpd.datamodel.HLoftElement#getHLoft <em>HLoft</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>HLoft</em>'.
	 * @see de.hykops.lpd.datamodel.HLoftElement#getHLoft()
	 * @see #getHLoftElement()
	 * @generated
	 */
	EReference getHLoftElement_HLoft();

	/**
	 * Returns the meta object for class '{@link de.hykops.lpd.datamodel.HLoftPath <em>HLoft Path</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>HLoft Path</em>'.
	 * @see de.hykops.lpd.datamodel.HLoftPath
	 * @generated
	 */
	EClass getHLoftPath();

	/**
	 * Returns the meta object for the attribute '{@link de.hykops.lpd.datamodel.HLoftPath#getDenominator <em>Denominator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Denominator</em>'.
	 * @see de.hykops.lpd.datamodel.HLoftPath#getDenominator()
	 * @see #getHLoftPath()
	 * @generated
	 */
	EAttribute getHLoftPath_Denominator();

	/**
	 * Returns the meta object for the containment reference '{@link de.hykops.lpd.datamodel.HLoftPath#getHLoftCoordinateSystem <em>HLoft Coordinate System</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>HLoft Coordinate System</em>'.
	 * @see de.hykops.lpd.datamodel.HLoftPath#getHLoftCoordinateSystem()
	 * @see #getHLoftPath()
	 * @generated
	 */
	EReference getHLoftPath_HLoftCoordinateSystem();

	/**
	 * Returns the meta object for the container reference '{@link de.hykops.lpd.datamodel.HLoftPath#getHLoft <em>HLoft</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>HLoft</em>'.
	 * @see de.hykops.lpd.datamodel.HLoftPath#getHLoft()
	 * @see #getHLoftPath()
	 * @generated
	 */
	EReference getHLoftPath_HLoft();

	/**
	 * Returns the meta object for class '{@link de.hykops.lpd.datamodel.HPair <em>HPair</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>HPair</em>'.
	 * @see de.hykops.lpd.datamodel.HPair
	 * @generated
	 */
	EClass getHPair();

	/**
	 * Returns the meta object for the containment reference '{@link de.hykops.lpd.datamodel.HPair#getA <em>A</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>A</em>'.
	 * @see de.hykops.lpd.datamodel.HPair#getA()
	 * @see #getHPair()
	 * @generated
	 */
	EReference getHPair_A();

	/**
	 * Returns the meta object for the containment reference '{@link de.hykops.lpd.datamodel.HPair#getB <em>B</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>B</em>'.
	 * @see de.hykops.lpd.datamodel.HPair#getB()
	 * @see #getHPair()
	 * @generated
	 */
	EReference getHPair_B();

	/**
	 * Returns the meta object for class '{@link de.hykops.lpd.datamodel.HParent <em>HParent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>HParent</em>'.
	 * @see de.hykops.lpd.datamodel.HParent
	 * @generated
	 */
	EClass getHParent();

	/**
	 * Returns the meta object for the reference '{@link de.hykops.lpd.datamodel.HParent#getHParent <em>HParent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>HParent</em>'.
	 * @see de.hykops.lpd.datamodel.HParent#getHParent()
	 * @see #getHParent()
	 * @generated
	 */
	EReference getHParent_HParent();

	/**
	 * Returns the meta object for class '{@link de.hykops.lpd.datamodel.HPoint2D <em>HPoint2 D</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>HPoint2 D</em>'.
	 * @see de.hykops.lpd.datamodel.HPoint2D
	 * @generated
	 */
	EClass getHPoint2D();

	/**
	 * Returns the meta object for the attribute '{@link de.hykops.lpd.datamodel.HPoint2D#getX1 <em>X1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>X1</em>'.
	 * @see de.hykops.lpd.datamodel.HPoint2D#getX1()
	 * @see #getHPoint2D()
	 * @generated
	 */
	EAttribute getHPoint2D_X1();

	/**
	 * Returns the meta object for the attribute '{@link de.hykops.lpd.datamodel.HPoint2D#getX2 <em>X2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>X2</em>'.
	 * @see de.hykops.lpd.datamodel.HPoint2D#getX2()
	 * @see #getHPoint2D()
	 * @generated
	 */
	EAttribute getHPoint2D_X2();

	/**
	 * Returns the meta object for class '{@link de.hykops.lpd.datamodel.HPoint3D <em>HPoint3 D</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>HPoint3 D</em>'.
	 * @see de.hykops.lpd.datamodel.HPoint3D
	 * @generated
	 */
	EClass getHPoint3D();

	/**
	 * Returns the meta object for the attribute '{@link de.hykops.lpd.datamodel.HPoint3D#getX1 <em>X1</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>X1</em>'.
	 * @see de.hykops.lpd.datamodel.HPoint3D#getX1()
	 * @see #getHPoint3D()
	 * @generated
	 */
	EAttribute getHPoint3D_X1();

	/**
	 * Returns the meta object for the attribute '{@link de.hykops.lpd.datamodel.HPoint3D#getX2 <em>X2</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>X2</em>'.
	 * @see de.hykops.lpd.datamodel.HPoint3D#getX2()
	 * @see #getHPoint3D()
	 * @generated
	 */
	EAttribute getHPoint3D_X2();

	/**
	 * Returns the meta object for the attribute '{@link de.hykops.lpd.datamodel.HPoint3D#getX3 <em>X3</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>X3</em>'.
	 * @see de.hykops.lpd.datamodel.HPoint3D#getX3()
	 * @see #getHPoint3D()
	 * @generated
	 */
	EAttribute getHPoint3D_X3();

	/**
	 * Returns the meta object for class '{@link de.hykops.lpd.datamodel.HSeriesProfileCurve2D <em>HSeries Profile Curve2 D</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>HSeries Profile Curve2 D</em>'.
	 * @see de.hykops.lpd.datamodel.HSeriesProfileCurve2D
	 * @generated
	 */
	EClass getHSeriesProfileCurve2D();

	/**
	 * Returns the meta object for the attribute '{@link de.hykops.lpd.datamodel.HSeriesProfileCurve2D#getDenomination <em>Denomination</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Denomination</em>'.
	 * @see de.hykops.lpd.datamodel.HSeriesProfileCurve2D#getDenomination()
	 * @see #getHSeriesProfileCurve2D()
	 * @generated
	 */
	EAttribute getHSeriesProfileCurve2D_Denomination();

	/**
	 * Returns the meta object for the attribute '{@link de.hykops.lpd.datamodel.HSeriesProfileCurve2D#getSide <em>Side</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Side</em>'.
	 * @see de.hykops.lpd.datamodel.HSeriesProfileCurve2D#getSide()
	 * @see #getHSeriesProfileCurve2D()
	 * @generated
	 */
	EAttribute getHSeriesProfileCurve2D_Side();

	/**
	 * Returns the meta object for the attribute '{@link de.hykops.lpd.datamodel.HSeriesProfileCurve2D#getSeries <em>Series</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Series</em>'.
	 * @see de.hykops.lpd.datamodel.HSeriesProfileCurve2D#getSeries()
	 * @see #getHSeriesProfileCurve2D()
	 * @generated
	 */
	EAttribute getHSeriesProfileCurve2D_Series();

	/**
	 * Returns the meta object for class '{@link de.hykops.lpd.datamodel.HSurfaceIdentifier <em>HSurface Identifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>HSurface Identifier</em>'.
	 * @see de.hykops.lpd.datamodel.HSurfaceIdentifier
	 * @generated
	 */
	EClass getHSurfaceIdentifier();

	/**
	 * Returns the meta object for the attribute '{@link de.hykops.lpd.datamodel.HSurfaceIdentifier#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see de.hykops.lpd.datamodel.HSurfaceIdentifier#getName()
	 * @see #getHSurfaceIdentifier()
	 * @generated
	 */
	EAttribute getHSurfaceIdentifier_Name();

	/**
	 * Returns the meta object for class '{@link java.util.Map <em>Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Map</em>'.
	 * @see java.util.Map
	 * @model instanceClass="java.util.Map" typeParameters="T T1"
	 * @generated
	 */
	EClass getMap();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	DatamodelFactory getDatamodelFactory();
	
	/**
	 * Returns the meta object for class '{@link java.util.Map.Entry <em>String To String Map</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>String To String Map</em>'.
	 * @see java.util.Map.Entry
	 * @model keyDataType="org.eclipse.emf.ecore.EString"
	 *        valueDataType="org.eclipse.emf.ecore.EString"
	 * @generated
	 */
	EClass getStringToStringMap();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		
		/**
		 * The meta object literal for the '{@link de.hykops.lpd.datamodel.impl.StringToStringMapImpl <em>String To String Map</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hykops.lpd.datamodel.impl.StringToStringMapImpl
		 * @see de.hykops.lpd.datamodel.impl.DatamodelPackageImpl#getStringToStringMap()
		 * @generated
		 */
		EClass STRING_TO_STRING_MAP = eINSTANCE.getStringToStringMap();
		
		/**
		 * The meta object literal for the '{@link de.hykops.lpd.datamodel.impl.HComponentImpl <em>HComponent</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hykops.lpd.datamodel.impl.HComponentImpl
		 * @see de.hykops.lpd.datamodel.impl.DatamodelPackageImpl#getHComponent()
		 * @generated
		 */
		EClass HCOMPONENT = eINSTANCE.getHComponent();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HCOMPONENT__NAME = eINSTANCE.getHComponent_Name();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HCOMPONENT__TYPE = eINSTANCE.getHComponent_Type();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HCOMPONENT__DESCRIPTION = eINSTANCE.getHComponent_Description();

		/**
		 * The meta object literal for the '<em><b>Parameters</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HCOMPONENT__PARAMETERS = eINSTANCE.getHComponent_Parameters();

		/**
		 * The meta object literal for the '<em><b>HCoordinate System3 D</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HCOMPONENT__HCOORDINATE_SYSTEM3_D = eINSTANCE.getHComponent_HCoordinateSystem3D();

		/**
		 * The meta object literal for the '<em><b>HLofts</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HCOMPONENT__HLOFTS = eINSTANCE.getHComponent_HLofts();

		/**
		 * The meta object literal for the '<em><b>HComposition</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HCOMPONENT__HCOMPOSITION = eINSTANCE.getHComponent_HComposition();

		/**
		 * The meta object literal for the '{@link de.hykops.lpd.datamodel.impl.HCompositionImpl <em>HComposition</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hykops.lpd.datamodel.impl.HCompositionImpl
		 * @see de.hykops.lpd.datamodel.impl.DatamodelPackageImpl#getHComposition()
		 * @generated
		 */
		EClass HCOMPOSITION = eINSTANCE.getHComposition();

		/**
		 * The meta object literal for the '<em><b>Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HCOMPOSITION__VERSION = eINSTANCE.getHComposition_Version();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HCOMPOSITION__NAME = eINSTANCE.getHComposition_Name();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HCOMPOSITION__DESCRIPTION = eINSTANCE.getHComposition_Description();

		/**
		 * The meta object literal for the '<em><b>Unit System</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HCOMPOSITION__UNIT_SYSTEM = eINSTANCE.getHComposition_UnitSystem();

		/**
		 * The meta object literal for the '<em><b>HCoordinate System3 D</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HCOMPOSITION__HCOORDINATE_SYSTEM3_D = eINSTANCE.getHComposition_HCoordinateSystem3D();

		/**
		 * The meta object literal for the '<em><b>HComponents</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HCOMPOSITION__HCOMPONENTS = eINSTANCE.getHComposition_HComponents();

		/**
		 * The meta object literal for the '{@link de.hykops.lpd.datamodel.impl.HCoordinateSystem3DImpl <em>HCoordinate System3 D</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hykops.lpd.datamodel.impl.HCoordinateSystem3DImpl
		 * @see de.hykops.lpd.datamodel.impl.DatamodelPackageImpl#getHCoordinateSystem3D()
		 * @generated
		 */
		EClass HCOORDINATE_SYSTEM3_D = eINSTANCE.getHCoordinateSystem3D();

		/**
		 * The meta object literal for the '<em><b>Origin</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HCOORDINATE_SYSTEM3_D__ORIGIN = eINSTANCE.getHCoordinateSystem3D_Origin();

		/**
		 * The meta object literal for the '<em><b>HHinges</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HCOORDINATE_SYSTEM3_D__HHINGES = eINSTANCE.getHCoordinateSystem3D_HHinges();

		/**
		 * The meta object literal for the '{@link de.hykops.lpd.datamodel.impl.HCoordinateSystem3DAffineImpl <em>HCoordinate System3 DAffine</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hykops.lpd.datamodel.impl.HCoordinateSystem3DAffineImpl
		 * @see de.hykops.lpd.datamodel.impl.DatamodelPackageImpl#getHCoordinateSystem3DAffine()
		 * @generated
		 */
		EClass HCOORDINATE_SYSTEM3_DAFFINE = eINSTANCE.getHCoordinateSystem3DAffine();

		/**
		 * The meta object literal for the '<em><b>X1</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HCOORDINATE_SYSTEM3_DAFFINE__X1 = eINSTANCE.getHCoordinateSystem3DAffine_X1();

		/**
		 * The meta object literal for the '<em><b>X2</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HCOORDINATE_SYSTEM3_DAFFINE__X2 = eINSTANCE.getHCoordinateSystem3DAffine_X2();

		/**
		 * The meta object literal for the '<em><b>X3</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HCOORDINATE_SYSTEM3_DAFFINE__X3 = eINSTANCE.getHCoordinateSystem3DAffine_X3();

		/**
		 * The meta object literal for the '{@link de.hykops.lpd.datamodel.impl.HCoordinateSystem3DCartesianImpl <em>HCoordinate System3 DCartesian</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hykops.lpd.datamodel.impl.HCoordinateSystem3DCartesianImpl
		 * @see de.hykops.lpd.datamodel.impl.DatamodelPackageImpl#getHCoordinateSystem3DCartesian()
		 * @generated
		 */
		EClass HCOORDINATE_SYSTEM3_DCARTESIAN = eINSTANCE.getHCoordinateSystem3DCartesian();

		/**
		 * The meta object literal for the '<em><b>X1</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HCOORDINATE_SYSTEM3_DCARTESIAN__X1 = eINSTANCE.getHCoordinateSystem3DCartesian_X1();

		/**
		 * The meta object literal for the '<em><b>X2</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HCOORDINATE_SYSTEM3_DCARTESIAN__E1 = eINSTANCE.getHCoordinateSystem3DCartesian_E1();

		/**
		 * The meta object literal for the '{@link de.hykops.lpd.datamodel.impl.HCoordinateSystem3DConeImpl <em>HCoordinate System3 DCone</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hykops.lpd.datamodel.impl.HCoordinateSystem3DConeImpl
		 * @see de.hykops.lpd.datamodel.impl.DatamodelPackageImpl#getHCoordinateSystem3DCone()
		 * @generated
		 */
		EClass HCOORDINATE_SYSTEM3_DCONE = eINSTANCE.getHCoordinateSystem3DCone();
		
		/**
		 * The meta object literal for the '<em><b>Axial Vector</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HCOORDINATE_SYSTEM3_DCONE__AXIAL_VECTOR = eINSTANCE.getHCoordinateSystem3DCone_AxialVector();

		/**
		 * The meta object literal for the '<em><b>Radial Vector</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HCOORDINATE_SYSTEM3_DCONE__RADIAL_VECTOR = eINSTANCE.getHCoordinateSystem3DCone_RadialVector();

		/**
		 * The meta object literal for the '<em><b>Angle</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HCOORDINATE_SYSTEM3_DCONE__ANGLE = eINSTANCE.getHCoordinateSystem3DCone_Angle();
		
		/**
		 * The meta object literal for the '{@link de.hykops.lpd.datamodel.impl.HCoordinateSystem3DPolarImpl <em>HCoordinate System3 DPolar</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hykops.lpd.datamodel.impl.HCoordinateSystem3DPolarImpl
		 * @see de.hykops.lpd.datamodel.impl.DatamodelPackageImpl#getHCoordinateSystem3DPolar()
		 * @generated
		 */
		EClass HCOORDINATE_SYSTEM3_DPOLAR = eINSTANCE.getHCoordinateSystem3DPolar();

		/**
		 * The meta object literal for the '<em><b>Axial Vector</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HCOORDINATE_SYSTEM3_DPOLAR__AXIAL_VECTOR = eINSTANCE.getHCoordinateSystem3DPolar_AxialVector();

		/**
		 * The meta object literal for the '<em><b>Radial Vector</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HCOORDINATE_SYSTEM3_DPOLAR__RADIAL_VECTOR = eINSTANCE.getHCoordinateSystem3DPolar_RadialVector();

		/**
		 * The meta object literal for the '<em><b>Angle</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HCOORDINATE_SYSTEM3_DPOLAR__ANGLE = eINSTANCE.getHCoordinateSystem3DPolar_Angle();

		/**
		 * The meta object literal for the '{@link de.hykops.lpd.datamodel.impl.HCurve2DImpl <em>HCurve2 D</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hykops.lpd.datamodel.impl.HCurve2DImpl
		 * @see de.hykops.lpd.datamodel.impl.DatamodelPackageImpl#getHCurve2D()
		 * @generated
		 */
		EClass HCURVE2_D = eINSTANCE.getHCurve2D();

		/**
		 * The meta object literal for the '<em><b>HSurface Identifiers</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HCURVE2_D__HSURFACE_IDENTIFIERS = eINSTANCE.getHCurve2D_HSurfaceIdentifiers();

		/**
		 * The meta object literal for the '<em><b>HLoft Element</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HCURVE2_D__HLOFT_ELEMENT = eINSTANCE.getHCurve2D_HLoftElement();

		/**
		 * The meta object literal for the '{@link de.hykops.lpd.datamodel.impl.HCurvePoint2DImpl <em>HCurve Point2 D</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hykops.lpd.datamodel.impl.HCurvePoint2DImpl
		 * @see de.hykops.lpd.datamodel.impl.DatamodelPackageImpl#getHCurvePoint2D()
		 * @generated
		 */
		EClass HCURVE_POINT2_D = eINSTANCE.getHCurvePoint2D();

		/**
		 * The meta object literal for the '<em><b>Curve Coordinate</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HCURVE_POINT2_D__CURVE_COORDINATE = eINSTANCE.getHCurvePoint2D_CurveCoordinate();

		/**
		 * The meta object literal for the '{@link de.hykops.lpd.datamodel.impl.HDiscreteCurve2DImpl <em>HDiscrete Curve2 D</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hykops.lpd.datamodel.impl.HDiscreteCurve2DImpl
		 * @see de.hykops.lpd.datamodel.impl.DatamodelPackageImpl#getHDiscreteCurve2D()
		 * @generated
		 */
		EClass HDISCRETE_CURVE2_D = eINSTANCE.getHDiscreteCurve2D();

		/**
		 * The meta object literal for the '<em><b>Interpolation Scheme</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HDISCRETE_CURVE2_D__INTERPOLATION_SCHEME = eINSTANCE.getHDiscreteCurve2D_InterpolationScheme();

		/**
		 * The meta object literal for the '<em><b>Curve Points</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HDISCRETE_CURVE2_D__CURVE_POINTS = eINSTANCE.getHDiscreteCurve2D_CurvePoints();

		/**
		 * The meta object literal for the '{@link de.hykops.lpd.datamodel.impl.HDiscreteLoftPathImpl <em>HDiscrete Loft Path</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hykops.lpd.datamodel.impl.HDiscreteLoftPathImpl
		 * @see de.hykops.lpd.datamodel.impl.DatamodelPackageImpl#getHDiscreteLoftPath()
		 * @generated
		 */
		EClass HDISCRETE_LOFT_PATH = eINSTANCE.getHDiscreteLoftPath();

		/**
		 * The meta object literal for the '<em><b>Loft Curve Points</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HDISCRETE_LOFT_PATH__LOFT_CURVE_POINTS = eINSTANCE.getHDiscreteLoftPath_LoftCurvePoints();

		/**
		 * The meta object literal for the '<em><b>Interpolation Scheme</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HDISCRETE_LOFT_PATH__INTERPOLATION_SCHEME = eINSTANCE.getHDiscreteLoftPath_InterpolationScheme();

		/**
		 * The meta object literal for the '{@link de.hykops.lpd.datamodel.impl.HDiscreteLoftPathPoint3DImpl <em>HDiscrete Loft Path Point3 D</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hykops.lpd.datamodel.impl.HDiscreteLoftPathPoint3DImpl
		 * @see de.hykops.lpd.datamodel.impl.DatamodelPackageImpl#getHDiscreteLoftPathPoint3D()
		 * @generated
		 */
		EClass HDISCRETE_LOFT_PATH_POINT3_D = eINSTANCE.getHDiscreteLoftPathPoint3D();

		/**
		 * The meta object literal for the '<em><b>Loft Curve Point</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HDISCRETE_LOFT_PATH_POINT3_D__LOFT_CURVE_POINT = eINSTANCE.getHDiscreteLoftPathPoint3D_LoftCurvePoint();

		/**
		 * The meta object literal for the '<em><b>Loft Coordinate</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HDISCRETE_LOFT_PATH_POINT3_D__LOFT_COORDINATE = eINSTANCE.getHDiscreteLoftPathPoint3D_LoftCoordinate();

		/**
		 * The meta object literal for the '<em><b>Loft Path Vector</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HDISCRETE_LOFT_PATH_POINT3_D__LOFT_PATH_VECTOR = eINSTANCE.getHDiscreteLoftPathPoint3D_LoftPathVector();

		/**
		 * The meta object literal for the '<em><b>Loft Path Xi</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HDISCRETE_LOFT_PATH_POINT3_D__LOFT_PATH_XI = eINSTANCE.getHDiscreteLoftPathPoint3D_LoftPathXi();

		/**
		 * The meta object literal for the '<em><b>Loft Path Eta</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HDISCRETE_LOFT_PATH_POINT3_D__LOFT_PATH_ETA = eINSTANCE.getHDiscreteLoftPathPoint3D_LoftPathEta();

		/**
		 * The meta object literal for the '{@link de.hykops.lpd.datamodel.impl.HHingeImpl <em>HHinge</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hykops.lpd.datamodel.impl.HHingeImpl
		 * @see de.hykops.lpd.datamodel.impl.DatamodelPackageImpl#getHHinge()
		 * @generated
		 */
		EClass HHINGE = eINSTANCE.getHHinge();

		/**
		 * The meta object literal for the '<em><b>Hinge Axis</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HHINGE__HINGE_AXIS = eINSTANCE.getHHinge_HingeAxis();

		/**
		 * The meta object literal for the '<em><b>Hinge Positions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HHINGE__HINGE_POSITIONS = eINSTANCE.getHHinge_HingePositions();

		/**
		 * The meta object literal for the '<em><b>Hinge Limited</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HHINGE__HINGE_LIMITED = eINSTANCE.getHHinge_HingeLimited();

		/**
		 * The meta object literal for the '<em><b>HCoordinate System3 D</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HHINGE__HCOORDINATE_SYSTEM3_D = eINSTANCE.getHHinge_HCoordinateSystem3D();

		/**
		 * The meta object literal for the '{@link de.hykops.lpd.datamodel.impl.HLinearLoftPathImpl <em>HLinear Loft Path</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hykops.lpd.datamodel.impl.HLinearLoftPathImpl
		 * @see de.hykops.lpd.datamodel.impl.DatamodelPackageImpl#getHLinearLoftPath()
		 * @generated
		 */
		EClass HLINEAR_LOFT_PATH = eINSTANCE.getHLinearLoftPath();

		/**
		 * The meta object literal for the '<em><b>Origin</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HLINEAR_LOFT_PATH__ORIGIN = eINSTANCE.getHLinearLoftPath_Origin();

		/**
		 * The meta object literal for the '<em><b>Loft Path Vector</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HLINEAR_LOFT_PATH__LOFT_PATH_VECTOR = eINSTANCE.getHLinearLoftPath_LoftPathVector();

		/**
		 * The meta object literal for the '<em><b>Loft Path Xi</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HLINEAR_LOFT_PATH__LOFT_PATH_XI = eINSTANCE.getHLinearLoftPath_LoftPathXi();

		/**
		 * The meta object literal for the '<em><b>Loft Path Eta</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HLINEAR_LOFT_PATH__LOFT_PATH_ETA = eINSTANCE.getHLinearLoftPath_LoftPathEta();

		/**
		 * The meta object literal for the '{@link de.hykops.lpd.datamodel.impl.HLoftImpl <em>HLoft</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hykops.lpd.datamodel.impl.HLoftImpl
		 * @see de.hykops.lpd.datamodel.impl.DatamodelPackageImpl#getHLoft()
		 * @generated
		 */
		EClass HLOFT = eINSTANCE.getHLoft();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HLOFT__NAME = eINSTANCE.getHLoft_Name();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HLOFT__DESCRIPTION = eINSTANCE.getHLoft_Description();

		/**
		 * The meta object literal for the '<em><b>Parameters</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HLOFT__PARAMETERS = eINSTANCE.getHLoft_Parameters();

		/**
		 * The meta object literal for the '<em><b>HCoordinate Systems3 D</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HLOFT__HCOORDINATE_SYSTEMS3_D = eINSTANCE.getHLoft_HCoordinateSystems3D();

		/**
		 * The meta object literal for the '<em><b>HLoft Path</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HLOFT__HLOFT_PATH = eINSTANCE.getHLoft_HLoftPath();

		/**
		 * The meta object literal for the '<em><b>HLoft Elements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HLOFT__HLOFT_ELEMENTS = eINSTANCE.getHLoft_HLoftElements();

		/**
		 * The meta object literal for the '<em><b>HSurface Identifications</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HLOFT__HSURFACE_IDENTIFICATIONS = eINSTANCE.getHLoft_HSurfaceIdentifications();

		/**
		 * The meta object literal for the '<em><b>HComponent</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HLOFT__HCOMPONENT = eINSTANCE.getHLoft_HComponent();

		/**
		 * The meta object literal for the '{@link de.hykops.lpd.datamodel.impl.HLoftElementImpl <em>HLoft Element</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hykops.lpd.datamodel.impl.HLoftElementImpl
		 * @see de.hykops.lpd.datamodel.impl.DatamodelPackageImpl#getHLoftElement()
		 * @generated
		 */
		EClass HLOFT_ELEMENT = eINSTANCE.getHLoftElement();

		/**
		 * The meta object literal for the '<em><b>Loft Plane Defining Coordinate System</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HLOFT_ELEMENT__LOFT_PLANE_DEFINING_COORDINATE_SYSTEM = eINSTANCE.getHLoftElement_LoftPlaneDefiningCoordinateSystem();

		/**
		 * The meta object literal for the '<em><b>HCurve2 Ds</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HLOFT_ELEMENT__HCURVE2_DS = eINSTANCE.getHLoftElement_HCurve2Ds();

		/**
		 * The meta object literal for the '<em><b>HLoft</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HLOFT_ELEMENT__HLOFT = eINSTANCE.getHLoftElement_HLoft();

		/**
		 * The meta object literal for the '{@link de.hykops.lpd.datamodel.impl.HLoftPathImpl <em>HLoft Path</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hykops.lpd.datamodel.impl.HLoftPathImpl
		 * @see de.hykops.lpd.datamodel.impl.DatamodelPackageImpl#getHLoftPath()
		 * @generated
		 */
		EClass HLOFT_PATH = eINSTANCE.getHLoftPath();

		/**
		 * The meta object literal for the '<em><b>Denominator</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HLOFT_PATH__DENOMINATOR = eINSTANCE.getHLoftPath_Denominator();

		/**
		 * The meta object literal for the '<em><b>HLoft Coordinate System</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HLOFT_PATH__HLOFT_COORDINATE_SYSTEM = eINSTANCE.getHLoftPath_HLoftCoordinateSystem();

		/**
		 * The meta object literal for the '<em><b>HLoft</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HLOFT_PATH__HLOFT = eINSTANCE.getHLoftPath_HLoft();

		/**
		 * The meta object literal for the '{@link de.hykops.lpd.datamodel.impl.HPairImpl <em>HPair</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hykops.lpd.datamodel.impl.HPairImpl
		 * @see de.hykops.lpd.datamodel.impl.DatamodelPackageImpl#getHPair()
		 * @generated
		 */
		EClass HPAIR = eINSTANCE.getHPair();

		/**
		 * The meta object literal for the '<em><b>A</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HPAIR__A = eINSTANCE.getHPair_A();

		/**
		 * The meta object literal for the '<em><b>B</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HPAIR__B = eINSTANCE.getHPair_B();

		/**
		 * The meta object literal for the '{@link de.hykops.lpd.datamodel.impl.HParentImpl <em>HParent</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hykops.lpd.datamodel.impl.HParentImpl
		 * @see de.hykops.lpd.datamodel.impl.DatamodelPackageImpl#getHParent()
		 * @generated
		 */
		EClass HPARENT = eINSTANCE.getHParent();

		/**
		 * The meta object literal for the '<em><b>HParent</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HPARENT__HPARENT = eINSTANCE.getHParent_HParent();

		/**
		 * The meta object literal for the '{@link de.hykops.lpd.datamodel.impl.HPoint2DImpl <em>HPoint2 D</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hykops.lpd.datamodel.impl.HPoint2DImpl
		 * @see de.hykops.lpd.datamodel.impl.DatamodelPackageImpl#getHPoint2D()
		 * @generated
		 */
		EClass HPOINT2_D = eINSTANCE.getHPoint2D();

		/**
		 * The meta object literal for the '<em><b>X1</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HPOINT2_D__X1 = eINSTANCE.getHPoint2D_X1();

		/**
		 * The meta object literal for the '<em><b>X2</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HPOINT2_D__X2 = eINSTANCE.getHPoint2D_X2();

		/**
		 * The meta object literal for the '{@link de.hykops.lpd.datamodel.impl.HPoint3DImpl <em>HPoint3 D</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hykops.lpd.datamodel.impl.HPoint3DImpl
		 * @see de.hykops.lpd.datamodel.impl.DatamodelPackageImpl#getHPoint3D()
		 * @generated
		 */
		EClass HPOINT3_D = eINSTANCE.getHPoint3D();

		/**
		 * The meta object literal for the '<em><b>X1</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HPOINT3_D__X1 = eINSTANCE.getHPoint3D_X1();

		/**
		 * The meta object literal for the '<em><b>X2</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HPOINT3_D__X2 = eINSTANCE.getHPoint3D_X2();

		/**
		 * The meta object literal for the '<em><b>X3</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HPOINT3_D__X3 = eINSTANCE.getHPoint3D_X3();

		/**
		 * The meta object literal for the '{@link de.hykops.lpd.datamodel.impl.HSeriesProfileCurve2DImpl <em>HSeries Profile Curve2 D</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hykops.lpd.datamodel.impl.HSeriesProfileCurve2DImpl
		 * @see de.hykops.lpd.datamodel.impl.DatamodelPackageImpl#getHSeriesProfileCurve2D()
		 * @generated
		 */
		EClass HSERIES_PROFILE_CURVE2_D = eINSTANCE.getHSeriesProfileCurve2D();

		/**
		 * The meta object literal for the '<em><b>Denomination</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HSERIES_PROFILE_CURVE2_D__DENOMINATION = eINSTANCE.getHSeriesProfileCurve2D_Denomination();

		/**
		 * The meta object literal for the '<em><b>Side</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HSERIES_PROFILE_CURVE2_D__SIDE = eINSTANCE.getHSeriesProfileCurve2D_Side();

		/**
		 * The meta object literal for the '<em><b>Series</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HSERIES_PROFILE_CURVE2_D__SERIES = eINSTANCE.getHSeriesProfileCurve2D_Series();

		/**
		 * The meta object literal for the '{@link de.hykops.lpd.datamodel.impl.HSurfaceIdentifierImpl <em>HSurface Identifier</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.hykops.lpd.datamodel.impl.HSurfaceIdentifierImpl
		 * @see de.hykops.lpd.datamodel.impl.DatamodelPackageImpl#getHSurfaceIdentifier()
		 * @generated
		 */
		EClass HSURFACE_IDENTIFIER = eINSTANCE.getHSurfaceIdentifier();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HSURFACE_IDENTIFIER__NAME = eINSTANCE.getHSurfaceIdentifier_Name();

		/**
		 * The meta object literal for the '{@link java.util.Map <em>Map</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see java.util.Map
		 * @see de.hykops.lpd.datamodel.impl.DatamodelPackageImpl#getMap()
		 * @generated
		 */
		EClass MAP = eINSTANCE.getMap();

	}

} //DatamodelPackage
