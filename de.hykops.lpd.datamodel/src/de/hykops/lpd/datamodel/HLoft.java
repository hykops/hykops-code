/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.lpd.datamodel;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;

/**
 * @model
 *
 * @author stoye
 * @since Jun 30, 2016
 */
public interface HLoft extends HParent {
	
	static final String CLASS_NAME = HLoft.class.getSimpleName();

	static final String PROPERTY_NAME = "name";
	static final String PROPERTY_DESCRIPTION = "description";
	static final String PROPERTY_LOFT_PATH = "hLoftPath";
	static final String PROPERTY_COMPONENT = "hComponent";
	static final String PROPERTY_COORDINATE_SYSTEMS = "hCoordinateSystems3D";
	static final String PROPERTY_SURFACE_IDENTIFICATIONS = "hSurfaceIdentifications";
	static final String PROPERTY_SURFACE_LOFT_ELEMENTS = "hLoftElements";
	static final String PROPERTY_PARAMETER = "parameters";
	
	
	/**
	 * @model
	 */
	String getName();

	/**
	 * Sets the value of the '{@link de.hykops.lpd.datamodel.HLoft#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * @model
	 */
	String getDescription();

	/**
	 * Sets the value of the '{@link de.hykops.lpd.datamodel.HLoft#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * @model containment = "true"
	 */
	EMap<String, String> getParameters();


	/**
	 * concatenated coordinate systems e.g. for VSP
	 * @model containment="true"
	 */
	EList<HCoordinateSystem3D> getHCoordinateSystems3D();

	/**
	 * @model containment = "true" opposite = "hLoft"
	 */
	HLoftPath getHLoftPath();

	/**
	 * Sets the value of the '{@link de.hykops.lpd.datamodel.HLoft#getHLoftPath <em>HLoft Path</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>HLoft Path</em>' containment reference.
	 * @see #getHLoftPath()
	 * @generated
	 */
	void setHLoftPath(HLoftPath value);

	/**
	 * @model containment = "true" opposite = "hLoft"
	 * @return all loft elements (2D-surfaces)
	 */
	public EList<HLoftElement> getHLoftElements();
	
	/**
	 * @model containment="true"
	 */
	EList<HSurfaceIdentifier> getHSurfaceIdentifications();

	/**
	 * @model opposite = "hLofts"
	 */
	HComponent getHComponent();

	/**
	 * Sets the value of the '{@link de.hykops.lpd.datamodel.HLoft#getHComponent <em>HComponent</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>HComponent</em>' container reference.
	 * @see #getHComponent()
	 * @generated
	 */
	void setHComponent(HComponent value);
}
