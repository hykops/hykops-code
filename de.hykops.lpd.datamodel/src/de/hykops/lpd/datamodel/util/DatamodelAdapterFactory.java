/**
 */
package de.hykops.lpd.datamodel.util;

import de.hykops.lpd.datamodel.*;

import java.util.Map;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see de.hykops.lpd.datamodel.de.hykops.lpd.datamodel.test.DatamodelPackage
 * @generated
 */
public class DatamodelAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static DatamodelPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DatamodelAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = DatamodelPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DatamodelSwitch<Adapter> modelSwitch =
		new DatamodelSwitch<Adapter>() {
			@Override
			public Adapter caseHComponent(HComponent object) {
				return createHComponentAdapter();
			}
			@Override
			public Adapter caseHComposition(HComposition object) {
				return createHCompositionAdapter();
			}
			@Override
			public Adapter caseHCoordinateSystem3D(HCoordinateSystem3D object) {
				return createHCoordinateSystem3DAdapter();
			}
			@Override
			public Adapter caseHCoordinateSystem3DAffine(HCoordinateSystem3DAffine object) {
				return createHCoordinateSystem3DAffineAdapter();
			}
			@Override
			public Adapter caseHCoordinateSystem3DCartesian(HCoordinateSystem3DCartesian object) {
				return createHCoordinateSystem3DCartesianAdapter();
			}
			@Override
			public Adapter caseHCoordinateSystem3DCone(HCoordinateSystem3DCone object) {
				return createHCoordinateSystem3DConeAdapter();
			}
			@Override
			public Adapter caseHCoordinateSystem3DPolar(HCoordinateSystem3DPolar object) {
				return createHCoordinateSystem3DPolarAdapter();
			}
			@Override
			public Adapter caseHCurve2D(HCurve2D object) {
				return createHCurve2DAdapter();
			}
			@Override
			public Adapter caseHCurvePoint2D(HCurvePoint2D object) {
				return createHCurvePoint2DAdapter();
			}
			@Override
			public Adapter caseHDiscreteCurve2D(HDiscreteCurve2D object) {
				return createHDiscreteCurve2DAdapter();
			}
			@Override
			public Adapter caseHDiscreteLoftPath(HDiscreteLoftPath object) {
				return createHDiscreteLoftPathAdapter();
			}
			@Override
			public Adapter caseHDiscreteLoftPathPoint3D(HDiscreteLoftPathPoint3D object) {
				return createHDiscreteLoftPathPoint3DAdapter();
			}
			@Override
			public Adapter caseHHinge(HHinge object) {
				return createHHingeAdapter();
			}
			@Override
			public Adapter caseHLinearLoftPath(HLinearLoftPath object) {
				return createHLinearLoftPathAdapter();
			}
			@Override
			public Adapter caseHLoft(HLoft object) {
				return createHLoftAdapter();
			}
			@Override
			public Adapter caseHLoftElement(HLoftElement object) {
				return createHLoftElementAdapter();
			}
			@Override
			public Adapter caseHLoftPath(HLoftPath object) {
				return createHLoftPathAdapter();
			}
			@Override
			public <A, B> Adapter caseHPair(HPair<A, B> object) {
				return createHPairAdapter();
			}
			@Override
			public Adapter caseHParent(HParent object) {
				return createHParentAdapter();
			}
			@Override
			public Adapter caseHPoint2D(HPoint2D object) {
				return createHPoint2DAdapter();
			}
			@Override
			public Adapter caseHPoint3D(HPoint3D object) {
				return createHPoint3DAdapter();
			}
			@Override
			public Adapter caseHSeriesProfileCurve2D(HSeriesProfileCurve2D object) {
				return createHSeriesProfileCurve2DAdapter();
			}
			@Override
			public Adapter caseHSurfaceIdentifier(HSurfaceIdentifier object) {
				return createHSurfaceIdentifierAdapter();
			}
			@Override
			public <T, T1> Adapter caseMap(Map<T, T1> object) {
				return createMapAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link de.hykops.lpd.datamodel.HComponent <em>HComponent</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hykops.lpd.datamodel.HComponent
	 * @generated
	 */
	public Adapter createHComponentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hykops.lpd.datamodel.HComposition <em>HComposition</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hykops.lpd.datamodel.HComposition
	 * @generated
	 */
	public Adapter createHCompositionAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hykops.lpd.datamodel.HCoordinateSystem3D <em>HCoordinate System3 D</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hykops.lpd.datamodel.HCoordinateSystem3D
	 * @generated
	 */
	public Adapter createHCoordinateSystem3DAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hykops.lpd.datamodel.HCoordinateSystem3DAffine <em>HCoordinate System3 DAffine</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hykops.lpd.datamodel.HCoordinateSystem3DAffine
	 * @generated
	 */
	public Adapter createHCoordinateSystem3DAffineAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hykops.lpd.datamodel.HCoordinateSystem3DCartesian <em>HCoordinate System3 DCartesian</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hykops.lpd.datamodel.HCoordinateSystem3DCartesian
	 * @generated
	 */
	public Adapter createHCoordinateSystem3DCartesianAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hykops.lpd.datamodel.HCoordinateSystem3DCone <em>HCoordinate System3 DCone</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hykops.lpd.datamodel.HCoordinateSystem3DCone
	 * @generated
	 */
	public Adapter createHCoordinateSystem3DConeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hykops.lpd.datamodel.HCoordinateSystem3DPolar <em>HCoordinate System3 DPolar</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hykops.lpd.datamodel.HCoordinateSystem3DPolar
	 * @generated
	 */
	public Adapter createHCoordinateSystem3DPolarAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hykops.lpd.datamodel.HCurve2D <em>HCurve2 D</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hykops.lpd.datamodel.HCurve2D
	 * @generated
	 */
	public Adapter createHCurve2DAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hykops.lpd.datamodel.HCurvePoint2D <em>HCurve Point2 D</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hykops.lpd.datamodel.HCurvePoint2D
	 * @generated
	 */
	public Adapter createHCurvePoint2DAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hykops.lpd.datamodel.HDiscreteCurve2D <em>HDiscrete Curve2 D</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hykops.lpd.datamodel.HDiscreteCurve2D
	 * @generated
	 */
	public Adapter createHDiscreteCurve2DAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hykops.lpd.datamodel.HDiscreteLoftPath <em>HDiscrete Loft Path</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hykops.lpd.datamodel.HDiscreteLoftPath
	 * @generated
	 */
	public Adapter createHDiscreteLoftPathAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hykops.lpd.datamodel.HDiscreteLoftPathPoint3D <em>HDiscrete Loft Path Point3 D</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hykops.lpd.datamodel.HDiscreteLoftPathPoint3D
	 * @generated
	 */
	public Adapter createHDiscreteLoftPathPoint3DAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hykops.lpd.datamodel.HHinge <em>HHinge</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hykops.lpd.datamodel.HHinge
	 * @generated
	 */
	public Adapter createHHingeAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hykops.lpd.datamodel.HLinearLoftPath <em>HLinear Loft Path</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hykops.lpd.datamodel.HLinearLoftPath
	 * @generated
	 */
	public Adapter createHLinearLoftPathAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hykops.lpd.datamodel.HLoft <em>HLoft</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hykops.lpd.datamodel.HLoft
	 * @generated
	 */
	public Adapter createHLoftAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hykops.lpd.datamodel.HLoftElement <em>HLoft Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hykops.lpd.datamodel.HLoftElement
	 * @generated
	 */
	public Adapter createHLoftElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hykops.lpd.datamodel.HLoftPath <em>HLoft Path</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hykops.lpd.datamodel.HLoftPath
	 * @generated
	 */
	public Adapter createHLoftPathAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hykops.lpd.datamodel.HPair <em>HPair</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hykops.lpd.datamodel.HPair
	 * @generated
	 */
	public Adapter createHPairAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hykops.lpd.datamodel.HParent <em>HParent</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hykops.lpd.datamodel.HParent
	 * @generated
	 */
	public Adapter createHParentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hykops.lpd.datamodel.HPoint2D <em>HPoint2 D</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hykops.lpd.datamodel.HPoint2D
	 * @generated
	 */
	public Adapter createHPoint2DAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hykops.lpd.datamodel.HPoint3D <em>HPoint3 D</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hykops.lpd.datamodel.HPoint3D
	 * @generated
	 */
	public Adapter createHPoint3DAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hykops.lpd.datamodel.HSeriesProfileCurve2D <em>HSeries Profile Curve2 D</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hykops.lpd.datamodel.HSeriesProfileCurve2D
	 * @generated
	 */
	public Adapter createHSeriesProfileCurve2DAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link de.hykops.lpd.datamodel.HSurfaceIdentifier <em>HSurface Identifier</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see de.hykops.lpd.datamodel.HSurfaceIdentifier
	 * @generated
	 */
	public Adapter createHSurfaceIdentifierAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link java.util.Map <em>Map</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see java.util.Map
	 * @generated
	 */
	public Adapter createMapAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //DatamodelAdapterFactory
