/**
 */
package de.hykops.lpd.datamodel.util;

import de.hykops.lpd.datamodel.*;

import java.util.Map;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see de.hykops.lpd.datamodel.de.hykops.lpd.datamodel.test.DatamodelPackage
 * @generated
 */
public class DatamodelSwitch<T2> extends Switch<T2> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static DatamodelPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DatamodelSwitch() {
		if (modelPackage == null) {
			modelPackage = DatamodelPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T2 doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case DatamodelPackage.HCOMPONENT: {
				HComponent hComponent = (HComponent)theEObject;
				T2 result = caseHComponent(hComponent);
				if (result == null) result = caseHParent(hComponent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DatamodelPackage.HCOMPOSITION: {
				HComposition hComposition = (HComposition)theEObject;
				T2 result = caseHComposition(hComposition);
				if (result == null) result = caseHParent(hComposition);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DatamodelPackage.HCOORDINATE_SYSTEM3_D: {
				HCoordinateSystem3D hCoordinateSystem3D = (HCoordinateSystem3D)theEObject;
				T2 result = caseHCoordinateSystem3D(hCoordinateSystem3D);
				if (result == null) result = caseHParent(hCoordinateSystem3D);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DatamodelPackage.HCOORDINATE_SYSTEM3_DAFFINE: {
				HCoordinateSystem3DAffine hCoordinateSystem3DAffine = (HCoordinateSystem3DAffine)theEObject;
				T2 result = caseHCoordinateSystem3DAffine(hCoordinateSystem3DAffine);
				if (result == null) result = caseHCoordinateSystem3D(hCoordinateSystem3DAffine);
				if (result == null) result = caseHParent(hCoordinateSystem3DAffine);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DatamodelPackage.HCOORDINATE_SYSTEM3_DCARTESIAN: {
				HCoordinateSystem3DCartesian hCoordinateSystem3DCartesian = (HCoordinateSystem3DCartesian)theEObject;
				T2 result = caseHCoordinateSystem3DCartesian(hCoordinateSystem3DCartesian);
				if (result == null) result = caseHCoordinateSystem3D(hCoordinateSystem3DCartesian);
				if (result == null) result = caseHParent(hCoordinateSystem3DCartesian);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DatamodelPackage.HCOORDINATE_SYSTEM3_DCONE: {
				HCoordinateSystem3DCone hCoordinateSystem3DCone = (HCoordinateSystem3DCone)theEObject;
				T2 result = caseHCoordinateSystem3DCone(hCoordinateSystem3DCone);
				if (result == null) result = caseHCoordinateSystem3D(hCoordinateSystem3DCone);
				if (result == null) result = caseHParent(hCoordinateSystem3DCone);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DatamodelPackage.HCOORDINATE_SYSTEM3_DPOLAR: {
				HCoordinateSystem3DPolar hCoordinateSystem3DPolar = (HCoordinateSystem3DPolar)theEObject;
				T2 result = caseHCoordinateSystem3DPolar(hCoordinateSystem3DPolar);
				if (result == null) result = caseHCoordinateSystem3D(hCoordinateSystem3DPolar);
				if (result == null) result = caseHParent(hCoordinateSystem3DPolar);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DatamodelPackage.HCURVE2_D: {
				HCurve2D hCurve2D = (HCurve2D)theEObject;
				T2 result = caseHCurve2D(hCurve2D);
				if (result == null) result = caseHParent(hCurve2D);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DatamodelPackage.HCURVE_POINT2_D: {
				HCurvePoint2D hCurvePoint2D = (HCurvePoint2D)theEObject;
				T2 result = caseHCurvePoint2D(hCurvePoint2D);
				if (result == null) result = caseHPoint2D(hCurvePoint2D);
				if (result == null) result = caseHParent(hCurvePoint2D);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DatamodelPackage.HDISCRETE_CURVE2_D: {
				HDiscreteCurve2D hDiscreteCurve2D = (HDiscreteCurve2D)theEObject;
				T2 result = caseHDiscreteCurve2D(hDiscreteCurve2D);
				if (result == null) result = caseHCurve2D(hDiscreteCurve2D);
				if (result == null) result = caseHParent(hDiscreteCurve2D);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DatamodelPackage.HDISCRETE_LOFT_PATH: {
				HDiscreteLoftPath hDiscreteLoftPath = (HDiscreteLoftPath)theEObject;
				T2 result = caseHDiscreteLoftPath(hDiscreteLoftPath);
				if (result == null) result = caseHLoftPath(hDiscreteLoftPath);
				if (result == null) result = caseHParent(hDiscreteLoftPath);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DatamodelPackage.HDISCRETE_LOFT_PATH_POINT3_D: {
				HDiscreteLoftPathPoint3D hDiscreteLoftPathPoint3D = (HDiscreteLoftPathPoint3D)theEObject;
				T2 result = caseHDiscreteLoftPathPoint3D(hDiscreteLoftPathPoint3D);
				if (result == null) result = caseHPoint3D(hDiscreteLoftPathPoint3D);
				if (result == null) result = caseHParent(hDiscreteLoftPathPoint3D);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DatamodelPackage.HHINGE: {
				HHinge hHinge = (HHinge)theEObject;
				T2 result = caseHHinge(hHinge);
				if (result == null) result = caseHParent(hHinge);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DatamodelPackage.HLINEAR_LOFT_PATH: {
				HLinearLoftPath hLinearLoftPath = (HLinearLoftPath)theEObject;
				T2 result = caseHLinearLoftPath(hLinearLoftPath);
				if (result == null) result = caseHLoftPath(hLinearLoftPath);
				if (result == null) result = caseHParent(hLinearLoftPath);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DatamodelPackage.HLOFT: {
				HLoft hLoft = (HLoft)theEObject;
				T2 result = caseHLoft(hLoft);
				if (result == null) result = caseHParent(hLoft);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DatamodelPackage.HLOFT_ELEMENT: {
				HLoftElement hLoftElement = (HLoftElement)theEObject;
				T2 result = caseHLoftElement(hLoftElement);
				if (result == null) result = caseHParent(hLoftElement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DatamodelPackage.HLOFT_PATH: {
				HLoftPath hLoftPath = (HLoftPath)theEObject;
				T2 result = caseHLoftPath(hLoftPath);
				if (result == null) result = caseHParent(hLoftPath);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DatamodelPackage.HPAIR: {
				HPair<?, ?> hPair = (HPair<?, ?>)theEObject;
				T2 result = caseHPair(hPair);
				if (result == null) result = caseHParent(hPair);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DatamodelPackage.HPARENT: {
				HParent hParent = (HParent)theEObject;
				T2 result = caseHParent(hParent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DatamodelPackage.HPOINT2_D: {
				HPoint2D hPoint2D = (HPoint2D)theEObject;
				T2 result = caseHPoint2D(hPoint2D);
				if (result == null) result = caseHParent(hPoint2D);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DatamodelPackage.HPOINT3_D: {
				HPoint3D hPoint3D = (HPoint3D)theEObject;
				T2 result = caseHPoint3D(hPoint3D);
				if (result == null) result = caseHParent(hPoint3D);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DatamodelPackage.HSERIES_PROFILE_CURVE2_D: {
				HSeriesProfileCurve2D hSeriesProfileCurve2D = (HSeriesProfileCurve2D)theEObject;
				T2 result = caseHSeriesProfileCurve2D(hSeriesProfileCurve2D);
				if (result == null) result = caseHCurve2D(hSeriesProfileCurve2D);
				if (result == null) result = caseHParent(hSeriesProfileCurve2D);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DatamodelPackage.HSURFACE_IDENTIFIER: {
				HSurfaceIdentifier hSurfaceIdentifier = (HSurfaceIdentifier)theEObject;
				T2 result = caseHSurfaceIdentifier(hSurfaceIdentifier);
				if (result == null) result = caseHParent(hSurfaceIdentifier);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DatamodelPackage.MAP: {
				Map<?, ?> map = (Map<?, ?>)theEObject;
				T2 result = caseMap(map);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>HComponent</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>HComponent</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T2 caseHComponent(HComponent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>HComposition</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>HComposition</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T2 caseHComposition(HComposition object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>HCoordinate System3 D</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>HCoordinate System3 D</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T2 caseHCoordinateSystem3D(HCoordinateSystem3D object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>HCoordinate System3 DAffine</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>HCoordinate System3 DAffine</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T2 caseHCoordinateSystem3DAffine(HCoordinateSystem3DAffine object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>HCoordinate System3 DCartesian</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>HCoordinate System3 DCartesian</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T2 caseHCoordinateSystem3DCartesian(HCoordinateSystem3DCartesian object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>HCoordinate System3 DCone</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>HCoordinate System3 DCone</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T2 caseHCoordinateSystem3DCone(HCoordinateSystem3DCone object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>HCoordinate System3 DPolar</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>HCoordinate System3 DPolar</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T2 caseHCoordinateSystem3DPolar(HCoordinateSystem3DPolar object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>HCurve2 D</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>HCurve2 D</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T2 caseHCurve2D(HCurve2D object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>HCurve Point2 D</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>HCurve Point2 D</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T2 caseHCurvePoint2D(HCurvePoint2D object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>HDiscrete Curve2 D</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>HDiscrete Curve2 D</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T2 caseHDiscreteCurve2D(HDiscreteCurve2D object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>HDiscrete Loft Path</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>HDiscrete Loft Path</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T2 caseHDiscreteLoftPath(HDiscreteLoftPath object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>HDiscrete Loft Path Point3 D</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>HDiscrete Loft Path Point3 D</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T2 caseHDiscreteLoftPathPoint3D(HDiscreteLoftPathPoint3D object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>HHinge</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>HHinge</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T2 caseHHinge(HHinge object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>HLinear Loft Path</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>HLinear Loft Path</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T2 caseHLinearLoftPath(HLinearLoftPath object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>HLoft</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>HLoft</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T2 caseHLoft(HLoft object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>HLoft Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>HLoft Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T2 caseHLoftElement(HLoftElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>HLoft Path</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>HLoft Path</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T2 caseHLoftPath(HLoftPath object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>HPair</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>HPair</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public <A, B> T2 caseHPair(HPair<A, B> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>HParent</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>HParent</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T2 caseHParent(HParent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>HPoint2 D</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>HPoint2 D</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T2 caseHPoint2D(HPoint2D object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>HPoint3 D</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>HPoint3 D</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T2 caseHPoint3D(HPoint3D object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>HSeries Profile Curve2 D</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>HSeries Profile Curve2 D</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T2 caseHSeriesProfileCurve2D(HSeriesProfileCurve2D object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>HSurface Identifier</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>HSurface Identifier</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T2 caseHSurfaceIdentifier(HSurfaceIdentifier object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Map</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Map</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public <T, T1> T2 caseMap(Map<T, T1> object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T2 defaultCase(EObject object) {
		return null;
	}

} //DatamodelSwitch
