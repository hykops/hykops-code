/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.lpd.datamodel;

import org.eclipse.emf.ecore.EObject;

/**
 * 
 * @model abstract = "true"
 * @author stoye
 * @since Jun 29, 2016
 */
public interface HParent extends EObject, Handler {
	
	static final String PROPERTY_HPARENT = "hParent";
	
	/**
	 * @model
	 * @return
	 */
	public HParent getHParent();

	/**
	 * Sets the value of the '{@link de.hykops.lpd.datamodel.HParent#getHParent <em>HParent</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>HParent</em>' reference.
	 * @see #getHParent()
	 * @generated
	 */
	void setHParent(HParent value);
}
