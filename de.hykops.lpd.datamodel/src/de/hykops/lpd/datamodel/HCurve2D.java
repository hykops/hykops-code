package de.hykops.lpd.datamodel;

import org.eclipse.emf.common.util.EList;

/**
 * @model abstract = "true"
 * @author Thomas Stoye
 *
 */
public interface HCurve2D extends HParent {
	
	static final String CLASS_NAME = HCurve2D.class.getSimpleName();

	static final String PROPERTY_SURFACE_IDENTIFIERS = "hSurfaceIdentifiers";
	static final String PROPERTY_LOFT_ELEMENT = "hLoftElement";
	
	/**
	 * @model
	 * @return all surfaces this curves is part of (usually one)
	 */
	public EList<HSurfaceIdentifier> getHSurfaceIdentifiers();
	
	/**
	 * @model opposite = "hCurve2Ds"
	 */
	public HLoftElement getHLoftElement();

	/**
	 * Sets the value of the '{@link de.hykops.lpd.datamodel.HCurve2D#getHLoftElement <em>HLoft Element</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>HLoft Element</em>' container reference.
	 * @see #getHLoftElement()
	 * @generated
	 */
	void setHLoftElement(HLoftElement value);

}
