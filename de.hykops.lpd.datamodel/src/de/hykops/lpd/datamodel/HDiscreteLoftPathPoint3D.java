/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.lpd.datamodel;

/**
 * 
 * @model
 * @author stoye
 * @since Jul 4, 2016
 */
public interface HDiscreteLoftPathPoint3D extends HPoint3D {
	
	static final String CLASS_NAME = HDiscreteLoftPathPoint3D.class.getSimpleName();

	static final String PROPERTY_LOFT_CURVE_POINT = "loftCurvePoint";
	static final String PROPERTY_LOFT_COORDINATE = "loftCoordinate";
	static final String PROPERTY_LOFT_PATH_VECTOR = "loftPathVector";
	static final String PROPERTY_LOFT_PATH_XI = "loftPathXi";
	static final String PROPERTY_LOFT_PATH_ETA = "loftPathEta";
	
	/**
	 * @model containment = "true"
	 * @return a point on the loft curve
	 *
	 * @author stoye
	 * @since Jul 4, 2016
	 */	
	public HPoint3D getLoftCurvePoint();

	/**
	 * Sets the value of the '{@link de.hykops.lpd.datamodel.HDiscreteLoftPathPoint3D#getLoftCurvePoint <em>Loft Curve Point</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Loft Curve Point</em>' containment reference.
	 * @see #getLoftCurvePoint()
	 * @generated
	 */
	void setLoftCurvePoint(HPoint3D value);

	/**
	 * @model
	 * @return the loft coordinate
	 *
	 * @author stoye
	 * @since Jul 4, 2016
	 */
	public Double getLoftCoordinate();

	/**
	 * Sets the value of the '{@link de.hykops.lpd.datamodel.HDiscreteLoftPathPoint3D#getLoftCoordinate <em>Loft Coordinate</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Loft Coordinate</em>' attribute.
	 * @see #getLoftCoordinate()
	 * @generated
	 */
	void setLoftCoordinate(Double value);

	/**
	 * @model containment = "true"
	 * @return the direction of the loft path
	 */
	public HPoint3D getLoftPathVector();
	/**
	 * Sets the value of the '{@link de.hykops.lpd.datamodel.HDiscreteLoftPathPoint3D#getLoftPathVector <em>Loft Path Vector</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Loft Path Vector</em>' containment reference.
	 * @see #getLoftPathVector()
	 * @generated
	 */
	void setLoftPathVector(HPoint3D value);

	/**
	 * 
	 * @model containment = "true"
	 * @return the 1st coordinate axis that is perpendicular to the loft path
	 *
	 * @author stoye
	 * @since Jul 4, 2016
	 */
	public HPoint3D getLoftPathXi();
	/**
	 * Sets the value of the '{@link de.hykops.lpd.datamodel.HDiscreteLoftPathPoint3D#getLoftPathXi <em>Loft Path Xi</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Loft Path Xi</em>' containment reference.
	 * @see #getLoftPathXi()
	 * @generated
	 */
	void setLoftPathXi(HPoint3D value);

	/**
	 * @model containment = "true"
	 * @return the 2nd coordinate that is perpendicular to the loft path
	 *
	 * @author stoye
	 * @since Jul 4, 2016
	 */
	public HPoint3D getLoftPathEta();

	/**
	 * Sets the value of the '{@link de.hykops.lpd.datamodel.HDiscreteLoftPathPoint3D#getLoftPathEta <em>Loft Path Eta</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Loft Path Eta</em>' containment reference.
	 * @see #getLoftPathEta()
	 * @generated
	 */
	void setLoftPathEta(HPoint3D value);
}
