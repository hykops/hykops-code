package de.hykops.lpd.datamodel;

import org.eclipse.emf.common.util.EList;

/**
 * @model
 * @author Thomas Stoye
 *
 */
public interface HLoftElement extends HParent {
	
	static final String CLASS_NAME = HLoftElement.class.getSimpleName();

	static final String PROPERTY_LOFT_PLANE_DEFINING_COORDINATE_SYSTEM = "loftPlaneDefiningCoordinateSystem";
	static final String PROPERTY_CURVES_2D = "hCurve2Ds";
	static final String PROPERTY_LOFT = "hLoft";
	
	/**
	 * @model containment = "true"
	 * @return the coordinate system defining the 2D-Plane for the loft element.
	 *         The 1st axis of the 3D-System is the first coordinate in the
	 *         2D-System, the second coordinate is the 2nd in the 2D-system and
	 *         the third coordinate is (perpendicular) to the 2D-plane.
	 */
	public HCoordinateSystem3D getLoftPlaneDefiningCoordinateSystem();

	/**
	 * Sets the value of the '{@link de.hykops.lpd.datamodel.HLoftElement#getLoftPlaneDefiningCoordinateSystem <em>Loft Plane Defining Coordinate System</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Loft Plane Defining Coordinate System</em>' containment reference.
	 * @see #getLoftPlaneDefiningCoordinateSystem()
	 * @generated
	 */
	void setLoftPlaneDefiningCoordinateSystem(HCoordinateSystem3D value);

	/**
	 * @model containment = "true" opposite = "hLoftElement"
	 * @return all curves defined in the present loft element
	 */
	public EList<HCurve2D> getHCurve2Ds();

	/**
	 * @model opposite = "hLoftElements"
	 */
	public HLoft getHLoft();

	/**
	 * Sets the value of the '{@link de.hykops.lpd.datamodel.HLoftElement#getHLoft <em>HLoft</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>HLoft</em>' container reference.
	 * @see #getHLoft()
	 * @generated
	 */
	void setHLoft(HLoft value);
}
