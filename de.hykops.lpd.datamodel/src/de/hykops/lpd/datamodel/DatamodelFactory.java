/**
 */
package de.hykops.lpd.datamodel;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see de.hykops.lpd.datamodel.de.hykops.lpd.datamodel.test.DatamodelPackage
 * @generated
 */
public interface DatamodelFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	DatamodelFactory eINSTANCE = Activator.getEmfService().getEMFDataModelFactory();

	/**
	 * Returns a new object of class '<em>HComponent</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>HComponent</em>'.
	 * @generated
	 */
	HComponent createHComponent();

	/**
	 * Returns a new object of class '<em>HComposition</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>HComposition</em>'.
	 * @generated
	 */
	HComposition createHComposition();

	/**
	 * Returns a new object of class '<em>HCoordinate System3 DAffine</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>HCoordinate System3 DAffine</em>'.
	 * @generated
	 */
	HCoordinateSystem3DAffine createHCoordinateSystem3DAffine();

	/**
	 * Returns a new object of class '<em>HCoordinate System3 DCartesian</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>HCoordinate System3 DCartesian</em>'.
	 * @generated
	 */
	HCoordinateSystem3DCartesian createHCoordinateSystem3DCartesian();

	/**
	 * Returns a new object of class '<em>HCoordinate System3 DCone</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>HCoordinate System3 DCone</em>'.
	 * @generated
	 */
	HCoordinateSystem3DCone createHCoordinateSystem3DCone();

	/**
	 * Returns a new object of class '<em>HCoordinate System3 DPolar</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>HCoordinate System3 DPolar</em>'.
	 * @generated
	 */
	HCoordinateSystem3DPolar createHCoordinateSystem3DPolar();

	/**
	 * Returns a new object of class '<em>HCurve Point2 D</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>HCurve Point2 D</em>'.
	 * @generated
	 */
	HCurvePoint2D createHCurvePoint2D();

	/**
	 * Returns a new object of class '<em>HDiscrete Curve2 D</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>HDiscrete Curve2 D</em>'.
	 * @generated
	 */
	HDiscreteCurve2D createHDiscreteCurve2D();

	/**
	 * Returns a new object of class '<em>HDiscrete Loft Path</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>HDiscrete Loft Path</em>'.
	 * @generated
	 */
	HDiscreteLoftPath createHDiscreteLoftPath();

	/**
	 * Returns a new object of class '<em>HDiscrete Loft Path Point3 D</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>HDiscrete Loft Path Point3 D</em>'.
	 * @generated
	 */
	HDiscreteLoftPathPoint3D createHDiscreteLoftPathPoint3D();

	/**
	 * Returns a new object of class '<em>HHinge</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>HHinge</em>'.
	 * @generated
	 */
	HHinge createHHinge();

	/**
	 * Returns a new object of class '<em>HLinear Loft Path</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>HLinear Loft Path</em>'.
	 * @generated
	 */
	HLinearLoftPath createHLinearLoftPath();

	/**
	 * Returns a new object of class '<em>HLoft</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>HLoft</em>'.
	 * @generated
	 */
	HLoft createHLoft();

	/**
	 * Returns a new object of class '<em>HLoft Element</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>HLoft Element</em>'.
	 * @generated
	 */
	HLoftElement createHLoftElement();

	/**
	 * Returns a new object of class '<em>HLoft Path</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>HLoft Path</em>'.
	 * @generated
	 */
	HLoftPath createHLoftPath();

	/**
	 * Returns a new object of class '<em>HPair</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>HPair</em>'.
	 * @generated
	 */
	<A, B> HPair<A, B> createHPair();

	/**
	 * Returns a new object of class '<em>HPoint2 D</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>HPoint2 D</em>'.
	 * @generated
	 */
	HPoint2D createHPoint2D();

	/**
	 * Returns a new object of class '<em>HPoint3 D</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>HPoint3 D</em>'.
	 * @generated
	 */
	HPoint3D createHPoint3D();

	/**
	 * Returns a new object of class '<em>HSeries Profile Curve2 D</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>HSeries Profile Curve2 D</em>'.
	 * @generated
	 */
	HSeriesProfileCurve2D createHSeriesProfileCurve2D();

	/**
	 * Returns a new object of class '<em>HSurface Identifier</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>HSurface Identifier</em>'.
	 * @generated
	 */
	HSurfaceIdentifier createHSurfaceIdentifier();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	DatamodelPackage getDatamodelPackage();

} //DatamodelFactory
