package de.hykops.lpd.datamodel;

import org.eclipse.emf.common.util.EList;

/**
 * @model opposite = "true" abstract="true"
 * @author Thomas Stoye
 *
 */
public interface HCoordinateSystem3D extends HParent {
	
	public static final String PROPERTY_ORIGIN = "origin";
	public static final String PROPERTY_HINGES = "hHinges";

	/**
	 * @model containment = "true"
	 * @return the origin of the current coordinate system
	 */
	public HPoint3D getOrigin();

	/**
	 * Sets the value of the '{@link de.hykops.lpd.datamodel.HCoordinateSystem3D#getOrigin <em>Origin</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Origin</em>' containment reference.
	 * @see #getOrigin()
	 * @generated
	 */
	void setOrigin(HPoint3D value);

	/**
	 * @model containment = "true" opposite = "hCoordinateSystem3D"
	 * @return all hinges of the current lofted element. The rotation of the
	 *         hinges is applied after all coordinates transformation in
	 *         cartesian coordinates
	 */
	public EList<HHinge> getHHinges();
}
