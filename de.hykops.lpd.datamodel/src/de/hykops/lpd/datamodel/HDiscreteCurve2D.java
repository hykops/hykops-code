package de.hykops.lpd.datamodel;

import org.eclipse.emf.common.util.EList;

/**
 * @model
 * @author Thomas Stoye
 *
 */
public interface HDiscreteCurve2D extends HCurve2D {

	static final String CLASS_NAME = HDiscreteCurve2D.class.getSimpleName();

	static final String PROPERTY_INTERPOLATION_SCHEME = "interpolationScheme";
	static final String PROPERTY_CURVE_POINTS = "curvePoints";
	
	/**
	 * @model
	 * @return the curve interpolation
	 */
	public String getInterpolationScheme();

	/**
	 * Sets the value of the '{@link de.hykops.lpd.datamodel.HDiscreteCurve2D#getInterpolationScheme <em>Interpolation Scheme</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Interpolation Scheme</em>' attribute.
	 * @see #getInterpolationScheme()
	 * @generated
	 */
	void setInterpolationScheme(String value);

	/**
	 * @model containment = "true"
	 * @return all points defining the curve. The 1st element is the coordinate
	 *         value along the curve, the 2nd element are the point coordinates
	 *         in the 2D-plane.
	 */
	public EList<HCurvePoint2D> getCurvePoints();

}
