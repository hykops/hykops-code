package de.hykops.lpd.datamodel;

import org.eclipse.emf.common.util.EList;

/**
 * @model
 * @author Thomas Stoye
 *
 */
public interface HHinge extends HParent {

	static final String CLASS_NAME = HHinge.class.getSimpleName();

	static final String PROPERTY_HINGE_AXIS = "hingeAxis";
	static final String PROPERTY_HINGE_LIMITED = "hingeLimited";
	static final String PROPERTY_HINGE_POSITIONS = "hingePositions";
	static final String PROPERTY_COORDINATE_SYSTEM = "hCoordinateSystem3D";
	
	/**
	 * @model containment = "true"
	 * @return the axis defining the hinge
	 */
	public HPoint3D getHingeAxis();

	/**
	 * Sets the value of the '{@link de.hykops.lpd.datamodel.HHinge#getHingeAxis <em>Hinge Axis</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hinge Axis</em>' containment reference.
	 * @see #getHingeAxis()
	 * @generated
	 */
	void setHingeAxis(HPoint3D value);

	/**
	 * @model containment = "true"
	 * @return defined hinge positions, e.g. "MIN", "MAX", "DESIGN", angle in
	 *         radians
	 */
	public EList<HPair<String, Double>> getHingePositions();

	/**
	 * @model
	 * @return whether the hinge is unlimited (e.g. propeller shaft) or limited
	 *         (e.g. rudder angle)
	 */
	public Boolean isHingeLimited();

	/**
	 * Sets the value of the '{@link de.hykops.lpd.datamodel.HHinge#isHingeLimited <em>Hinge Limited</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Hinge Limited</em>' attribute.
	 * @see #isHingeLimited()
	 * @generated
	 */
	void setHingeLimited(Boolean value);

	/**
	 * @model opposite = "hHinges"
	 */
	public HCoordinateSystem3D getHCoordinateSystem3D();

	/**
	 * Sets the value of the '{@link de.hykops.lpd.datamodel.HHinge#getHCoordinateSystem3D <em>HCoordinate System3 D</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>HCoordinate System3 D</em>' container reference.
	 * @see #getHCoordinateSystem3D()
	 * @generated
	 */
	void setHCoordinateSystem3D(HCoordinateSystem3D value);
}
