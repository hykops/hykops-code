package de.hykops.lpd.datamodel;

/**
 * @model
 * @author Thomas Stoye
 *
 */
public interface HSeriesProfileCurve2D extends HCurve2D {
	
	static final String CLASS_NAME = HSeriesProfileCurve2D.class.getSimpleName();

	static final String PROPERTY_SERIES = "series";
	static final String PROPERTY_DENOMINATION = "denomination";
	static final String PROPERTY_SIDE = "side";


	/**
	 * @model
	 * @return the series profile denomination
	 */
	public String getDenomination();

	/**
	 * Sets the value of the '{@link de.hykops.lpd.datamodel.HSeriesProfileCurve2D#getDenomination <em>Denomination</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Denomination</em>' attribute.
	 * @see #getDenomination()
	 * @generated
	 */
	void setDenomination(String value);

	/**
	 * @model
	 * @return e.g. whether the curve defines the suction- or pressure side of the
	 *         profile
	 */
	public String getSide();

	/**
	 * Sets the value of the '{@link de.hykops.lpd.datamodel.HSeriesProfileCurve2D#getSide <em>Side</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Side</em>' attribute.
	 * @see #getSide()
	 * @generated
	 */
	void setSide(String value);

	/**
	 * @model
	 * @return the profile series name, e.g. "NACA 1-series", "NACA 4-digit"
	 */
	public String getSeries();
	
	/**
	 * Sets the value of the '{@link de.hykops.lpd.datamodel.HSeriesProfileCurve2D#getSeries <em>Series</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Denomination</em>' attribute.
	 * @see #getSeries()
	 * @generated
	 */
	void setSeries(String value);

}
