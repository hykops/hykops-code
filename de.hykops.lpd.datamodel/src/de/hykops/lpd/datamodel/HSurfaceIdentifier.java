package de.hykops.lpd.datamodel;

/**
 * @model
 * @author Thomas Stoye
 *
 */
public interface HSurfaceIdentifier extends HParent {
	
	static final String CLASS_NAME = HSurfaceIdentifier.class.getSimpleName();

	static final String PROPERTY_NAME = "name";
	
	/**
	 * @model
	 * @return a name of the surface
	 */
	public String getName();

	/**
	 * Sets the value of the '{@link de.hykops.lpd.datamodel.HSurfaceIdentifier#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);
}
