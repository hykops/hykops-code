package de.hykops.lpd.datamodel;

import org.eclipse.emf.common.util.EList;

/**
 * @model
 * @author Thomas Stoye
 *
 */
public interface HDiscreteLoftPath extends HLoftPath {
	
	static final String CLASS_NAME = HDiscreteLoftPath.class.getSimpleName();

	static final String PROPERTY_LOFT_CURVE_POINTS = "loftCurvePoints";
	static final String PROPERTY_INTERPOLATIONS_SCHEME = "interpolationScheme";
	
	/**
	 * @model containment = "true"
	 * @return the points defining the nonlinear lofted axis. The first
	 *         component is the coordinate value of along the axis, the second
	 *         component is the coordinate of the point defining the lofted axis
	 *         in the current coordinate system.
	 */
	public EList<HDiscreteLoftPathPoint3D> getLoftCurvePoints();

	/**
	 * @model
	 * @return the interpolation scheme along the axis, e.g. linear or cubic
	 *         spline
	 */
	public String getInterpolationScheme();

	/**
	 * Sets the value of the '{@link de.hykops.lpd.datamodel.HDiscreteLoftPath#getInterpolationScheme <em>Interpolation Scheme</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Interpolation Scheme</em>' attribute.
	 * @see #getInterpolationScheme()
	 * @generated
	 */
	void setInterpolationScheme(String value);

}
