package de.hykops.lpd.datamodel;

/**
 * @model
 * @author Thomas Stoye
 *
 */
public interface HCoordinateSystem3DCartesian extends HCoordinateSystem3D {

	static final String CLASS_NAME = HCoordinateSystem3DCartesian.class.getSimpleName();

	static final String PROPERTY_X1 = "x1";
	static final String PROPERTY_E1 = "e1";

	/**
	 * @model containment = "true"
	 * @return the 1st coordinate axis vector
	 */
	public HPoint3D getX1();

	/**
	 * Sets the value of the '{@link de.hykops.lpd.datamodel.HCoordinateSystem3DCartesian#getX1 <em>X1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>X1</em>' containment reference.
	 * @see #getX1()
	 * @generated
	 */
	void setX1(HPoint3D value);

	/**
	 * @model containment = "true"
	 * @return the euler rotation anglea
	 */
	public HPoint3D getE1();

	/**
	 * Sets the value of the '{@link de.hykops.lpd.datamodel.HCoordinateSystem3DCartesian#getE1 <em>E1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>E1</em>' containment reference.
	 * @see #getE1()
	 * @generated
	 */
	void setE1(HPoint3D value);

}
