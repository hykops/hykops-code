package de.hykops.lpd.datamodel;

import org.eclipse.emf.common.util.EList;

/**
 * @author Thomas Stoye
 * @model
 */
public interface HComposition extends HParent {
	
	static final String CLASS_NAME = HComposition.class.getSimpleName();
	static final String PROPERTY_NAME = "name";
	static final String PROPERTY_DESCRIPTION = "description";
	static final String PROPERTY_VERSION = "version";
	static final String PROPERTY_COORDINATESYSTEM = "hCoordinateSystem3D";
	static final String PROPERTY_UNITSYSTEM = "unitSystem";
	static final String PROPERTY_COMPONENTS = "hComponents";
	
	
	/**
	 * @model
	 * @return the Version of the datamodel
	 */
	public Integer getVersion();

	/**
	 * Sets the value of the '{@link de.hykops.lpd.datamodel.HComposition#getVersion <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Version</em>' attribute.
	 * @see #getVersion()
	 * @generated
	 */
	void setVersion(Integer value);

	/**
	 * @model
	 * @return the name of the composition
	 */
	public String getName();

	/**
	 * Sets the value of the '{@link de.hykops.lpd.datamodel.HComposition#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * @model
	 * @return a description of the composition
	 */
	public String getDescription();

	/**
	 * Sets the value of the '{@link de.hykops.lpd.datamodel.HComposition#getDescription <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Description</em>' attribute.
	 * @see #getDescription()
	 * @generated
	 */
	void setDescription(String value);

	/**
	 * @model
	 * @return the unit system
	 */
	public String getUnitSystem();

	/**
	 * Sets the value of the '{@link de.hykops.lpd.datamodel.HComposition#getUnitSystem <em>Unit System</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unit System</em>' attribute.
	 * @see #getUnitSystem()
	 * @generated
	 */
	void setUnitSystem(String value);

	/**
	 * @model containment = "true"
	 * @return the global coordinate system of the composition
	 */
	public HCoordinateSystem3D getHCoordinateSystem3D();

	/**
	 * Sets the value of the '{@link de.hykops.lpd.datamodel.HComposition#getHCoordinateSystem3D <em>HCoordinate System3 D</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>HCoordinate System3 D</em>' containment reference.
	 * @see #getHCoordinateSystem3D()
	 * @generated
	 */
	void setHCoordinateSystem3D(HCoordinateSystem3D value);

	/**
	 * @model containment = "true" opposite = "hComposition"
	 * @return all components of the composition
	 */
	public EList<HComponent> getHComponents();
}
