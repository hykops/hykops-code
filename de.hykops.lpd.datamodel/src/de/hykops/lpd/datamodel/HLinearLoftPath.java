package de.hykops.lpd.datamodel;

/**
 * @model
 * @author Thomas Stoye
 *
 */
public interface HLinearLoftPath extends HLoftPath {
	
	static final String CLASS_NAME = HLinearLoftPath.class.getSimpleName();

	static final String PROPERTY_ORIGIN = "origin";
	static final String PROPERTY_LOFT_PATH_VECTOR = "loftPathVector";
	static final String PROPERTY_LOFT_PATH_XI = "loftPathXi";
	static final String PROPERTY_LOFT_PATH_ETA = "loftPathEta";

	/**
	 * @model containment = "true"
	 * @return the origin of the lofted axis
	 */
	public HPoint3D getOrigin();

	/**
	 * Sets the value of the '{@link de.hykops.lpd.datamodel.HLinearLoftPath#getOrigin <em>Origin</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Origin</em>' containment reference.
	 * @see #getOrigin()
	 * @generated
	 */
	void setOrigin(HPoint3D value);

	/**
	 * @model containment = "true"
	 * @return the direction of the loft path
	 */
	public HPoint3D getLoftPathVector();
	
	/**
	 * Sets the value of the '{@link de.hykops.lpd.datamodel.HLinearLoftPath#getLoftPathVector <em>Loft Path Vector</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Loft Path Vector</em>' containment reference.
	 * @see #getLoftPathVector()
	 * @generated
	 */
	void setLoftPathVector(HPoint3D value);

	/**
	 * 
	 * @model containment = "true"
	 * @return the 1st coordinate axis that is perpendicular to the loft path
	 *
	 * @author stoye
	 * @since Jul 4, 2016
	 */
	public HPoint3D getLoftPathXi();
	
	/**
	 * Sets the value of the '{@link de.hykops.lpd.datamodel.HLinearLoftPath#getLoftPathXi <em>Loft Path Xi</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Loft Path Xi</em>' containment reference.
	 * @see #getLoftPathXi()
	 * @generated
	 */
	void setLoftPathXi(HPoint3D value);

	/**
	 * @model containment = "true"
	 * @return the 2nd coordinate that is perpendicular to the loft path
	 *
	 * @author stoye
	 * @since Jul 4, 2016
	 */
	public HPoint3D getLoftPathEta();

	/**
	 * Sets the value of the '{@link de.hykops.lpd.datamodel.HLinearLoftPath#getLoftPathEta <em>Loft Path Eta</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Loft Path Eta</em>' containment reference.
	 * @see #getLoftPathEta()
	 * @generated
	 */
	void setLoftPathEta(HPoint3D value);

}
