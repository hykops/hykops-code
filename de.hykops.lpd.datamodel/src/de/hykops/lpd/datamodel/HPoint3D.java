package de.hykops.lpd.datamodel;

/**
 * @model
 * @author Thomas Stoye
 *
 */
public interface HPoint3D extends HParent {
	
	static final String CLASS_NAME = HPoint3D.class.getSimpleName();

	static final String PROPERTY_X1 = "x1";
	static final String PROPERTY_X2 = "x2";
	static final String PROPERTY_X3 = "x3";
	
	/**
	 * @model
	 * @return the 1st coordinate value
	 */
	public Double getX1();

	/**
	 * Sets the value of the '{@link de.hykops.lpd.datamodel.HPoint3D#getX1 <em>X1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>X1</em>' attribute.
	 * @see #getX1()
	 * @generated
	 */
	void setX1(Double value);

	/**
	 * @model
	 * @return the 2nd coordinate
	 */
	public Double getX2();

	/**
	 * Sets the value of the '{@link de.hykops.lpd.datamodel.HPoint3D#getX2 <em>X2</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>X2</em>' attribute.
	 * @see #getX2()
	 * @generated
	 */
	void setX2(Double value);

	/**
	 * @model
	 * @return the 3rd coordinate
	 */
	public Double getX3();

	/**
	 * Sets the value of the '{@link de.hykops.lpd.datamodel.HPoint3D#getX3 <em>X3</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>X3</em>' attribute.
	 * @see #getX3()
	 * @generated
	 */
	void setX3(Double value);

}
