#ifndef DOMREADER_H
#define DOMREADER_H

#include <QDomDocument>
#include <QString>
#include <QList>
#include <QObject>
#include <QMultiMap>
#include <QFile>
#include "HParent.h"

class ObjectReference
{
public:
    QObject* parentObj;
    QString attribName;
    QString dataType;
    QString xpath;
    ObjectReference(QObject* o, const QString& n, const QString& t, const QString& x)
        : parentObj(o), attribName(n), dataType(t), xpath(x) {;}
    bool setChildObject(QObject *obj) const;
};

class DomReader : public QDomDocument
{
    void objectRecursion(const QDomElement &elem, QObject *parentObj);

    QObject* rootObj;
    QList<ObjectReference> refList;

public:
    DomReader(const QString& filename);
    DomReader(QFile& file);

    QObject* toplevelObject();

    void setDataAttributes(QObject* obj, const QDomElement& elem);
    QObject* objectByXpath(const QString& xpath);

    void setParent(HParent* parent, HParent *child);

    /// remove namespace part from XML attribute or tag
    static QString className(const QString& xmlName);
    static void setObjectDefaults(QObject* obj);
};

#endif // DOMREADER_H
