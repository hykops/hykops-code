#ifndef OBJECTPROPERTY_H
#define OBJECTPROPERTY_H

#include <QObject>
#include <QMetaProperty>
#include <QString>

class ObjectProperty
{
public:
    const QObject* parentObj;
    QString varName;
    bool isValid;
    bool isList;
    QMetaProperty mp;
    QString className;
    int index;  // in case of list
    ObjectProperty(const QObject* parentObj_, const QString& varName_);
    QObject* childObj() const;
};

#endif // OBJECTPROPERTY_H
