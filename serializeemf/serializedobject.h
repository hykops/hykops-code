#ifndef SERIALIZEDOBJECT_H
#define SERIALIZEDOBJECT_H

#include <QObject>
#include <QString>

class SerializedObject : public QObject
{
public:
    SerializedObject(QString str);
    QString getString() { return this->str; }
private:
    QString str;
};

#endif // SERIALIZEDOBJECT_H
