#include <QRegExp>
#include <QDebug>
#include "objectproperty.h"
#include "EFactory.h"
#include "debug.h"

ObjectProperty::ObjectProperty(const QObject *parentObj_, const QString &varName_)
    : parentObj(parentObj_), varName(varName_), isValid(false), isList(false), index(0)
{
    if (varName.isEmpty()) return;

    QRegExp reListVal("@(.*)\\.(\\d+)");

    const QMetaObject* mobj = parentObj->metaObject();

    int inx = -1;
    if (reListVal.exactMatch(varName))
    {
        inx = mobj->indexOfProperty(reListVal.cap(1).toUtf8().data());
        isList = true;
        index = reListVal.cap(2).toInt();
    }
    else
    {
        isList = false;
        QString propName = varName;
        if (propName.at(0)=='@') propName.remove(0,1);
        inx = mobj->indexOfProperty(propName.toUtf8().data());
    }

    if (inx < 0) {
        QString s;
        for (int i=0; i<mobj->propertyCount(); i++)
        {
            s += QString( " <%1>").arg(mobj->property(i).name());
        }
        if(Debug::enabled) qDebug() << "Property not found" << isList << varName << s;
        return;
    }

    mp = mobj->property(inx);
    isValid = true;
    className = mp.typeName();
    if (className.endsWith('*')) className.chop(1);
    if (className.startsWith("QList<"))
    {
        isList = true;
        className.remove(0,6); // QList<
        className.chop(2);     // *>
    }
}

QObject* ObjectProperty::childObj() const
{
    if (!isList)
        return EFactory::getObject(className,mp.read(parentObj));

    QList<QObject*>* objList = EFactory::getList(className,mp.read(parentObj));
    if (!objList || objList->size() <= index) return 0;
    return objList->at(index);
}
