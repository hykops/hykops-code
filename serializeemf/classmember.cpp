#include "classmember.h"
#include <QDebug>
#include <QDomElement>
#include <QObject>
#include "debug.h"

ClassMember::ClassMember(const QMetaProperty& prop, const QObject* parentObj)
    : isList(false)
{
    varName = prop.name();
    constructor(parentObj);
}

ClassMember::ClassMember(const QDomElement& elem, const QObject* parentObj)
    : isList(false)
{
    if (elem.isNull()) return;
    varName = elem.tagName();
    constructor(parentObj);
}

void ClassMember::constructor(const QObject *parentObj)
{
    const QMetaObject* mobj = parentObj->metaObject();
    int inx = mobj->indexOfProperty(varName.toUtf8().data());

    if (inx<0)
    {
        if(Debug::enabled) {
            qDebug() << "Property not found" << varName << "in" << mobj->className();
            for (int i=0; i<mobj->propertyCount(); i++)
                qDebug() << "  " << mobj->property(i).name();
            qDebug() << "------------------";
        }
        return;

    }

    mProperty = mobj->property(inx);
    className = mProperty.typeName();
    if (className.endsWith('*'))
        className.chop(1);


    if (className.startsWith("QList<"))
    {
        isList = true;
        className.remove(0,6);
        className.chop(2);  //  remove *>
    }

    // qDebug() << isList << varName << className;
}
