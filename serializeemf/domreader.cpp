#include "domreader.h"
#include <QFile>
#include <QDebug>
#include <QDomElement>
#include "classmember.h"
#include "EFactory.h"
#include <QDomNamedNodeMap>
#include <math.h>
#include <QStringList>
#include "objectproperty.h"
#include "debug.h"

Q_DECLARE_METATYPE(QList<QObject*>*)

// TODO : define auto generated map types in header file
#include <QMap>
typedef QMap<QString,QString>* HMapQStringQStringPtr;
Q_DECLARE_METATYPE(HMapQStringQStringPtr)

bool ObjectReference::setChildObject(QObject* obj) const
{
    QVariant v = parentObj->property(attribName.toUtf8().data());
    if (!v.isValid()) return false;

    if (QString(v.typeName()).startsWith("QList<"))
    {
        QList<QObject*>* listPtr = EFactory::getList(dataType,v);
        if (!listPtr) return false;
        listPtr->append(obj);
        return true;
    }

    return parentObj->setProperty(attribName.toUtf8().data(),
                                  EFactory::toVariant(dataType,obj));
}

DomReader::DomReader(QFile& file)
{
    qRegisterMetaType<QList<QObject*>*>("QList<QObject*>*");

    if (!file.open(QIODevice::ReadOnly))
    {
        qDebug() << "DomReader cannot open input file" << file.fileName();
        return;
    }

    QString errMsg;
    int errLine, errCol;
    if (!setContent(&file,&errMsg,&errLine,&errCol))
    {
        qDebug() << "error parsing XML document" << file.fileName() << errMsg <<
                    "\nline,column" << errLine << errCol;
    }

    if(Debug::enabled) qDebug() << "document parsed successfully";
}

DomReader::DomReader(const QString &xml)
{
    qRegisterMetaType<QList<QObject*>*>("QList<QObject*>*");

    if (xml.isEmpty())
    {
        qDebug() << "DomReader requires argument (input xml)";
        return;
    }

    QString errMsg;
    int errLine, errCol;
    if (!setContent(xml,&errMsg,&errLine,&errCol))
    {
        qDebug() << "error parsing XML string" << xml << errMsg <<
                    "\nline,column" << errLine << errCol;
    }

    if(Debug::enabled) qDebug() << "string parsed successfully";
}

void DomReader::setObjectDefaults(QObject *obj)
{
    if (!obj) return;
    const QMetaObject* mobj = obj->metaObject();
    for (int i=0; i<mobj->propertyCount(); i++)
    {
        QMetaProperty mp = mobj->property(i);
        bool ok = true;
        switch (mp.type()) {
        case QVariant::Double:
            ok = mp.write(obj, 0.);
            break;
        case QVariant::Int:
            ok = mp.write(obj, 0);
            break;
        case QVariant::Bool:
            ok = mp.write(obj, false);
            break;
        case QVariant::String:
            ok = mp.write(obj, QString());
            break;
        case QVariant::UserType:
        {
            QString className = mp.typeName();
            if (className.startsWith("QList<"))
                ok = true; // nothing to do (empty list already constructed)
            else if (className.startsWith("HMap") && className.endsWith("Ptr"))
                ok = true; // nothing to do (empty map already constructed)
            else
            {
                if (className.endsWith('*')) className.chop(1);

                QVariant nullPointer = EFactory::toVariant(className,0);
                ok = mp.write(obj, nullPointer);
            }
        }
            break;
        default:
            if(Debug::enabled) qDebug() << "DomReader::setObjectDefaults: type not handled" << mp.typeName();
            break;
        }
        if (!ok)
            if(Debug::enabled) qDebug() << "DomReader::setObjectDefaults: failed to set property" << mp.typeName();
    }
}

void DomReader::objectRecursion(const QDomElement& elem, QObject* parentObj)
{
//    qDebug() << "parse" << elem.tagName() << parentObj->metaObject()->className();
    QObject* childObj = 0;
    /// find class name in property
    ClassMember cm(elem, parentObj);

    /// derived class: explicit type attribute
    QString childType = className(elem.attribute("xsi:type"));

    if (childType.isEmpty())
    {
        /// should never happen since simple data members are stored as XML-attribute
        if (cm.mProperty.type() != QVariant::UserType)
        {
            qDebug() << "not user defined type" << cm.mProperty.typeName() << elem.tagName();
            return;
        }

        childType = cm.className;
    }

    if (childType.startsWith("HMap"))
    {
        /// might consider generic approach if there are a lot of map types (right now 1)
        if (childType == "HMapQStringQStringPtr")
        {
            QVariant v = cm.mProperty.read(parentObj);
            QMap<QString,QString>* map = v.value<HMapQStringQStringPtr>();
            map->insert(elem.attribute("key"),elem.text());
            return;
        }
    }

    childObj = EFactory::construct(childType);

    setObjectDefaults(childObj);
    setParent((HParent*)parentObj, (HParent*)childObj);

    if(parentObj)
    {
        setDataAttributes(childObj, elem);

        bool ok = false;

        if (cm.isList)
        {
            QList<QObject*>* listPtr = EFactory::getList(cm.className,
                                                         cm.mProperty.read(parentObj));
            if (listPtr)
            {
                listPtr->append(childObj);
                ok = true;
            }
        }
        else
        {
            QVariant val = EFactory::toVariant(cm.className,childObj);
            ok = parentObj->setProperty(elem.tagName().toUtf8().data(),val);
        }

        int dummyInx = parentObj->metaObject()->indexOfProperty(elem.tagName().toUtf8().data());
        QString dummyType = dummyInx<0 ? "(none)" : parentObj->metaObject()->property(dummyInx).typeName();

        if(Debug::enabled) {
            QTextStream xout(stdout, QIODevice::WriteOnly);
            xout << "\n------------------------------\n";
            xout << (ok ? "success" : "failed");
            xout << "\n parent class....." << parentObj->metaObject()->className();
            xout << "\n child class......" << childObj->metaObject()->className();
            xout << "\n var name........." << elem.tagName();
            xout << "\n var type........." << dummyType;
            xout << "\n";
            xout.flush();
        }
    }
    else
        qDebug() << "child element not constructed" << elem.tagName() << childObj;

    for (QDomElement childElem = elem.firstChildElement(); !childElem.isNull(); childElem=childElem.nextSiblingElement())
    {
        objectRecursion(childElem, childObj);
    }
}

void DomReader::setParent(HParent* parent, HParent *child) {
    child->setHParent(parent);
    child->setParent(parent);
    if(Debug::enabled) qDebug() << child << ".setHParent(" << parent << ")";
}

void DomReader::setDataAttributes(QObject* obj, const QDomElement &elem)
{
    QDomNamedNodeMap nnm = elem.attributes();
    for (int i=0; i<nnm.size(); i++)
    {
        QString propName  = nnm.item(i).nodeName();
        if (propName=="xsi:type") continue; // TODO: check occurence
        QString propValue = nnm.item(i).nodeValue();

        ObjectProperty objProp(obj,propName);
        if (!objProp.isValid)
        {
            if(Debug::enabled) qDebug() << "DomReader::setDataAttributes: Property not found"
                                        << propName << obj->metaObject()->className();
            continue;
        }

        bool ok = false;

        switch (objProp.mp.type()) {
        case QVariant::Bool:
            ok = obj->setProperty(propName.toUtf8().data(),
                                  (bool) (propValue.toLower()=="true"));
            break;
        case QVariant::Int:
            ok = obj->setProperty(propName.toUtf8().data(), propValue.toInt());
            break;
        case QVariant::Double:
        {
            double tmpDouble = 0;
            tmpDouble = propValue.toDouble(&ok);
            if (!ok)
            {
                if(Debug::enabled) qDebug() << "DomReader::setDataAttributes: cannot parse double"
                                            << propName << propValue;
                continue;
            }
            ok = obj->setProperty(propName.toUtf8().data(), tmpDouble);

        }
            break;
        case QVariant::String:
            ok = obj->setProperty(propName.toUtf8().data(), propValue);
            break;
        case QVariant::UserType:
            /// if list: multiple references separated by space
            foreach (const QString& xpath, propValue.split(' '))
            {
                if (!xpath.isEmpty())
                    refList.append(ObjectReference(obj,propName,objProp.className,xpath));
            }
            ok = true;
            break;
        default:
            break;
        }

        if (!ok)
            if(Debug::enabled) qDebug() << "...failed attribute" << objProp.mp.type() << objProp.index << propName << propValue;
    }
}

QString DomReader::className(const QString& xmlName)
{
    QString retval(xmlName);
    int pos = retval.lastIndexOf(':');
    if (pos >= 0)
        retval.remove(0,pos+1);
    return retval;
}

QObject* DomReader::objectByXpath(const QString& xpath)
{
    if (!rootObj) return 0;

    QObject* retval = 0;


    if (xpath.startsWith("//"))
    {
        retval = rootObj;
        foreach (QString fragment, xpath.mid(2).split('/'))
        {
            ObjectProperty prop(retval,fragment);
            if (!prop.isValid)
            {
                if(Debug::enabled) qDebug() << "DomReader::objectByXpath: fragment property not found"
                                            << fragment << retval->metaObject()->className();
                return 0;
            }
            QObject* childObj = prop.childObj();
            if (!childObj)
            {
                if(Debug::enabled) qDebug() << "DomReader::objectByXpath: object not found"
                                            << xpath << prop.className << prop.index << retval->metaObject()->className();
                return 0;
            }
            retval = childObj;
        }
        return retval;
    }
    else
        if(Debug::enabled) qDebug() << "DomReader::objectByXpath: failed to resolve path" << xpath;

    return 0;
}


QObject* DomReader::toplevelObject()
{
    QDomElement root = documentElement();
    if (root.isNull()) return 0;

    /// top level object : class name in tag name
    rootObj = EFactory::construct(className(root.tagName()));
    setDataAttributes(rootObj, root);


    for (QDomElement elem = root.firstChildElement();
         !elem.isNull(); elem=elem.nextSiblingElement())
    {
        objectRecursion(elem, rootObj);
    }

    QString xpath;

    QMap<QString,QObject*> objectByXpathMap;
    QSet<QString> unresolved;
    foreach (const ObjectReference& objRef, refList)
    {
        QObject* childObj = objectByXpathMap.value(objRef.xpath,0);
        if (!childObj)
        {
            childObj = objectByXpath(objRef.xpath);
            if (!childObj)
            {
                if (!unresolved.contains(objRef.xpath))
                {
                    if (Debug::enabled) qDebug() << "cannot resolve xpath" << objRef.xpath;
                    unresolved.insert(objRef.xpath);
                }
            }
            else
                objectByXpathMap.insert(objRef.xpath,childObj);
        }

        bool ok = false;
        if (childObj)
        {
            ok = objRef.setChildObject(childObj);
            if (!ok)
            {

                 if (Debug::enabled) qDebug() << "failed to set reference"
                         << objRef.attribName << objRef.dataType << childObj->metaObject()->className();
            }
        }
    }

    return rootObj;
}

