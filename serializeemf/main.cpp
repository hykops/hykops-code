#include <QCoreApplication>
#include <QObject>
#include <QDebug>
#include <QString>
#include <QFile>
#include "domreader.h"
#include "domwriter.h"

int main(int argc, char* argv[])
{
    QString filename(":/data/Example1.xml");
    if (argc >= 2) filename = argv[1];

    QFile fInput(filename);
    DomReader dr(fInput);

    QObject* obj = dr.toplevelObject();
    if (obj)
        qDebug() << "object parsed from " << filename;
    else
        qDebug() << "failed to parse object from " << filename;

    filename = "/tmp/dump.xml";
    if (argc >= 3) filename = argv[2];
    DomWriter dw(obj);
    dw.writeToFile(filename);
    qDebug() << "object serialized to " << filename;
}
