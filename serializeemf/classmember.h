#ifndef CLASSMEMBER_H
#define CLASSMEMBER_H

#include <QString>
#include <QMetaProperty>

class QObject;
class QDomElement;

class ClassMember
{
    void constructor(const QObject* parentObj);
public:
    QString varName;
    QString className;
    bool isList;
    bool hasProperty;
    QMetaProperty mProperty;


    ClassMember() : isList(false) {;}
    ClassMember(const QMetaProperty& prop, const QObject* parentObj);
    ClassMember(const QDomElement &elem, const QObject* parentObj);
};

#endif // CLASSMEMBER_H
