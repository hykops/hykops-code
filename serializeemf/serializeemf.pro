#-------------------------------------------------
#
# Project created by QtCreator 2016-07-06T09:52:11
#
#-------------------------------------------------

TARGET    = serializeemf
QT       += core xml
QT       -= gui

TEMPLATE  = lib
unix:DESTDIR = /tmp/libs/
win32:DESTDIR = C:\tmp\libs\


win32 {
    CONFIG += static
}

CONFIG   += console
CONFIG   -= app_bundle

INCLUDEPATH += ../hykops_data
LIBS += -L/tmp/libs/ -lhykops_data

unix {
    target.path = /usr/lib
    INSTALLS += target
}

SOURCES += main.cpp \
    domreader.cpp \
    classmember.cpp \
    objectproperty.cpp \
    domwriter.cpp \
    debug.cpp \
    serializedobject.cpp

HEADERS += \
    domreader.h \
    classmember.h \
    objectproperty.h \
    domwriter.h \
    debug.h \
    serializedobject.h

RESOURCES += \
    qresource.qrc


