#include <QObject>
#include <QDebug>
#include <QFile>
#include <QTextStream>
#include <QMetaProperty>
#include <QDebug>

#include "domwriter.h"
#include "objectproperty.h"
#include "classmember.h"
#include "EFactory.h"

// TODO : define auto generated map types in header file
#include <QMap>
typedef QMap<QString,QString>* HMapQStringQStringPtr;
Q_DECLARE_METATYPE(HMapQStringQStringPtr)

DomWriter::DomWriter(const QObject* toplevelObject)
{
    serialize(toplevelObject);
}

SerializedObject* DomWriter::serialize(const QObject* toplevelObject)
{
    ignoreProp.insert("objectName");

    if (!toplevelObject) return NULL;
    QString docNodeString =
            QString("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                    "<de.hykops.lpd.datamodel:%1"
                    " xmi:version=\"2.0\" xmlns:xmi=\"http://www.omg.org/XMI\""
                    " xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\""
                    " xmlns:de.hykops.lpd.datamodel=\"http:///de/hykops/lpd/datamodel.ecore\""
                    " hParent=\"/\""
                    " unitSystem=\"SI\"/>").arg(toplevelObject->metaObject()->className());
    if (!setContent(docNodeString))
    {
        qDebug() << "DOM document initialization failed";
        return NULL;
    }


    QDomElement docElem = documentElement();
    serializeRecursion(docElem,toplevelObject,"/");

    QString res;
    QTextStream xout(&res);
    docElem.save(xout, 2);

    SerializedObject *ser = new SerializedObject(res);

    return ser;
}

void DomWriter::serializeRecursion(QDomElement& parentElem, const QObject* parentObj, const QString &parentXPath)
{
    if (!parentObj) return;
    const QMetaObject* mobj = parentObj->metaObject();
    for (int i=0; i<mobj->propertyCount(); i++)
    {
        QMetaProperty mp = mobj->property(i);
        QString varName = mp.name();

        if (!mp.isStored()) continue;

        // TODO: deal with parent
        if (ignoreProp.contains(varName)) continue;

        if (mp.type() == QVariant::UserType)
        {
            if (mp.typeName() == QString("HMapQStringQStringPtr"))
            {
                QVariant v = mp.read(parentObj);
                const QMap<QString,QString>* map = v.value<HMapQStringQStringPtr>();
                for (QMap<QString,QString>::const_iterator it = map->constBegin();
                     it != map->constEnd(); ++it)
                {
                    QDomElement elem = createElement(varName);
                    elem.setAttribute("key", it.key());
                    elem.appendChild(createTextNode(it.value()));
                    parentElem.appendChild(elem);
                }
                continue; // done with map property
            }

            ObjectProperty objProp(parentObj, varName);
/*
            if (varName == "hSurfaceIdentifiers")
            {
                QList<QObject*>* list = EFactory::getList(objProp.className,mp.read(parentObj));
                qDebug() << "hSurfaceIdentifiers" << list->size();
            }
*/
            if (objProp.isList)
            {
                int listIndex = 0;
                QList<QObject*>* list = EFactory::getList(objProp.className,mp.read(parentObj));
                if (!list) continue;
                foreach (QObject* childObj, *list)
                {
                    if (!childObj) continue;
                    QDomElement elem = serializeObject(parentElem, objProp, parentObj, childObj);

                    if (!elem.isNull())
                        serializeRecursion(elem,childObj, QString("%1/@%2.%3")
                                           .arg(parentXPath)
                                           .arg(mp.name())
                                           .arg(listIndex));
                    parentElem.appendChild(elem);
                    ++listIndex;
                }
            }
            else
            {
                QObject* childObj = EFactory::getObject(objProp.className, mp.read(parentObj));

                if(childObj) {
                    QDomElement elem = serializeObject(parentElem, objProp, parentObj, childObj);
                    // elem.setAttribute("hParent", parentXPath);

                    if (!elem.isNull())
                        serializeRecursion(elem,childObj, QString("%1/@%2")
                                           .arg(parentXPath)
                                           .arg(mp.name()));
                } else {
                    // null reference
                    continue;
                }
            }
        }
        else
        {
            switch (mp.type())
            {
            case QVariant::Bool:
                // TODO: default type for bool is false ?
                break;
            case QVariant::Int:
                if (mp.read(parentObj).toInt() == 0) continue;
                break;
            case QVariant::Double:
                if (mp.read(parentObj).toDouble() == 0.) continue;
                break;
            case QVariant::String:
                if (mp.read(parentObj).toString().isEmpty()) continue;
                break;
            default:
                break;
            }
            parentElem.setAttribute(varName,mp.read(parentObj).toString());
        }

    }
}

QString DomWriter::xpathFromObject(const QObject* childObj)
{
    QString retval;
    const QObject* parent;
    while ((parent = childObj->parent()))
    {
        ClassMember cm;
        bool found = false;

        for (int iprop=0; iprop<parent->metaObject()->propertyCount(); iprop++)
        {
            cm = ClassMember(parent->metaObject()->property(iprop), parent);
            if (cm.isList)
            {
                int listIndex = 0;
                QList<QObject*>* listPtr = EFactory::getList(cm.className,
                                                             cm.mProperty.read(parent));
                for (listIndex=0; listIndex<listPtr->size(); listIndex++)
                    if (listPtr->at(listIndex) == childObj)
                    {
                        found = true;
                        break;
                    }
                if (found)
                {
                    retval.prepend(QString("/@%1.%2").arg(cm.varName).arg(listIndex));
                    break;
                }
            }
            else
            {
                QObject* testObj = EFactory::getObject(cm.className,
                                                       cm.mProperty.read(parent));
                if (testObj != childObj) continue;

                retval.prepend(QString("/@%1").arg(cm.varName));
                found = true;
                break;
            }
        }

        if (!found) break;
        childObj = parent;
    }

    retval.prepend("/");
    return retval;
}

QDomElement DomWriter::serializeObject(QDomElement& parentElem, const ObjectProperty& objProp,
                                       const QObject* parentObj, const QObject* childObj)
{
    if (childObj->parent() != parentObj)
    {
        QString currentAttrib = parentElem.attribute(objProp.varName);
        if (!currentAttrib.isEmpty()) currentAttrib.append(' ');
        currentAttrib.append(xpathFromObject(childObj));
        parentElem.setAttribute(objProp.varName, currentAttrib);
        return QDomElement();
    }
    QDomElement elem = createElement(objProp.varName);

    if(childObj->metaObject()->className() != objProp.className) {
        elem.setAttribute("xsi:type", childObj->metaObject()->className());
    }

    parentElem.appendChild(elem);

    return elem;
}

void DomWriter::writeToFile(const QString& filename)
{
    QFile of(filename);
    if (!of.open(QIODevice::WriteOnly))
    {
        qDebug() << "cannot open output file" << filename;
        return;
    }
    QTextStream xout(&of);
    save(xout,2);
}
