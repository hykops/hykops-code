#ifndef DOMWRITER_H
#define DOMWRITER_H

#include <QDomDocument>
#include <QString>
#include <QSet>

#include "serializedobject.h"

class QObject;
class ObjectProperty;

class DomWriter : public QDomDocument
{
    QSet<QString> ignoreProp;
    void serializeRecursion(QDomElement& parentElem, const QObject* parentObj, const QString& parentXPath);
    QDomElement serializeObject(QDomElement& parentElem, const ObjectProperty& objProp,
                                const QObject* parentObj, const QObject* childObj);
    QString xpathFromObject(const QObject* childObj);
public:
    DomWriter() {;}
    DomWriter(const QObject* toplevelObject);
    SerializedObject* serialize(const QObject* toplevelObject);

    void writeToFile(const QString& filename);
};

#endif // DOMWRITER_H
