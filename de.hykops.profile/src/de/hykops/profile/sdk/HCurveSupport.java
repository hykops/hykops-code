/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.profile.sdk;

import de.hykops.lpd.core.Activator;
import de.hykops.lpd.core.nomenclature.ESurfaceIdentifiers;
import de.hykops.lpd.datamodel.HLoftElement;
import de.hykops.lpd.datamodel.HSeriesProfileCurve2D;
import de.hykops.profile.series.EProfileSide;

/**
 * Curve support without import of propeller plugins (for NACA ident)
 *
 * @author stoye
 * @since Jul 13, 2016
 */
public class HCurveSupport {
	public static HSeriesProfileCurve2D createSeriesProfile(HLoftElement parent, String profileSeries, String profileName, ESurfaceIdentifiers surfaceSide) {

		EProfileSide profileSide = SurfaceToProfileIdentifierMapping.getProfileSide(surfaceSide); 
		HSeriesProfileCurve2D curve = Activator.getFactoryService().getFactory().createSeriesProfileCurve2D();
		curve.setHParent(parent);
		curve.setHLoftElement(parent);
		curve.setSide(profileSide.toString());
		curve.setDenomination(profileName);
		curve.setSeries(profileSeries);
		return curve;
	}
	
	public static HSeriesProfileCurve2D createSeriesProfile(HLoftElement parent, String profileSeries, String profileDenominator, EProfileSide profileSide) {		
		HSeriesProfileCurve2D curve = Activator.getFactoryService().getFactory().createSeriesProfileCurve2D();
		curve.setHParent(parent);
		curve.setHLoftElement(parent);
		curve.setSide(profileSide.toString());
		String denominator = profileDenominator;
		curve.setDenomination(denominator);
		curve.setSeries(profileSeries);
		return curve;
	}


}
