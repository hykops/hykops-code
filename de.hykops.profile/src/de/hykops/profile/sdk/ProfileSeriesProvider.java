/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.profile.sdk;

import java.util.HashMap;
import java.util.Map;

import de.hykops.lpd.datamodel.HSeriesProfileCurve2D;
import de.hykops.profile.series.ISeriesProfile;
import de.hykops.profile.series.impl.Circle;
import de.hykops.profile.series.impl.Ellipse;
import de.hykops.profile.series.impl.NACA_FOURDIGIT;
import de.hykops.profile.series.impl.NACA_ONE;

/**
 * 
 *
 * @author stoye
 * @since Dec 7, 2016
 */
public class ProfileSeriesProvider {

	private Map<HSeriesProfileCurve2D, ISeriesProfile> map;

	private ProfileSeriesProvider() {
		this.map = new HashMap<HSeriesProfileCurve2D, ISeriesProfile>();
	}

	private static ProfileSeriesProvider getProfileSeriesProvider() {
		if (ProfileSeriesProvider.singleton == null) {
			ProfileSeriesProvider.singleton = new ProfileSeriesProvider();
		}
		return ProfileSeriesProvider.singleton;
	}

	private static ProfileSeriesProvider singleton;

	@SuppressWarnings("unused")
	public static ISeriesProfile getSeriesProfile(HSeriesProfileCurve2D curve) {
		ProfileSeriesProvider provider = ProfileSeriesProvider.getProfileSeriesProvider();
		if (provider.map.containsKey(curve)) {
			ISeriesProfile profile = provider.map.get(curve);
			boolean changed = profile.update(curve.getDenomination()); /*- check whether the profile designation has changed, then update*/
			return profile;
		}
		ISeriesProfile profile = ProfileSeriesProvider.createSeriesProfile(curve);
		provider.map.put(curve, profile);
		return getSeriesProfile(curve);
	}

	private static ISeriesProfile createSeriesProfile(HSeriesProfileCurve2D curve) {
		String name = curve.getDenomination();
		String series = curve.getSeries();
		ISeriesProfile seriesProfile;
		if (series.equals(NACA_ONE.SERIESNAME)) {
			seriesProfile = new NACA_ONE(name);
		} else if (series.equals(NACA_FOURDIGIT.SERIESNAME)) {
			seriesProfile = new NACA_FOURDIGIT(name);
		} else if (series.equals(Circle.SERIESNAME)) {
			seriesProfile = new Circle();
		} else if (series.equals(Ellipse.SERIESNAME)) {
			seriesProfile = new Ellipse(name);
		} else // failsafe
		{
			seriesProfile = new Circle();
		}

		return seriesProfile;
	}
}
