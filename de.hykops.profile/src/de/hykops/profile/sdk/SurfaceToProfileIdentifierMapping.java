/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.profile.sdk;

import java.util.ArrayList;
import java.util.List;

import de.fsg.tk.math.maps.Pair;
import de.hykops.lpd.core.nomenclature.ESurfaceIdentifiers;
import de.hykops.profile.series.EProfileSide;

/**
 * 
 * Zuordnung von Flaechenbezeichner zu Profilseitenbezeichnern
 * @author stoye
 * @since Jul 13, 2016
 */
public class SurfaceToProfileIdentifierMapping {
	public static List<Pair<EProfileSide, ESurfaceIdentifiers>> sides = new ArrayList<Pair<EProfileSide, ESurfaceIdentifiers>>();

	static {
		sides.add(new Pair<EProfileSide, ESurfaceIdentifiers>(EProfileSide.SUCTION, ESurfaceIdentifiers.PROFILE_SUCTION_SIDE));
		sides.add(new Pair<EProfileSide, ESurfaceIdentifiers>(EProfileSide.PRESSURE, ESurfaceIdentifiers.PROFILE_PRESSURE_SIDE));
	}

	public static EProfileSide getProfileSide(ESurfaceIdentifiers ident) {
		for (Pair<EProfileSide, ESurfaceIdentifiers> p : sides) {
			if (p.getT2().equals(ident)) {
				return p.getT1();
			}
		}
		return null;
	}

	public static ESurfaceIdentifiers getESurfaceIdentification(EProfileSide side) {
		for (Pair<EProfileSide, ESurfaceIdentifiers> p : sides) {
			if (side.equals(p.getT1())) {
				return p.getT2();
			}
		}
		return null;
	}

}
