/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.profile.sdk;

import de.hykops.jna.datamodel.core.HykopsCore;
import de.hykops.lpd.core.Activator;
import de.hykops.lpd.datamodel.HCurve2D;
import de.hykops.lpd.datamodel.HDiscreteCurve2D;
import de.hykops.lpd.datamodel.HSeriesProfileCurve2D;
import de.hykops.profile.series.EProfileSide;
import de.hykops.profile.series.ISeriesProfile;

/**
 * 
 *
 * @author stoye
 * @since Jul 11, 2016
 */
public class ProfileSupport {

	public static boolean isValidProfile(String name) {
// TODO: einbauen		
		return true;
	}

	public static double[] getXiEta(HCurve2D curve, double c) {
		
		if (curve.getHLoftElement() != null) {
			if (curve instanceof HDiscreteCurve2D) {
				double xe[] = new double[2];
				int success = HykopsCore.INSTANCE.getXiEta(curve.getHandle(), c, xe);
				return xe;				
			} else if (curve instanceof HSeriesProfileCurve2D) {

				double xe[] = new double[2];
				int success = HykopsCore.INSTANCE.getXiEta(curve.getHandle(), c, xe);
				return xe;				

				
				
//				// identify abstract profile identification
//				HSeriesProfileCurve2D serie = (HSeriesProfileCurve2D) curve;
//				ISeriesProfile iProfile = ProfileSeriesProvider.getSeriesProfile(serie);
//				String sside = ((HSeriesProfileCurve2D) curve).getSide();
//				EProfileSide side = EProfileSide.valueOf(sside);
//				double xieta[] = iProfile.getPoint(c, side);
//				return xieta;
			}
		}
		return null;
	}

	public static double[] getCartesianCoordinate(HCurve2D curve, double c) {
		double xiEta[] = getXiEta(curve, c);
		if (xiEta != null) {
			return Activator.getFactoryService().getFactory().createCoordinateTransformation().toGlobalCoordinate(curve.getHLoftElement(), xiEta[0], xiEta[1]);
		}
		return null;
	}
}
