/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.profile.series;

/**
 * @author stoye
 * @since Dec 5, 2016
 */
public interface ISeriesProfile {
/**
 * 
 * @param denominator
 * @return wether the profile denomination has changed
 *
 * @author stoye
 * @since Dec 8, 2016
 */
	public boolean update(String denominator);
/**
 * @return the name of the series the profile belongs to
 *
 * @author stoye
 * @since Dec 5, 2016
 */
	public String getSeriesName();
	
/**
 * @return the profile denomination
 *
 * @author stoye
 * @since Dec 5, 2016
 */
	public String getProfileName();
	
/**
 * @return Series-specific profile parameters
 *
 * @author stoye
 * @since Dec 5, 2016
 */
	public String getProfileParameters();

/**
 * @param chord the nondimensional chord position
 * @param side the side (or camber line) of the profile 
 * @return the offset position of the profile in perpendicular direction to the chord line
 *
 * @author stoye
 * @since Dec 5, 2016
 */
	public double[] getPoint(double chord, EProfileSide side);
}
