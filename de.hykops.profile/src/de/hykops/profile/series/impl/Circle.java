/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.profile.series.impl;

import de.hykops.profile.series.EProfileSide;
import de.hykops.profile.series.ISeriesProfile;

/**
 * 
 *
 * @author stoye
 * @since Dec 7, 2016
 */
public class Circle implements ISeriesProfile {
	public static final String SERIESNAME = "CIRCLE";

	public Circle() {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hykops.profile.series.ISeriesProfile#getSeriesName()
	 */
	@Override
	public String getSeriesName() {
		return SERIESNAME;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hykops.profile.series.ISeriesProfile#getProfileName()
	 */
	@Override
	public String getProfileName() {
		return "";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hykops.profile.series.ISeriesProfile#getProfileParameters()
	 */
	@Override
	public String getProfileParameters() {
		return "";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hykops.profile.series.ISeriesProfile#getPoint(double,
	 * de.fsg.tk.propeller.core.sdk.ProfileSide)
	 */
	@Override
	public double[] getPoint(double chord, EProfileSide side) {
		double x = chord - 0.5;
		double phi;
		if (chord < 0.) {
			phi = Math.PI;
		} else if (chord > 1.) {
			phi = 0.;
		} else {
			phi = Math.acos(2. * x);
		}

		double y;
		switch (side) {
			case SUCTION: {
				y = 0.5 * Math.sin(phi);
				break;
			}
			case PRESSURE: {
				y = -0.5 * Math.sin(phi);
				break;
			}
			case MEAN: {
				y = 0.;
				break;
			}
			default: {
				y = 0.;
			}
		}
		return new double[] { chord, y };
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hykops.profile.series.ISeriesProfile#update(java.lang.String)
	 */
	@Override
	public boolean update(String denominator) {
		// no denominator for circle, nothing to do.
		return false; 
	}
}
