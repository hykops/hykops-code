/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.profile.series.impl;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Locale;

import de.hykops.profile.series.EProfileSide;
import de.hykops.profile.series.ISeriesProfile;

/**
 * 
 *
 * @author stoye
 * @since Dec 8, 2016
 */
public class Ellipse implements ISeriesProfile {
	public static final String	SERIESNAME	= "ELLIPSE";

	private double				relThickness;

	private String currentDenominator;
	/**
	 * 
	 * @param denominator
	 *            the ellipse denomination, i.e. "0.5" for a thickness of half
	 *            profile length
	 *
	 * @author stoye
	 * @since Dec 8, 2016
	 */
	public Ellipse(String denominator) {
		initString(denominator);
		this.setCurrentDenominator(denominator);
	}

	/**
	 * 
	 * @param relThickness_
	 *            the thickness of the ellipse relative to the length
	 *
	 * @author stoye
	 * @since Dec 8, 2016
	 */
	public Ellipse(double relThickness_) {
		init(relThickness_);
	}

	private void initString(String denominator)
	{
		double relT = 0.;
		if (denominator != null && denominator.length() > 0) {
			
			try {
				relT = NumberFormat.getInstance(Locale.UK).parse(denominator).doubleValue();
			} catch (ParseException e) {				
// TODO nothing				
			}
			init(relT);
		} else {
			init(1.);
		}		
	}
	
	private void init(double relThickness_) {
		setRelThickness(relThickness_);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hykops.profile.series.ISeriesProfile#getSeriesName()
	 */
	@Override
	public String getSeriesName() {
		return SERIESNAME;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hykops.profile.series.ISeriesProfile#getProfileName()
	 */
	@SuppressWarnings("boxing")
	@Override
	public String getProfileName() {
		return String.format("%s %12.4f", getSeriesName(), getRelThickness());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hykops.profile.series.ISeriesProfile#getProfileParameters()
	 */
	@SuppressWarnings("boxing")
	@Override
	public String getProfileParameters() {
		return String.format("T=%12.4f", getRelThickness());
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hykops.profile.series.ISeriesProfile#getPoint(double,
	 * de.fsg.tk.propeller.core.sdk.ProfileSide)
	 */
	@Override
	public double[] getPoint(double chord, EProfileSide side) {
		double x = chord - 0.5;
		double phi;
		if (chord < 0.) {
			phi = Math.PI;
		} else if (chord > 1.) {
			phi = 0.;
		} else {
			phi = Math.acos(2. * x);
		}

		double y;
		double relT = getRelThickness();
		switch (side) {
			case SUCTION: {
				y = 0.5 * Math.sin(phi) * relT;
				break;
			}
			case PRESSURE: {
				y = -0.5 * Math.sin(phi) * relT;
				break;
			}
			case MEAN: {
				y = 0.;
				break;
			}
			default: {
				y = 0.;
			}
		}
		return new double[] { chord, y };
	}

	/**
	 * @return the relThickness
	 */
	public double getRelThickness() {
		return this.relThickness;
	}

	/**
	 * @param relThickness
	 *            the relThickness to set
	 */
	public void setRelThickness(double relThickness_) {
		this.relThickness = relThickness_;
	}

	/**
	 * @return the currentDenominator
	 */
	public String getCurrentDenominator() {
		return this.currentDenominator;
	}

	/**
	 * @param currentDenominator the currentDenominator to set
	 */
	public void setCurrentDenominator(String currentDenominator_) {
		this.currentDenominator = currentDenominator_;
	}

	/* (non-Javadoc)
	 * @see de.hykops.profile.series.ISeriesProfile#update(java.lang.String)
	 */
	@Override
	public boolean update(String denominator) {
		if(!(getCurrentDenominator().equals(denominator)))
		{
			initString(denominator);
			setCurrentDenominator(denominator);
			return true;
		}		
		return false;
	}
}
