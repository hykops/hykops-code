/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.profile.series.impl;

import org.apache.commons.math.FunctionEvaluationException;
import org.apache.commons.math.analysis.UnivariateRealFunction;

import de.fsg.tk.math.analysis.spline.HermiteSplineInterpolator;
import de.fsg.tk.math.analysis.spline.NaturalSplineInterpolator;
import de.fsg.tk.math.constants.Epsilon;
import de.hykops.profile.series.EProfileSide;
import de.hykops.profile.series.ISeriesProfile;

/**
 * 
 *
 * @author stoye
 * @since Dec 7, 2016
 */
public class NACA_ONE implements ISeriesProfile {

	public static final String		SERIESNAME			= "NACA 1-series";
	public static final double		DEFAULT_ACCURACY	= 1.e-4;

	private double					camberCoefficient;
	private double					maxRelThickness;
	private double					chordwiseMinPressure;
	private String					profileName;
	private UnivariateRealFunction	intRelThicknessX;
	private UnivariateRealFunction	intRelThicknessY;

	private static final double		x[]					= { 0., 0.01250, 0.02500, 0.05000, 0.07500, 0.10000, 0.15000, 0.20000, 0.30000,
																0.40000, 0.50000, 0.60000, 0.70000, 0.80000, 0.90000, 0.95000, 1.0 };
	private static final double		y[]					= { 0., 0.646, 0.903, 1.255, 1.516, 1.729, 2.067, 2.332, 2.709, 2.927, 3.000, 2.917,
																2.635, 2.099, 1.259, 0.707, 0.060 };
	private static final double		yOffsetFactor		= 3.;																				/*- data given for NACA 16-006!*/

	private double					bx[];
	private double					by[];
	private double					accuracy;

	private static final double		gradLEx				= 0.;
	private static final double		gradTEx				= 1.;

	private String currentDenominator;
	
	/**
	 * 
	 * @param denominator
	 *            Constructor for String definition of NACA 1-series profile.
	 *            Syntax: 16-234 1: The series name, unchanged 6: The chordwise
	 *            min pressure location in 1/10th of chord. 6 means location 0.6
	 *            and designates a standard 16-series profile 2: The camber
	 *            coefficient in [1/10 * CL] 34: The thickness in % of the chord
	 * 
	 * @author stoye
	 * @since Dec 7, 2016
	 */
	public NACA_ONE(String denominator) {
		initString(denominator);
		this.setCurrentDenominator(denominator);

	}

	/**
	 * Constructor for coefficient definition of 'NACA 1-series. Syntax:
	 * 
	 * @param camberCoefficient_
	 *            the camber coefficient
	 * @param maxRelThickness_
	 *            the relative profile thickness in [1]
	 *
	 * @author stoye
	 * @since Dec 7, 2016
	 */
	public NACA_ONE(double camberCoefficient_, double maxRelThickness_) {
		this(camberCoefficient_, maxRelThickness_, 0.6, DEFAULT_ACCURACY);
	}

	/**
	 * 
	 * @param camberCoefficient_
	 *            see above
	 * @param maxRelThickness_
	 *            see above
	 * @param chordwiseMinPressure_
	 *            the chordwise minimum pressure location. 0.6 means 60%
	 *            chordlength and desigantes a series16-profile
	 *
	 * @author stoye
	 * @since Dec 7, 2016
	 */
	public NACA_ONE(double camberCoefficient_, double maxRelThickness_, double chordwiseMinPressure_) {
		this(camberCoefficient_, maxRelThickness_, chordwiseMinPressure_, DEFAULT_ACCURACY);
	}

	/**
	 * 
	 * @param camberCoefficient_
	 *            see above
	 * @param maxRelThickness_
	 *            see above
	 * @param chordwiseMinPressure_
	 *            see above
	 * @param accuracy_
	 *            The accuracy in the determination of the chord position
	 *
	 * @author stoye
	 * @since Dec 7, 2016
	 */
	public NACA_ONE(double camberCoefficient_, double maxRelThickness_, double chordwiseMinPressure_, double accuracy_) {
		init(camberCoefficient_, maxRelThickness_, chordwiseMinPressure_, accuracy_);
		this.setCurrentDenominator(this.getProfileParameters());
	}

	private void initString(String denominator)
	{		
		if (denominator.length() == 11) // one digit for camber: NACA 16-x00, x=camber
		{
			String sChordMinPressure = denominator.substring(6, 7);
			String sCamber = denominator.substring(8, 9);
			String sThickness = denominator.substring(9, 11);

			double chordwiseMinPressure_ = Double.parseDouble(sChordMinPressure) / 10.;
			double camber_ = Double.parseDouble(sCamber) / 10.;
			double thickness_ = Double.parseDouble(sThickness) / 100.;
			init(camber_, thickness_, chordwiseMinPressure_, DEFAULT_ACCURACY);
		} else if (denominator.length() == 12) // two digits for camber: 16-xx00,
												// xx=camber
		{
			String sChordMinPressure = denominator.substring(6, 7);
			String sCamber = denominator.substring(8, 10);
			String sThickness = denominator.substring(10, 12);

			double chordwiseMinPressure_ = Double.parseDouble(sChordMinPressure) / 10.;
			double camber_ = Double.parseDouble(sCamber) / 10.;
			double thickness_ = Double.parseDouble(sThickness) / 100.;
			init(camber_, thickness_, chordwiseMinPressure_, DEFAULT_ACCURACY);
		} else {
			init(0., 0., 0.6, DEFAULT_ACCURACY);
		}
	}

	@SuppressWarnings("cast")
	private void init(double camberCoefficient_, double maxRelThickness_, double chordwiseMinPressure_, double accuracy_) {
		setCamberCoefficient(camberCoefficient_);
		setMaxRelThickness(maxRelThickness_);
		setChordwiseMinPressure(chordwiseMinPressure_);
		setAccuracy(accuracy_);
		this.bx = new double[x.length];
		this.by = new double[x.length];
		double bmax = ((double) x.length) - 1.;
		for (int i = 0; i < x.length; i++) {
			this.bx[i] = ((double) i) / bmax;
			this.by[i] = ((double) i) / bmax;
		}

		this.intRelThicknessX = new HermiteSplineInterpolator().interpolate(x.length, this.bx, x, gradLEx, gradTEx);
		this.intRelThicknessY = new NaturalSplineInterpolator().interpolate(this.by, y);

		calculateDigits();
	}

	/**
	 * @author stoye
	 * @since Dec 7, 2016 Determine the profile digits from camber, thickness
	 *        and chordwise min. pressure location. Returns e.g. "16-123"
	 */
	@SuppressWarnings("boxing")
	protected void calculateDigits() {
		int i1 = (int) Math.rint(this.chordwiseMinPressure * 10.);
		int i2 = (int) Math.rint(getCamberCoefficient() * 10.);
		int i3 = (int) Math.rint(getMaxRelThickness() * 100.);
		if (i1 > 9)
			i1 = 9;
		// if(i2 > 9) i2 = 9; // sometimes, max. camber is larger
		if (i3 > 99)
			i3 = 99;
		this.profileName = String.format("1%1d-%1d%02d", i1, i2, i3);
	}

	/**
	 * Estimate coordinate value b for a given chord position by recusrion.
	 * 0<b<1 with b=0@LE and b=1@TE. This is not equvalent to the chord
	 * position. b is defined by i/(x.length-1) given by the thickness line
	 * offset points.
	 * 
	 * @param relC
	 *            the relative chord coordinate
	 * @return parameter b for given chord location
	 *
	 * @author stoye
	 * @since Dec 7, 2016
	 */
	private double getB(double relC) {
		int nr = (int) (Math.log10(1. / this.accuracy));
		double b = recursiveGetB(relC, 0., 1., 10, nr);
		return b;
	}

	/**
	 * Recursion for estimation of chord location for given b value
	 * 
	 * @param relC
	 * @param bMin
	 * @param bMax
	 * @param nb
	 * @param recursion
	 * @return
	 *
	 * @author stoye
	 * @since Dec 7, 2016
	 */
	@SuppressWarnings("cast")
	private double recursiveGetB(double relC, double bMin, double bMax, int nb, int recursion) {
		double b0, b1;
		double c0, c1;
		b1 = bMin;
		c1 = getRelThicknessFromBCoordinate(b1)[0];
		for (int i = 0; i < (nb - 1); i++) {
			b0 = b1;
			b1 = bMin + ((bMax - bMin) / ((double) (nb - 1))) * ((double) (i + 1));
			c0 = c1;
			c1 = getRelThicknessFromBCoordinate(b1)[0];
			if (c0 <= relC && c1 >= relC) {
				if (recursion > 0) {
					return recursiveGetB(relC, b0, b1, nb, (recursion - 1));
				}
				return (b0 + b1) / 2.;
			}
		}
		return (bMin + bMax) / 2.;
	}

	/**
	 * 
	 * @param relB
	 * @return relative profile thickness
	 *
	 * @author stoye
	 * @since Dec 7, 2016
	 */
	private double[] getRelThicknessFromBCoordinate(double relB) {
		double p[] = new double[2];
		try {
			p[0] = this.intRelThicknessX.value(relB);
			p[1] = (this.intRelThicknessY.value(relB) / yOffsetFactor) * this.maxRelThickness;
		} catch (FunctionEvaluationException e) {
			e.printStackTrace();
		}
		return p;
	}

	/**
	 * 
	 * @param relC
	 *            relative chord position
	 * @return mean line offset at given chord location
	 *
	 * @author stoye
	 * @since Dec 7, 2016
	 */
	private double getRelYMeanLine(double relC) {
		double CLi = this.camberCoefficient;
		if (relC < Epsilon.PICO)
			return 0.;
		else if (relC > (1. - Epsilon.PICO))
			return 0.;
		else
			return (0. - CLi / (4. * Math.PI)) * ((1. - relC) * Math.log(1. - relC) + relC * Math.log(relC));
	}

	/**
	 * 
	 * @param relC
	 *            relative chord position
	 * @return mean line angle at given chord location
	 *
	 * @author stoye
	 * @since Dec 7, 2016
	 */
	private double getRelYMeanLineAngle(double relC) {
		double c0, c1;
		if (relC <= (Epsilon.PICO + this.accuracy)) {
			c0 = relC;
			c1 = relC + this.accuracy / 2.;
		} else if (relC >= (1. - Epsilon.PICO - this.accuracy)) {
			c0 = relC - this.accuracy / 2.;
			c1 = relC;
		} else {
			c0 = relC - this.accuracy / 2.;
			c1 = relC + this.accuracy / 2.;
		}
		double y0 = getRelYMeanLine(c0);
		double y1 = getRelYMeanLine(c1);
		return Math.atan2((y1 - y0), (c1 - c0));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hykops.profile.series.ISeriesProfile#getSeriesName()
	 */
	@Override
	public String getSeriesName() {
		return SERIESNAME;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hykops.profile.series.ISeriesProfile#getProfileName()
	 */
	@Override
	public String getProfileName() {
		return this.profileName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hykops.profile.series.ISeriesProfile#getProfileParameters()
	 */
	@SuppressWarnings("boxing")
	@Override
	public String getProfileParameters() {
		double camber = getCamberCoefficient();
		double thickness = getMaxRelThickness();
		return String.format("C=%21.5f T=%21.5f", camber, thickness);
	}

	@Override
	public double[] getPoint(double chord, EProfileSide side) {
		double relB = getB(chord);
		double p[] = getRelThicknessFromBCoordinate(relB);
		double t = p[1];
		double c = p[0];
		double yt = 0.5 * t;
		double yc = getRelYMeanLine(c);
		double phi = getRelYMeanLineAngle(c);

		double xi, eta;
		switch (side) {

			case SUCTION: {
				xi = c - yt * Math.sin(phi);
				eta = yc + yt * Math.cos(phi);
				break;
			}
			case PRESSURE: {
				xi = c + yt * Math.sin(phi);
				eta = yc - yt * Math.cos(phi);
				break;
			}
			case MEAN: {
				xi = c;
				eta = yc;
				break;
			}
			default: {
				xi = c;
				eta = yc;
			}
		}
		return new double[] { xi, eta };
	}

	/**
	 * @return the camberCoefficient
	 */
	public double getCamberCoefficient() {
		return this.camberCoefficient;
	}

	/**
	 * @param camberCoefficient
	 *            the camberCoefficient to set
	 */
	public void setCamberCoefficient(double camberCoefficient_) {
		this.camberCoefficient = camberCoefficient_;
	}

	/**
	 * @return the maxRelThickness
	 */
	public double getMaxRelThickness() {
		return this.maxRelThickness;
	}

	/**
	 * @param maxRelThickness
	 *            the maxRelThickness to set
	 */
	public void setMaxRelThickness(double maxRelThickness_) {
		this.maxRelThickness = maxRelThickness_;
	}

	/**
	 * @return the accuracy
	 */
	public double getAccuracy() {
		return this.accuracy;
	}

	/**
	 * @param accuracy
	 *            the accuracy to set
	 */
	public void setAccuracy(double accuracy_) {
		this.accuracy = accuracy_;
	}

	/**
	 * @return the chordwiseMinPressure
	 */
	public double getChordwiseMinPressure() {
		return this.chordwiseMinPressure;
	}

	/**
	 * @param chordwiseMinPressure
	 *            the chordwiseMinPressure to set
	 */
	public void setChordwiseMinPressure(double chordwiseMinPressure_) {
		this.chordwiseMinPressure = chordwiseMinPressure_;
	}

	/**
	 * @return the currentDenominator
	 */
	public String getCurrentDenominator() {
		return this.currentDenominator;
	}

	/**
	 * @param current_denominator the current_denominator to set
	 */
	public void setCurrentDenominator(String currentDenominator_) {
		this.currentDenominator = currentDenominator_;
	}

	/* (non-Javadoc)
	 * @see de.hykops.profile.series.ISeriesProfile#update(java.lang.String)
	 */
	@Override
	public boolean update(String denominator) {
		if(!(getCurrentDenominator().equals(denominator)))
		{
			initString(denominator);
			setCurrentDenominator(denominator);
			return true;
		}		
		return false;
	}

}
