/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.profile.series.impl;

import de.fsg.tk.math.constants.Epsilon;
import de.hykops.profile.series.EProfileSide;
import de.hykops.profile.series.ISeriesProfile;

/**
 * 
 *
 * @author stoye
 * @since Dec 8, 2016
 */
public class NACA_FOURDIGIT implements ISeriesProfile {

	private double				maxRelCamber;
	private double				maxRelThickness;
	private double				distLEMaxCamber;

	public static final String	SERIESNAME			= "NACA 4-digit";
	public static final double	DEFAULT_ACCURACY	= 1.e-4;

	private String				profileName;
	private String				currentDenominator;
	private double				accuracy;

	public NACA_FOURDIGIT(String denominator) {
		initString(denominator);
		setCurrentDenominator(denominator);
	}

	@SuppressWarnings("cast")
	private void initString(String denominator) {
		String digits_ = denominator.substring(5);
		int mc = Integer.parseInt(digits_.substring(0, 1));
		int cl = Integer.parseInt(digits_.substring(1, 2));
		int mt = Integer.parseInt(digits_.substring(2, 4));
		setMaxRelCamber(((double) mc) / 100.);
		setDistLEMaxCamber(((double) cl) / 10.);
		setMaxRelThickness(((double) mt) / 100.);
		setAccuracy(DEFAULT_ACCURACY);
		calculateDigits();

	}

	@SuppressWarnings("boxing")
	protected void calculateDigits() {
		// 1st: max. camber [%chord]
		// 2nd: dist. LE-Max.camber [chord/10]
		// 3rd/4th: max. thickness location [%chord]
		int mc = (int) Math.rint(this.maxRelCamber * 100.);
		if (mc > 9)
			mc = 9; /*- more than 9% camber cannot be represented as NACA4digit*/
		int cl = (int) Math.rint(this.distLEMaxCamber * 10.);
		if (cl > 9)
			cl = 9; /*- more than 90% max. camber location cannot be represented as NACA4digit*/
		int mt = (int) Math.rint(this.maxRelThickness * 100.);
		if (mt > 99)
			mt = 99; /*- more than 99% max. rel. thickness cannot be represented as NACA4digit*/
		this.setProfileName(String.format("NACA %1d%1d%02d", mc, cl, mt));
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hykops.profile.series.ISeriesProfile#update(java.lang.String)
	 */
	@Override
	public boolean update(String denominator) {
		if (!(getCurrentDenominator().equals(denominator))) {
			initString(denominator);
			setCurrentDenominator(denominator);
			return true;
		}
		return false;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hykops.profile.series.ISeriesProfile#getSeriesName()
	 */
	@Override
	public String getSeriesName() {
		return NACA_FOURDIGIT.SERIESNAME;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hykops.profile.series.ISeriesProfile#getProfileName()
	 */
	@Override
	public String getProfileName() {
		return this.profileName;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hykops.profile.series.ISeriesProfile#getProfileParameters()
	 */
	@SuppressWarnings("boxing")
	@Override
	public String getProfileParameters() {
		double mc = getMaxRelCamber();
		double cl = getDistLEMaxCamber();
		double rt = getMaxRelThickness();

		return String.format("C=%21.5f CL=%21.5f T=%21.5f", mc, cl, rt);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hykops.profile.series.ISeriesProfile#getPoint(double,
	 * de.hykops.profile.series.EProfileSide)
	 */
	@Override
	public double[] getPoint(double chord, EProfileSide side) {
		double c = chord;
		double yt = 0.5 * getRelThickness(c);
		double yc = getRelYMeanLine(c);
		double phi = getRelYMeanLineAngle(c);

		double xi, eta;
		switch (side) {

			case SUCTION: {
				xi = c - yt * Math.sin(phi);
				eta = yc + yt * Math.cos(phi);
				break;
			}
			case PRESSURE: {
				xi = c + yt * Math.sin(phi);
				eta = yc - yt * Math.cos(phi);
				break;
			}
			case MEAN: {
				xi = c;
				eta = yc;
				break;
			}
			default: {
				xi = c;
				eta = yc;
			}
		}
		return new double[] { xi, eta };
	}

	/**
	 * Return ordinate of NACA00XX-profile at given chord coordinate.
	 * 
	 * @param c
	 *            nondimensional chord coordinate: 0=LE, 1=TE
	 * @return thickness of profile at given coordinate. rel. to chordlength
	 */
	public double getRelThickness(double relC) {
		// TODO: Leading edge circle einbauen
		return 5.
				* (0.29690 * Math.sqrt(relC) - 0.12600 * relC - 0.35160 * Math.pow(relC, 2.) + 0.28430 * Math.pow(relC, 3.) - 0.10150 * Math
						.pow(relC, 4.))
				* this.maxRelThickness * 2.;
	}

	/**
	 * Return the ordinate of the mean line
	 * 
	 * @param relC
	 *            the chord coordinate
	 * @return the mean line offset from the chord line
	 *
	 * @author stoye
	 * @since Dec 9, 2016
	 */
	public double getRelYMeanLine(double relC) {
		double m = this.maxRelCamber;
		double p = this.distLEMaxCamber;

		if (relC < this.distLEMaxCamber) // mean line forward of maximum
											// ordinate
			return (m / (p * p)) * (2. * p * relC - relC * relC);
		// mean line aft of maximum ordinate
		return (m / Math.pow((1. - p), 2.)) * ((1. - 2. * p) + 2 * p * relC - relC * relC);
	}

	/**
	 * 
	 * @param relC
	 *            relative chord position
	 * @return mean line angle at given chord location
	 *
	 * @author stoye
	 * @since Dec 7, 2016
	 */
	private double getRelYMeanLineAngle(double relC) {
		double c0, c1;
		if (relC <= (Epsilon.PICO + this.accuracy)) {
			c0 = relC;
			c1 = relC + this.accuracy / 2.;
		} else if (relC >= (1. - Epsilon.PICO - this.accuracy)) {
			c0 = relC - this.accuracy / 2.;
			c1 = relC;
		} else {
			c0 = relC - this.accuracy / 2.;
			c1 = relC + this.accuracy / 2.;
		}
		double y0 = getRelYMeanLine(c0);
		double y1 = getRelYMeanLine(c1);
		return Math.atan2((y1 - y0), (c1 - c0));
	}

	/**
	 * @return the maxRelCamber
	 */
	public double getMaxRelCamber() {
		return this.maxRelCamber;
	}

	/**
	 * @param maxRelCamber
	 *            the maxRelCamber to set
	 */
	public void setMaxRelCamber(double maxRelCamber_) {
		this.maxRelCamber = maxRelCamber_;
	}

	/**
	 * @return the maxRelThickness
	 */
	public double getMaxRelThickness() {
		return this.maxRelThickness;
	}

	/**
	 * @param maxRelThickness
	 *            the maxRelThickness to set
	 */
	public void setMaxRelThickness(double maxRelThickness_) {
		this.maxRelThickness = maxRelThickness_;
	}

	/**
	 * @return the currentDenominator
	 */
	public String getCurrentDenominator() {
		return this.currentDenominator;
	}

	/**
	 * @param currentDenominator
	 *            the currentDenominator to set
	 */
	public void setCurrentDenominator(String currentDenominator_) {
		this.currentDenominator = currentDenominator_;
	}

	/**
	 * @return the distLEMaxCamber
	 */
	public double getDistLEMaxCamber() {
		return this.distLEMaxCamber;
	}

	/**
	 * @param distLEMaxCamber
	 *            the distLEMaxCamber to set
	 */
	public void setDistLEMaxCamber(double distLEMaxCamber_) {
		this.distLEMaxCamber = distLEMaxCamber_;
	}

	/**
	 * @param profileName
	 *            the profileName to set
	 */
	public void setProfileName(String profileName_) {
		this.profileName = profileName_;
	}

	/**
	 * @return the accuracy
	 */
	public double getAccuracy() {
		return this.accuracy;
	}

	/**
	 * @param accuracy
	 *            the accuracy to set
	 */
	public void setAccuracy(double accuracy_) {
		this.accuracy = accuracy_;
	}
}
