package de.hykops.lpd.core;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

import de.hykops.lpd.core.factory.IHykopsFactoryService;

public class Activator implements BundleActivator {

	public static final String PLUGIN_ID = "de.hykops.lpd.core";
	
	private static BundleContext context;
	private static IHykopsFactoryService factoryService;

	@Override
	public void start(BundleContext bundleContext) throws Exception {
		Activator.context = bundleContext;
		ServiceReference<IHykopsFactoryService> reference = context.getServiceReference(IHykopsFactoryService.class);
		Activator.factoryService = context.getService(reference);
	}

	@Override
	public void stop(BundleContext bundleContext) throws Exception {
		Activator.context = null;
	}

	public static BundleContext getBundleContext() {
		return Activator.context;
	}

	public static IHykopsFactoryService getFactoryService() {
		return factoryService;
	}
}
