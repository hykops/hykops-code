package de.hykops.lpd.core.serialize;

import de.hykops.lpd.datamodel.HComposition;

public interface IDeserializer {

	HComposition deserialize(String xml);
}
