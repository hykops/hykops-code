package de.hykops.lpd.core.serialize;

import de.hykops.lpd.datamodel.HComposition;

public interface ISerializer {
	
	String serialize(HComposition composition);

}
