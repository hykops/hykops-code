/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.lpd.core.sdk;

import de.hykops.lpd.core.Activator;
import de.hykops.lpd.datamodel.HCoordinateSystem3D;
import de.hykops.lpd.datamodel.HCoordinateSystem3DAffine;
import de.hykops.lpd.datamodel.HCoordinateSystem3DCartesian;
import de.hykops.lpd.datamodel.HCoordinateSystem3DCone;
import de.hykops.lpd.datamodel.HCoordinateSystem3DPolar;
import de.hykops.lpd.datamodel.HParent;
import de.hykops.lpd.datamodel.HPoint3D;

/**
 * 
 *
 * @author stoye
 * @since Jun 30, 2016
 */
public class CoordinateSystem3DSupport {

	public static HCoordinateSystem3D createCoordinateSystem3DCartesian(HParent parent, double origx1, double origx2, double origx3, double lx, double ly, double lz, double alpha, double beta,
			double gamma) {
		HCoordinateSystem3DCartesian cos = Activator.getFactoryService().getFactory().createCoordinateSystemCartesian();
		if (parent != null)
			cos.setHParent(parent);
		cos.setOrigin(Point3DSupport.createPoint3D(cos, origx1, origx2, origx3));
		cos.setX1(Point3DSupport.createPoint3D(cos, lx, ly, lz));
		cos.setE1(Point3DSupport.createPoint3D(cos, alpha, beta, gamma));
		return cos;
	}

	public static HCoordinateSystem3D createCoordinateSystem3DAffine(HParent parent, double origx1, double origx2, double origx3, double x1x, double x1y, double x1z, double x2x, double x2y,
			double x2z, double x3x, double x3y, double x3z) {

		HCoordinateSystem3DAffine cos = Activator.getFactoryService().getFactory().createCoordinateSystemAffine();
		if (parent != null)
			cos.setHParent(parent);
		cos.setOrigin(Point3DSupport.createPoint3D(cos, origx1, origx2, origx3));
		cos.setX1(Point3DSupport.createPoint3D(cos, x1x, x1y, x1z));
		cos.setX2(Point3DSupport.createPoint3D(cos, x2x, x2y, x2z));
		cos.setX3(Point3DSupport.createPoint3D(cos, x3x, x3y, x3z));
		return cos;
	}

	public static HCoordinateSystem3D createCoordinatesSystem3DAffine(HParent parent, HPoint3D orig, HPoint3D x1, HPoint3D x2, HPoint3D x3) {

		HCoordinateSystem3DAffine cos = Activator.getFactoryService().getFactory().createCoordinateSystemAffine();
		cos.setHParent(parent);
		cos.setOrigin(orig);
		cos.setX1(x1);
		cos.setX2(x2);
		cos.setX3(x3);
		return cos;
	}

	public static HCoordinateSystem3D createCoordinateSystem3DPolar(HParent parent, double origx1, double origx2, double origx3, double x1x, double x1y, double x1z, double radx, double rady,
			double radz) {

		HCoordinateSystem3DPolar cos = Activator.getFactoryService().getFactory().createCoordinateSystemPolar();
		cos.setHParent(parent);
		cos.setOrigin(Point3DSupport.createPoint3D(cos, origx1, origx2, origx3));
		cos.setAxialVector(Point3DSupport.createPoint3D(cos, x1x, x1y, x1z));
		cos.setRadialVector(Point3DSupport.createPoint3D(cos, radx, rady, radz));
		return cos;
	}

	public static HCoordinateSystem3D createCoordinatesSystem3DCone(HParent parent, double origx1, double origx2, double origx3, double x1x, double x1y, double x1z, double radx, double rady,
			double radz) {

		HCoordinateSystem3DCone cos = Activator.getFactoryService().getFactory().createCoordinateSystemCone();
		cos.setHParent(parent);
		cos.setOrigin(Point3DSupport.createPoint3D(cos, origx1, origx2, origx3));
		cos.setAxialVector(Point3DSupport.createPoint3D(cos, x1x, x1y, x1z));
		cos.setRadialVector(Point3DSupport.createPoint3D(cos, radx, rady, radz));
		return cos;
	}
}
