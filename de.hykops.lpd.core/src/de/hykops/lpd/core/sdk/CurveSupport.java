/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.lpd.core.sdk;

import de.hykops.lpd.core.Activator;
import de.hykops.lpd.core.nomenclature.EInterpolationSchemes;
import de.hykops.lpd.datamodel.HCurvePoint2D;
import de.hykops.lpd.datamodel.HDiscreteCurve2D;
import de.hykops.lpd.datamodel.HLoftElement;

/**
 * 
 *
 * @author stoye
 * @since Jun 30, 2016
 */
public class CurveSupport {

	/**
	 * Concatenates a list of points to a curve
	 * @param parent
	 * @param relP relP[0]=curve coordinate, relP[1]=coordinate1, relP[2]=coordinate2
	 * @return
	 *
	 * @author stoye
	 * @since Jun 30, 2016
	 */
	public static HDiscreteCurve2D createDiscreteCurve(HLoftElement parent, double relP[][]) {
		HDiscreteCurve2D curve = Activator.getFactoryService().getFactory().createDiscreteCurve2D();
		curve.setHParent(parent);
		curve.setHLoftElement(parent);
		curve.setInterpolationScheme(EInterpolationSchemes.LINEAR.toString());

		for (double[] point : relP) {
			double c = point[0];
			double relX = point[1];
			double relY = point[2];
						
			HCurvePoint2D p = Point2DSupport.createCurvePoint2D(curve, relX, relY, c);
			curve.getCurvePoints().add(p);
		}
		return curve;
	}	
}

