/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.lpd.core.sdk;

import java.util.HashSet;
import java.util.Set;

import de.hykops.lpd.core.Activator;
import de.hykops.lpd.datamodel.HCoordinateSystem3D;
import de.hykops.lpd.datamodel.HCurve2D;
import de.hykops.lpd.datamodel.HLoft;
import de.hykops.lpd.datamodel.HLoftElement;
import de.hykops.lpd.datamodel.HSeriesProfileCurve2D;
import de.hykops.lpd.datamodel.HSurfaceIdentifier;

/**
 * 
 *
 * @author stoye
 * @since Jun 30, 2016
 */
public class LoftElementSupport {

	/**
	 * 
	 * @param element
	 * @return "" if the profile series name of any HCurve2D in loft element
	 *         are different, otherwise the common profile denomination
	 *
	 * @author stoye
	 * @since Dec 1, 2016
	 */
	public static String getSingleSeriesInLoftElement(HLoftElement element) {
		String series = null;
		for (HCurve2D c : element.getHCurve2Ds()) {
			if (c instanceof HSeriesProfileCurve2D) {
				String d = ((HSeriesProfileCurve2D) c).getSeries();
				if (series == null) {
					series = d;
				} else {
					if (series.equals(d)) {
						// identical profiles
					} else {
						return ""; /*- different profile series e.g. for suction- and pressure side*/
					}
				}
			} else {
				return "";
			}
		}
		return series;
	}

	/**
	 * 
	 * @param element
	 * @param newProfileSeries
	 *            Set profile series of all curves in profile to given
	 *            value
	 * @author stoye
	 * @since Dec 1, 2016
	 */
	public static void setSingleSeriesInLoftElement(HLoftElement element, String newProfileSeries) {
		if (getSingleSeriesInLoftElement(element).length() > 0) {
			for (HCurve2D c : element.getHCurve2Ds()) {
				if (c instanceof HSeriesProfileCurve2D) {
					((HSeriesProfileCurve2D) c).setSeries(newProfileSeries);
				}
			}
		}
	}

	/**
	 * 
	 * @param element
	 * @return "" if the profile denominations of any HCurve2D in loft element
	 *         are different, otherwise the common profile denomination
	 *
	 * @author stoye
	 * @since Dec 1, 2016
	 */
	public static String getSingleProfileInLoftElement(HLoftElement element) {
		String denom = null;
		for (HCurve2D c : element.getHCurve2Ds()) {
			if (c instanceof HSeriesProfileCurve2D) {
				String d = ((HSeriesProfileCurve2D) c).getDenomination();
				if (denom == null) {
					denom = d;
				} else {
					if (denom.equals(d)) {
						// identical profiles
					} else {
						return ""; /*- different profile denominations e.g. for suction- and pressure side*/
					}
				}
			} else {
				return "";
			}
		}
		return denom;
	}

	/**
	 * 
	 * @param element
	 * @param newProfileDenomination
	 *            Set profile denominations of all curves in profile to given
	 *            value
	 * @author stoye
	 * @since Dec 1, 2016
	 */
	public static void setSingleProfileInLoftElement(HLoftElement element, String newProfileDenomination) {
		if (getSingleProfileInLoftElement(element).length() > 0) {
			for (HCurve2D c : element.getHCurve2Ds()) {
				if (c instanceof HSeriesProfileCurve2D) {
					((HSeriesProfileCurve2D) c).setDenomination(newProfileDenomination);
				}
			}
		}
	}

	public static HSurfaceIdentifier[] getConnectedSurfaceIdentifiers(HLoftElement element) {
		Set<HSurfaceIdentifier> idents = new HashSet<HSurfaceIdentifier>();
		for (HCurve2D c : element.getHCurve2Ds()) {
			idents.addAll(c.getHSurfaceIdentifiers());
		}
		return idents.toArray(new HSurfaceIdentifier[] {});
	}

	public static HSurfaceIdentifier[] getConnectedSurfaceIdentifiers(HCurve2D element) {
		return element.getHSurfaceIdentifiers().toArray(new HSurfaceIdentifier[] {});
	}

	public static HLoftElement createLoftElement(HLoft parent, HCoordinateSystem3D cos) {
		HLoftElement loftElement = Activator.getFactoryService().getFactory().createLoftElement();
		loftElement.setHParent(parent);
		loftElement.setHLoft(parent);
		loftElement.setLoftPlaneDefiningCoordinateSystem(cos);
		if(parent != null)
		{
			parent.getHLoftElements().add(loftElement);
		}
		return loftElement;
	}
}
