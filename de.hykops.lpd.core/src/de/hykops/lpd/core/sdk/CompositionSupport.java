/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.lpd.core.sdk;

import de.hykops.lpd.core.Activator;
import de.hykops.lpd.core.nomenclature.EUnitSystems;
import de.hykops.lpd.core.version.FrameworkVersion;
import de.hykops.lpd.datamodel.HComposition;

/**
 * 
 *
 * @author stoye
 * @since Jun 30, 2016
 */
public class CompositionSupport {

	public static HComposition createEmptyComposition(String name, String description) {
		HComposition composition = Activator.getFactoryService().getFactory().createComposition();
		composition.setName(name);
		composition.setDescription(description);
		composition.setHCoordinateSystem3D(CoordinateSystem3DSupport.createCoordinateSystem3DAffine(composition, 0., 0., 0., 1., 0., 0., 0., 1., 0., 0., 0., 1.));
		composition.setUnitSystem(EUnitSystems.SI.toString());
		composition.setVersion(FrameworkVersion.VERSION);
		return composition;
	}
}

