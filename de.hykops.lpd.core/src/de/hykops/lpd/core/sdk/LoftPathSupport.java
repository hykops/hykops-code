/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.lpd.core.sdk;

import de.fsg.tfe.rde.logging.LogLevel;
import de.fsg.tfe.rde.logging.RDELogger;
import de.hykops.lpd.core.Activator;
import de.hykops.lpd.datamodel.HCoordinateSystem3D;
import de.hykops.lpd.datamodel.HDiscreteLoftPath;
import de.hykops.lpd.datamodel.HLinearLoftPath;
import de.hykops.lpd.datamodel.HLoftElement;
import de.hykops.lpd.datamodel.HLoftPath;

/**
 * 
 *
 * @author stoye
 * @since Jul 4, 2016
 */
public class LoftPathSupport {

	public static HCoordinateSystem3D getTemporaryLoftCoordinateSystem(HLoftElement element) {
		HLoftPath path = element.getHLoft().getHLoftPath();
		return getTemporaryLoftCoordinateSystem(path);
	}

	public static HCoordinateSystem3D getTemporaryLoftCoordinateSystem(HLoftPath path) {
		if (path instanceof HLinearLoftPath) {
			HLinearLoftPath lPath = (HLinearLoftPath) path;
			double origx1 = lPath.getOrigin().getX1();
			double origx2 = lPath.getOrigin().getX2();
			double origx3 = lPath.getOrigin().getX3();

			double x1x = lPath.getLoftPathXi().getX1();
			double x1y = lPath.getLoftPathXi().getX2();
			double x1z = lPath.getLoftPathXi().getX3();

			double x2x = lPath.getLoftPathEta().getX1();
			double x2y = lPath.getLoftPathEta().getX2();
			double x2z = lPath.getLoftPathEta().getX3();

			double x3x = lPath.getLoftPathVector().getX1();
			double x3y = lPath.getLoftPathVector().getX2();
			double x3z = lPath.getLoftPathVector().getX3();

			HCoordinateSystem3D cos = CoordinateSystem3DSupport.createCoordinateSystem3DAffine(null, origx1, origx2, origx3, x1x, x1y, x1z, x2x, x2y, x2z, x3x, x3y, x3z);
			return cos;
		} else if (path instanceof HDiscreteLoftPath) {
			RDELogger.log(LogLevel.ERROR, Activator.getBundleContext(), "Hier wird klar, dass der LoftPath durch ein weiteres affines Koordinatensystem ersetzt werden muss");
		}
		RDELogger.log(LogLevel.ERROR, Activator.getBundleContext(), "Hier wird klar, dass der LoftPath durch ein weiteres affines Koordinatensystem ersetzt werden muss");
		return null;
	}
}
