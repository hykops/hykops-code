/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.lpd.core.sdk;

import de.fsg.tk.math.constants.Epsilon;
import de.hykops.lpd.core.Activator;
import de.hykops.lpd.datamodel.HParent;
import de.hykops.lpd.datamodel.HPoint3D;

/**
 * @author stoye
 * @since Jun 30, 2016
 */
public class Point3DSupport {

	public static HPoint3D createPoint3D(HParent parent, double x1, double x2, double x3) {
		HPoint3D p = Activator.getFactoryService().getFactory().createPoint3D();
		p.setHParent(parent);
		p.setX1(x1);
		p.setX2(x2);
		p.setX3(x3);
		return p;
	}

	public static double[] getVector(HPoint3D p) {
		return new double[] { p.getX1(), p.getX2(), p.getX3() };
	}

	public static double getLength(HPoint3D p) {
		return Math.sqrt(Math.pow(p.getX1(), 2.) + Math.pow(p.getX2(), 2.) + Math.pow(p.getX3(), 2.));
	}

}
