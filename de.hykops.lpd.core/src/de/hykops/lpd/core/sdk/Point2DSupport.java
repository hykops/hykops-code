/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.lpd.core.sdk;

import de.hykops.lpd.core.Activator;
import de.hykops.lpd.datamodel.HCurvePoint2D;
import de.hykops.lpd.datamodel.HParent;
import de.hykops.lpd.datamodel.HPoint2D;

/**
 * 
 *
 * @author stoye
 * @since Jun 30, 2016
 */
public class Point2DSupport {
	public static HPoint2D createPoint2D(HParent parent, double x1, double x2) {
		HPoint2D p = Activator.getFactoryService().getFactory().createPoint2D();
		p.setHParent(parent);
		p.setX1(x1);
		p.setX2(x2);
		return p;
	}

	public static HCurvePoint2D createCurvePoint2D(HParent parent, double x1, double x2, double coordinate) {
		HCurvePoint2D p = Activator.getFactoryService().getFactory().createCurvePoint2D();
		p.setHParent(parent);
		p.setX1(x1);
		p.setX2(x2);
		p.setCurveCoordinate(coordinate);
		return p;
	}

}
