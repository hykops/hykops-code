package de.hykops.lpd.core.sdk;

import de.hykops.lpd.datamodel.HLoftElement;
import de.hykops.lpd.datamodel.HLoftPath;

public interface ICoordinateTransformation {

	/**
	 * Transform point in loft path coordinates to global coordinate
	 * 
	 * @param path
	 *            the loft path
	 * @param xi
	 *            coordinate 1 perpendicular to loft axis
	 * @param eta
	 *            coordinate 2 perpendicular to loft axis
	 * @param loft
	 *            coordinate the loft coordinate
	 * @return global cartesian point coordinates
	 *
	 * @author stoye
	 * @since Jul 18, 2016
	 */
	double[] toGlobalCoordinate(HLoftPath path, double xi, double eta, double loftCoordinate);

	/**
	 * Transform point on loft element to global coordinate
	 * 
	 * @param element
	 *            the loft element
	 * @param xi
	 *            coordinate 1 in loft plane
	 * @param eta
	 *            coordinate 2 in loft plane
	 * @return global cartesian point coordinates
	 *
	 * @author stoye
	 * @since Jul 18, 2016
	 */
	double[] toGlobalCoordinate(HLoftElement element, double xi, double eta);

	/**
	 * Transform point in global coordinates to loft plane coordinate point
	 * 
	 * @param element
	 *            the loft element
	 * @param p
	 *            point in global coordinates
	 * @return point in loft plane coordinates
	 *
	 * @author stoye
	 * @since Jul 18, 2016
	 */
	public double[] toLocalCoordinate(HLoftElement element, double p[]);

}