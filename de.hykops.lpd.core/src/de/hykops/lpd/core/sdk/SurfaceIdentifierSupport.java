/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.lpd.core.sdk;

import java.util.ArrayList;
import java.util.List;

import de.hykops.lpd.core.Activator;
import de.hykops.lpd.core.nomenclature.ESurfaceIdentifiers;
import de.hykops.lpd.datamodel.HCurve2D;
import de.hykops.lpd.datamodel.HLoft;
import de.hykops.lpd.datamodel.HLoftElement;
import de.hykops.lpd.datamodel.HParent;
import de.hykops.lpd.datamodel.HSurfaceIdentifier;

/**
 * 
 *
 * @author stoye
 * @since Jun 30, 2016
 */
public class SurfaceIdentifierSupport {

	public static HSurfaceIdentifier createHSurfaceIdentifier(HParent parent, ESurfaceIdentifiers ident) {
		HSurfaceIdentifier identifier = Activator.getFactoryService().getFactory().createSurfaceIdentifier();
		identifier.setHParent(parent);
		identifier.setName(ident.toString());
		return identifier;
	}

	public static List<HCurve2D> getLoftedCurves(HLoft loft, HSurfaceIdentifier identifier) {
		List<HCurve2D> curves = new ArrayList<HCurve2D>();
		for (HLoftElement se : loft.getHLoftElements()) {
			for (HCurve2D c : se.getHCurve2Ds()) {
				if (c.getHSurfaceIdentifiers().contains(identifier)) {
					curves.add(c);
				}
			}
		}
		return curves;
	}

	public static HSurfaceIdentifier getSurfaceIdentifierInstance(HLoft loft, ESurfaceIdentifiers ident) {
		for (HSurfaceIdentifier id : loft.getHSurfaceIdentifications()) {
			if (id.getName().equals(ident.toString())) {
				return id;
			}
		}
		return null;
	}
}
