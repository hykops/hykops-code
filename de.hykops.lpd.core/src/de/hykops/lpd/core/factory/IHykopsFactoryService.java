package de.hykops.lpd.core.factory;

public interface IHykopsFactoryService {

	public IHykopsFactory getFactory();
	
}
