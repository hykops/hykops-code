package de.hykops.lpd.core.factory;

import de.hykops.lpd.core.sdk.ICoordinateTransformation;
import de.hykops.lpd.core.serialize.IDeserializer;
import de.hykops.lpd.core.serialize.ISerializer;
import de.hykops.lpd.datamodel.HComponent;
import de.hykops.lpd.datamodel.HComposition;
import de.hykops.lpd.datamodel.HCoordinateSystem3DAffine;
import de.hykops.lpd.datamodel.HCoordinateSystem3DCartesian;
import de.hykops.lpd.datamodel.HCoordinateSystem3DCone;
import de.hykops.lpd.datamodel.HCoordinateSystem3DPolar;
import de.hykops.lpd.datamodel.HCurvePoint2D;
import de.hykops.lpd.datamodel.HDiscreteCurve2D;
import de.hykops.lpd.datamodel.HLinearLoftPath;
import de.hykops.lpd.datamodel.HLoft;
import de.hykops.lpd.datamodel.HLoftElement;
import de.hykops.lpd.datamodel.HLoftPath;
import de.hykops.lpd.datamodel.HPoint2D;
import de.hykops.lpd.datamodel.HPoint3D;
import de.hykops.lpd.datamodel.HSeriesProfileCurve2D;
import de.hykops.lpd.datamodel.HSurfaceIdentifier;

public interface IHykopsFactory {
	
	public HComposition createComposition();
	
	public HComponent createComponent();

	public HCoordinateSystem3DCartesian createCoordinateSystemCartesian();

	public HCoordinateSystem3DAffine createCoordinateSystemAffine();
	
	public HCoordinateSystem3DPolar createCoordinateSystemPolar();
	
	public HCoordinateSystem3DCone createCoordinateSystemCone();

	public HLoft createLoft();
	
	public HLoftElement createLoftElement();
	
	public HLoftPath createLoftPath();

	public HLinearLoftPath createLinearLoftPath();

	public HSeriesProfileCurve2D createSeriesProfileCurve2D();

	public HDiscreteCurve2D createDiscreteCurve2D();

	public HPoint2D createPoint2D();

	public HCurvePoint2D createCurvePoint2D();

	public HPoint3D createPoint3D();

	public HSurfaceIdentifier createSurfaceIdentifier();
	
	public ISerializer createSerializer();
	
	public IDeserializer createDeserializer();
	
	public ICoordinateTransformation createCoordinateTransformation();
	
}
