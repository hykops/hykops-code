/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.lpd.ui.vtk.sdk;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;

import de.hykops.lpd.core.Activator;
import de.hykops.lpd.core.nomenclature.EMapKeys;
import de.hykops.lpd.core.sdk.SurfaceIdentifierSupport;
import de.hykops.lpd.datamodel.HComponent;
import de.hykops.lpd.datamodel.HCurve2D;
import de.hykops.lpd.datamodel.HLoft;
import de.hykops.lpd.datamodel.HLoftElement;
import de.hykops.lpd.datamodel.HPoint3D;
import de.hykops.lpd.datamodel.HSurfaceIdentifier;
import de.hykops.profile.sdk.ProfileSupport;
import vtk.vtkActor;
import vtk.vtkAppendPolyData;
import vtk.vtkCellArray;
import vtk.vtkCleanPolyData;
import vtk.vtkPoints;
import vtk.vtkPolyData;
import vtk.vtkPolyDataMapper;
import vtk.vtkTransform;
import vtk.vtkTransformPolyDataFilter;
import vtk.vtkTubeFilter;
import vtk.fsg.vtkAbstractView.EScalarMode;
import vtk.fsg.vtkSupport;

/**
 * 
 *
 * @author stoye
 * @since Jul 1, 2016
 */
public final class HYKOPSVTKSupport {
	public static final double relLineThickness = 1. / 50.;

	// no instance, only support class
	private HYKOPSVTKSupport() {
	}

	public static Collection<vtkActor> createGlobalCoordinateSystem(double orig[]) {
		Set<vtkActor> axes = new HashSet<vtkActor>();
		double r = 2.5;

		vtkPoints points;
		vtkCellArray cells;
		vtkPolyData polyData;
		vtkCleanPolyData cleaner; // = new vtkCleanPolyData();

		vtkTubeFilter filter;
		double c[] = new double[3];
		double col[] = new double[3];

		for (int i = 0; i < 3; i++) {
			for (int j = 0; j < 3; j++) {
				c[j] = (i == j) ? 1. : 0.;
				col[j] = (i == j) ? .9 : 0.1;
			}

			points = new vtkPoints();
			cells = new vtkCellArray();
			cells.InsertNextCell(2);
			cells.InsertCellPoint(points.InsertNextPoint(orig[0], orig[1], orig[2]));
			cells.InsertCellPoint(points.InsertNextPoint(+r * c[0] + orig[0], +r * c[1] + orig[1], +r * c[2] + orig[2]));

			polyData = new vtkPolyData();
			polyData.SetPoints(points);
			polyData.SetLines(cells);

			cleaner = new vtkCleanPolyData();
			cleaner.SetInputData(polyData);

			vtkPolyDataMapper mapper = new vtkPolyDataMapper();
			mapper.SetInputConnection(cleaner.GetOutputPort());

			filter = new vtkTubeFilter();
			filter.SetInputConnection(cleaner.GetOutputPort());
			filter.SetNumberOfSides(10);
			filter.SetRadius(r * relLineThickness);

			vtkActor actor = makeActor(filter.GetOutput(), 0, false);
			actor.GetProperty().SetDiffuseColor(col);
			actor.SetMapper(mapper);
			axes.add(actor);
		}

		return axes;
	}

	/**
	 * Curvilinear coordinate axes
	 * 
	 * @param element
	 * @return
	 *
	 * @author stoye
	 * @since Jul 14, 2016
	 */
	public static Collection<vtkActor> createCoordinateSystem(HLoftElement element) {
		Set<vtkActor> axes = new HashSet<vtkActor>();
		double f = .1;

		vtkPoints points;
		vtkCellArray cells;
		vtkPolyData polyData;
		vtkCleanPolyData cleaner; // = new vtkCleanPolyData();

		vtkTubeFilter filter;
		double c[] = new double[3];
		double col[] = new double[3];

		for (int i = 0; i < 2; i++) {
			c[2] = 0.;
			col[2] = 0.;
			for (int j = 0; j < 2; j++) {
				c[j] = (i == j) ? 1. : 0.;
				col[j] = (i == j) ? .9 : 0.1;
			}

			int np = 10;

			points = new vtkPoints();
			cells = new vtkCellArray();
			cells.InsertNextCell(np);

			for (int ip = 0; ip < (np); ip++) {
				double d = ((double) ip) / ((double) (np - 1));
				double c2[] = Activator.getFactoryService().getFactory().createCoordinateTransformation().toGlobalCoordinate(element, d * f * c[0], d * f * c[1]);

				cells.InsertCellPoint(points.InsertNextPoint(c2[0], c2[1], c2[2]));
			}

			polyData = new vtkPolyData();
			polyData.SetPoints(points);
			polyData.SetLines(cells);

			cleaner = new vtkCleanPolyData();
			cleaner.SetInputData(polyData);

			vtkPolyDataMapper mapper = new vtkPolyDataMapper();
			mapper.SetInputConnection(cleaner.GetOutputPort());

			filter = new vtkTubeFilter();
			filter.SetInputConnection(cleaner.GetOutputPort());
			filter.SetNumberOfSides(10);
			filter.SetRadius(f * relLineThickness);

			vtkActor actor = makeActor(filter.GetOutput(), 0, false);
			actor.GetProperty().SetDiffuseColor(col);
			actor.SetMapper(mapper);
			axes.add(actor);
		}

		return axes;
	}

	static public vtkActor makeActor(final vtkPolyData polyData, final int smoothAngle, final boolean curvature) {
		return makeActor(polyData, smoothAngle, curvature, 0.7, 0.65, 0.26);
	}

	static public vtkActor makeActor(final vtkPolyData polyData, final int smoothAngle, final boolean curvature, double r, double g, double b) {
		vtkActor actor = vtkSupport.createActorFromPolyData(polyData, smoothAngle, curvature ? EScalarMode.CURVATURE : EScalarMode.NONE);
		actor.GetProperty().SetInterpolationToPhong();
		// actor.GetProperty().SetDiffuseColor(0.7, 0.65, 0.26);
		// actor.GetProperty().SetSpecularColor(1, 0.65, 0.26);
		actor.GetProperty().SetDiffuseColor(r, g, b);
		actor.GetProperty().SetSpecularColor(r, g, b);
		actor.GetProperty().SetAmbient(0.2);
		actor.GetProperty().SetDiffuse(0.7);
		actor.GetProperty().SetSpecular(0.8);
		actor.GetProperty().SetSpecularPower(40);
		return actor;
	}

	public static vtkPolyData copyAndRotatePolyData(vtkPolyData polyData) {
		vtkAppendPolyData appender = new vtkAppendPolyData();
		appender.UserManagedInputsOff();
		// for (int i = 0; i < nBlades; ++i) {
		vtkTransform transform = new vtkTransform();
		transform.RotateX(0. * 360. / 1.);
		vtkTransformPolyDataFilter rotate = new vtkTransformPolyDataFilter();
		rotate.SetTransform(transform);
		rotate.SetInputData(polyData);
		appender.AddInputConnection(rotate.GetOutputPort());
		// }
		appender.Update();
		return appender.GetOutput();
	}

	@SuppressWarnings("cast")
	public static vtkPolyData rotFreqPolyData(vtkPolyData polyData, double ox, double oy, double oz, double vx, double vy, double vz, int z) {
		vtkAppendPolyData appender = new vtkAppendPolyData();
		appender.UserManagedInputsOff();

		for (int i = 0; i < z; i++) {

			vtkTransform transform1 = new vtkTransform();
			transform1.Translate(-ox, -oy, -oz);
			vtkTransformPolyDataFilter preShift = new vtkTransformPolyDataFilter();
			preShift.SetInputData(polyData);
			preShift.SetTransform(transform1);
			preShift.Update();

			vtkTransform transform = new vtkTransform();
			transform.RotateWXYZ(((double) i) * 360. / ((double) z), vx, vy, vz);
			vtkTransformPolyDataFilter rotate = new vtkTransformPolyDataFilter();
			rotate.SetInputData(preShift.GetOutput()); // (polyData);
			rotate.SetTransform(transform);
			rotate.Update();

			vtkTransform transform2 = new vtkTransform();
			transform2.Translate(ox, oy, oz);
			vtkTransformPolyDataFilter postShift = new vtkTransformPolyDataFilter();
			postShift.SetInputData(rotate.GetOutput()); // (polyData);
			postShift.SetTransform(transform2);
			postShift.Update();

			appender.AddInputConnection(postShift.GetOutputPort());
		}
		appender.Update();
		return appender.GetOutput();
	}

	public static vtkPolyData mirrorPolyData(vtkPolyData polyData, double ox, double oy, double oz, boolean mx, boolean my, boolean mz) {
		vtkAppendPolyData appender = new vtkAppendPolyData();
		appender.UserManagedInputsOff();

		// keep original
		vtkTransform transform0 = new vtkTransform();
		transform0.Scale(1., 1., 1.);
		vtkTransformPolyDataFilter original = new vtkTransformPolyDataFilter();
		original.SetInputData(polyData);
		original.SetTransform(transform0);
		original.Update();
		appender.AddInputConnection(original.GetOutputPort());

		// create mirror image
		vtkTransform transform1 = new vtkTransform();
		transform1.Translate(-ox, -oy, -oz);
		vtkTransformPolyDataFilter preShift = new vtkTransformPolyDataFilter();
		preShift.SetInputData(polyData);
		preShift.SetTransform(transform1);
		preShift.Update();

		vtkTransform transform = new vtkTransform();
		transform.Scale(mx ? -1. : 1., my ? -1. : 1., mz ? -1. : 1.);
		vtkTransformPolyDataFilter mirror = new vtkTransformPolyDataFilter();
		mirror.SetInputData(preShift.GetOutput()); // (polyData);
		mirror.SetTransform(transform);
		mirror.Update();

		vtkTransform transform2 = new vtkTransform();
		transform2.Translate(ox, oy, oz);
		vtkTransformPolyDataFilter postShift = new vtkTransformPolyDataFilter();
		postShift.SetInputData(mirror.GetOutput()); // (polyData);
		postShift.SetTransform(transform2);
		postShift.Update();

		appender.AddInputConnection(postShift.GetOutputPort());

		appender.Update();
		return appender.GetOutput();

	}

	@SuppressWarnings("boxing")
	public static vtkPolyData createSurfacePolyData(HLoft loft, HSurfaceIdentifier ident) {
		vtkPoints points = new vtkPoints();
		vtkCellArray cells = new vtkCellArray();

		HCurve2D curves[] = SurfaceIdentifierSupport.getLoftedCurves(loft, ident).toArray(new HCurve2D[0]);

		int nv = 50;
		int nu = curves.length;

		double cv[] = new double[nv];
		for (int iv = 0; iv < nv; iv++) {
			cv[iv] = ((double) iv) / ((double) (nv - 1));
			cv[iv] = cv[iv] * cv[iv]; // quadratic cluster@LE
		}

		for (int iu = 0; iu < nu; ++iu) {
			HCurve2D c = curves[iu];
			for (int iv = 0; iv < nv; ++iv) {
				double p[] = ProfileSupport.getCartesianCoordinate(c, cv[iv]);
				int index = iv + iu * nv;
				if (p != null) {
					points.InsertPoint(index, p[0], p[1], p[2]);
				} else {
					points.InsertPoint(index, 0., 0., 0.);
				}
			}
		}
		for (int i = 0; i < (nu - 1); ++i) {
			for (int j = 0; j < (nv - 1); ++j) {
				cells.InsertNextCell(4);
				cells.InsertCellPoint(j + (i + 1) * nv);
				cells.InsertCellPoint(j + 1 + (i + 1) * nv);
				cells.InsertCellPoint(j + 1 + i * nv);
				cells.InsertCellPoint(j + i * nv);

			}
		}

		vtkPolyData polyData = new vtkPolyData();
		polyData.SetPoints(points);
		polyData.SetPolys(cells);

		
// map operations: Reihenfolge beachten! 
// 1. lofts, 2. components, 3. composition
		
		// loft map entries:
		if (loft.getParameters().containsKey(EMapKeys.ROTFREQ.toString())) {
			double rotAxis[] = new double[3];
			int z = 1;
			String value = (loft.getParameters().get(EMapKeys.ROTFREQ.toString()));
			if (value != null && value.length() > 0) {
				StringTokenizer st = new StringTokenizer(value);
				if (st.countTokens() == 4) {
					rotAxis[0] = Double.parseDouble(st.nextToken());
					rotAxis[1] = Double.parseDouble(st.nextToken());
					rotAxis[2] = Double.parseDouble(st.nextToken());
					z = Integer.parseInt(st.nextToken());
					HPoint3D orig = loft.getHComponent().getHCoordinateSystem3D().getOrigin();
					polyData = rotFreqPolyData(polyData, orig.getX1(), orig.getX2(), orig.getX3(), rotAxis[0], rotAxis[1], rotAxis[2], z);
				}
			}
		}
		
		// component map entries:
		HComponent component = loft.getHComponent();
		if (component.getParameters().containsKey(EMapKeys.MIRROR.toString())) {
			String value = component.getParameters().get(EMapKeys.MIRROR.toString());
			if (value != null && value.length() > 0) {
				StringTokenizer st = new StringTokenizer(value);
				if (st.countTokens() == 6) {
					double pMirror[] = new double[3];
					pMirror[0] = Double.parseDouble(st.nextToken());
					pMirror[1] = Double.parseDouble(st.nextToken());
					pMirror[2] = Double.parseDouble(st.nextToken());
					boolean mirrorX = Boolean.parseBoolean(st.nextToken());
					boolean mirrorY = Boolean.parseBoolean(st.nextToken());
					boolean mirrorZ = Boolean.parseBoolean(st.nextToken());
					polyData = mirrorPolyData(polyData, pMirror[0], pMirror[1], pMirror[2], mirrorX, mirrorY, mirrorZ);
				}
			}
		}
		
		
		return polyData;
	}

}
