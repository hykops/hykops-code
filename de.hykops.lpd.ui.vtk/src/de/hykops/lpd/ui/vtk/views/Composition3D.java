/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.lpd.ui.vtk.views;

import java.awt.Color;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.commands.operations.IUndoContext;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IMenuListener;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.swt.events.DisposeEvent;
import org.eclipse.swt.events.DisposeListener;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IActionBars;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.IPartListener;
import org.eclipse.ui.ISelectionListener;
import org.eclipse.ui.IViewSite;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.operations.UndoRedoActionGroup;

import de.fsg.tfe.rde.logging.LogLevel;
import de.fsg.tfe.rde.logging.RDELogger;
import de.fsg.tk.ui.basic.EUniqueColorProviderPalettes;
import de.fsg.tk.ui.basic.UniqueColorProvider;
import de.fsg.tk.ui.model.EMFNotifier;
import de.hykops.lpd.datamodel.HComponent;
import de.hykops.lpd.datamodel.HComposition;
import de.hykops.lpd.datamodel.HLoft;
import de.hykops.lpd.datamodel.HLoftElement;
import de.hykops.lpd.datamodel.HLoftPath;
import de.hykops.lpd.datamodel.HSurfaceIdentifier;
import de.hykops.lpd.ui.vtk.Activator;
import de.hykops.lpd.ui.vtk.sdk.HYKOPSVTKSupport;
import vtk.vtkActor;
import vtk.vtkPolyData;
import vtk.vtkProp;
import vtk.fsg.vtkAbstractView;

/**
 * 
 *
 * @author stoye
 * @since Jul 11, 2016
 */
@SuppressWarnings("restriction")
public class Composition3D extends vtkAbstractView implements ISelectionListener, IPartListener {

	public static final String ID = "de.hykops.lpd.ui.vtk.views.composition3D";
	private final static double xCamera = 30.;
	private static final String TAG_LOFTVIEW = "LOFT3D";
	private static final String TAG_SHOWCURVATURE = "chowCurvature";
	private Object rendererLock = new Object();
	private boolean showCurvature = false;

	private List<vtkProp> actors = new LinkedList<vtkProp>();
	private HComposition composition;
	private Collection<vtkActor> coordSysActor;

	protected EMFNotifier notifier = new EMFNotifier(500, Job.LONG) {

		@Override
		protected void notifyListeners() {
			refresh();
		}
	};

	public Composition3D() {
		UniqueColorProvider.setPalette(HLoftPath.class.toString(), EUniqueColorProviderPalettes.MOTLEY);
	}

	@Override
	public void createPartControl(Composite parent) {
		super.createPartControl(parent);
		initializeSelection();

		createViewPulldownMenu();
		getSite().getPage().addPartListener(this);
		this.notifier.setTimeOut(500);
		getPanel().GetRenderer().AddActor(new vtkActor());
		resetCamera(true);

		parent.addDisposeListener(new DisposeListener() {

			@Override
			public void widgetDisposed(DisposeEvent arg0) {
				getSite().getPage().removePostSelectionListener(Composition3D.this);
				getSite().getPage().removePartListener(Composition3D.this);
				setComposition(null);
			}
		});
	}

	private void createViewPulldownMenu() {
		IMenuManager menu = getViewSite().getActionBars().getMenuManager();
		menu.setRemoveAllWhenShown(true);
		menu.add(new Action("Fake") {
		});
		menu.addMenuListener(new IMenuListener() {

			@Override
			public void menuAboutToShow(IMenuManager manager) {
				Action curvatureAction = new Action("Show Curvature", IAction.AS_CHECK_BOX) {
					@Override
					public void run() {
						setShowCurvature(isChecked());
					}
				};
				curvatureAction.setChecked(Composition3D.this.showCurvature);
				manager.add(curvatureAction);
			}
		});
	}

	void setShowCurvature(boolean checked) {
		this.showCurvature = checked;
		refresh();
	}

	@Override
	protected void updateGraphic() {
		synchronized (this.rendererLock) {
			for (vtkProp actor : this.actors) {
				getPanel().GetRenderer().RemoveActor(actor);
			}

			this.actors.clear();

			this.coordSysActor = HYKOPSVTKSupport.createGlobalCoordinateSystem(new double[] { 0., 0., 0. });
			this.actors.addAll(this.coordSysActor);

			if (this.composition != null) {
				try {

					for (HComponent component : this.composition.getHComponents()) {
						for (HLoft loft : component.getHLofts()) {

							for (HLoftElement element : loft.getHLoftElements()) {
								Collection<vtkActor> loftElementCoordSystems = HYKOPSVTKSupport
										.createCoordinateSystem(element);
								this.actors.addAll(loftElementCoordSystems);
							}

							for (HSurfaceIdentifier ident : loft.getHSurfaceIdentifications()) {
								vtkPolyData loftPolyData = HYKOPSVTKSupport.createSurfacePolyData(loft, ident);
								vtkPolyData loftRotatedPolyData = HYKOPSVTKSupport.copyAndRotatePolyData(loftPolyData);
								int smoothImageAngleDeg = getSmoothImageAngleDeg();
								boolean showCurvature_ = getShowCurvature();

								Color color = UniqueColorProvider.getAWTColor(component, HComponent.class.toString());
								double r = (color.getRed()) / (255.);
								double g = (color.getGreen()) / (255.);
								double b = (color.getBlue()) / (255.);

								vtkActor loftActor = HYKOPSVTKSupport.makeActor(loftRotatedPolyData,
										smoothImageAngleDeg, showCurvature_, r, g, b);
								this.actors.add(loftActor);
							}
						}
					}
				} catch (Exception e) {
					RDELogger.log(LogLevel.ERROR, Activator.getBundleContext(), "Can't draw composition", e);
				}

				for (vtkProp actor : this.actors) {
					getPanel().GetRenderer().AddActor(actor);
				}
			}
		}
	}

	private boolean getShowCurvature() {
		return this.showCurvature;
	}

	@SuppressWarnings("boxing")
	@Override
	public void init(IViewSite site, IMemento memento) throws PartInitException {
		super.init(site, memento);
		if (memento == null) {
			return;
		}
		IMemento mem = memento.getChild(TAG_LOFTVIEW);
		if (mem == null) {
			return;
		}
		Integer val;
		// val = mem.getInteger(TAG_SHOWINTERPOLATEDPROFILES);
		// if (val != null) {
		// this.showInterpolatedProfiles = val != 0;
		// }
		// val = mem.getInteger(TAG_ROTATEPROPELLER);
		// if (val != null) {
		// this.rotatePropeller = val != 0;
		// }
		val = mem.getInteger(TAG_SHOWCURVATURE);
		if (val != null) {
			this.showCurvature = val != 0;
		}
	}

	@Override
	public void saveState(IMemento memento) {
		super.saveState(memento);
		IMemento mem = memento.createChild(TAG_LOFTVIEW);
		// mem.putInteger(TAG_SHOWINTERPOLATEDPROFILES,
		// this.showInterpolatedProfiles ? 1 : 0);
		// mem.putInteger(TAG_ROTATEPROPELLER, this.rotatePropeller ? 1 : 0);
		mem.putInteger(TAG_SHOWCURVATURE, this.showCurvature ? 1 : 0);
	}

	@Override
	public void selectionChanged(IWorkbenchPart part, ISelection selection) {

		if (selection instanceof IStructuredSelection) {
			IStructuredSelection ssel = (IStructuredSelection) selection;
			Object firstElement = ssel.getFirstElement();
			if (firstElement instanceof HComposition) {
				HComposition cmp = (HComposition) firstElement;

				if (cmp != composition) {
					setComposition(cmp);
				}
			}
		}

	}

	protected void setComposition(HComposition composition_) {
		if (composition_ == this.composition) {
			return;
		}

		this.notifier.removeAllObservedModels();
		this.composition = composition_;
		if (this.composition != null) {
			this.notifier.addObservedModel(this.composition);
			IActionBars bars = getViewSite().getActionBars();
			IEditorPart activeEditor = getSite().getPage().getActiveEditor();
			if (activeEditor != null) {
				IUndoContext undoContext = activeEditor.getAdapter(IUndoContext.class);
				if (undoContext != null) {
					new UndoRedoActionGroup(getSite(), undoContext, true).fillActionBars(bars);
				}
			}
		}
		refresh();
		resetCamera(false);
	}

	@Override
	protected void resetCamera(final boolean repositionCamera) {
		if (repositionCamera) {
			getPanel().GetRenderer().GetActiveCamera().SetPosition(xCamera, 0, 0);
			getPanel().GetRenderer().GetActiveCamera().SetViewUp(0, 0, 1);
			getPanel().GetRenderer().SetBackground(255., 255., 255.);
			getPanel().UpdateLight();
		}
		getPanel().redraw();
	}

	private HComposition getCompositionFromActiveEditor() {
		IEditorPart activeEditor = getSite().getPage().getActiveEditor();
		if (activeEditor != null) {
			return activeEditor.getAdapter(HComposition.class);
		}
		return null;
	}

	protected void initializeSelection() {
		getSite().getPage().addPostSelectionListener(this);
		selectionChanged(getSite().getPart(), getSite().getPage().getSelection());
		if (this.composition == null) {
			setComposition(getCompositionFromActiveEditor());
		}
		refresh();
	}

	@Override
	public void partActivated(IWorkbenchPart part) {}

	@Override
	public void partBroughtToTop(IWorkbenchPart part) {}

	@Override
	public void partClosed(IWorkbenchPart part) {
		IEditorPart activeEditor = getSite().getPage().getActiveEditor();
		if (activeEditor == null || activeEditor.getAdapter(HComposition.class) == null) {
			setComposition(null);
			refresh();
		}
	}

	@Override
	public void partDeactivated(IWorkbenchPart part) {}

	@Override
	public void partOpened(IWorkbenchPart part) {}

	/**
	 * @return the composition
	 */
	public HComposition getComposition() {
		return this.composition;
	}

}
