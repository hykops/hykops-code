/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.rudder.core.sdk;

import de.hykops.lpd.core.Activator;
import de.hykops.lpd.core.factory.IHykopsFactory;
import de.hykops.lpd.core.sdk.CoordinateSystem3DSupport;
import de.hykops.lpd.core.sdk.Point3DSupport;
import de.hykops.lpd.datamodel.HComponent;
import de.hykops.lpd.datamodel.HComposition;
import de.hykops.lpd.datamodel.HCoordinateSystem3D;
import de.hykops.lpd.datamodel.HLinearLoftPath;
import de.hykops.lpd.datamodel.HLoft;
import de.hykops.lpd.datamodel.HPoint3D;
import de.hykops.rudder.core.TypeID;

/**
 * 
 *
 * @author stoye
 * @since Sep 2, 2016
 */
public class RudderSupport {

	public static HComponent createEmptyRudder(String description, HComposition parent, double xref, double yref, double zref, double height) {
		IHykopsFactory factory = Activator.getFactoryService().getFactory();
		HComponent rudder = factory.createComponent();
		rudder.setHParent(parent);
		rudder.setName("Rudder");
		rudder.setType(TypeID.TYPE_ID);
		rudder.setDescription(description);
		// Coordinate system of the whole nozzle component: cartesian
		HCoordinateSystem3D cos = CoordinateSystem3DSupport.createCoordinateSystem3DAffine(rudder, /*-*/
				xref, yref, zref, /*-origin*/
				1., 0., 0., /*-x1 axis*/
				0., 1., 0., /*-x2 axis*/
				0., 0., 1.);/*-x3 axis*/
		rudder.setHCoordinateSystem3D(cos);
		HLoft shaft = factory.createLoft();
		rudder.getHLofts().add(shaft);

		shaft.setHParent(rudder);
		shaft.setName("rudder profile loft");
		shaft.setDescription("Default rudder profile loft");

		HCoordinateSystem3D cos2 = CoordinateSystem3DSupport.createCoordinateSystem3DAffine(rudder, /*-*/
				0., 0., 0., /*-origin*/
				1., 0., 0., /*-x1 axis*/
				0., 1., 0., /*-x2 axis*/
				0., 0., 1.);/*-x3 axis*/

		shaft.getHCoordinateSystems3D().add(cos2);

		// Set blade loft axis: Linear, z upwards
		HLinearLoftPath path = factory.createLinearLoftPath();
		path.setHParent(shaft);
		path.setHLoft(shaft);
		path.setDenominator("BLADE_LOFT");
		HCoordinateSystem3D cos3 = CoordinateSystem3DSupport.createCoordinateSystem3DAffine(path, /*-*/
				0., 0., 0., /*-origin*/
				1., 0., 0., /*-x1 axis*/
				0., 1., 0., /*-x2 axis*/
				0., 0., 1.);/*-x3 axis*/
		path.setHLoftCoordinateSystem(cos3);

		// origin of blade loft axis
		HPoint3D orig = Point3DSupport.createPoint3D(path, 0., 0., 0.);
		path.setOrigin(orig);
		// blade loft axis vector
		HPoint3D axis = Point3DSupport.createPoint3D(path, 0., 0., height);
		path.setLoftPathVector(axis);
		// blade loft perpendicular coordinate 1
		HPoint3D xi = Point3DSupport.createPoint3D(path, 1., 0., 0.);
		path.setLoftPathXi(xi);
		// blade loft perpendicular coordinate 2
		HPoint3D eta = Point3DSupport.createPoint3D(path, 0., 1., 0.);
		path.setLoftPathEta(eta);

		shaft.setHLoftPath(path);

		return rudder;
	}
}
