/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.rudder.core.sdk;

import de.hykops.lpd.core.sdk.CoordinateSystem3DSupport;
import de.hykops.lpd.core.sdk.Point3DSupport;
import de.hykops.lpd.datamodel.HComponent;
import de.hykops.lpd.datamodel.HCoordinateSystem3D;
import de.hykops.lpd.datamodel.HCoordinateSystem3DAffine;
import de.hykops.lpd.datamodel.HLinearLoftPath;
import de.hykops.lpd.datamodel.HLoftElement;
import de.hykops.lpd.datamodel.HLoftPath;
import de.hykops.lpd.datamodel.HPoint3D;

/**
 * 
 *
 * @author stoye
 * @since Sep 7, 2016
 */
public class RudderDataSupport {

	public static double getRudderHeight(HComponent component) {
		if (component.getHLofts().size() == 1) {
			HLoftPath path = component.getHLofts().get(0).getHLoftPath();
			if (path instanceof HLinearLoftPath) {
				HPoint3D p = ((HLinearLoftPath) path).getLoftPathVector();
				double height = p.getX3();
				return height;
			}
		}
		return 0.;
	}

	public static void setRudderHeight(HComponent component, double height) {
		if (component.getHLofts().size() == 1) {
			HLoftPath path = component.getHLofts().get(0).getHLoftPath();
			if (path instanceof HLinearLoftPath) {
				HPoint3D p = ((HLinearLoftPath) path).getLoftPathVector();
				p.setX3(height);
			}
		}
	}

	public static double getChordlength(HComponent component) {
		if (component.getHLofts().size() == 1) {
			HLoftPath path = component.getHLofts().get(0).getHLoftPath();
			if (path instanceof HLinearLoftPath) {
				{
					int np = component.getHLofts().get(0).getHLoftElements().size();
					double chord = 0.;

					if (np > 0) {
						for (int i = 0; i < np; i++) {
							HLoftElement element = component.getHLofts().get(0).getHLoftElements().get(i);
							double l = getChordlength(element);
							chord += l;
						}
						chord /= np; // average chord length
					}
					return chord;

				}
			}
		}
		return 0.;
	}
	
	public static void setChordlength(HComponent component, double chord) {
		double oldChord = getChordlength(component);
		double f = chord / oldChord;
		for (int i = 0; i < component.getHLofts().get(0).getHLoftElements().size(); i++) {
			HLoftElement element = component.getHLofts().get(0).getHLoftElements().get(i);
			double lChord = getChordlength(element);
			double newChord = lChord * f;
			setChordlength(element, newChord);
		}
	}

	public static double getChordlength(HLoftElement element) {
		HCoordinateSystem3D cos = element.getLoftPlaneDefiningCoordinateSystem();
		if (cos instanceof HCoordinateSystem3DAffine) {
			HPoint3D xi = ((HCoordinateSystem3DAffine) cos).getX1();
			double chord = Point3DSupport.getLength(xi);
			return chord;
		}
		return 0.;
	}

	public static void setChordlength(HLoftElement element, double chord) {
		double oldChord = getChordlength(element);
		double f = chord/oldChord;
		
		HCoordinateSystem3D cos = element.getLoftPlaneDefiningCoordinateSystem();
		HPoint3D horig = ((HCoordinateSystem3DAffine) cos).getOrigin();

		HPoint3D xi = ((HCoordinateSystem3DAffine) cos).getX1();
		double xi1 = xi.getX1()*f;
		double xi2 = xi.getX2()*f;
		double xi3 = xi.getX3()*f;
//		xi.setX1(xi1);
//		xi.setX2(xi2);
//		xi.setX3(xi3);
		
		HPoint3D eta = ((HCoordinateSystem3DAffine) cos).getX2();
		double eta1 = eta.getX1()*f;
		double eta2 = eta.getX2()*f;
		double eta3 = eta.getX3()*f;
//		eta.setX1(eta1);
//		eta.setX2(eta2);
//		eta.setX3(eta3);
		
		HPoint3D z = ((HCoordinateSystem3DAffine) cos).getX3();
		double z1 = z.getX1();
		double z2 = z.getX2();
		double z3 = z.getX3();
		
		HCoordinateSystem3D planeCos = CoordinateSystem3DSupport.createCoordinateSystem3DAffine(null, horig.getX1(), horig.getX2(), horig.getX3(), xi1, xi2, xi3, eta1, eta2, eta3, z1, z2, z3);
		planeCos.setHParent(element);
		element.setLoftPlaneDefiningCoordinateSystem(planeCos);

	}

	
}
