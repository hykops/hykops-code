#!/bin/bash

# Providing fallback for multicore compilation
NCORES=$1
if [[ $NCORES =~ ^-?[0-9]+$ ]]
then
    print $NCORES
else
    NCORES=$(cat /proc/cpuinfo | grep processor | tail -1 | awk '{print $3+1 }')
fi

ARGS="linux-x86_64"

# First argument is directory to build
# Second argument is optional: lib if it is a library
function build {
    cd $1

    qmake 
    make clean 
    make -j $NCORES

    if [[ $2 == "lib" ]]
    then
        ./postbuild.sh $NCORES $ARGS
    fi

    cd ..
}

build hykops_data lib

build serializeemf lib

build hykops_core lib

build hykops_tests
