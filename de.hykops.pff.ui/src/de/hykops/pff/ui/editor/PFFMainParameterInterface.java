/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.pff.ui.editor;

import java.util.ArrayList;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.NotificationImpl;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import de.fsg.tk.sdk.values.AbstractPhysicalDoubleValue;
import de.fsg.tk.ui.basic.ColorChooser;
import de.fsg.tk.ui.basic.UniqueColorProvider;
import de.fsg.tk.ui.widgets.BooleanValueWidget;
import de.fsg.tk.ui.widgets.IValueWidget;
import de.fsg.tk.ui.widgets.IntegerValueWidget;
import de.fsg.tk.ui.widgets.PhysicalValueWidget;
import de.fsg.tk.ui.widgets.ValueWidgetManager;
import de.hykops.lpd.datamodel.HComponent;
import de.hykops.lpd.ui.editor.component.ComponentMainParameterUIInterface;
import de.hykops.lpd.ui.editor.transformations.TransformationHelper;
import de.hykops.propeller.core.sdk.PropellerDataSupport;

/**
 * 
 *
 * @author stoye
 * @since Jul 22, 2016
 */
@SuppressWarnings("restriction")
public class PFFMainParameterInterface extends ComponentMainParameterUIInterface {

	de.fsg.tk.sdk.values.IPhysicalDoubleValue<HComponent>	diameterValue;
	PhysicalValueWidget<HComponent>							diameterWidget;
	de.fsg.tk.sdk.values.IPhysicalDoubleValue<HComponent>	designPitchValue;
	PhysicalValueWidget<HComponent>							designPitchRatioWidget;
	de.fsg.tk.sdk.values.IPhysicalDoubleValue<HComponent>	designPitchRatioValue;
	PhysicalValueWidget<HComponent>							designPitchWidget;
	de.fsg.tk.sdk.values.IPhysicalDoubleValue<HComponent>	designChordValue;
	PhysicalValueWidget<HComponent>							designChordWidget;
	de.fsg.tk.sdk.values.IPhysicalDoubleValue<HComponent>	designRakeValue;
	PhysicalValueWidget<HComponent>							designRakeWidget;
	de.fsg.tk.sdk.values.IPhysicalDoubleValue<HComponent>	designChiValue;
	PhysicalValueWidget<HComponent>							designChiWidget;
	de.fsg.tk.sdk.values.IPhysicalDoubleValue<HComponent>	designSkewValue;
	PhysicalValueWidget<HComponent>							designSkewWidget;
	de.fsg.tk.sdk.values.AbstractIntegerValue<HComponent>	nBladeValue;
	IntegerValueWidget<HComponent>							nBladeWidget;

	de.fsg.tk.sdk.values.IBooleanValue<HComponent> mirrorYValue;
	BooleanValueWidget<HComponent> mirrorYWidget;

	private ColorChooser									colorChooser;

	public PFFMainParameterInterface(Composite parent_) {
		super(parent_);
		this.mainParameterWidgets = new ArrayList<IValueWidget<?>>();
	}

	/* (non-Javadoc)
	 * @see de.hykops.lpd.ui.editor.component.IComponentMainParameterUIInterface#getHRComponentType()
	 */
	@Override
	public String getHRComponentType() {
		return de.hykops.propeller.core.TypeID.TYPE;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.hykops.lpd.ui.editor.component.IComponentMainParameterUIInterface#createControl()
	 */
	@Override
	public void createMainParameterControl() {

		Composite propComposite = new Composite(this.mainParameterComposite, SWT.BORDER);
		propComposite.setLayout(new GridLayout(2, true));
		propComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		this.diameterValue = new AbstractPhysicalDoubleValue<HComponent>(de.fsg.tk.sdk.units.Length.meter, "D", "Propeller diameter",
				"Propeller diameter", true) {
			@Override
			public double getValue(HComponent model) {
				return 2. * PropellerDataSupport.getBladeRadius(model);
			}

			@Override
			public void setValueImpl(HComponent model, double value) {
				EditingDomain eD = TransformationHelper.getEditingDomain();
				eD.getCommandStack().execute(new RecordingCommand((TransactionalEditingDomain) eD, "Set propeller diameter") {

					@Override
					protected void doExecute() {
						PropellerDataSupport.setBladeRadius(model, value / 2.);
						refreshMainParameterComposite();
					}
				});
			}
		};

		this.diameterWidget = new PhysicalValueWidget<HComponent>(propComposite, this.diameterValue, null);
		this.mainParameterWidgets.add(this.diameterWidget);

		this.designPitchValue = new AbstractPhysicalDoubleValue<HComponent>(de.fsg.tk.sdk.units.Angle.radian, "Phi", "Propeller design pitch angle",
				"Propeller design pitch angle at 0.7R", true) {
			@Override
			public double getValue(HComponent model) {
				return PropellerDataSupport.getPitchAngle(model, 0.7);
			}

			@Override
			public void setValueImpl(HComponent model, double value) {
				EditingDomain eD = TransformationHelper.getEditingDomain();
				eD.getCommandStack().execute(new RecordingCommand((TransactionalEditingDomain) eD, "Set propeller design pitch") {

					@Override
					protected void doExecute() {
						PropellerDataSupport.setPitchAngle(model, 0.7, value);
						PFFMainParameterInterface.this.designPitchRatioWidget.refresh();
						refreshMainParameterComposite();
					}
				});
			}
		};
		this.designPitchWidget = new PhysicalValueWidget<HComponent>(propComposite, this.designPitchValue, null);
		this.designPitchWidget.setUnit(de.fsg.tk.sdk.units.Angle.degree);
		this.mainParameterWidgets.add(this.designPitchWidget);

		this.designPitchRatioValue = new AbstractPhysicalDoubleValue<HComponent>(de.fsg.tk.sdk.units.One.one, "P/D", "Propeller design pitch ratio",
				"Propeller design pitch ratio at 0.7R", true) {
			@Override
			public double getValue(HComponent model) {
				return PropellerDataSupport.getPitchRatio(model, 0.7);
			}

			@Override
			public void setValueImpl(HComponent model, double value) {
				EditingDomain eD = TransformationHelper.getEditingDomain();
				eD.getCommandStack().execute(new RecordingCommand((TransactionalEditingDomain) eD, "Set propeller design pitch ratio") {

					@Override
					protected void doExecute() {
						PropellerDataSupport.setPitchRatio(model, 0.7, value);
						PFFMainParameterInterface.this.designPitchWidget.refresh();
						refreshMainParameterComposite();
					}
				});
			}
		};
		this.designPitchRatioWidget = new PhysicalValueWidget<HComponent>(propComposite, this.designPitchRatioValue, null);
		this.mainParameterWidgets.add(this.designPitchRatioWidget);

		this.designChordValue = new AbstractPhysicalDoubleValue<HComponent>(de.fsg.tk.sdk.units.Length.meter, "C", "Propeller design chord",
				"Propeller design chord length at 0.7R", false) {
			@Override
			public double getValue(HComponent model) {
				return PropellerDataSupport.getChordLength(model, 0.7);
			}
		};
		this.designChordWidget = new PhysicalValueWidget<HComponent>(propComposite, this.designChordValue, null);
		this.mainParameterWidgets.add(this.designChordWidget);

		this.designRakeValue = new AbstractPhysicalDoubleValue<HComponent>(de.fsg.tk.sdk.units.Length.meter, "RK", "Propeller design rake",
				"Propeller design rake at 0.7R", true) {
			@Override
			public double getValue(HComponent model) {
				return PropellerDataSupport.getRake(model, 0.7);
			}
			@Override
			public void setValueImpl(HComponent model, double value)
			{
				EditingDomain eD = TransformationHelper.getEditingDomain();
				eD.getCommandStack().execute(new RecordingCommand((TransactionalEditingDomain) eD, "Set propeller design pitch ratio") {

					@Override
					protected void doExecute() {
						PropellerDataSupport.setRake(model, 0.7, value);
						PFFMainParameterInterface.this.designRakeWidget.refresh();
						refreshMainParameterComposite();
					}
				});
			}
		};
		this.designRakeWidget = new PhysicalValueWidget<HComponent>(propComposite, this.designRakeValue, null);
		this.mainParameterWidgets.add(this.designRakeWidget);

		this.designChiValue = new AbstractPhysicalDoubleValue<HComponent>(de.fsg.tk.sdk.units.Length.meter, "CHI", "Propeller design chi",
				"Propeller design chi at 0.7R", false) {
			@Override
			public double getValue(HComponent model) {
				return PropellerDataSupport.getChi(model, 0.7);
			}
		};
		this.designChiWidget = new PhysicalValueWidget<HComponent>(propComposite, this.designChiValue, null);
		this.mainParameterWidgets.add(this.designChiWidget);

		this.designSkewValue = new AbstractPhysicalDoubleValue<HComponent>(de.fsg.tk.sdk.units.Angle.radian, "SKEW", "Propeller skew",
				"Propeller skew", false) {
			@Override
			public double getValue(HComponent model) {
				return PropellerDataSupport.getSkew(model, 0.7);
			}
		};
		this.designSkewWidget = new PhysicalValueWidget<HComponent>(propComposite, this.designSkewValue, null);
		this.designSkewWidget.setUnit(de.fsg.tk.sdk.units.Angle.degree);
		this.mainParameterWidgets.add(this.designSkewWidget);

		this.nBladeValue = new de.fsg.tk.sdk.values.AbstractIntegerValue<HComponent>("Z", "Z",
				"Number of blades", true) {

			@Override
			public int getValue(HComponent model) {
				return PropellerDataSupport.getBladeCount(model);
			}

			@Override
			public void setValueImpl(HComponent model, int bladeCount) {
				EditingDomain eD = TransformationHelper.getEditingDomain();
				eD.getCommandStack().execute(new RecordingCommand((TransactionalEditingDomain) eD, "Set propeller blade count") {
					@Override
					protected void doExecute() {
						PropellerDataSupport.setBladeCount(model, bladeCount);
						refreshMainParameterComposite();
					}
				});
			}
		};
		this.nBladeWidget = new IntegerValueWidget<HComponent>(propComposite, this.nBladeValue, null);
		this.mainParameterWidgets.add(this.nBladeWidget);

		this.mirrorYValue = de.hykops.lpd.ui.dataaccess.ComponentDataAdaptor.getMirrorY();
		this.mirrorYWidget = new BooleanValueWidget<HComponent>(propComposite, this.mirrorYValue, null);
		this.mainParameterWidgets.add(this.mirrorYWidget);
		
		this.colorChooser = new ColorChooser(propComposite, HComponent.class.toString()) {
			@Override
			protected void refreshAllViews() {
				((EObject) getReference()).eNotify(new NotificationImpl(Notification.NO_FEATURE_ID, 0, 0));
			}
		};

		ValueWidgetManager.align(this.mainParameterWidgets);
		this.mainParameterComposite.pack();
		this.mainParameterComposite.layout();
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void refreshMainParameterComposite() {
		super.refreshMainParameterComposite();

		HComponent model = getModel();

		Color col = UniqueColorProvider.getSWTColor(model, HComponent.class.toString());
		this.colorChooser.setColor(col, model);

		for (IValueWidget<?> w : this.mainParameterWidgets) {
			IValueWidget w2 = w;
			w2.setModel(model);
			w2.refresh();
		}
	}

}
