/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.pff.ui.editor;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;

import de.fsg.tk.sdk.values.AbstractPhysicalDoubleValue;
import de.fsg.tk.sdk.values.IValue;
import de.fsg.tk.sdk.values.IValueAdapter;
import de.hykops.lpd.datamodel.HLoftElement;
import de.hykops.lpd.ui.dataaccess.ProfileDataAdapter;
import de.hykops.lpd.ui.editor.transformations.TransformationHelper;
import de.hykops.propeller.core.sdk.ProfileDataSupport;

/**
 * 
 *
 * @author stoye
 * @since Nov 30, 2016
 */
public class PFFProfileDataAdapter implements IValueAdapter<HLoftElement> {

	public static AbstractPhysicalDoubleValue<HLoftElement> getProfileRelRadius() {
		return new AbstractPhysicalDoubleValue<HLoftElement>(de.fsg.tk.sdk.units.One.one, "r/R", "Rel. radius",
				"Relative profile radius", true) {
			@Override
			public double getValue(HLoftElement model) {
				return ProfileDataSupport.getRelRadius(model);
			}

			@Override
			public void setValueImpl(HLoftElement model, double value) {
				EditingDomain eD = TransformationHelper.getEditingDomain();
				eD.getCommandStack().execute(new RecordingCommand((TransactionalEditingDomain) eD, "Set rel. profile radius") {

					@Override
					protected void doExecute() {
						ProfileDataSupport.setRelRadius(model, value);
						// PFFMainParameterInterface.this.designPitchRatioWidget.refresh();
						// refreshMainParameterComposite();
					}
				});
			}
		};
	}

	public static AbstractPhysicalDoubleValue<HLoftElement> getProfilePitchAngle() {
		return new AbstractPhysicalDoubleValue<HLoftElement>(de.fsg.tk.sdk.units.Angle.radian, "Phi", "Profile pitch angle",
				"Profile pitch angle", true) {
			@Override
			public double getValue(HLoftElement model) {
				return ProfileDataSupport.getPitchAngle(model);
			}

			@Override
			public void setValueImpl(HLoftElement model, double value) {
				EditingDomain eD = TransformationHelper.getEditingDomain();
				eD.getCommandStack().execute(new RecordingCommand((TransactionalEditingDomain) eD, "Set profile pitch angle") {

					@Override
					protected void doExecute() {
						ProfileDataSupport.setPitchAngle(model, value);
						// PFFMainParameterInterface.this.designPitchRatioWidget.refresh();
						// refreshMainParameterComposite();
					}
				});
			}
		};
	}

	public static AbstractPhysicalDoubleValue<HLoftElement> getRelChordLength() {
		return new AbstractPhysicalDoubleValue<HLoftElement>(de.fsg.tk.sdk.units.One.one, "c/R", "Relative chord length",
				"Relative chord length", true) {
			@Override
			public double getValue(HLoftElement model) {
				return ProfileDataSupport.getRelChordLength(model);
			}

			@Override
			public void setValueImpl(HLoftElement model, double value) {
				EditingDomain eD = TransformationHelper.getEditingDomain();
				eD.getCommandStack().execute(new RecordingCommand((TransactionalEditingDomain) eD, "Set relative chord length") {
					@Override
					protected void doExecute() {
						ProfileDataSupport.setRelChordLength(model, value);
						// PFFMainParameterInterface.this.designPitchRatioWidget.refresh();
						// refreshMainParameterComposite();
					}
				});
			}
		};
	}

	public static AbstractPhysicalDoubleValue<HLoftElement> getRelRake() {
		return new AbstractPhysicalDoubleValue<HLoftElement>(de.fsg.tk.sdk.units.One.one, "rake/R", "Relative rake",
				"Relative rake", true) {
			@Override
			public double getValue(HLoftElement model) {
				return ProfileDataSupport.getRelProfileRake(model);
			}

			@Override
			public void setValueImpl(HLoftElement model, double value) {
				EditingDomain eD = TransformationHelper.getEditingDomain();
				eD.getCommandStack().execute(new RecordingCommand((TransactionalEditingDomain) eD, "Set relative rake") {
					@Override
					protected void doExecute() {
						ProfileDataSupport.setRake(model, value);
						// PFFMainParameterInterface.this.designPitchRatioWidget.refresh();
						// refreshMainParameterComposite();
					}
				});
			}
		};
	}

	public static AbstractPhysicalDoubleValue<HLoftElement> getSkewAngle() {
		return new AbstractPhysicalDoubleValue<HLoftElement>(de.fsg.tk.sdk.units.Angle.radian, "S", "Skew angle",
				"Skew angle", false) {
			@Override
			public double getValue(HLoftElement model) {
				return ProfileDataSupport.getSkewAngle(model);
			}
		};
	}

	public static AbstractPhysicalDoubleValue<HLoftElement> getAbsChordLength() {
		return new AbstractPhysicalDoubleValue<HLoftElement>(de.fsg.tk.sdk.units.Length.meter, "c", "Chord length",
				"Chord length", true) {
			@Override
			public double getValue(HLoftElement model) {
				return ProfileDataSupport.getChordLength(model);
			}

			@Override
			public void setValueImpl(HLoftElement model, double value) {
				EditingDomain eD = TransformationHelper.getEditingDomain();
				eD.getCommandStack().execute(new RecordingCommand((TransactionalEditingDomain) eD, "Set chord length") {
					@Override
					protected void doExecute() {
						ProfileDataSupport.setChordLength(model, value);
						// PFFMainParameterInterface.this.designPitchRatioWidget.refresh();
						// refreshMainParameterComposite();
					}
				});
			}
		};
	}

	public static AbstractPhysicalDoubleValue<HLoftElement> getAbsRake() {
		return new AbstractPhysicalDoubleValue<HLoftElement>(de.fsg.tk.sdk.units.Length.meter, "r", "Rake",
				"Rake", true) {
			@Override
			public double getValue(HLoftElement model) {
				return ProfileDataSupport.getRake(model);
			}

			@Override
			public void setValueImpl(HLoftElement model, double value) {
				EditingDomain eD = TransformationHelper.getEditingDomain();
				eD.getCommandStack().execute(new RecordingCommand((TransactionalEditingDomain) eD, "Set rake") {
					@Override
					protected void doExecute() {
						ProfileDataSupport.setRake(model, value);
						// PFFMainParameterInterface.this.designPitchRatioWidget.refresh();
						// refreshMainParameterComposite();
					}
				});
			}
		};
	}

	private static final String	RREL			= "r/R";
	private static final String SERIES			= "Series";
	private static final String	NAME			= "Name";
	private static final String	PITCHANGLE		= "pitch";
	private static final String	SKEWANGLE		= "skew";
	private static final String	RELCHORDLENGTH	= "c/R";
	private static final String	RELRAKE			= "rk/R";
	private static final String	CHORDLENGTH		= "c";
	private static final String	RAKE			= "rk";

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fsg.tk.sdk.values.IValueAdapter#getValueNames()
	 */
	@Override
	public List<String> getValueNames() {
		List<String> list = new ArrayList<String>();
		list.add(RREL);
		list.add(PITCHANGLE);
		list.add(SKEWANGLE);
		list.add(RELCHORDLENGTH);
		list.add(RELRAKE);
		list.add(CHORDLENGTH);
		list.add(RAKE);
		list.add(SERIES);
		list.add(NAME);
		return list;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fsg.tk.sdk.values.IValueAdapter#getValue(java.lang.String)
	 */
	@Override
	public IValue<HLoftElement> getValue(String string) {
		if (RREL.equals(string)) {
			return PFFProfileDataAdapter.getProfileRelRadius();
		} else if (PITCHANGLE.equals(string)) {
			return PFFProfileDataAdapter.getProfilePitchAngle();
		} else if (SKEWANGLE.equals(string)) {
			return PFFProfileDataAdapter.getSkewAngle();
		} else if (RELCHORDLENGTH.equals(string)) {
			return PFFProfileDataAdapter.getRelChordLength();
		} else if (RELRAKE.equals(string)) {
			return PFFProfileDataAdapter.getRelRake();
		} else if (CHORDLENGTH.equals(string)) {
			return PFFProfileDataAdapter.getAbsChordLength();
		} else if (RAKE.equals(string)) {
			return PFFProfileDataAdapter.getAbsRake();
		} else if (SERIES.equals(string)) {
			return ProfileDataAdapter.getProfileSeries();
		} else if (NAME.equals(string)) {
			return ProfileDataAdapter.getProfileName();
		}
		return null;
	}

}
