/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.pff.ui.sdk;

import de.fsg.tk.math.maps.Pair;
import de.fsg.tk.propeller.core.eval.profileseries.ProfileSeriesTools;
import de.fsg.tk.propeller.core.sdk.ProfileSide;
import de.fsg.tk.propeller.core.sdk.StationSupport;
import de.fsg.tk.propeller.datamodel.Profile;
import de.fsg.tk.propeller.datamodel.Propeller;
import de.fsg.tk.propeller.datamodel.Station;
import de.hykops.lpd.core.nomenclature.ESurfaceIdentifiers;
import de.hykops.lpd.core.sdk.CoordinateSystem3DSupport;
import de.hykops.lpd.core.sdk.CurveSupport;
import de.hykops.lpd.core.sdk.LoftElementSupport;
import de.hykops.lpd.core.sdk.SurfaceIdentifierSupport;
import de.hykops.lpd.datamodel.HComponent;
import de.hykops.lpd.datamodel.HComposition;
import de.hykops.lpd.datamodel.HCoordinateSystem3D;
import de.hykops.lpd.datamodel.HCurve2D;
import de.hykops.lpd.datamodel.HLoft;
import de.hykops.lpd.datamodel.HLoftElement;
import de.hykops.lpd.datamodel.HSurfaceIdentifier;
import de.hykops.profile.sdk.HCurveSupport;
import de.hykops.profile.sdk.SurfaceToProfileIdentifierMapping;
import de.hykops.profile.series.EProfileSide;
import de.hykops.profile.series.impl.NACA_ONE;
import de.hykops.propeller.core.sdk.PropellerSupport;
/**
 * 
 *
 * @author stoye
 * @since Jun 30, 2016
 */
@SuppressWarnings("restriction")
public class PropellerToHYKOPS {

	/**
	 * Create propeller component with discrete offsets
	 * 
	 * @param name
	 * @param parent
	 * @param propeller
	 *            PFF-style RDE-propeller data
	 * @return HYKOPS propeller component
	 *
	 * @author stoye
	 * @since Jul 18, 2016
	 */

	public static HComponent convertPropellerWithDiscreteOffsets(String name, HComposition parent, Propeller propeller) {
		double bladeRadius = propeller.getBladeGeometry().getBladeRadius();		
		// create empty component from support class
		HComponent prop = PropellerSupport.createEmptyPropeller(propeller.getDescription(), parent, propeller.getTemporaryData().getRefX(), propeller.getTemporaryData().getRefY(),
				propeller.getTemporaryData().getRefZ(), bladeRadius, propeller.getBladeCount());
		parent.getHComponents().add(prop);

		HLoft blade = prop.getHLofts().get(0);
		// HLoftPath axis =
		blade.getHLoftPath();
		// create suction- and pressure side identifiers
		for (Pair<EProfileSide, ESurfaceIdentifiers> p : SurfaceToProfileIdentifierMapping.sides) {
			HSurfaceIdentifier hident = SurfaceIdentifierSupport.createHSurfaceIdentifier(blade, p.getT2());
			blade.getHSurfaceIdentifications().add(hident); // containment
		}
		boolean clockwise = propeller.getBladeGeometry().isClockwiseOrientation();

		// add profiles
		for (Profile p : propeller.getBladeGeometry().getProfiles()) {
			// get profile characteristics

			double chord = p.getRelChordLength();
			double pitch = p.getPitch();
			double rake = p.getRelRake();
			double chi = p.getRelChi();

			// John Carlton, Marine Propellers and Propulsions: Eq. 3.17:
			double r = p.getRelRadius();
			double iG = -rake;
			double thetaNt = pitch; /// Math.atan2(pitch, (2. * Math.PI *r));
			double thetaS = Math.cos(thetaNt) * (chi - (chord / 2.)) / (r);
			double c = chord;

			// transform to HYKOPS coordinate systems

			// local coordinate system origin
			double polarOrig[] = PropellerSupport.convertPointToPolar3DCarlton(iG, r, thetaS,
					thetaNt, c, 0., 0., clockwise);
			// local coordinate system xi-axis endpoint
			double polarXi[] = PropellerSupport.convertPointToPolar3DCarlton(iG, r, thetaS,
					thetaNt, c, chord, 0., clockwise);
			// local coordinate system eta-axis endpoint
			double polarEta[] = PropellerSupport.convertPointToPolar3DCarlton(iG, r, thetaS,
					thetaNt, c, 0., -chord, clockwise);
			// local coordinate axes:
			// xi = chordline
			double xi[] = de.fsg.tk.math.vec.VecOp.sub(polarXi, polarOrig);
			// eta
			double eta[] = de.fsg.tk.math.vec.VecOp.sub(polarEta, polarOrig);
			// perpendicular vector
			double z1 = 0.;
			double z2 = 0.;
			double z3 = 1.;

			// create loft element (2D-)coordinate system
			HCoordinateSystem3D planeCos = CoordinateSystem3DSupport.createCoordinateSystem3DAffine(null, /*-*/
					polarOrig[0], polarOrig[1], polarOrig[2], /*-*/
					xi[0], xi[1], xi[2], /*-*/ //
					eta[0], eta[1], eta[2], /*-*/
					z1, z2, z3);/*-*/
			HLoftElement plane = LoftElementSupport.createLoftElement(blade, planeCos);
			planeCos.setHParent(plane);

			// add curves (suction- and pressure side)
			for (Pair<EProfileSide, ESurfaceIdentifiers> side : SurfaceToProfileIdentifierMapping.sides) {

				ProfileSide rdeSide = ProfileSide.valueOf(side.getT1().toString());
				
				int ns = p.getStations().size();
				double s[][] = new double[ns][3];
				// add offset points
				for (int is = 0; is < ns; is++) {
					Station st = p.getStations().get(is);
					s[is][0] = st.getRelChordPosition(); // c
					s[is][1] = st.getRelChordPosition(); // relX
					s[is][2] = StationSupport.getRelSurfaceOffset(st, rdeSide); // relY
				}
				HCurve2D curve = CurveSupport.createDiscreteCurve(plane, s);
				// set curve type (suction- or pressure side)
				HSurfaceIdentifier ident = SurfaceIdentifierSupport.getSurfaceIdentifierInstance(blade, side.getT2());
				if (ident != null) {
					blade.getHSurfaceIdentifications().add(ident); // containment
					curve.getHSurfaceIdentifiers().add(ident); // reference
					plane.getHCurve2Ds().add(curve);
				}
			}
		}
		return prop;
	}

	/**
	 * Create propeller component with profile series names
	 * 
	 * @param name
	 * @param parent
	 * @param propeller
	 *            PFF-style RDE-propeller data
	 * @return HYKOPS propeller component
	 *
	 * @author stoye
	 * @since Jul 18, 2016
	 */

	public static HComponent convertPropellerWithSeriesProfiles(String name, HComposition parent, Propeller propeller) {
		double bladeRadius = propeller.getBladeGeometry().getBladeRadius();
		// create empty component from support class
		HComponent prop = PropellerSupport.createEmptyPropeller(propeller.getDescription(), parent, propeller.getTemporaryData().getRefX(), propeller.getTemporaryData().getRefY(),
				propeller.getTemporaryData().getRefZ(), bladeRadius, propeller.getBladeCount());
//		HComponent prop = TipRakeSupport.createEmptyTipRakePropeller(propeller.getDescription(), parent, propeller.getTemporaryData().getRefX(), propeller.getTemporaryData().getRefY(),
//				propeller.getTemporaryData().getRefZ(), bladeRadius, propeller.getBladeCount(), Math.toRadians(30.));
		parent.getHComponents().add(prop);

		HLoft blade = prop.getHLofts().get(0);
		// HLoftPath axis =
		blade.getHLoftPath();
		// create suction- and pressure side identifiers
		for (Pair<EProfileSide, ESurfaceIdentifiers> p : SurfaceToProfileIdentifierMapping.sides) {
			HSurfaceIdentifier hident = SurfaceIdentifierSupport.createHSurfaceIdentifier(blade, p.getT2());
			blade.getHSurfaceIdentifications().add(hident); // containment
		}
		boolean clockwise = propeller.getBladeGeometry().isClockwiseOrientation();

		// add profiles
		for (Profile p : propeller.getBladeGeometry().getProfiles()) {
			// get profile characteristics

			double chord = p.getRelChordLength();
			double pitch = p.getPitch();
			double rake = p.getRelRake();
			double chi = p.getRelChi();

			// John Carlton, Marine Propellers and Propulsion: Eq. 3.17:
			double r = p.getRelRadius();
			double iG = -rake;
			double thetaNt = pitch; /// Math.atan2(pitch, (2. * Math.PI *r));
			double thetaS = Math.cos(thetaNt) * (chi - (chord / 2.)) / (r);
			double c = chord;

			// transform to HYKOPS coordinate systems

			// local coordinate system origin
			double polarOrig[] = PropellerSupport.convertPointToPolar3DCarlton(iG, r, thetaS,
					thetaNt, c, 0., 0., clockwise);
			// local coordinate system xi-axis endpoint
			double polarXi[] = PropellerSupport.convertPointToPolar3DCarlton(iG, r, thetaS,
					thetaNt, c, chord, 0., clockwise);
			// local coordinate system eta-axis endpoint
			double polarEta[] = PropellerSupport.convertPointToPolar3DCarlton(iG, r, thetaS,
					thetaNt, c, 0., -chord, clockwise);
			// local coordinate axes:
			// xi = chordline
			double xi[] = de.fsg.tk.math.vec.VecOp.sub(polarXi, polarOrig);
			// eta
			double eta[] = de.fsg.tk.math.vec.VecOp.sub(polarEta, polarOrig);
			// perpendicular vector
			double z1 = 0.;
			double z2 = 0.;
			double z3 = 1.;

			// create loft element (2D-)coordinate system
			HCoordinateSystem3D planeCos = CoordinateSystem3DSupport.createCoordinateSystem3DAffine(null, /*-*/
					polarOrig[0], polarOrig[1], polarOrig[2], /*-*/
					xi[0], xi[1], xi[2], /*-*/ //
					eta[0], eta[1], eta[2], /*-*/
					z1, z2, z3);/*-*/
			HLoftElement plane = LoftElementSupport.createLoftElement(blade, planeCos);
			planeCos.setHParent(plane);
//			blade.getHLoftElements().add(plane);
			
			// add curves (suction- and pressure side)
			for (Pair<EProfileSide, ESurfaceIdentifiers> side : SurfaceToProfileIdentifierMapping.sides) {
				// identify Curve with profile series name
				String profileDenominator = ProfileSeriesTools.getBestProfileIdentification(p).getName();
				String rdeSeries = ProfileSeriesTools.getBestProfileIdentification(p).getSeriesName();
				String series = rdeSeries; // besser hykops NACA_ONE.series
				
				HCurve2D curve = HCurveSupport.createSeriesProfile(plane, series, profileDenominator, side.getT1());

				HSurfaceIdentifier ident = SurfaceIdentifierSupport.getSurfaceIdentifierInstance(blade, side.getT2());
				if (ident != null) {
					blade.getHSurfaceIdentifications().add(ident); // containment
					curve.getHSurfaceIdentifiers().add(ident); // reference
					plane.getHCurve2Ds().add(curve);
				}
			}

		}
		return prop;
	}

}
