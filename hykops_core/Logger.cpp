#include "Logger.h"

#include <QFile>
#include <QDebug>



void Logger::log(QString message) {

    QFile file("/tmp/hykops.log");
    file.open(QIODevice::WriteOnly | QIODevice::Append);
    file.write(message.toUtf8() + "\n");
    file.close();
}
