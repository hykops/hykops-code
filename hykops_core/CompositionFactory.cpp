#include "CompositionFactory.h"
#include "HComponent.h"
#include "CoordinateSystem3DFactory.h"
#include "HCoordinateSystem3D.h"




CompositionFactory::CompositionFactory()
{

}

HComposition* CompositionFactory::create(QString name, QString description) {

    HComposition *composition = new HComposition;
    HComponent* component = new HComponent;
    composition->getHComponents()->append(component);
    component->setHParent(composition);
    HCoordinateSystem3D *cos = CoordinateSystem3DFactory::createCoordinatesSystem3DAffine(composition, 0., 0., 0., 1., 0., 0., 0., 1., 0., 0., 0., 1.);
    composition->setHCoordinateSystem3D(cos);


    return composition;
}

void create_composition(int *handle, char *name, char *description) {

    CompositionFactory::create(name, description);


}




