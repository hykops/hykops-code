#include "C_Interface.h"
#include "EFactory.h"
#include "EUtility.h"
#include "HComponent.h"
#include "serializedobject.h"
#include "serializedobject.h"
#include "CompositionSerializer.h"
#include "CoordinateTransformation.h"
#include "LoftElementSupport.h"

#include <QObject>
#include <QDebug>

#include "Logger.h"

Logger logger;

void init() {
    EFactory::init();
    logDateMessage(getDateMessage());
}

long create_object(const char *className) {
    logger.log("Create object: " + QString(className));
    return (long) EFactory::construct(className);
}

int delete_object(long handle) {
    logger.log(QString("delete_object: %1").arg(handle));
    QObject *object = (QObject*) handle;

    if(object) {
        delete object;
        return 0;
    }
    return 1;
}

void set_string(long handle, const char* property, const char *value) {
    logger.log(QString("set_string: %1 (%2) value: %3").arg(property).arg(handle).arg(value));
    set_property(handle, property, QVariant(value));
}

void set_int(long handle, const char *property, int value) {
    logger.log(QString("set_int: %1 (%2) value: %3").arg(property).arg(handle).arg(value));
    set_property(handle, property, QVariant(value));
}

void set_double(long handle, const char *property, double value) {
    logger.log(QString("set_double: %1 (%2) value: %3").arg(property).arg(handle).arg(value));
    set_property(handle, property, QVariant(value));
}

void set_object(long handle, const char *property, long object) {
    logger.log(QString("set_object: %1 (%2) value: %3").arg(property).arg(handle).arg(object));

    if(property == QString("hParent")) {
        QObject *obj = (QObject*)handle;
        QObject *parent = (QObject*)object;
        obj->setParent(parent);
    }

    EUtility::setObjectProperty(property, (QObject*)handle, (QObject*)object);
}

int get_string(long handle, const char *property, char *buffer, int bufLength) {
    logger.log(QString("get_string: %1 (%2)").arg(property).arg(handle));
    QString string = get_property(handle, property).toString();


    if(string.toUtf8().length() > bufLength) {
        return -1;
    }

    strcpy(buffer, string.toUtf8());
    return 0;
}

int get_int(long handle, const char *property) {
    logger.log(QString("get_int: %1 (%2)").arg(property).arg(handle));
    return get_property(handle, property).toInt();
}

double get_double(long handle, const char *property) {
    logger.log(QString("get_double: %1 (%2)").arg(property).arg(handle));
    return get_property(handle, property).toDouble();
}

int get_object(long handle, const char *property) {
    logger.log(QString("get_object: %1 (%2)").arg(property).arg(handle));
    return (long) EUtility::getObjectProperty(property, (QObject*)handle);
}

void set_list(long handle, const char *property, long *objects, int size) {
    QList<QObject*> *list = EUtility::getListObjectProperty(property, (QObject*)handle);
    list->clear();
    for(int i = 0; i < size; i++) {
        list->append((QObject*)objects[i]);
    }
}

int get_type(long handle, char *buffer, int bufferLength) {
    logger.log(QString("get_type: %1").arg(handle));
    QString type = ((QObject*) handle)->metaObject()->className();

    if(type.toUtf8().length() > bufferLength) {
        return -1;
    }

    strcpy(buffer, type.toUtf8());
    return 0;
}

int get_list(long handle, const char *property, long* object_array, int object_array_size) {
    logger.log(QString("get_list: %1 (%2)").arg(property).arg(handle));

    QList<QObject*> *list  = EUtility::getListObjectProperty(property, (QObject*)handle);

    int copyCounter = 0;
    for(int i = 0; i < object_array_size; i++) {
        if(i < list->size()) {
            object_array[i] = (long)list->at(i);
            copyCounter++;
        }
    }

    return copyCounter;
}

int get_list_size(long handle, const char *property) {
    logger.log(QString("get_list_size: %1 (%2)").arg(property).arg(handle));

    QList<QObject*> *list  = EUtility::getListObjectProperty(property, (QObject*)handle);
    return list->size();
}

int get_map_size(long handle, const char *property) {
    logger.log(QString("get_map_size: %1 (%2)").arg(property).arg(handle));

    QMap<QString, QString>* elements = EUtility::getMapObjectProperty(property, (QObject*)handle);
    return elements->size();
}

int get_map(long handle, const char *property, char **keys, char **values) {
    logger.log(QString("get_map: %1 (%2)").arg(property).arg(handle));

    QMap<QString, QString> *map = EUtility::getMapObjectProperty(property, (QObject*)handle);

    int index = 0;
    for(QMap<QString,QString>::iterator it = map->begin(); it != map->end();)
    {
        QByteArray key = it.key().toUtf8();
        QByteArray value = it.value().toUtf8();

        strcpy(keys[index], key.data());
        strcpy(values[index], value.data());
        index++; it++;
    }
    return index;
}

void set_map(long handle, const char *property, const char **keys, int keyLength[], const char **values, int valueLength[], int mapSize) {
    logger.log(QString("set_map: %1 (%2)").arg(property).arg(handle));

    QMap<QString, QString> *map = EUtility::getMapObjectProperty(property, (QObject*)handle);
    map->clear();
    for(int i = 0; i < mapSize; i++) {
        char* key = new char[keyLength[i]];
        char* value = new char[valueLength[i]];

        strcpy(key, keys[i]);
        strcpy(value, values[i]);

        map->insert(QString(key), QString(value));
    }
}

int get_serialized_string(long handle, char *xml) {
    logger.log("get_serialized_string");
    SerializedObject *ser = (SerializedObject*)handle;
    QByteArray ba = ser->getString().toUtf8();
    strcpy(xml, ba);

    return 0;
}

int get_serialized_text_size(long serialized_handle) {
    logger.log("get_serialized_text_size");
    SerializedObject *ser = (SerializedObject*)serialized_handle;
    return (int)ser->getString().toUtf8().size();
}

long serialize(long composition_handle) {
    logger.log("serialize");
    HComposition *cmp = (HComposition*)composition_handle;
    SerializedObject* serialized = CompostionSerializer::serialize(cmp);
    return (long)serialized;
}

int deserialize(long *composition_handle, char *xml) {
    logger.log("deserialize");
    HComposition *cmp = CompostionSerializer::deserialize(QString(xml));
    *composition_handle = (long)cmp;
    return 0;
}

/***
 * Coordinatesystem transformations
 */

int toGlobalCoordinate(long loftElementHandle, double xi, double eta, double global[]) {
    logger.log(QString("toGlobalCoordinate: %1").arg(loftElementHandle));

    HLoftElement *element = (HLoftElement*)loftElementHandle;
    QVector3D coords = CoordinateTransformation::toGlobalCoordinate(element, xi, eta);

    global[0] = coords.x();
    global[1] = coords.y();
    global[2] = coords.z();
    return 0;
}

int toGlobalCoordinatePath(long loftPathHandle, double xi, double eta, double loftCoordinate, double global[]) {
    logger.log(QString("toGlobalCoordinatePath: %1").arg(loftPathHandle));

    HLoftPath *path = (HLoftPath*)loftPathHandle;
    QVector3D coords = CoordinateTransformation::toGlobalCoordinate(path, xi, eta, loftCoordinate);

    global[0] = coords.x();
    global[1] = coords.y();
    global[2] = coords.z();
    return 0;
}

int toLocalCoordinate(long loftElementHandle, double global[], double local[]) {
    logger.log(QString("toLocalCoordinate: %1").arg(loftElementHandle));

    HLoftElement *loftElement = (HLoftElement*)loftElementHandle;
    QVector3D vector(global[0], global[1], global[2]);
    QVector3D result = CoordinateTransformation::toLocalCoordinate(loftElement, vector);

    local[0] = result.x();
    local[1] = result.y();
    local[2] = result.z();
    return 0;
}

/***
 * Loft curve interpolations
 */
int getXiEta(long curve2DHandle, double c, double xieta[]){
    HCurve2D* curve = (HCurve2D*)curve2DHandle;
// Curve2D interpolation in separate class
    QVector3D result = LoftElementSupport::getXiEta(curve, c);
    xieta[0] = result.x();
    xieta[1] = result.y();
    return 0;
}

// NON C functions

void logDateMessage(QString message) {
    qDebug() << message;
    logger.log(message);
}

QString getDateMessage() {
    return QString("This library was built on: %1 %2").arg(__DATE__).arg(__TIME__);
}

void set_property(long handle, const char* property, QVariant value) {

    QObject *obj = (QObject*) handle;
    obj->setProperty(property, value);
}

QVariant get_property(long handle, const char *property) {

    QObject *obj = (QObject*) handle;
    return obj->property(property);
}
