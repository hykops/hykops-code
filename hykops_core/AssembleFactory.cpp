#include "AssembleFactory.h"

#include "HCoordinateSystem3DAffine.h"
#include "HLinearLoftPath.h"
#include "HLoft.h"

HCoordinateSystem3D *AssembleFactory::createTemporaryLoftCoordinateSystem(HLoftPath *path) {
    if(HLinearLoftPath *lPath = dynamic_cast<HLinearLoftPath*>(path)) {


        double origx1 = lPath->getOrigin()->getX1();
        double origx2 = lPath->getOrigin()->getX2();
        double origx3 = lPath->getOrigin()->getX3();

        double x1x = lPath->getLoftPathXi()->getX1();
        double x1y = lPath->getLoftPathXi()->getX2();
        double x1z = lPath->getLoftPathXi()->getX3();

        double x2x = lPath->getLoftPathEta()->getX1();
        double x2y = lPath->getLoftPathEta()->getX2();
        double x2z = lPath->getLoftPathEta()->getX3();

        double x3x = lPath->getLoftPathVector()->getX1();
        double x3y = lPath->getLoftPathVector()->getX2();
        double x3z = lPath->getLoftPathVector()->getX3();

        HCoordinateSystem3D *cos = createCoordinatesSystem3DAffine(NULL, origx1, origx2, origx3, x1x, x1y, x1z, x2x, x2y, x2z, x3x, x3y, x3z);
        cos->setHParent(path);
        return cos;
    }
    return 0;
}



HCoordinateSystem3D *AssembleFactory::createTemporaryLoftCoordinateSystem(HLoftElement *element) {
    return createTemporaryLoftCoordinateSystem(element->getHLoft()->getHLoftPath());
}

HCoordinateSystem3D *AssembleFactory::createCoordinatesSystem3DAffine(HParent *parent, double origx1, double origx2, double origx3, double x1x, double x1y, double x1z, double x2x, double x2y, double x2z, double x3x, double x3y, double x3z) {

    HCoordinateSystem3DAffine *cos = new HCoordinateSystem3DAffine();
    if(parent != 0) cos->setHParent(parent);

    HPoint3D *origin = new HPoint3D;
    origin->setHParent(cos);
    origin->setX1(origx1);
    origin->setX2(origx2);
    origin->setX3(origx3);
    cos->setOrigin(origin);

    HPoint3D *x1 = new HPoint3D;
    x1->setHParent(cos);
    x1->setX1(x1x);
    x1->setX2(x1y);
    x1->setX3(x1z);
    cos->setX1(x1);

    HPoint3D *x2 = new HPoint3D;
    x2->setHParent(cos);
    x2->setX1(x2x);
    x2->setX2(x2y);
    x2->setX3(x2z);
    cos->setX2(x2);

    HPoint3D *x3 = new HPoint3D;
    x3->setHParent(cos);
    x3->setX1(x3x);
    x3->setX2(x3y);
    x3->setX3(x3z);
    cos->setX3(x3);
    return cos;
}
