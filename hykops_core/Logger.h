#ifndef LOGGER_H
#define LOGGER_H

#include <QString>

class Logger
{
public:
    Logger() { }
    void log(QString message);
};

#endif // LOGGER_H
