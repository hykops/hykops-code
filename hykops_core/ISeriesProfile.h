#ifndef ISERIESPROFILE_H
#define ISERIESPROFILE_H

#include <QVector3D>
#include <QMap>

#include "HCurve2D.h"
#include "HSeriesProfileCurve2D.h"

class ISeriesProfile
{
public:
    virtual QString getSeriesName() = 0;
    virtual QString getProfileName() = 0;
    virtual void getXiEta(HSeriesProfileCurve2D *curve, double c, QVector3D* qxieta) = 0;
};

#endif // ISERIESPROFILE_H
