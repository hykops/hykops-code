#ifndef LOFTELEMENTSUPPORT_H
#define LOFTELEMENTSUPPORT_H

#include "HCurve2D.h"

class LoftElementSupport
{
public:
    static QVector3D getXiEta(HCurve2D *curve, double c);
};

#endif // LOFTELEMENTSUPPORT_H
