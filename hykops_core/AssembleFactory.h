#ifndef ASSEMBLEFACTORY_H
#define ASSEMBLEFACTORY_H


#include <HCoordinateSystem3D.h>
#include <HLoftElement.h>
#include <HLoftPath.h>

class AssembleFactory
{
public:
    static HCoordinateSystem3D *createTemporaryLoftCoordinateSystem(HLoftPath *path);
    static HCoordinateSystem3D *createTemporaryLoftCoordinateSystem(HLoftElement *element);
    static HCoordinateSystem3D *createCoordinatesSystem3DAffine(HParent *parent, double origx1, double origx2, double origx3, double x1x, double x1y, double x1z, double x2x, double x2y, double x2z, double x3x, double x3y, double x3z);
};

#endif // ASSEMBLEFACTORY_H
