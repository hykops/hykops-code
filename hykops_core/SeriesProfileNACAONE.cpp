#define _USE_MATH_DEFINES

#include <cmath>
#include <math.h>
#include <stdio.h>

#include <QVector3D>

#include "SeriesProfileNACAONE.h"
#include "Epsilon.h"

#include "Logger.h"





/**
 * Constructor with profile name (e.g. NACA 16-1234)
 * @brief SeriesProfileNACAONE::SeriesProfileNACAONE
 * @param denominator_
 */

SeriesProfileNACAONE::SeriesProfileNACAONE(QString denominator_)
{
        initString(denominator_);
        currentDenominator = denominator_;
}

QString SeriesProfileNACAONE::getSeriesName()
{
    return seriesNameNACAONE;
}

QString SeriesProfileNACAONE::getProfileName()
{
    return profileName;
}

void SeriesProfileNACAONE::initString(QString denominator_)
{
    QString sChordMinPressure;
    QString sCamber;
    QString sThickness;
    if(denominator_.length() == 11)
    {
        sChordMinPressure = denominator_.mid(6,1);
        sCamber = denominator_.mid(8,1);
        sThickness = denominator_.mid(9,2);
    } else if(denominator_.length() == 12)
    {
        sChordMinPressure = denominator_.mid(6,1);
        sCamber = denominator_.mid(8,2);
        sThickness = denominator_.mid(10,2);
    } else
    {
        sChordMinPressure = QString("6");
        sCamber = QString("0");
        sThickness = QString("00");
    }
    double chordwiseMinPressure_ = sChordMinPressure.toDouble()/10.;
    double camber_ = sCamber.toDouble()/10.;
    double thickness_ = sThickness.toDouble()/100.;

    init(camber_, thickness_, chordwiseMinPressure_, DEFAULT_ACCURACY);
}

void SeriesProfileNACAONE::init(double camberCoefficient_, double maxRelThickness_, double chordwiseMinPressure_, double accuracy_)
{
    setCamberCoefficient(camberCoefficient_);
    setMaxRelThickness(maxRelThickness_);
    setChordwiseMinPressure(chordwiseMinPressure_);
    setAccuracy(accuracy_);
    eps = new Epsilon();

    for(int i = 0; i < np; i++)
    {
        b[i] = ((double)i)/((double)(np-1));
    }
    calculateDigits();
}

void SeriesProfileNACAONE::calculateDigits(){

    int i1 = (int) rint(getChordwiseMinPressure() * 10.);
    int i2 = (int) rint(getCamberCoefficient() * 10.);
    int i3 = (int) rint(getMaxRelThickness() * 100.);

    if (i1 > 9)
        i1 = 9;
    // if(i2 > 9) i2 = 9; // sometimes, max. camber is larger
    if (i3 > 99)
        i3 = 99;

    QString profileName;
    profileName.sprintf("1%1d-%1d%02d", i1, i2, i3);

    setProfileName(profileName);
}


void SeriesProfileNACAONE::getXiEta(HSeriesProfileCurve2D *curve, double c, QVector3D* qxieta)
{
    double relB = getRelB(c);
    double t = (getRelThicknessFromB(relB) / yOffsetFactor) * getMaxRelThickness();
    double yt = 0.5 * t;
    double yc = getRelYMeanLine(c);
    double phi = getRelYMeanLineAngle(c);

    double xi, eta;

    QString side = curve->getSide();
    if(side == QString("SUCTION"))
    {
        xi = c - yt * sin(phi);
        eta = yc + yt * cos(phi);
    }
    else if(side == QString("PRESSURE"))
    {
        xi = c + yt * sin(phi);
        eta = yc - yt * cos(phi);
    }
    else if(side == QString("MEAN"))
    {
        xi = c;
        eta = yc;
    }
    else
    {
        xi = c;
        eta = yc;
    }
    qxieta->setX(xi);
    qxieta->setY(eta);
    qxieta->setZ(0.);
}

/**
 * Estimate coordinate value b for a given chord position by recusrion.
 * 0<b<1 with b=0@LE and b=1@TE. This is not equvalent to the chord
 * position. b is defined by i/(x.length-1) given by the thickness line
 * offset points.
 *
 * @param relC
 *            the relative chord coordinate
 * @return parameter b for given chord location
 *
 * @author stoye
 * @since Dec 7, 2016
 */
double SeriesProfileNACAONE::getRelB(double relC){
    int nr = (int) (log10(1. / accuracy));
    return recursiveGetB(relC, 0., 1., 10, nr);
}

/**
 * Recursion for estimation of chord location for given b value
 *
 * @param relC
 * @param bMin
 * @param bMax
 * @param nb
 * @param recursion
 * @return
 *
 * @author stoye
 * @since Dec 7, 2016
 */
double SeriesProfileNACAONE::recursiveGetB(double relC, double bMin, double bMax, int nb, int recursion) {
    double b0, b1;
    double c0, c1;
    b1 = bMin;
    c1 = getRelC(b1);
    for (int i = 0; i < (nb - 1); i++) {
        b0 = b1;
        b1 = bMin + ((bMax - bMin) / ((double) (nb - 1))) * ((double) (i + 1));
        c0 = c1;
        c1 = getRelC(b1);
        if (c0 <= relC && c1 >= relC) {
            if (recursion > 0) {
                return recursiveGetB(relC, b0, b1, nb, (recursion - 1));
            }
            return (b0 + b1) / 2.;
        }
    }
    return (bMin + bMax) / 2.;
}

double SeriesProfileNACAONE::getRelC(double relB)
{
    double b0;
    double b1 = b[0];

    for(int i = 1; i < np; i++)
    {
        b0 = b1;
        b1 = b[i];
        if(relB >= b0 && relB <= b1)
        {
            double db = b1-b0;
            double dc = x[i]-x[i-1];
            return x[i-1] + dc*(relB-b0)/(db);
        }
    }
    return 0.;
}

double SeriesProfileNACAONE::getRelThicknessFromB(double relB)
{
    double b0;
    double b1 = b[0];

    for(int i = 1; i < np; i++)
    {
        b0 = b1;
        b1 = b[i];
        if(relB >= b0 && relB <= b1)
        {
            double db = b1-b0;
            double dt = y[i]-y[i-1];
            return y[i-1] + dt*(relB-b0)/(db);
        }
    }
    return 0.;
}

/**
 *
 * @param relC
 *            relative chord position
 * @return mean line offset at given chord location
 *
 * @author stoye
 * @since Dec 7, 2016
 */
double SeriesProfileNACAONE::getRelYMeanLine(double relC) {
    double CLi = getCamberCoefficient();

    double eps_ = eps->NANO;
    if (relC < eps_)
        return 0.;
    else if (relC > (1. - eps_))
        return 0.;
    else
        return (0. - CLi / (4. * M_PI)) * ((1. - relC) * log(1. - relC) + relC * log(relC));
}

/**
 *
 * @param relC
 *            relative chord position
 * @return mean line angle at given chord location
 *
 * @author stoye
 * @since Dec 7, 2016
 */
double SeriesProfileNACAONE::getRelYMeanLineAngle(double relC) {
    double c0, c1;
    double eps_ = eps->NANO;


    if (relC <= (eps_ + accuracy)) {
        c0 = relC;
        c1 = relC + accuracy / 2.;
    } else if (relC >= (1. - eps_ - accuracy)) {
        c0 = relC - accuracy / 2.;
        c1 = relC;
    } else {
        c0 = relC - accuracy / 2.;
        c1 = relC + accuracy / 2.;
    }
    double y0 = getRelYMeanLine(c0);
    double y1 = getRelYMeanLine(c1);
    return atan2((y1 - y0), (c1 - c0));
}


/**
 * @return the camberCoefficient
 */
double SeriesProfileNACAONE::getCamberCoefficient() {
    return camberCoefficient;
}

/**
 * @param camberCoefficient
 *            the camberCoefficient to set
 */
void SeriesProfileNACAONE::setCamberCoefficient(double camberCoefficient_) {
    camberCoefficient = camberCoefficient_;
}

/**
 * @return the maxRelThickness
 */
double SeriesProfileNACAONE::getMaxRelThickness() {
    return maxRelThickness;
}

/**
 * @param maxRelThickness
 *            the maxRelThickness to set
 */
void SeriesProfileNACAONE::setMaxRelThickness(double maxRelThickness_) {
    maxRelThickness = maxRelThickness_;
}

/**
 * @return the accuracy
 */
double SeriesProfileNACAONE::getAccuracy() {
    return accuracy;
}

/**
 * @param accuracy
 *            the accuracy to set
 */
void SeriesProfileNACAONE::setAccuracy(double accuracy_) {
    accuracy = accuracy_;
}

/**
 * @return the chordwiseMinPressure
 */
double SeriesProfileNACAONE::getChordwiseMinPressure() {
    return chordwiseMinPressure;
}

/**
 * @param chordwiseMinPressure
 *            the chordwiseMinPressure to set
 */
void SeriesProfileNACAONE::setChordwiseMinPressure(double chordwiseMinPressure_) {
    chordwiseMinPressure = chordwiseMinPressure_;
}

/**
 * @return the currentDenominator
 */
QString SeriesProfileNACAONE::getCurrentDenominator() {
    return currentDenominator;
}

/**
 * @param current_denominator the current_denominator to set
 */
void SeriesProfileNACAONE::setCurrentDenominator(QString currentDenominator_) {
    currentDenominator = currentDenominator_;
}

void SeriesProfileNACAONE::setProfileName(QString profileName_) {
    profileName = profileName_;
}
