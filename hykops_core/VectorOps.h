#ifndef VECTOROPS_H
#define VECTOROPS_H

#include <QVector3D>
#include <QMatrix3x3>

class VectorOps
{
public:
    static QVector3D multiplicate(QMatrix3x3 matrix, QVector3D vector);

private:
    static double indexValue(QVector3D vector, int index);
};

#endif // VECTOROPS_H
