#include "CoordinateTransformation.h"

#include <stdexcept>
#include <cmath>
#include <limits>
#include <QDebug>
#include <QList>

#include "VectorOps.h"
#include "MatrixOps.h"
#include "HComposition.h"
#include "HComponent.h"
#include "HLinearLoftPath.h"
#include "HLoft.h"
#include "AssembleFactory.h"

#include "Logger.h"

QMap<CoordinateSystem3DKey, QMatrix3x3> CoordinateTransformation::forwardMap;
QMap<CoordinateSystem3DKey, QMatrix3x3> CoordinateTransformation::reverseMap;



QVector3D CoordinateTransformation::toGlobalCoordinate(HLoftPath *path, double xi, double eta, double loftCoordinate) {

    QList<HCoordinateSystem3D*> cs = getConcatenatedCoordinateSystemList(path);
    std::reverse(cs.begin(), cs.end());
    QVector3D p(xi, eta, loftCoordinate);
    try {
        foreach(HCoordinateSystem3D* system, cs ) {
            p = reverseTransformation(system, p);
        }
    }
    catch(std::invalid_argument e) {
        qDebug() << e.what();
    }
    return p;
}

QVector3D CoordinateTransformation::toGlobalCoordinate(HLoftElement *element, double xi, double eta) {

    QList<HCoordinateSystem3D*> cs = getConcatenatedCoordinateSystemList(element);
    std::reverse(cs.begin(), cs.end());
    QVector3D p( xi, eta, 0. );
    try {
        foreach(HCoordinateSystem3D* system, cs ) {
            p = reverseTransformation(system, p);
        }
    }
    catch(std::invalid_argument e) {
        qDebug() << e.what();
    }
    return p;
}

QVector3D CoordinateTransformation::toLocalCoordinate(HLoftElement *element, QVector3D p) {

    QList<HCoordinateSystem3D*> cs = getConcatenatedCoordinateSystemList(element);
    QVector3D q(p.x(), p.y(), p.z());

    try {
        foreach(HCoordinateSystem3D* system, cs ) {
            q = forwardTransformation(system, q);
        }
    }
    catch(std::invalid_argument e) {
        qDebug() << e.what();
    }
    return q;
}

QList<HCoordinateSystem3D*> CoordinateTransformation::getConcatenatedCoordinateSystemList(HLoftElement *element) {

    QList<HCoordinateSystem3D*> systems;
    systems.append(element->getHLoft()->getHComponent()->getHComposition()->getHCoordinateSystem3D());
    systems.append(element->getHLoft()->getHComponent()->getHCoordinateSystem3D());
    systems.append(*(element->getHLoft()->getHCoordinateSystems3D()));
    systems.append(element->getHLoft()->getHLoftPath()->getHLoftCoordinateSystem());
    systems.append(AssembleFactory::createTemporaryLoftCoordinateSystem(element));
    systems.append(element->getLoftPlaneDefiningCoordinateSystem());


    foreach (HCoordinateSystem3D *system, systems) {
        if (0 == system) {
            qDebug() << "Error in composition: Coordinate system undefined";
        }
    }
    return systems;
}


QList<HCoordinateSystem3D*> CoordinateTransformation::getConcatenatedCoordinateSystemList(HLoftPath *path) {

    QList<HCoordinateSystem3D*> systems;
    systems.append(path->getHLoft()->getHComponent()->getHComposition()->getHCoordinateSystem3D());
    systems.append(path->getHLoft()->getHComponent()->getHCoordinateSystem3D());
    systems.append(*(path->getHLoft()->getHCoordinateSystems3D()));
    systems.append(path->getHLoftCoordinateSystem());
    systems.append(AssembleFactory::createTemporaryLoftCoordinateSystem(path));

    foreach (HCoordinateSystem3D *system, systems) {
        if (0 == system) {
            qDebug() << "Error in composition: Coordinate system undefined";
        }
    }
    return systems;
}



QVector3D CoordinateTransformation::forwardTransformation(HCoordinateSystem3D *cos, QVector3D vector) {

    try {
        if(HCoordinateSystem3DCartesian *cart = dynamic_cast<HCoordinateSystem3DCartesian*>(cos)) {
            return forwardTransformation(cart, vector, getForwardTransformationMatrix(cos));
        }
        else if(HCoordinateSystem3DAffine *affine = dynamic_cast<HCoordinateSystem3DAffine*>(cos)) {
            return forwardTransformation(affine, vector, getForwardTransformationMatrix(cos));
        }
        else if(HCoordinateSystem3DPolar *polar = dynamic_cast<HCoordinateSystem3DPolar*>(cos)) {
            return forwardTransformation(polar, vector, getForwardTransformationMatrix(cos));
        }
        else if(HCoordinateSystem3DCone *cone = dynamic_cast<HCoordinateSystem3DCone*>(cos)) {
            return forwardTransformation(cone, vector, getForwardTransformationMatrix(cos), getReverseTransformationMatrix(cos));
        }
    }
    catch(std::invalid_argument e) {
        qDebug() << e.what();
    }
    throw std::invalid_argument("invalid coordinate system type");
}


QVector3D CoordinateTransformation::reverseTransformation(HCoordinateSystem3D *cos, QVector3D vector) {

    try {
        if(HCoordinateSystem3DCartesian *cart = dynamic_cast<HCoordinateSystem3DCartesian*>(cos)) {
            return reverseTransformation(cart, vector, getReverseTransformationMatrix(cart));
        }
        else if(HCoordinateSystem3DAffine *affine = dynamic_cast<HCoordinateSystem3DAffine*>(cos)) {
            return reverseTransformation(affine, vector, getReverseTransformationMatrix(affine));
        }
        else if(HCoordinateSystem3DPolar *polar = dynamic_cast<HCoordinateSystem3DPolar*>(cos)) {
            return reverseTransformation(polar, vector, getReverseTransformationMatrix(polar));
        }
        else if(HCoordinateSystem3DCone *cone = dynamic_cast<HCoordinateSystem3DCone*>(cos)) {
            return reverseTransformation(cone, vector, getReverseTransformationMatrix(cone));
        }
    }
    catch(std::invalid_argument e) {
        qDebug() << e.what();
    }
    throw std::invalid_argument("invalid coordinate system type");
}

QVector3D CoordinateTransformation::forwardTransformation(HCoordinateSystem3DAffine *cos, QVector3D vector, QMatrix3x3 fwdMatrix) {
    HPoint3D *origin = cos->getOrigin();
    QVector3D originVector(origin->getX1(), origin->getX2(), origin->getX3());
    return VectorOps::multiplicate(fwdMatrix, vector - originVector);
}

QVector3D CoordinateTransformation::forwardTransformation(HCoordinateSystem3DPolar *cos, QVector3D vector, QMatrix3x3 fwdMatrix) {
    // 1. transformation into temporary affine local cos
    HPoint3D *origin = cos->getOrigin();
    QVector3D originVector(origin->getX1(), origin->getX2(), origin->getX3());
    QVector3D qaffine = VectorOps::multiplicate(fwdMatrix, vector - originVector);
    // 2. transformation from x2, x3 to radial and angular values
    double x = qaffine.x();
    double phi = std::atan2(qaffine.y(), qaffine.z());
    double r = std::sqrt(std::pow(qaffine.y(), 2.) + std::pow(qaffine.z(), 2.));
    double s = phi;

    return QVector3D(x, s, r);
}

QVector3D CoordinateTransformation::forwardTransformation(HCoordinateSystem3DCone *cos, QVector3D vector, QMatrix3x3 fwdMatrix, QMatrix3x3 revMatrix) {

    // 1. transformation into temporary affine local cos
    HPoint3D *origin = cos->getOrigin();
    QVector3D originVector(origin->getX1(), origin->getX2(), origin->getX3());
    QVector3D qaffine = VectorOps::multiplicate(fwdMatrix, vector - originVector);

    // 2. transformation of qaffine[2] into cone coordinates
    QVector3D x1(revMatrix(0,0), revMatrix(1, 0), revMatrix(2, 0)); // axial vector
    QVector3D x3(revMatrix(0, 2), revMatrix(1, 2), revMatrix(2, 2)); // radial vector
    double ax1 = sqrt(x1.x()*x1.x()+x1.y()*x1.y()+x1.z()*x1.z());
    double ax3 = sqrt(x3.x()*x3.x()+x3.y()*x3.y()+x3.z()*x3.z());
    double f;
    if((std::abs(ax1) > 1.e-9) && (std::abs(ax3) > 1.e-9))
    {
        f = (x1.x()*x3.x()+x1.y()*x3.y()+x1.z()*x3.z())/(ax1*ax3);
    }else
    {
        f = 0.;
    }
    QVector3D qaffine2(qaffine.x(), qaffine.y(), (qaffine.z()+qaffine.x()*f));

    // 3. transformation from x2, x3 to radial and angular values
    double x = qaffine2.x();
    double phi = std::atan2(qaffine.y(), qaffine.z());
    double r = std::sqrt(std::pow(qaffine.y(), 2.) + std::pow(qaffine.z(), 2.));
    double s = phi;

    return QVector3D(x, s, r);
}

QVector3D CoordinateTransformation::forwardTransformation(HCoordinateSystem3DCartesian *cos, QVector3D vector, QMatrix3x3 fwdMatrix) {
    (void)fwdMatrix;
// 1. affine transformation from global to local coordinates:
    HPoint3D *origin = cos->getOrigin();
    QVector3D originVector(origin->getX1(), origin->getX2(), origin->getX3());
    QVector3D v1 = vector - originVector;

// 2. rotation around origin (tait-bryan angles)
    QMatrix3x3 taitbryanFwd = getTaitBryanForwardMatrix(cos);
    QVector3D v2 = VectorOps::multiplicate(taitbryanFwd, v1);

// 3. local coordinate axis length scaling:
    double lx = cos->getX1()->getX1(); // length of x1-axis
    double ly = cos->getX1()->getX2(); // length of x2-axis
    double lz = cos->getX1()->getX3(); // length of x3-axis
    QVector3D v3(v2.x()/lx, v2.y()/ly, v2.z()/lz);

    return v3;
}


QVector3D CoordinateTransformation::reverseTransformation(HCoordinateSystem3DAffine *cos, QVector3D v, QMatrix3x3 revMatrix) {

    HPoint3D *origin = cos->getOrigin();
    QVector3D vector(origin->getX1(), origin->getX2(), origin->getX3());
    return vector + VectorOps::multiplicate(revMatrix, v);
}

QVector3D CoordinateTransformation::reverseTransformation(HCoordinateSystem3DPolar *cos, QVector3D v, QMatrix3x3 revMatrix) {

    double xl = v.x();
    double s = v.y();
    double r = v.z();
    double phi = 0;

    if (fabs(r) > std::numeric_limits<double>::epsilon()) {
        phi = s;
    } else {
        phi = 0.;
    }

    double yl = r * std::sin(phi);
    double zl = r * std::cos(phi);
    QVector3D qaffine(xl, yl, zl);

    // transformation to global coordinates
    HPoint3D *origin = cos->getOrigin();
    QVector3D vector(origin->getX1(), origin->getX2(), origin->getX3());
    return vector + VectorOps::multiplicate(revMatrix, qaffine);
}

QVector3D CoordinateTransformation::reverseTransformation(HCoordinateSystem3DCone *cos, QVector3D v, QMatrix3x3 revMatrix) {

    double xl = v.x();
    double s = v.y();
    double r = v.z();

    double phi =0.;
    if (fabs(r) > std::numeric_limits<double>::epsilon()) {
        phi = s;
    } else {
        phi = 0.;
    }
    double yl = r * std::sin(phi);
    double zl = r * std::cos(phi);
    QVector3D qaffine2(xl, yl, zl);

// cone-correction of z
    QVector3D x1(revMatrix(0,0), revMatrix(1,0), revMatrix(2, 0));
    QVector3D x3(revMatrix(0,2),revMatrix(1,2),revMatrix(2,2));
    double ax1 = std::sqrt(x1.x()*x1.x()+x1.y()*x1.y()+x1.z()*x1.z());
    double ax3 = std::sqrt(x3.x()*x3.x()+x3.y()*x3.y()+x3.z()*x3.z());
    double f; // alpha = cos(f) = angle between axial and radial vector
    if((fabs(ax1) > std::numeric_limits<double>::epsilon()) && (fabs(ax3) > std::numeric_limits<double>::epsilon()))
    {
        f = (x1.x()*x3.x()+x1.y()*x3.y()+x1.z()*x3.z())/(ax1*ax3);
    }else
    {
        f = 0.;
    }
    double q2 = qaffine2.z() - qaffine2.x() * f;
    QVector3D qaffine(qaffine2.x(), qaffine2.y(), q2);

    // transformation to global coordinates
    HPoint3D *origin = cos->getOrigin();
    QVector3D vector(origin->getX1(), origin->getX2(), origin->getX3());
    return vector + VectorOps::multiplicate(revMatrix, qaffine);
}

QVector3D CoordinateTransformation::reverseTransformation(HCoordinateSystem3DCartesian *cos, QVector3D vector, QMatrix3x3 revMatrix) {
    (void)revMatrix;
    // 1. local coordinate axis length scaling:
        double lx = cos->getX1()->getX1(); // length of x1-axis
        double ly = cos->getX1()->getX2(); // length of x2-axis
        double lz = cos->getX1()->getX3(); // length of x3-axis
        QVector3D v2(vector.x()*lx, vector.y()*ly, vector.z()*lz);

    // 2. rotation around origin (tait-bryan angles)
        QMatrix3x3 taitBryanRev = getTaitBryanReverseMatrix(cos);
        QVector3D w3 = VectorOps::multiplicate(taitBryanRev, v2);

    // 3. transformation from local to global coordinates:
        HPoint3D *origin = cos->getOrigin();        
        QVector3D qorigin(origin->getX1(), origin->getX2(), origin->getX3());

        return qorigin + w3;
}

QMatrix3x3 CoordinateTransformation::getForwardTransformationMatrix(HCoordinateSystem3D *cos) {
//    if (forwardMap.contains(CoordinateSystem3DKey(cos))) {
//        return forwardMap.value(cos);
//    }

    if(HCoordinateSystem3DCartesian* cart = dynamic_cast<HCoordinateSystem3DCartesian*>(cos)){
        return getTaitBryanForwardMatrix(cart);
//        forwardMap.insert(CoordinateSystem3DKey(cart), getTaitBryanForwardMatrix(cart));
//        return getForwardTransformationMatrix(cart);
    }
    else if(HCoordinateSystem3DAffine* affine = dynamic_cast<HCoordinateSystem3DAffine*>(cos)) {
        return getAffineForwardTransformationMatrix(affine);
//        forwardMap.insert(CoordinateSystem3DKey(affine), getAffineForwardTransformationMatrix(affine));
//        return getForwardTransformationMatrix(affine);
    }
    else if(HCoordinateSystem3DPolar* polar = dynamic_cast<HCoordinateSystem3DPolar*>(cos)) {
        return getPolarForwardTransformationMatrix(polar);
//        forwardMap.insert(CoordinateSystem3DKey(polar), getPolarForwardTransformationMatrix(polar));
//        return getForwardTransformationMatrix(cos);
    }
    else if(HCoordinateSystem3DCone* cone = dynamic_cast<HCoordinateSystem3DCone*>(cos)) {
        return getConeForwardTransformationMatrix(cone);
//        forwardMap.insert(CoordinateSystem3DKey(cone), getConeForwardTransformationMatrix(cone));
//        return getForwardTransformationMatrix(cos);
    }
    else {
        qDebug() << "Cannot create Forward Transfomration Matrix. Unkown Coordinate System Type";
    }
    throw std::invalid_argument("invalid coordinate system type");
}

QMatrix3x3 CoordinateTransformation::getReverseTransformationMatrix(HCoordinateSystem3D *cos) {
//    if (reverseMap.contains(CoordinateSystem3DKey(cos))) {
//        return reverseMap.value(cos);
//    }

    if(HCoordinateSystem3DCartesian* cart = dynamic_cast<HCoordinateSystem3DCartesian*>(cos)) {
        return getTaitBryanReverseMatrix(cart);
//        reverseMap.insert(CoordinateSystem3DKey(cart), getTaitBryanReverseMatrix(cart));
//        return getReverseTransformationMatrix(cart);
    }
   else if(HCoordinateSystem3DAffine* affine = dynamic_cast<HCoordinateSystem3DAffine*>(cos)) {
        return getAffineReverseTransformationMatrix(affine);
//        reverseMap.insert(CoordinateSystem3DKey(affine), getAffineReverseTransformationMatrix(affine));
//        return getReverseTransformationMatrix(affine);
    }
    else if(HCoordinateSystem3DPolar* polar = dynamic_cast<HCoordinateSystem3DPolar*>(cos)) {
        return getPolarReverseTransformationMatrix(polar);
//        reverseMap.insert(CoordinateSystem3DKey(polar), getPolarReverseTransformationMatrix(polar));
//        return getReverseTransformationMatrix(polar);
    }
    else if(HCoordinateSystem3DCone* cone = dynamic_cast<HCoordinateSystem3DCone*>(cos)) {
        return getConeReverseTransformationMatrix(cone);
//        reverseMap.insert(CoordinateSystem3DKey(cone), getPolarReverseTransformationMatrix(cone));
//        return getReverseTransformationMatrix(cone);
    }
    else {
        qDebug() << "Cannot create Reverse Transfomration Matrix. Unkown Coordinate System Type";
    }
    throw std::invalid_argument("invalid coordinate system type");
}

/**
 * Create affine forward transformation matrix: inverse of forward matrix
 *
 * @param cos
 * @return inverse forward transformation matrix
 */
QMatrix3x3 CoordinateTransformation::getAffineForwardTransformationMatrix(HCoordinateSystem3DAffine *cos){
    QMatrix3x3 matrix = getAffineReverseTransformationMatrix(cos);
    return MatrixOps::inverse3x3(matrix);
}

/**
 * Create polar forward transformation matrix: inverse of forward matrix
 *
 * @param cos
 * @return inverse forward transformation matrix
 */
QMatrix3x3 CoordinateTransformation::getPolarForwardTransformationMatrix(HCoordinateSystem3DPolar *cos) {
    QMatrix3x3 matrix = getPolarReverseTransformationMatrix(cos);
    return MatrixOps::inverse3x3(matrix);
}

/**
 *
 * @param cos
 * @return inverse forward transformation matrix
 */
QMatrix3x3 CoordinateTransformation::getConeForwardTransformationMatrix(HCoordinateSystem3DCone *cos) {
    QMatrix3x3 matrix = getConeReverseTransformationMatrix(cos);
    return MatrixOps::inverse3x3(matrix);
}

/**
 * @brief CoordinateTransformation::getTaitBryanForwardMatrix
 * Default Tait-Bryan rotation x, y', z'' Matrix
 * @param cos
 * @return
 */
QMatrix3x3 CoordinateTransformation::getTaitBryanForwardMatrix(HCoordinateSystem3DCartesian *coos){

    HPoint3D *e = coos->getE1();

    double phi = e->getX1();
    double theta = e->getX2();
    double psi = e->getX3();

    QMatrix3x3 matrix;

    matrix(0,0) = cos(theta)*cos(psi);
    matrix(0,1) = sin(phi)*sin(theta)*cos(psi)-cos(phi)*sin(psi);
    matrix(0,2) = cos(phi)*sin(theta)*cos(psi)+sin(phi)*sin(psi);
    matrix(1,0) = cos(theta)*sin(psi);
    matrix(1,1) = sin(phi)*sin(theta)*sin(psi)+cos(phi)*cos(psi);
    matrix(1,2) = cos(phi)*sin(theta)*sin(psi)-sin(phi)*cos(psi);
    matrix(2,0) = -sin(theta);
    matrix(2,1) = sin(phi)*cos(theta);
    matrix(2,2) = cos(phi)*cos(theta);

    return matrix;
}

/**
 * Create affine reverse transformation matrix:
 *
 * | t1x t2x t3x| a = | t1y t2y t3y| | t1z t2z t3z|
 *
 * t1..t3 = affine coordinate axis
 */
QMatrix3x3 CoordinateTransformation::getAffineReverseTransformationMatrix(HCoordinateSystem3DAffine *cos) {

    HPoint3D *x1 = cos->getX1();
    HPoint3D *x2 = cos->getX2();
    HPoint3D *x3 = cos->getX3();

    QMatrix3x3 matrix;
    matrix(0,0) = x1->getX1();
    matrix(1,0) = x1->getX2();
    matrix(2,0) = x1->getX3();
    matrix(0,1) = x2->getX1();
    matrix(1,1) = x2->getX2();
    matrix(2,1) = x2->getX3();
    matrix(0,2) = x3->getX1();
    matrix(1,2) = x3->getX2();
    matrix(2,2) = x3->getX3();

    return matrix;
}

/**
 * Create temporary affine reverse transformation matrix (affine coordinate
 * system with axial and radial axis equal to x1/x3 axis and x2-axis
 * perpendicular)
 *
 * | t1x t2x t3x| a = | t1y t2y t3y| | t1z t2z t3z|
 *
 * t1..t3 = affine coordinate axis
 *
 * @param cos
 * @return affine transformation matrix
 *
 */
QMatrix3x3 CoordinateTransformation::getPolarReverseTransformationMatrix(HCoordinateSystem3DPolar *cos) {

    HPoint3D *x1 = cos->getAxialVector();
    HPoint3D *x3 = cos->getRadialVector();

    QVector3D ax(x1->getX1(), x1->getX2(), x1->getX3());
    QVector3D rad(x3->getX1(), x3->getX2(), x3->getX3());
    QVector3D temp = QVector3D::crossProduct(ax, rad);

    QMatrix3x3 matrix;
    matrix(0,0) = ax.x();
    matrix(1,0) = ax.y();
    matrix(2,0) = ax.z();

    matrix(0,1) = temp.x();
    matrix(1,1) = temp.y();
    matrix(2,1) = temp.z();

    matrix(0,2) = rad.x();
    matrix(1,2) = rad.y();
    matrix(2,2) = rad.z();

    return matrix;
}

/**
 * Create temporary affine reverse transformation matrix (affine coordinate
 * system with axial and radial axis equal to x1/x3 axis and x2-axis
 * perpendicular). Equivalent to polar coordinate system
 *
 * |t1x t2x t3x| a = | t1y t2y t3y| | t1z t2z t3z|
 *
 * t1..t3 = affine coordinate axis
 *
 * @param cos
 * @return affine transformation matrix
 */
QMatrix3x3 CoordinateTransformation::getConeReverseTransformationMatrix(HCoordinateSystem3DCone *cos) {

    HPoint3D *x1 = cos->getAxialVector();
    HPoint3D *x3 = cos->getRadialVector();

    QVector3D ax(x1->getX1(), x1->getX2(), x1->getX3());
    QVector3D rad(x3->getX1(), x3->getX2(), x3->getX3());
    QVector3D temp = QVector3D::crossProduct(ax, rad);

    QMatrix3x3 matrix;
    matrix(0,0) = ax.x();
    matrix(1,0) = ax.y();
    matrix(2,0) = ax.z();

    matrix(0,1) = temp.x();
    matrix(1,1) = temp.y();
    matrix(2,1) = temp.z();

    matrix(0,2) = rad.x();
    matrix(1,2) = rad.y();
    matrix(2,2) = rad.z();

    return matrix;
}

/**
 * @brief CoordinateTransformation::getTaitBryanReverseMatrix
 * Default Tait-Bryan rotation x, y', z'' Matrix
 * @param coos
 * @return
 */
QMatrix3x3 CoordinateTransformation::getTaitBryanReverseMatrix(HCoordinateSystem3DCartesian *coos){
    HPoint3D *e = coos->getE1();

    double phi = e->getX1();
    double theta = e->getX2();
    double psi = e->getX3();

    QMatrix3x3 matrix;

    matrix(0,0) = cos(theta)*cos(psi);
    matrix(1,0) = sin(phi)*sin(theta)*cos(psi)-cos(phi)*sin(psi);
    matrix(2,0) = cos(phi)*sin(theta)*cos(psi)+sin(phi)*sin(psi);
    matrix(0,1) = cos(theta)*sin(psi);
    matrix(1,1) = sin(phi)*sin(theta)*sin(psi)+cos(phi)*cos(psi);
    matrix(2,1) = cos(phi)*sin(theta)*sin(psi)-sin(phi)*cos(psi);
    matrix(0,2) = -sin(theta);
    matrix(1,2) = sin(phi)*cos(theta);
    matrix(2,2) = cos(phi)*cos(theta);

    return matrix;
}



