#ifndef C_INTERFACE_H
#define C_INTERFACE_H

#ifdef _WIN32
#  ifdef MODULE_API_EXPORTS
#    define MODULE_API __declspec(dllexport)
#  else
#    define MODULE_API __declspec(dllimport)
#  endif
#else
#  define MODULE_API
#endif

#include <QVariant>

//TODO Doku aktualisieren nach string buffer umbau

/**
 * @file C_Interface.h
 *
 */

/**
 * @mainpage
 * \section Interface
 * @link C_Interface.h
 *  API description
 * @endlink
 */
extern "C" {


/**
 * @brief Initializes HYKOPS library
 *
 * Initialization registers all user defined
 * classes and lists so that they can be used
 * as object properties
 */
MODULE_API void init();

/**
 * @brief Creates object in HYKOPS library
 *
 * The object of type className gets created
 * and the handle will be returned.
 * It is the responsibility of the user to
 * handle the ownership of the new object.
 * @param className class name as defined in data model
 * @return handle to object
 */
 MODULE_API long create_object(const char *className);

/**
 * @brief Deletes object from HYKOPS library
 *
 * The object and all child objects owned by object get
 * deleted.
 * @param handle handle to object
 * @return status, zero indicates success
 */
MODULE_API int delete_object(long handle);

/**
 * @brief Converts a list into an array of objects
 *
 * This function is used to copy a list of object
 * handles into a user provided array. On success
 * the number of objects is returned.
 * @param handle from object that contains the list
 * @param property the name from list property
 * @param object_array address of user provided int array
 * @param object_array_size maximum number of handles that can be stored in array
 * Hint: use get_list_size(..) to determine the size
 * @return if positive: number of handles returned, else: error code (e.g. overflow)
 */
MODULE_API int get_list(long handle, const char *property, long* object_array, int object_array_size);



/**
 * @brief Converts a string map into two arrays of strings
 *
 * This function is used to copy a map of strings
 * into a user provided array. On success
 * the number of objects is returned.
 * @param handle of object that contains the map
 * @param property the name from map property
 * @param keys will be filled up with keys (TODO: user provided)
 * @param values will be filled up with values (TODO: user provided)
 * Hint: use get_map_size(..) to determine the size
 * @return if positive: number of handles returned, else: error code (e.g. overflow)
 */
MODULE_API int get_map(long handle, const char *property,  char **keys, char **values);


/**
 * @brief Get class name from object
 *
 * Returns class name of object (see create_object)
 * @param handle handle to object
 * @return class name
 */
MODULE_API int get_type(long handle, char *buffer, int bufferLength);

/**
 * @brief Set string property (attribute) to value
 * @param handle handle to object
 * @param property name of string property
 * @param value the value
 */
MODULE_API void set_string(long handle, const char *property, const char *value);

/**
 * @brief Set integer property (attribute) to value
 * @param handle handle to object
 * @param property name of integer property
 * @param value the value
 */
MODULE_API void set_int(long handle, const char *property, int value);

/**
 * @brief Set double precision property (attribute) to value
 * @param handle handle to object
 * @param property name of double precision property
 * @param value the value
 */
MODULE_API void set_double(long handle, const char *property, double value);

/**
 * @brief Set boolean property (attribute) to value
 * @param handle handle to object
 * @param property name of boolean property
 * @param value the value
 */
MODULE_API void set_bool(long handle, const char *property, bool value);

/**
 * @brief Set object reference on given object handle
 * @param handle handle to object
 * @param property name of object reference
 * @param object handle of referenced object
 */
MODULE_API void set_object(long handle, const char *property, long object);

/**
 * @brief Set list of objects on given handle object
 * @param handle object adress that contains the list
 * @param property name of list property
 * @param objects array of object handles
 * @param size number of object handles in array
 */
MODULE_API void set_list(long handle, const char *property, long *objects, int size);

/**
 * @brief Set string map on given handle object
 * @param handle object adress that contains the map
 * @param property name of map property
 * @param objects array of object handles
 * @param size number of object handles in array
 */

MODULE_API void set_map(long handle, const char *property, const char **keys, int keyLength[], const char **values, int valueLength[], int mapSize);
/**
 * @brief Set ownership of child object to parent object
 *
 * Hierarchical parent child ownership is important when it comes to
 * deleting or serializing objects (including child objects). A reference
 * to an object does not automatically mean that the referenced object is
 * owned.
 * @param handle handle to (child) object
 * @param parent handle to parent object
 */
MODULE_API void set_parent(long handle, long parent);

/**TODO überarbeiten
 * @brief Get string property (attribute) from object
 * @param handle handle to object
 * @param property name of string property
 * @return string value
 */
MODULE_API int get_string(long handle, const char *property, char *string, int bufLength);

/**
 * @brief Get integer property (attribute) from object
 * @param handle handle to object
 * @param property name of integer property
 * @return integer value
 */
MODULE_API int         get_int(long handle, const char *property);

/**
 * @brief Get double precision property (attribute) from object
 * @param handle handle to object
 * @param property name of double precision property
 * @return double precision value
 */
MODULE_API double      get_double(long handle, const char *property);

/**
 * @brief Get boolean property (attribute) from object
 * @param handle handle to object
 * @param property name of boolean property
 * @return boolean value
 */
MODULE_API bool        get_bool(long handle, const char* property);

/**
 * @brief Get reference of object from parent object
 * @param handle handle to parent object
 * @param property name of object reference
 * @return child object reference (might be 0)
 */
MODULE_API int         get_object(long handle, const char *property);



/**
 * @brief Get size of list
 * @param handle object adress that contains the list
 * @param property name of list property
 * @return the size of the list
 */
MODULE_API int         get_list_size(long handle, const char *property);



/**
 * @brief Get size of map
 * @param handle object adress that contains the map
 * @param property name of map property
 * @return the size of the map
 */
MODULE_API int get_map_size(long handle, const char *property);




/**
 * @brief Serializes a Composition into a SerializeObject
 * @param Handle of compostion to serialize
 * @return Handle of SerializedObject
 */
MODULE_API long serialize(long composition_handle);


/**
* @brief Returns the text size of SerializedObject
* @param Handle of Compostion
* @return The size of the serialized text attribute
*/
MODULE_API int get_serialized_text_size(long serialized_handle);


/**
* @brief Fill the char pointer with the serialized xml text
* @param The handle of the SerializedObject
* @param The char pointer to fill with text
* @return 0 on success, negative number on error
*/
MODULE_API int get_serialized_string(long handle, char *xml);

/**
 * @brief Deserialized a text into a Composition
 * @param The handle of the deserialized composition
 * @param The text to deserialize
 * @return 0 on success, negative number on error
 */
MODULE_API int deserialize(long *composition_handle, char *xml);


/**
 * @brief Transform point on loft element to global coordinate
 * @param element the loft element
 * @param xi coordinate 1 in loft plane
 * @param eta coordinate 2 in loft plane
 * @param global cartesian point coordinates
 * @return zero on success, otherwise a negative error number

 */
MODULE_API int toGlobalCoordinate(long loftElementHandle, double xi, double eta, double global[]);


/**
 * @brief Transform point in loft path coordinates to global coordinate
 * @param path the loft path
 * @param xi coordinate 1 perpendicular to loft axis
 * @param eta coordinate 2 perpendicular to loft axis
 * @param loft coordinate the loft coordinate
 * @param global cartesian point coordinates

 * @return zero on success, otherwise a negative error number
 *
 */
MODULE_API int toGlobalCoordinatePath(long loftPathHandle, double xi, double eta, double loftCoordinate, double global[]);


/**
 * @brief Transform point in global coordinates to loft plane coordinate point
 * @param element the loft element
 * @param global point in global coordinates
 * @param local point in loft plane coordinates
 * @return zero on success, otherwise a negative error number
 */
MODULE_API int toLocalCoordinate(long loftElementHandle, double global[], double local[]);


/**
 * @brief getXiEta the coordinates of a point on the curve in loft plane coordinates
 * @param curve2DHandle the HCurve2D element
 * @param c the chord position
 * @param xieta xieta[0]:the xi(=chordwise) coordinate, xieta[1]: the perpendicular offset coordinate
 * @return zero on success, otherwise a negative error number
 */
MODULE_API int getXiEta(long curve2DHandle, double c, double xieta[]);

}

void logDateMessage(QString message);

QString getDateMessage();



/**
 * @cond QT_INTERN
 * @brief set_property
 * @param handle
 * @param property
 * @param value
 */
void set_property(long handle, const char* property, QVariant value);

QVariant get_property(long handle, const char *property);

/**
 * @endcond
 */

#endif // C_INTERFACE_H
