#include "MatrixOps.h"

QMatrix3x3 MatrixOps::inverse3x3(QMatrix3x3 matrix) {
    /* calculate inverse (3D)-Matrix:
     *
     * ( a11 a12 a13 ) A = ( a21 a22 a23 ) ( a31 a32 a33 )
     * ( a11 a21 a31 ) A^-1 = 1./det(A) * ( a12 a22 a32 ) ( a13 a23 a33 )
     */
    QMatrix3x3 inv;

    double det = determinante3x3(matrix);

    for (int i = 0; i < 3; i++) {
        for (int j = 0; j < 3; j++) {
            inv(i,j) = matrix(i,j) / det;
        }
    }

    return inv;
}

double MatrixOps::determinante3x3(QMatrix3x3 matrix) {
    return  matrix(0,0) * matrix(1,1) * matrix(2,2) +
            matrix(0,1) * matrix(1,2) * matrix(2,0) +
            matrix(0,2) * matrix(1,0) * matrix(2,1) -
            matrix(0,2) * matrix(1,1) * matrix(2,0) -
            matrix(0,0) * matrix(1,2) * matrix(2,1) -
            matrix(0,1) * matrix(1,0) * matrix(2,2);
}
