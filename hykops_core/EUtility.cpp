#include "EUtility.h"
#include <QMetaProperty>
#include <QMetaType>
#include <QDebug>

QString LIST_DEF = "QList<";
QString MAP_DEF = "QMap<";
QString COLLECTION_PTR_END = "*>*";


typedef QMap<QString, QString> *strStrMap;
Q_DECLARE_METATYPE(strStrMap)

QObject* EUtility::getObjectProperty(const QString& variableName, const QObject* parentObj)
{
    if (!parentObj) return 0; // no parent object given

    const QMetaObject* mobj = parentObj->metaObject();
    int inx = mobj->indexOfProperty(variableName.toUtf8().data());
    if (inx<0) return 0; // cannot find property name (variable)

    QMetaProperty mp = mobj->property(inx);
    QVariant v = mp.read(parentObj);
    if (!v.isValid()) return 0; // property read error

    QString classType = mp.typeName();
    if (classType.endsWith('*')) classType.chop(1);


    QObject *obj = EFactory::getObject(classType,v);
    return obj;
}

void EUtility::setObjectProperty(const QString& variableName, const QObject* parentObj, QObject *object)
{
    const QMetaObject* mobj = parentObj->metaObject();
    int inx = mobj->indexOfProperty(variableName.toUtf8().data());

    QMetaProperty mp = mobj->property(inx);
    QVariant v = mp.read(parentObj);

    QString classType = mp.typeName();
    if (classType.endsWith('*')) classType.chop(1);


    EFactory::setObject(classType,v, object);
    mp.write(const_cast<QObject*>(parentObj), v);
}

QList<QObject*> *EUtility::getListObjectProperty(const QString& variableName, const QObject* parentObj)
{
    if (!parentObj) return 0; // no parent object given
    const QMetaObject* mobj = parentObj->metaObject();

    int inx = mobj->indexOfProperty(variableName.toUtf8().data());
    if (inx<0) return 0; // cannot find property name (variable)

    QMetaProperty mp = mobj->property(inx);
    QVariant v = mp.read(parentObj);
    if (!v.isValid()) return 0; // property read error

    QString classType = mp.typeName();

    if(classType.startsWith(LIST_DEF)) {
        classType = classType.replace(LIST_DEF, "").replace(COLLECTION_PTR_END, "");
    }

    return EFactory::getList(classType,v);
}


QMap<QString, QString> *EUtility::getMapObjectProperty(const QString& variableName, const QObject *parentObj) {

    if (!parentObj) return 0; // no parent object given

    const QMetaObject* mobj = parentObj->metaObject();
    int inx = mobj->indexOfProperty(variableName.toUtf8().data());
    if (inx<0) return 0; // cannot find property name (variable)

    QMetaProperty mp = mobj->property(inx);
    QVariant v = mp.read(parentObj);
    if (!v.isValid()) return 0; // property read error

    QString classType = mp.typeName();

    if(classType.startsWith(MAP_DEF)) {
        classType = classType.replace(MAP_DEF, "").replace(COLLECTION_PTR_END, "");
    }


    return v.value<strStrMap>();
}


