#ifndef COMPOSITIONFACTORY_H
#define COMPOSITIONFACTORY_H

#include <QList>
#include <QPair>

#include "HComposition.h"

extern "C" {

    void create_composition(int *handle, char *name, char *description);

}


class CompositionFactory
{
public:

    CompositionFactory();
    static HComposition *create(QString name, QString description);
};

#endif // COMPOSITIONFACTORY_H
