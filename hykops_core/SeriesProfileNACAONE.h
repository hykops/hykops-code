#ifndef SERIESPROFILENACAONE_H
#define SERIESPROFILENACAONE_H

#include <QString>

#include <QVector3D>

#include "ISeriesProfile.h"
#include "Epsilon.h"

const QString seriesNameNACAONE("NACA 1-series");
const double DEFAULT_ACCURACY	= 1.e-4;
const double            x[]	= { 0., 0.01250, 0.02500, 0.05000, 0.07500, 0.10000, 0.15000, 0.20000, 0.30000,
                                                            0.40000, 0.50000, 0.60000, 0.70000, 0.80000, 0.90000, 0.95000, 1.0 };
const double            y[]	= { 0., 0.646, 0.903, 1.255, 1.516, 1.729, 2.067, 2.332, 2.709, 2.927, 3.000, 2.917,
                                                            2.635, 2.099, 1.259, 0.707, 0.060 };
const int               np                  = 17; // 17 offset points
const double            yOffsetFactor		= 3.;/*- data given for NACA 16-006!*/
const double            gradLEx				= 0.;
const double            gradTEx				= 1.;


class SeriesProfileNACAONE : ISeriesProfile
{
public:
    SeriesProfileNACAONE(QString denominator_);
    QString getSeriesName();
    QString getProfileName();
    void getXiEta(HSeriesProfileCurve2D *curve, double c, QVector3D* qxieta);

private:

    void initString(QString denominator_);
    void init(double camberCoefficient_, double maxRelThickness_, double chordwiseMinPressure_, double accuracy_);

    QString                 currentDenominator;
    QString					profileName;

    double					camberCoefficient;
    double					maxRelThickness;
    double					chordwiseMinPressure;
    double                  accuracy;
    Epsilon*                eps;

    double                b[np];

    double getRelB(double c);
    double getRelC(double b);
    double recursiveGetB(double relC, double bMin, double bMax, int nb, int recursion);
    double getRelThicknessFromB(double relC);

    void  calculateDigits();

    double getRelYMeanLine(double relC);
    double getRelYMeanLineAngle(double relC);

    void setProfileName(QString profileName);    
    double getCamberCoefficient();
    void setCamberCoefficient(double camberCoefficient_);
    double getMaxRelThickness();
    void setMaxRelThickness(double maxRelThickness_);
    double getAccuracy();
    void setAccuracy(double accuracy_);
    double getChordwiseMinPressure();
    void setChordwiseMinPressure(double chordwiseMinPressure_);
    QString getCurrentDenominator();
    void setCurrentDenominator(QString currentDenominator_);

};

#endif // SERIESPROFILENACAONE_H
