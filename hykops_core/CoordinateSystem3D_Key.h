#ifndef COORDINATESYSTEM3DKEY_H
#define COORDINATESYSTEM3DKEY_H

#include "HCoordinateSystem3D.h"

class CoordinateSystem3DKey
{
public:
    CoordinateSystem3DKey() : cos(0) {}
    CoordinateSystem3DKey(const HCoordinateSystem3D *key) : cos(key) {}

    bool operator< (const CoordinateSystem3DKey &other) const {
        return this->cos < other.cos;
    }


private:
    const HCoordinateSystem3D *cos;

};

#endif // COORDINATESYSTEM3DKEY_H
