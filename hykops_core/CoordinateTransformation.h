#ifndef COORDINATETRANSFORMATION_H
#define COORDINATETRANSFORMATION_H

#include <QVector3D>
#include <QMatrix3x3>
#include <QMap>

#include "CoordinateSystem3D_Key.h"
#include "HCoordinateSystem3DCartesian.h"
#include "HCoordinateSystem3DAffine.h"
#include "HCoordinateSystem3DPolar.h"
#include "HCoordinateSystem3DCone.h"
#include "HPoint3D.h"
#include "HLoftPath.h"
#include "HLoftElement.h"


class CoordinateTransformation
{
private:
    static QMap<CoordinateSystem3DKey, QMatrix3x3> forwardMap;
    static QMap<CoordinateSystem3DKey, QMatrix3x3> reverseMap;

    static QMatrix3x3 getForwardTransformationMatrix(HCoordinateSystem3D *cos);
    static QMatrix3x3 getReverseTransformationMatrix(HCoordinateSystem3D *cos);

    static QMatrix3x3 getAffineForwardTransformationMatrix(HCoordinateSystem3DAffine *cos);
    static QMatrix3x3 getPolarForwardTransformationMatrix(HCoordinateSystem3DPolar *cos);
    static QMatrix3x3 getConeForwardTransformationMatrix(HCoordinateSystem3DCone *cos);
    static QMatrix3x3 getTaitBryanForwardMatrix(HCoordinateSystem3DCartesian *cos);


    static QMatrix3x3 getAffineReverseTransformationMatrix(HCoordinateSystem3DAffine *cos);
    static QMatrix3x3 getPolarReverseTransformationMatrix(HCoordinateSystem3DPolar *cos);
    static QMatrix3x3 getConeReverseTransformationMatrix(HCoordinateSystem3DCone *cos);
    static QMatrix3x3 getTaitBryanReverseMatrix(HCoordinateSystem3DCartesian *cos);


    static QVector3D forwardTransformation(HCoordinateSystem3DCartesian *cos, QVector3D vector, QMatrix3x3 fwdMatrix);
    static QVector3D forwardTransformation(HCoordinateSystem3DAffine *cos, QVector3D vector, QMatrix3x3 fwdMatrix);
    static QVector3D forwardTransformation(HCoordinateSystem3DPolar *cos, QVector3D vector, QMatrix3x3 fwdMatrix);
    static QVector3D forwardTransformation(HCoordinateSystem3DCone *cos, QVector3D vector, QMatrix3x3 fwdMatrix, QMatrix3x3 revMatrix);


    static QVector3D reverseTransformation(HCoordinateSystem3DCartesian *cos, QVector3D vector, QMatrix3x3 revMatrix);
    static QVector3D reverseTransformation(HCoordinateSystem3DAffine *cos, QVector3D vector, QMatrix3x3 revMatrix);
    static QVector3D reverseTransformation(HCoordinateSystem3DPolar *cos, QVector3D vector, QMatrix3x3 revMatrix);
    static QVector3D reverseTransformation(HCoordinateSystem3DCone *cos, QVector3D vector, QMatrix3x3 revMatrix);

public:

    static QVector3D toGlobalCoordinate(HLoftPath *path, double xi, double eta, double loftCoordinate);
    static QVector3D toGlobalCoordinate(HLoftElement *element, double xi, double eta);
    static QVector3D toLocalCoordinate(HLoftElement *element, QVector3D p);

    static QList<HCoordinateSystem3D*> getConcatenatedCoordinateSystemList(HLoftElement *element);
    static QList<HCoordinateSystem3D*> getConcatenatedCoordinateSystemList(HLoftPath *path);

    static QVector3D forwardTransformation(HCoordinateSystem3D *cos, QVector3D vector);
    static QVector3D reverseTransformation(HCoordinateSystem3D *cos, QVector3D vector);

};

#endif // COORDINATETRANSFORMATION_H
