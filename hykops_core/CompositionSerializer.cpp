#include "CompositionSerializer.h"
#include "domreader.h"
#include "domwriter.h"

#include <QDebug>



SerializedObject* CompostionSerializer::serialize(const HComposition *cmp) {
    qDebug() << "Serializing Composition (" << cmp->getName() << ")..";
    DomWriter writer;
    SerializedObject *serObj = writer.serialize(cmp);
    qDebug() << "Serialized.";
    return serObj;
}

HComposition *CompostionSerializer::deserialize(QString xml) {
    qDebug() << "Deserializing Composition..";

    DomReader reader(xml);
    HComposition *deserObj = (HComposition*)reader.toplevelObject();
    qDebug() << "Deserialized (" << deserObj->getName() << ").";

    return deserObj;
}




