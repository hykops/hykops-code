#include "VectorOps.h"



QVector3D VectorOps::multiplicate(QMatrix3x3 matrix, QVector3D vector)
{
    QVector3D mulVec;
    for (int i = 0; i < 3; i++)
    {
        mulVec.setX(mulVec.x() + matrix(0, i) * indexValue(vector, i));
        mulVec.setY(mulVec.y() + matrix(1, i) * indexValue(vector, i));
        mulVec.setZ(mulVec.z() + matrix(2, i) * indexValue(vector, i));
    }
    return mulVec;
}



double VectorOps::indexValue(QVector3D vector, int index) {
    switch(index) {
    case 0: return vector.x();
    case 1: return vector.y();
    case 2: return vector.z();
    default: return 0;
    }
}
