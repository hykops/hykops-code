#include <cmath>
#include <math.h>

#include "SeriesProfileCIRCLE.h"
#include "Epsilon.h"




SeriesProfileCIRCLE::SeriesProfileCIRCLE()
{
}

QString SeriesProfileCIRCLE::getSeriesName()
{
    return seriesNameCIRCLE;
}

QString SeriesProfileCIRCLE::getProfileName()
{
    return QString("");
}

void SeriesProfileCIRCLE::getXiEta(HSeriesProfileCurve2D *curve, double c, QVector3D* qxieta)
{
    double x = c - 0.5;
    double phi;
    if (c < 0.) {
        phi = M_PI;
    } else if (c > 1.) {
        phi = 0.;
    } else {
        phi = acos(2. * x);
    }

    double y;

    QString side = curve->getSide();
    if(side == QString("SUCTION"))
    {
        y = 0.5 * sin(phi);
    }
    else if(side == QString("PRESSURE"))
    {
        y = -0.5 * sin(phi);
    }
    else if(side == QString("MEAN"))
    {
        y = 0.;
    }
    else
    {
        y = 0.;
    }
    qxieta->setX(c);
    qxieta->setY(y);
    qxieta->setZ(0.);
}
