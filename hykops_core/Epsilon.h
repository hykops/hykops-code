#ifndef EPSILON_H
#define EPSILON_H

//! Encapsulates a small value of epsilon \f$ \epsilon = 10^{-9}\f$

class Epsilon
{
public:

    Epsilon();

    /**
     * Nondimensional scales
     */
    const double PICO				= 1.E-12;
    const double NANO				= 1.E-09;
    const double MICRO				= 1.E-06;
    const double MILLI				= 1.E-03;
    const double CENTI				= 1.E-02;
    const double DECI				= 1.E-01;
    const double ONE				= 1.E+00;
    const double DECA				= 1.E+01;
    const double KILO				= 1.E+03;
    const double MEGA				= 1.E+06;
    const double GIGA				= 1.E+09;
    const double TERA				= 1.E+12;

};

#endif // EPSILON_H
