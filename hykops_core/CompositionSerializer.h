#ifndef COMPOSTIONSERIALIZER_H
#define COMPOSTIONSERIALIZER_H

#include "HComposition.h"
#include "serializedobject.h"


class CompostionSerializer
{
private:
    CompostionSerializer();
public:
    static SerializedObject* serialize(const HComposition *cmp);
    static HComposition *deserialize(QString xml);
};

#endif // COMPOSTIONSERIALIZER_H
