#ifndef SERIESPROFILECIRCLE_H
#define SERIESPROFILECIRCLE_H

#include "ISeriesProfile.h"
#include "Epsilon.h"


const QString seriesNameCIRCLE("CIRCLE");

class SeriesProfileCIRCLE : ISeriesProfile
{
public:
    SeriesProfileCIRCLE();
    QString getSeriesName();
    QString getProfileName();
    void getXiEta(HSeriesProfileCurve2D *curve, double c, QVector3D* qxieta);

};

#endif // SERIESPROFILECIRCLE_H
