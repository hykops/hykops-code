#ifndef MATRIXOPS_H
#define MATRIXOPS_H

#include <QMatrix3x3>

class MatrixOps
{
public:
    static QMatrix3x3 inverse3x3(QMatrix3x3 matrix);
    static double determinante3x3(QMatrix3x3 matrix);
};

#endif // MATRIXOPS_H
