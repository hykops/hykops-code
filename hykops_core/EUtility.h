#ifndef EUTILITY_H
#define EUTILITY_H

#include "EFactory.h"

class EUtility
{
public:
    static QObject* getObjectProperty(const QString& variableName, const QObject* parentObj);
    static void setObjectProperty(const QString& variableName, const QObject* parentObj, QObject *object);

    static QList<QObject*>  *getListObjectProperty(const QString& variableName, const QObject* parentObj);
    static QMap<QString, QString> *getMapObjectProperty(const QString& variableName, const QObject *parentObj);

};

#endif // EUTILITY_H
