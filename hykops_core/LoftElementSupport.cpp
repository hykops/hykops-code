#include "LoftElementSupport.h"

#include <stdexcept>
#include <cmath>
#include <limits>
#include <QDebug>
#include <QList>
#include "QVector3D"

#include "HDiscreteCurve2D.h"
#include "HSeriesProfileCurve2D.h"
#include "HCurvePoint2D.h"
#include "ISeriesProfile.h"
#include "SeriesProfileNACAONE.h"
#include "SeriesProfileCIRCLE.h"


#include "Logger.h"

QVector3D LoftElementSupport::getXiEta(HCurve2D *curve, double c){
     Logger logger;
     try
     {

        if(HDiscreteCurve2D *crv = dynamic_cast<HDiscreteCurve2D*>(curve))
         {
            QList<HCurvePoint2D*>* list = crv->getCurvePoints();
            int np = list->size();
            if(np > 1)
            {
               HCurvePoint2D* p0;
               HCurvePoint2D* p1 = list->at(0);
// piecewise linear interpolation
// TODO: replace by spline interpolation of xi(c) and eta(c)
               for(int i = 1; i < np; i++)
               {
                   p0 = p1;
                   p1 = list->at(i);
                   double c0 = p0->getCurveCoordinate();
                   double c1 = p1->getCurveCoordinate();

                   if((c0>=c && c1 <=c)||(c0<=c && c1 >= c))
                   {
                       double xi0 = p0->getX1();
                       double xi1 = p1->getX1();
                       double eta0 = p0->getX2();
                       double eta1 = p1->getX2();
                       double lc = (c - c0) / (c1 - c0);
                       double xi = xi0 * (1. - lc) + xi1 * lc;
                       double eta = eta0 * (1. - lc) + eta1 * lc;
                       return QVector3D(xi, eta ,0.);
                   }
               }
            }
         }
         else if(HSeriesProfileCurve2D *crv = dynamic_cast<HSeriesProfileCurve2D*>(curve))
         {
            QVector3D xieta(0.,0.,0.);
            QString series = crv->getSeries();
            QString denom = crv->getDenomination();

// TODO: ISeriesProfile aus Map<HSeriesProfileCurve*, ISeriesProfileCurve> holen (vorher auf update checken)
            ISeriesProfile* p;
            if(series == "NACA 1-series")
            {
                p = (ISeriesProfile*) new SeriesProfileNACAONE(denom);
            }
            else if(series == "CIRCLE")
            {
                p = (ISeriesProfile*) new SeriesProfileCIRCLE(); // no denominator for circle
            }
            else
            {
                logger.log(QString("unknwon profile series"));
                return QVector3D(0.,0.,0.);
            }
            p->getXiEta(crv, c, &xieta);
            return xieta;
        }
         else
         {
             logger.log(QString("failed"));
         }
     }
     catch(std::invalid_argument e)
     {
         qDebug() << e.what();
     }
     return QVector3D(0.,0.,0.);
}

