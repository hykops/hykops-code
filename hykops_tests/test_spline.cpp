#include <QtTest/QtTest>

#include "../hykops_data/HPoint3D.h"

class TestHPoint3D
:
    public QObject
{
    Q_OBJECT
private slots:
    void constructFromCoordinates();

    void constructFromNothing();
};

void TestHPoint3D::constructFromCoordinates()
{
    HPoint3D refPoint = HPoint3D(1.0,2.0,3.0);
    QVERIFY(refPoint.getX1() == 1.0);
    QVERIFY(refPoint.getX2() == 2.0);
    QVERIFY(refPoint.getX3() == 3.0);
}

void TestHPoint3D::constructFromNothing()
{
    HPoint3D refPoint = HPoint3D();
    refPoint.setX1(1.0);
    refPoint.setX2(2.0);
    refPoint.setX3(3.0);
    QVERIFY(refPoint.getX1() == 1.0);
    QVERIFY(refPoint.getX2() == 2.0);
    QVERIFY(refPoint.getX3() == 3.0);
}

QTEST_MAIN(TestHPoint3D)
#include "test_spline.moc"
