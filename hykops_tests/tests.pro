QT       -= gui
QT += core \
    testlib

TARGET = test_spline

LIBS_PATH = -L/tmp/libs
INC_PATH = -I../hykops_data \
            -I../hykops_core \
            -I../serializeemf

LIBS += -L/tmp/libs/ \
        -L/tmp/libs/hykops_data \
        -L../hykops_data \
        -L../hykops_core \
        -L../serializeemf \
        -lhykops_data \ 
        -lhykops_core \
        -lserializeemf

DEPENDPATH += . \
                /tmp/libs \
                ../hykops_core \
                ../hykops_data \
                ../serializeemf

INCLUDEPATH += . \
                /tmp/libs \
                ../hykops_core \
                ../hykops_data \
                ../serializeemf

SOURCES = test_spline.cpp

# install
unix {
    target.path = .
    INSTALLS += target
}
