package de.hykops.jna.datamodel.list;


import java.util.List;
import java.util.ListIterator;

import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;

import com.sun.jna.NativeLong;

import de.hykops.jna.datamodel.core.InteropUtil;
import de.hykops.lpd.datamodel.Handler;

public class EMFHykopsListWithInverse<E extends Handler> extends EObjectContainmentWithInverseEList<E> {

	private static final long serialVersionUID = 2798491368269715845L;

	private InteropUtil<E> interop;
	private String property;

	public EMFHykopsListWithInverse(NativeLong handle, String property, Class<?> clazz, InternalEObject eobject, int feautreId, int inverseFeatureId) {
		super(clazz, eobject, feautreId, inverseFeatureId);
		this.property = property;
		this.interop = new InteropUtil<E>(handle);
	}

	@Override
	public boolean add(E e) {
		boolean result = super.add(e);
		interop.setList(property, this);
		return result;
	}

	@Override
	public E remove(int index) {
		E removedObj = super.remove(index);
		interop.setList(property, this);
		return removedObj;
	}
	
	@Override
	public boolean contains(Object o) {
		for (E e : this) {
			if (e.equals(o)) {
				return true;
			}
		}
		return super.contains(o);
	}
	
	public boolean equals(Object o) {
	 if (o == this)
         return true;
     if (!(o instanceof List))
         return false;

     ListIterator<E> e1 = listIterator();
     ListIterator<?> e2 = ((List<?>) o).listIterator();
     while (e1.hasNext() && e2.hasNext()) {
         E o1 = e1.next();
         Object o2 = e2.next();
         if (!(o1==null ? o2==null : o1.equals(o2)))
             return false;
     }
     return !(e1.hasNext() || e2.hasNext());
	}
}
