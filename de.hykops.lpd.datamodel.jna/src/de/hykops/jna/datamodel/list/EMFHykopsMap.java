package de.hykops.jna.datamodel.list;

import java.util.List;
import java.util.ListIterator;
import java.util.Map;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.util.EcoreEMap;

import com.sun.jna.NativeLong;

import de.hykops.jna.datamodel.core.InteropUtil;
import de.hykops.lpd.datamodel.HComposition;

public class EMFHykopsMap extends EcoreEMap<String, String> {

	private static final long serialVersionUID = 8781035590631423112L;
	private InteropUtil<HComposition> interop;
	private String propertyName;

	public EMFHykopsMap(NativeLong handle, String property, EClass entryEClass, Class<?> entryClass, InternalEObject owner,
			int featureId) {
		super(entryEClass, entryClass, owner, featureId);
		interop = new InteropUtil<>(handle);
		this.propertyName = property;
	}

	@Override
	public String put(String key, String value) {
		String v = super.put(key, value);
		interop.setMap(propertyName, this);
		return v;
	}
	
	@Override
	public boolean contains(Object o) {
		for (Map.Entry<String, String> e : this) {
			if (e.getKey().equals(o)) {
				return true;
			}
		}
		return super.contains(o);
	}

	public boolean equals(Object o) {
		if (o == this)
			return true;
		if (!(o instanceof List))
			return false;

		ListIterator<Map.Entry<String, String>> e1 = listIterator();
		ListIterator<?> e2 = ((List<?>) o).listIterator();
		while (e1.hasNext() && e2.hasNext()) {
			Map.Entry<String, String> o1 = e1.next();
			Object o2 = e2.next();
			if (!(o1 == null ? o2 == null : o1.equals(o2)))
				return false;
		}
		return !(e1.hasNext() || e2.hasNext());
	}

}
