package de.hykops.jna.datamodel.factory;

import com.sun.jna.NativeLong;

import de.hykops.jna.datamodel.core.InteropUtil;
import de.hykops.jna.datamodel.impl.HComponentImpl;
import de.hykops.jna.datamodel.impl.HCompositionImpl;
import de.hykops.jna.datamodel.impl.HCoordinateSystem3DAffineImpl;
import de.hykops.jna.datamodel.impl.HCoordinateSystem3DCartesianImpl;
import de.hykops.jna.datamodel.impl.HCoordinateSystem3DConeImpl;
import de.hykops.jna.datamodel.impl.HCoordinateSystem3DPolarImpl;
import de.hykops.jna.datamodel.impl.HCurvePoint2DImpl;
import de.hykops.jna.datamodel.impl.HDiscreteCurve2DImpl;
import de.hykops.jna.datamodel.impl.HDiscreteLoftPathImpl;
import de.hykops.jna.datamodel.impl.HDiscreteLoftPathPoint3DImpl;
import de.hykops.jna.datamodel.impl.HHingeImpl;
import de.hykops.jna.datamodel.impl.HLinearLoftPathImpl;
import de.hykops.jna.datamodel.impl.HLoftElementImpl;
import de.hykops.jna.datamodel.impl.HLoftImpl;
import de.hykops.jna.datamodel.impl.HLoftPathImpl;
import de.hykops.jna.datamodel.impl.HPoint2DImpl;
import de.hykops.jna.datamodel.impl.HPoint3DImpl;
import de.hykops.jna.datamodel.impl.HSeriesProfileCurve2DImpl;
import de.hykops.jna.datamodel.impl.HSurfaceIdentifierImpl;
import de.hykops.lpd.datamodel.HComposition;
import de.hykops.lpd.datamodel.HParent;

public class HykopsObjectFactory {

	public static HParent createObject(NativeLong handle) {

		String className = InteropUtil.getType(handle);
		
		final String bla = getClassName(HComposition.class);
		
		switch (className) {

		case "HComposition":
			return new HCompositionImpl(handle);
		case "HComponent":
			return new HComponentImpl(handle);
		case "HSurfaceIdentifier":
			return new HSurfaceIdentifierImpl();
		case "HPoint2D":
			return new HPoint2DImpl();
		case "HPoint3D":
			return new HPoint3DImpl(handle);
		case "HCurvePoint2D":
			return new HCurvePoint2DImpl(handle);
		case "HDiscreteLoftPathPoint3D":
			return new HDiscreteLoftPathPoint3DImpl(handle);
		case "HHinge":
			return new HHingeImpl(handle);
		case "HDiscreteCurve2D":
			return new HDiscreteCurve2DImpl(handle);
		case "HSeriesProfileCurve2D":
			return new HSeriesProfileCurve2DImpl(handle);
		case "HCoordinateSystem3DCartesian":
			return new HCoordinateSystem3DCartesianImpl(handle);
		case "HCoordinateSystem3DPolar":
			return new HCoordinateSystem3DPolarImpl(handle);
		case "HCoordinateSystem3DAffine":
			return new HCoordinateSystem3DAffineImpl(handle);
		case "HLoftElement":
			return new HLoftElementImpl(handle);
		case "HCoordinateSystem3DCone":
			return new HCoordinateSystem3DConeImpl(handle);
		case "HLoftPath":
			return new HLoftPathImpl(handle);
		case "HDiscreteLoftPath":
			return new HDiscreteLoftPathImpl(handle);
		case "HLinearLoftPath":
			return new HLinearLoftPathImpl(handle);
		case "HLoft":
			return new HLoftImpl(handle);
		default:
			return null;
		}
	}
	
	private static String getClassName(Class<?> clazz) {
		return clazz.getSimpleName();
	}

}
