package de.hykops.jna.datamodel.factory;

import de.hykops.jna.datamodel.core.CoordinateTransformationSupport;
import de.hykops.jna.datamodel.impl.HComponentImpl;
import de.hykops.jna.datamodel.impl.HCompositionImpl;
import de.hykops.jna.datamodel.impl.HCoordinateSystem3DAffineImpl;
import de.hykops.jna.datamodel.impl.HCoordinateSystem3DCartesianImpl;
import de.hykops.jna.datamodel.impl.HCoordinateSystem3DConeImpl;
import de.hykops.jna.datamodel.impl.HCoordinateSystem3DPolarImpl;
import de.hykops.jna.datamodel.impl.HCurvePoint2DImpl;
import de.hykops.jna.datamodel.impl.HDiscreteCurve2DImpl;
import de.hykops.jna.datamodel.impl.HLinearLoftPathImpl;
import de.hykops.jna.datamodel.impl.HLoftElementImpl;
import de.hykops.jna.datamodel.impl.HLoftImpl;
import de.hykops.jna.datamodel.impl.HLoftPathImpl;
import de.hykops.jna.datamodel.impl.HPoint2DImpl;
import de.hykops.jna.datamodel.impl.HPoint3DImpl;
import de.hykops.jna.datamodel.impl.HSeriesProfileCurve2DImpl;
import de.hykops.jna.datamodel.impl.HSurfaceIdentifierImpl;
import de.hykops.jna.datamodel.serialize.Deserializer;
import de.hykops.jna.datamodel.serialize.Serializer;
import de.hykops.lpd.core.factory.IHykopsFactory;
import de.hykops.lpd.core.sdk.ICoordinateTransformation;
import de.hykops.lpd.core.serialize.IDeserializer;
import de.hykops.lpd.core.serialize.ISerializer;
import de.hykops.lpd.datamodel.HComponent;
import de.hykops.lpd.datamodel.HComposition;
import de.hykops.lpd.datamodel.HCoordinateSystem3DAffine;
import de.hykops.lpd.datamodel.HCoordinateSystem3DCartesian;
import de.hykops.lpd.datamodel.HCoordinateSystem3DCone;
import de.hykops.lpd.datamodel.HCoordinateSystem3DPolar;
import de.hykops.lpd.datamodel.HCurvePoint2D;
import de.hykops.lpd.datamodel.HDiscreteCurve2D;
import de.hykops.lpd.datamodel.HLinearLoftPath;
import de.hykops.lpd.datamodel.HLoft;
import de.hykops.lpd.datamodel.HLoftElement;
import de.hykops.lpd.datamodel.HLoftPath;
import de.hykops.lpd.datamodel.HPoint2D;
import de.hykops.lpd.datamodel.HPoint3D;
import de.hykops.lpd.datamodel.HSeriesProfileCurve2D;
import de.hykops.lpd.datamodel.HSurfaceIdentifier;

public class HykopsFactory implements IHykopsFactory {

	@Override
	public HComponent createComponent() {
		return new HComponentImpl();
	}

	@Override
	public HComposition createComposition() {
		return new HCompositionImpl();
	}

	@Override
	public HCoordinateSystem3DCartesian createCoordinateSystemCartesian() {
		return new HCoordinateSystem3DCartesianImpl();
	}

	@Override
	public HCoordinateSystem3DAffine createCoordinateSystemAffine() {
		return new HCoordinateSystem3DAffineImpl();
	}

	@Override
	public HCoordinateSystem3DPolar createCoordinateSystemPolar() {
		return new HCoordinateSystem3DPolarImpl();
	}

	@Override
	public HCoordinateSystem3DCone createCoordinateSystemCone() {
		return new HCoordinateSystem3DConeImpl();
	}
	
	@Override
	public HLoft createLoft() {
		return new HLoftImpl();
	}

	@Override
	public HLoftElement createLoftElement() {
		return new HLoftElementImpl();
	}

	@Override
	public HLoftPath createLoftPath() {
		return new HLoftPathImpl();
	}


	@Override
	public HLinearLoftPath createLinearLoftPath() {
		return new HLinearLoftPathImpl();
	}

	@Override
	public HSeriesProfileCurve2D createSeriesProfileCurve2D() {
		return new HSeriesProfileCurve2DImpl();
	}

	@Override
	public HDiscreteCurve2D createDiscreteCurve2D() {
		return new HDiscreteCurve2DImpl();
	}

	@Override
	public HPoint2D createPoint2D() {
		return new HPoint2DImpl();
	}

	@Override
	public HCurvePoint2D createCurvePoint2D() {
		return new HCurvePoint2DImpl();
	}

	@Override
	public HPoint3D createPoint3D() {
		return new HPoint3DImpl();
	}

	@Override
	public HSurfaceIdentifier createSurfaceIdentifier() {
		return new HSurfaceIdentifierImpl();
	}

	@Override
	public ISerializer createSerializer() {
		return new Serializer();
	}

	@Override
	public IDeserializer createDeserializer() {
		return new Deserializer();
	}

	@Override
	public ICoordinateTransformation createCoordinateTransformation() {
		return new CoordinateTransformationSupport();
	}

}
