package de.hykops.jna.datamodel.factory;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import com.sun.jna.NativeLong;

import de.hykops.jna.datamodel.impl.HCoordinateSystem3DAffineImpl;
import de.hykops.jna.datamodel.impl.HCoordinateSystem3DCartesianImpl;
import de.hykops.jna.datamodel.impl.HCoordinateSystem3DConeImpl;
import de.hykops.jna.datamodel.impl.HCoordinateSystem3DPolarImpl;
import de.hykops.lpd.datamodel.HCoordinateSystem3D;

public class CoordinateSystemFactory {

	private static Map<String, Class<? extends HCoordinateSystem3D>> types = new HashMap<>();
	static {

		types.put("HCoordinateSystem3DCartesian", HCoordinateSystem3DCartesianImpl.class);
		types.put("HCoordinateSystem3DAffine", HCoordinateSystem3DAffineImpl.class);
		types.put("hCoordinateSystem3DPolar", HCoordinateSystem3DPolarImpl.class);
		types.put("hCoordinateSystem3DCone", HCoordinateSystem3DConeImpl.class);

	}

	public static HCoordinateSystem3D create(String name, NativeLong handle) {

		try {
			if (types.containsKey(name)) {
				return types.get(name).getDeclaredConstructor(NativeLong.class).newInstance(handle);
			} else {
				System.out.println("Unknown type: " + name);
			}
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		}
		return null;

	}

	public static HCoordinateSystem3D create(String name) {

		try {
			return types.get(name).newInstance();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (SecurityException e) {
			e.printStackTrace();
		}
		return null;

	}

}
