package de.hykops.jna.datamodel.core;

import com.sun.jna.Native;
import com.sun.jna.ptr.PointerByReference;

public class StringByReference extends PointerByReference {

	private static final int DEFAULT_SIZE = 1024;
	private int size = 0;

	public StringByReference() {}

	public StringByReference(int size) {
		this.size = size;
	}

	public String get() {
	
		int arraySize = (size == 0) ? DEFAULT_SIZE : size;
		return Native.toString(getValue().getByteArray(0, arraySize));
	}

}
