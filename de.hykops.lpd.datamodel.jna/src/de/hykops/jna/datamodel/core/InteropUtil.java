package de.hykops.jna.datamodel.core;

import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import com.sun.jna.Native;
import com.sun.jna.NativeLong;

import de.hykops.jna.datamodel.factory.HykopsObjectFactory;
import de.hykops.jna.datamodel.list.EMFHykopsList;
import de.hykops.jna.datamodel.list.EMFHykopsListWithInverse;
import de.hykops.jna.datamodel.list.EMFHykopsListWithResolving;
import de.hykops.jna.datamodel.list.EMFHykopsMap;
import de.hykops.lpd.datamodel.Handler;

public class InteropUtil<T extends Handler> {

	private static final HykopsCore CORE = HykopsCore.INSTANCE;
	private static final int DEFAULT_STRING_BUFFER_SIZE = 256;
	private static final int STRING_BUFFER_STEP_SIZE = 256;

	private NativeLong handle;

	/**
	 * The handle from the object that holds the list
	 * 
	 * @param handle
	 */
	public InteropUtil(NativeLong handle) {
		this.handle = handle;
	}

	/***
	 * Returns an list from the given object handler.
	 * 
	 * @param clazz
	 *            the type from the list elements
	 * @param the
	 *            propertyName from the object handle list
	 * @return
	 */
	public EList<T> getList(String propertyName, Class<?> clazz, InternalEObject internalObject, int featureId,
			int inverseFeatureId) {
		return getList(propertyName, new EMFHykopsListWithInverse<>(handle, propertyName, clazz, internalObject,
				featureId, inverseFeatureId));
	}

	public EList<T> getList(String propertyName, Class<?> clazz, InternalEObject internalObject, int featureId,
			boolean withResolving) {
		if (withResolving) {
			return getList(propertyName,
					new EMFHykopsListWithResolving<>(handle, propertyName, clazz, internalObject, featureId));
		}
		return getList(propertyName, new EMFHykopsList<>(handle, propertyName, clazz, internalObject, featureId));
	}

	@SuppressWarnings("unchecked")
	private EList<T> getList(String propertyName, EList<T> result) {
		int listSize = CORE.get_list_size(handle, propertyName);

		if (listSize > 0) {
			final NativeLong[] handles = new NativeLong[listSize];

			int size = CORE.get_list(handle, propertyName, handles, listSize);
			if (size > 0) {
				for (NativeLong nHandle : handles) {
					try {
						Object o = HykopsObjectFactory.createObject(nHandle);
						result.add((T) o.getClass().getDeclaredConstructor(NativeLong.class).newInstance(nHandle));
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
		return result;
	}

	/***
	 * Sets an list on object handler
	 * 
	 * @param the
	 *            propertyName from the object handler list
	 * @param items
	 *            to set
	 */
	public void setList(String propertyName, List<T> items) {
		NativeLong[] handles = items.stream().map(Handler::getHandle).toArray(NativeLong[]::new);
		CORE.set_list(handle, propertyName, handles, items.size());
	}

	public void setMap(String propertyName, EMap<String, String> map) {

		int mapSize = map.size();
		if (mapSize > 0) {
			String[] keys = new String[mapSize];
			int[] keyLength = new int[mapSize];

			String[] values = new String[mapSize];
			int[] valueLength = new int[mapSize];

			for (int i = 0; i < mapSize; i++) {

				Map.Entry<String, String> entry = map.get(i);
				keys[i] = entry.getKey();
				keyLength[i] = entry.getKey().length();

				values[i] = entry.getValue();
				valueLength[i] = entry.getValue().length();
			}
			CORE.set_map(handle, propertyName, keys, keyLength, values, valueLength, map.size());
		}
	}

	public EMap<String, String> getMap(String propertyName, EClass entryEClass, Class<?> entryClass,
			InternalEObject owner, int featureId) {

		EMap<String, String> result = new EMFHykopsMap(handle, propertyName, entryEClass, entryClass, owner, featureId);

		int mapSize = CORE.get_map_size(handle, propertyName);
		if (mapSize > 0) {

			String[] keys = new String[mapSize];
			String[] values = new String[mapSize];

			for (int i = 0; i < mapSize; i++) {
				keys[i] = new String(new char[1024]);
				values[i] = new String(new char[1024]);
			}

			CORE.get_map(handle, propertyName, keys, values);
			for (int i = 0; i < mapSize; i++) {
				result.put(keys[i], values[i]);
			}

		}
		return result;
	}

	public static String getString(NativeLong handle, String property) {
		return getString(handle, property, DEFAULT_STRING_BUFFER_SIZE);
	}

	private static String getString(NativeLong handle, String property, int size) {
		byte[] str = new byte[size];
		if (CORE.get_string(handle, property, str, str.length) == -1) {
			return getString(handle, property, str.length + STRING_BUFFER_STEP_SIZE);
		}
		return Native.toString(str);
	}

	public static String getType(NativeLong handle) {

		byte[] buffer = new byte[128];
		if (CORE.get_type(handle, buffer, buffer.length) == 0) {
			return Native.toString(buffer);
		}
		return "Buffer too small";
	}
}
