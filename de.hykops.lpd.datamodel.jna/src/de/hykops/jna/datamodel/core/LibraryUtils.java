package de.hykops.jna.datamodel.core;

public class LibraryUtils {

	private static final String FOLDER_WINDOWS = "win32";
	private static final String FOLDER_LINUX = "linux";

	private static final String FOLDER_32 = "x86";
	private static final String FOLDER_64 = "x86_64";
	
	private LibraryUtils() { }
	
	public static String getOSGiLibraryDirectoryName() {
		String os = System.getProperty("os.name").toLowerCase();
		String arch = System.getProperty("os.arch").toLowerCase();

		StringBuilder builder = new StringBuilder();
		builder.append(os.startsWith("win") ? FOLDER_WINDOWS : FOLDER_LINUX);
		builder.append("-");
		builder.append(arch.endsWith("64") ? FOLDER_64 : FOLDER_32);
		builder.append("/");
		return builder.toString();
	}

	public static String buildLibName(String lib) {
		String os = System.getProperty("os.name").toLowerCase();
		if (os.startsWith("win")) {
			return lib + ".dll";
		}
		return "lib" + lib + ".so.1";
	}

}
