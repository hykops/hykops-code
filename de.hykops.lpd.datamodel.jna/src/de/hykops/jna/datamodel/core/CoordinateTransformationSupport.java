/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.jna.datamodel.core;

import de.hykops.lpd.core.sdk.ICoordinateTransformation;
import de.hykops.lpd.datamodel.HCoordinateSystem3DCartesian;
import de.hykops.lpd.datamodel.HLoftElement;
import de.hykops.lpd.datamodel.HLoftPath;

/**
 *
 * @author stoye
 * @since Jul 1, 2016
 */
public class CoordinateTransformationSupport implements ICoordinateTransformation {

	/**
	 * Transform point in loft path coordinates to global coordinate
	 * 
	 * @param path
	 *            the loft path
	 * @param xi
	 *            coordinate 1 perpendicular to loft axis
	 * @param eta
	 *            coordinate 2 perpendicular to loft axis
	 * @param loft
	 *            coordinate the loft coordinate
	 * @return global cartesian point coordinates
	 *
	 * @author stoye
	 * @since Jul 18, 2016
	 */
	public double[] toGlobalCoordinate(HLoftPath path, double xi, double eta, double loftCoordinate) {
		double[] global = new double[3];
		HykopsCore.INSTANCE.toGlobalCoordinatePath(path.getHandle(), xi, eta, loftCoordinate, global);
		return global;
	}

	/**
	 * Transform point on loft element to global coordinate
	 * 
	 * @param element
	 *            the loft element
	 * @param xi
	 *            coordinate 1 in loft plane
	 * @param eta
	 *            coordinate 2 in loft plane
	 * @return global cartesian point coordinates
	 *
	 * @author stoye
	 * @since Jul 18, 2016
	 */
	public double[] toGlobalCoordinate(HLoftElement element, double xi, double eta) {
		double[] global = new double[3];
		HykopsCore.INSTANCE.toGlobalCoordinate(element.getHandle(), xi, eta, global);
		return global;
	}

	/**
	 * Transform point in global coordinates to loft plane coordinate point
	 * 
	 * @param element
	 *            the loft element
	 * @param p
	 *            point in global coordinates
	 * @return point in loft plane coordinates
	 *
	 * @author stoye
	 * @since Jul 18, 2016
	 */
	public double[] toLocalCoordinate(HLoftElement element, double p[]) {		
		double[] local = new double[3];
		HykopsCore.INSTANCE.toLocalCoordinate(element.getHandle(), p, local);
		return local;
	}
}
