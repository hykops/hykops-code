package de.hykops.jna.datamodel.core;

import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.NativeLong;
import com.sun.jna.ptr.NativeLongByReference;

public interface HykopsCore extends Library {

	public static final String LIB_HYKOPS_CORE = "hykops_core";

	HykopsCore INSTANCE = (HykopsCore) Native.loadLibrary(getCoreLibName(), HykopsCore.class);

	static String getCoreLibName() {
		return LibraryUtils.buildLibName(LIB_HYKOPS_CORE);
	}

	/***
	 * Initializing of Framework
	 */
	void init();

	/**
	 * Object creation / deletion
	 */
	NativeLong create_object(String className);

	int delete_object(NativeLong handle);

	/***
	 * Getter
	 */
	boolean get_boolean(NativeLong handle, String property);

	int get_string(NativeLong handle, String property, byte[] buffer, int bufLength);

	int get_int(NativeLong handle, String property);

	double get_double(NativeLong handle, String property);

	NativeLong get_object(NativeLong handle, String property);

	int get_list(NativeLong handle, String propertyName, NativeLong[] arrayToFill, int size);

	int get_map(NativeLong handle, String property, String[] keys, String[] values);

	/***
	 * Setter
	 */
	void set_boolean(NativeLong handle, String property, boolean value);

	void set_string(NativeLong handle, String property, String value);

	void set_int(NativeLong handle, String property, int value);

	void set_double(NativeLong handle, String property, double value);

	void set_object(NativeLong handle, String property, NativeLong object);

	void set_list(NativeLong handle, String property, NativeLong[] list, int size);

	void set_map(NativeLong handle, String property, String[] keys, int keyLength[], String[] values, int valueLength[],
			int mapSize);

	/**
	 * Collections
	 */
	int get_list_size(NativeLong handle, String propertyName);

	int get_map_size(NativeLong handle, String propertyName);

	/***
	 * Utility
	 */
	int get_type(NativeLong handle, byte[] buffer, int bufferLength);

	/***
	 * De-/Serializing
	 */
	NativeLong serialize(NativeLong handle);

	int get_serialized_text_size(NativeLong serialized_handle);

	int get_serialized_string(NativeLong handle, byte[] ptr);

	int deserialize(NativeLongByReference handle, String xml);

	/***
	 * Coordinatesystem Transformation
	 */
	int toGlobalCoordinate(NativeLong loftElementHandle, double xi, double eta, double[] points);

	int toGlobalCoordinatePath(NativeLong loftElementHandle, double xi, double eta, double loftCoordinate, double[] points);

	int toLocalCoordinate(NativeLong loftElementHandle, double [] global, double[] points);

	/***
	 * Loft curve interpolation
	 */
	int getXiEta(NativeLong curve2DHandle, double c, double[] xieta); 
}
