/**
 */
package de.hykops.jna.datamodel.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import com.sun.jna.NativeLong;

import de.hykops.jna.datamodel.factory.HykopsObjectFactory;
import de.hykops.lpd.datamodel.DatamodelPackage;
import de.hykops.lpd.datamodel.HCoordinateSystem3D;
import de.hykops.lpd.datamodel.HHinge;
import de.hykops.lpd.datamodel.HPair;
import de.hykops.lpd.datamodel.HPoint3D;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>HHinge</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.hykops.lpd.datamodel.impl.HHingeImpl#getHingeAxis <em>Hinge Axis</em>}</li>
 *   <li>{@link de.hykops.lpd.datamodel.impl.HHingeImpl#getHingePositions <em>Hinge Positions</em>}</li>
 *   <li>{@link de.hykops.lpd.datamodel.impl.HHingeImpl#isHingeLimited <em>Hinge Limited</em>}</li>
 *   <li>{@link de.hykops.lpd.datamodel.impl.HHingeImpl#getHCoordinateSystem3D <em>HCoordinate System3 D</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HHingeImpl extends HParentImpl implements HHinge {
	/**
	 * The cached value of the '{@link #getHingeAxis() <em>Hinge Axis</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHingeAxis()
	 * @generated
	 * @ordered
	 */
	protected HPoint3D hingeAxis;

	/**
	 * The cached value of the '{@link #getHingePositions() <em>Hinge Positions</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHingePositions()
	 * @generated
	 * @ordered
	 */
	protected EList<HPair<String, Double>> hingePositions;

	/**
	 * The default value of the '{@link #isHingeLimited() <em>Hinge Limited</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHingeLimited()
	 * @generated
	 * @ordered
	 */
	protected static final boolean HINGE_LIMITED_EDEFAULT = false;

	/**
	 * The cached value of the '{@link #isHingeLimited() <em>Hinge Limited</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #isHingeLimited()
	 * @generated
	 * @ordered
	 */
	protected Boolean hingeLimited = HINGE_LIMITED_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HHingeImpl() {
		this(CORE.create_object(CLASS_NAME));
	}

	public HHingeImpl(NativeLong handle) {
		super();
		this.handle = handle;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DatamodelPackage.Literals.HHINGE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HPoint3D getHingeAxis() {
		if (this.hingeAxis == null) {
			NativeLong objHandle = CORE.get_object(getHandle(), PROPERTY_HINGE_AXIS);
			if (objHandle .intValue() != 0) {
				this.hingeAxis = (HPoint3D) HykopsObjectFactory.createObject(objHandle);
			}
		}
		return hingeAxis;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHingeAxis(HPoint3D newHingeAxis, NotificationChain msgs) {
		HPoint3D oldHingeAxis = hingeAxis;
		hingeAxis = newHingeAxis;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DatamodelPackage.HHINGE__HINGE_AXIS, oldHingeAxis, newHingeAxis);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHingeAxis(HPoint3D newHingeAxis) {
		CORE.set_object(getHandle(), PROPERTY_HINGE_AXIS, newHingeAxis.getHandle());
		if (newHingeAxis != hingeAxis) {
			NotificationChain msgs = null;
			if (hingeAxis != null)
				msgs = ((InternalEObject)hingeAxis).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DatamodelPackage.HHINGE__HINGE_AXIS, null, msgs);
			if (newHingeAxis != null)
				msgs = ((InternalEObject)newHingeAxis).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DatamodelPackage.HHINGE__HINGE_AXIS, null, msgs);
			msgs = basicSetHingeAxis(newHingeAxis, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DatamodelPackage.HHINGE__HINGE_AXIS, newHingeAxis, newHingeAxis));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<HPair<String, Double>> getHingePositions() {
		if (hingePositions == null) {
			//hingePositions = new InteropUtil<HPair<String, Double>>(getHandle()).getList(, HPair.class, this, DatamodelPackage.HHINGE__HINGE_POSITIONS);
			hingePositions = new EObjectContainmentEList<HPair<String, Double>>(HPair.class, this, DatamodelPackage.HHINGE__HINGE_POSITIONS);
		}
		return hingePositions;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Boolean isHingeLimited() {
		if (this.hingeLimited == null) {
			this.hingeLimited = CORE.get_boolean(getHandle(), PROPERTY_HINGE_LIMITED);
		}
		return hingeLimited;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHingeLimited(Boolean newHingeLimited) {
		CORE.set_boolean(getHandle(), PROPERTY_HINGE_LIMITED, newHingeLimited);
		boolean oldHingeLimited = hingeLimited;
		hingeLimited = newHingeLimited;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DatamodelPackage.HHINGE__HINGE_LIMITED, oldHingeLimited, hingeLimited.booleanValue()));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HCoordinateSystem3D getHCoordinateSystem3D() {
		if (eContainerFeatureID() != DatamodelPackage.HHINGE__HCOORDINATE_SYSTEM3_D) return null;
		return (HCoordinateSystem3D)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHCoordinateSystem3D(HCoordinateSystem3D newHCoordinateSystem3D, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newHCoordinateSystem3D, DatamodelPackage.HHINGE__HCOORDINATE_SYSTEM3_D, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHCoordinateSystem3D(HCoordinateSystem3D newHCoordinateSystem3D) {
		CORE.set_object(getHandle(), PROPERTY_COORDINATE_SYSTEM, newHCoordinateSystem3D.getHandle());
		if (newHCoordinateSystem3D != eInternalContainer() || (eContainerFeatureID() != DatamodelPackage.HHINGE__HCOORDINATE_SYSTEM3_D && newHCoordinateSystem3D != null)) {
			if (EcoreUtil.isAncestor(this, newHCoordinateSystem3D))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newHCoordinateSystem3D != null)
				msgs = ((InternalEObject)newHCoordinateSystem3D).eInverseAdd(this, DatamodelPackage.HCOORDINATE_SYSTEM3_D__HHINGES, HCoordinateSystem3D.class, msgs);
			msgs = basicSetHCoordinateSystem3D(newHCoordinateSystem3D, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DatamodelPackage.HHINGE__HCOORDINATE_SYSTEM3_D, newHCoordinateSystem3D, newHCoordinateSystem3D));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DatamodelPackage.HHINGE__HCOORDINATE_SYSTEM3_D:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetHCoordinateSystem3D((HCoordinateSystem3D)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DatamodelPackage.HHINGE__HINGE_AXIS:
				return basicSetHingeAxis(null, msgs);
			case DatamodelPackage.HHINGE__HINGE_POSITIONS:
				return ((InternalEList<?>)getHingePositions()).basicRemove(otherEnd, msgs);
			case DatamodelPackage.HHINGE__HCOORDINATE_SYSTEM3_D:
				return basicSetHCoordinateSystem3D(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case DatamodelPackage.HHINGE__HCOORDINATE_SYSTEM3_D:
				return eInternalContainer().eInverseRemove(this, DatamodelPackage.HCOORDINATE_SYSTEM3_D__HHINGES, HCoordinateSystem3D.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DatamodelPackage.HHINGE__HINGE_AXIS:
				return getHingeAxis();
			case DatamodelPackage.HHINGE__HINGE_POSITIONS:
				return getHingePositions();
			case DatamodelPackage.HHINGE__HINGE_LIMITED:
				return isHingeLimited();
			case DatamodelPackage.HHINGE__HCOORDINATE_SYSTEM3_D:
				return getHCoordinateSystem3D();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DatamodelPackage.HHINGE__HINGE_AXIS:
				setHingeAxis((HPoint3D)newValue);
				return;
			case DatamodelPackage.HHINGE__HINGE_POSITIONS:
				getHingePositions().clear();
				getHingePositions().addAll((Collection<? extends HPair<String, Double>>)newValue);
				return;
			case DatamodelPackage.HHINGE__HINGE_LIMITED:
				setHingeLimited((Boolean)newValue);
				return;
			case DatamodelPackage.HHINGE__HCOORDINATE_SYSTEM3_D:
				setHCoordinateSystem3D((HCoordinateSystem3D)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DatamodelPackage.HHINGE__HINGE_AXIS:
				setHingeAxis((HPoint3D)null);
				return;
			case DatamodelPackage.HHINGE__HINGE_POSITIONS:
				getHingePositions().clear();
				return;
			case DatamodelPackage.HHINGE__HINGE_LIMITED:
				setHingeLimited(HINGE_LIMITED_EDEFAULT);
				return;
			case DatamodelPackage.HHINGE__HCOORDINATE_SYSTEM3_D:
				setHCoordinateSystem3D((HCoordinateSystem3D)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DatamodelPackage.HHINGE__HINGE_AXIS:
				return hingeAxis != null;
			case DatamodelPackage.HHINGE__HINGE_POSITIONS:
				return hingePositions != null && !hingePositions.isEmpty();
			case DatamodelPackage.HHINGE__HINGE_LIMITED:
				return hingeLimited != HINGE_LIMITED_EDEFAULT;
			case DatamodelPackage.HHINGE__HCOORDINATE_SYSTEM3_D:
				return getHCoordinateSystem3D() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (hingeLimited: ");
		result.append(hingeLimited);
		result.append(')');
		return result.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getHingeAxis() == null) ? 0 : getHingeAxis().hashCode());
		result = prime * result + ((isHingeLimited() == null) ? 0 : isHingeLimited().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HHingeImpl other = (HHingeImpl) obj;
		if (getHingeAxis() == null) {
			if (other.getHingeAxis() != null)
				return false;
		} else if (!getHingeAxis().equals(other.getHingeAxis()))
			return false;
		if (isHingeLimited() == null) {
			if (other.isHingeLimited() != null)
				return false;
		} else if (!isHingeLimited().equals(other.isHingeLimited()))
			return false;
		return true;
	}
	
} //HHingeImpl
