/**
 */
package de.hykops.jna.datamodel.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import com.sun.jna.NativeLong;

import de.hykops.jna.datamodel.factory.HykopsObjectFactory;
import de.hykops.lpd.datamodel.DatamodelPackage;
import de.hykops.lpd.datamodel.HLinearLoftPath;
import de.hykops.lpd.datamodel.HPoint3D;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>HLinear Loft Path</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.hykops.lpd.datamodel.impl.HLinearLoftPathImpl#getOrigin <em>Origin</em>}</li>
 *   <li>{@link de.hykops.lpd.datamodel.impl.HLinearLoftPathImpl#getLoftPathVector <em>Loft Path Vector</em>}</li>
 *   <li>{@link de.hykops.lpd.datamodel.impl.HLinearLoftPathImpl#getLoftPathXi <em>Loft Path Xi</em>}</li>
 *   <li>{@link de.hykops.lpd.datamodel.impl.HLinearLoftPathImpl#getLoftPathEta <em>Loft Path Eta</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HLinearLoftPathImpl extends HLoftPathImpl implements HLinearLoftPath {
	/**
	 * The cached value of the '{@link #getOrigin() <em>Origin</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOrigin()
	 * @generated
	 * @ordered
	 */
	protected HPoint3D origin;

	/**
	 * The cached value of the '{@link #getLoftPathVector() <em>Loft Path Vector</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLoftPathVector()
	 * @generated
	 * @ordered
	 */
	protected HPoint3D loftPathVector;

	/**
	 * The cached value of the '{@link #getLoftPathXi() <em>Loft Path Xi</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLoftPathXi()
	 * @generated
	 * @ordered
	 */
	protected HPoint3D loftPathXi;

	/**
	 * The cached value of the '{@link #getLoftPathEta() <em>Loft Path Eta</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLoftPathEta()
	 * @generated
	 * @ordered
	 */
	protected HPoint3D loftPathEta;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HLinearLoftPathImpl(NativeLong handle) {
		this.handle = handle;
	}

	public HLinearLoftPathImpl() {
		this.handle = CORE.create_object(HLinearLoftPath.CLASS_NAME);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DatamodelPackage.Literals.HLINEAR_LOFT_PATH;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HPoint3D getOrigin() {
		if (this.origin == null) {
			NativeLong objHandle = CORE.get_object(getHandle(), PROPERTY_ORIGIN);
			if (objHandle.intValue() != 0) {
				this.origin = (HPoint3D) HykopsObjectFactory.createObject(objHandle);
			}
		}
		return origin;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOrigin(HPoint3D newOrigin, NotificationChain msgs) {
		HPoint3D oldOrigin = origin;
		origin = newOrigin;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DatamodelPackage.HLINEAR_LOFT_PATH__ORIGIN, oldOrigin, newOrigin);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOrigin(HPoint3D newOrigin) {
		CORE.set_object(getHandle(), PROPERTY_ORIGIN, newOrigin.getHandle());
		if (newOrigin != origin) {
			NotificationChain msgs = null;
			if (origin != null)
				msgs = ((InternalEObject)origin).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DatamodelPackage.HLINEAR_LOFT_PATH__ORIGIN, null, msgs);
			if (newOrigin != null)
				msgs = ((InternalEObject)newOrigin).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DatamodelPackage.HLINEAR_LOFT_PATH__ORIGIN, null, msgs);
			msgs = basicSetOrigin(newOrigin, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DatamodelPackage.HLINEAR_LOFT_PATH__ORIGIN, newOrigin, newOrigin));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HPoint3D getLoftPathVector() {
		if (this.loftPathVector == null) {
			NativeLong objHandle = CORE.get_object(getHandle(), PROPERTY_LOFT_PATH_VECTOR);
			if (objHandle.intValue() != 0) {
				this.loftPathVector = (HPoint3D) HykopsObjectFactory.createObject(objHandle);
			}
		}
		return loftPathVector;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLoftPathVector(HPoint3D newLoftPathVector, NotificationChain msgs) {
		HPoint3D oldLoftPathVector = loftPathVector;
		loftPathVector = newLoftPathVector;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DatamodelPackage.HLINEAR_LOFT_PATH__LOFT_PATH_VECTOR, oldLoftPathVector, newLoftPathVector);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLoftPathVector(HPoint3D newLoftPathVector) {
		CORE.set_object(getHandle(), PROPERTY_LOFT_PATH_VECTOR, newLoftPathVector.getHandle());
		if (newLoftPathVector != loftPathVector) {
			NotificationChain msgs = null;
			if (loftPathVector != null)
				msgs = ((InternalEObject)loftPathVector).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DatamodelPackage.HLINEAR_LOFT_PATH__LOFT_PATH_VECTOR, null, msgs);
			if (newLoftPathVector != null)
				msgs = ((InternalEObject)newLoftPathVector).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DatamodelPackage.HLINEAR_LOFT_PATH__LOFT_PATH_VECTOR, null, msgs);
			msgs = basicSetLoftPathVector(newLoftPathVector, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DatamodelPackage.HLINEAR_LOFT_PATH__LOFT_PATH_VECTOR, newLoftPathVector, newLoftPathVector));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HPoint3D getLoftPathXi() {
		if (this.loftPathXi == null) {
			NativeLong objHandle = CORE.get_object(getHandle(), PROPERTY_LOFT_PATH_XI);
			if (objHandle.intValue() != 0) {
				this.loftPathXi = (HPoint3D) HykopsObjectFactory.createObject(objHandle);
			}
		}
		return loftPathXi;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLoftPathXi(HPoint3D newLoftPathXi, NotificationChain msgs) {
		HPoint3D oldLoftPathXi = loftPathXi;
		loftPathXi = newLoftPathXi;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DatamodelPackage.HLINEAR_LOFT_PATH__LOFT_PATH_XI, oldLoftPathXi, newLoftPathXi);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLoftPathXi(HPoint3D newLoftPathXi) {
		CORE.set_object(getHandle(), PROPERTY_LOFT_PATH_XI, newLoftPathXi.getHandle());
		if (newLoftPathXi != loftPathXi) {
			NotificationChain msgs = null;
			if (loftPathXi != null)
				msgs = ((InternalEObject)loftPathXi).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DatamodelPackage.HLINEAR_LOFT_PATH__LOFT_PATH_XI, null, msgs);
			if (newLoftPathXi != null)
				msgs = ((InternalEObject)newLoftPathXi).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DatamodelPackage.HLINEAR_LOFT_PATH__LOFT_PATH_XI, null, msgs);
			msgs = basicSetLoftPathXi(newLoftPathXi, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DatamodelPackage.HLINEAR_LOFT_PATH__LOFT_PATH_XI, newLoftPathXi, newLoftPathXi));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HPoint3D getLoftPathEta() {
		if (this.loftPathEta == null) {
			NativeLong objHandle = CORE.get_object(getHandle(), PROPERTY_LOFT_PATH_ETA);
			if (objHandle.intValue() != 0) {
				this.loftPathEta = (HPoint3D) HykopsObjectFactory.createObject(objHandle);
			}
		}
		return loftPathEta;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLoftPathEta(HPoint3D newLoftPathEta, NotificationChain msgs) {
		HPoint3D oldLoftPathEta = loftPathEta;
		loftPathEta = newLoftPathEta;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DatamodelPackage.HLINEAR_LOFT_PATH__LOFT_PATH_ETA, oldLoftPathEta, newLoftPathEta);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLoftPathEta(HPoint3D newLoftPathEta) {
		CORE.set_object(getHandle(), PROPERTY_LOFT_PATH_ETA, newLoftPathEta.getHandle());
		if (newLoftPathEta != loftPathEta) {
			NotificationChain msgs = null;
			if (loftPathEta != null)
				msgs = ((InternalEObject)loftPathEta).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DatamodelPackage.HLINEAR_LOFT_PATH__LOFT_PATH_ETA, null, msgs);
			if (newLoftPathEta != null)
				msgs = ((InternalEObject)newLoftPathEta).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DatamodelPackage.HLINEAR_LOFT_PATH__LOFT_PATH_ETA, null, msgs);
			msgs = basicSetLoftPathEta(newLoftPathEta, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DatamodelPackage.HLINEAR_LOFT_PATH__LOFT_PATH_ETA, newLoftPathEta, newLoftPathEta));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DatamodelPackage.HLINEAR_LOFT_PATH__ORIGIN:
				return basicSetOrigin(null, msgs);
			case DatamodelPackage.HLINEAR_LOFT_PATH__LOFT_PATH_VECTOR:
				return basicSetLoftPathVector(null, msgs);
			case DatamodelPackage.HLINEAR_LOFT_PATH__LOFT_PATH_XI:
				return basicSetLoftPathXi(null, msgs);
			case DatamodelPackage.HLINEAR_LOFT_PATH__LOFT_PATH_ETA:
				return basicSetLoftPathEta(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DatamodelPackage.HLINEAR_LOFT_PATH__ORIGIN:
				return getOrigin();
			case DatamodelPackage.HLINEAR_LOFT_PATH__LOFT_PATH_VECTOR:
				return getLoftPathVector();
			case DatamodelPackage.HLINEAR_LOFT_PATH__LOFT_PATH_XI:
				return getLoftPathXi();
			case DatamodelPackage.HLINEAR_LOFT_PATH__LOFT_PATH_ETA:
				return getLoftPathEta();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DatamodelPackage.HLINEAR_LOFT_PATH__ORIGIN:
				setOrigin((HPoint3D)newValue);
				return;
			case DatamodelPackage.HLINEAR_LOFT_PATH__LOFT_PATH_VECTOR:
				setLoftPathVector((HPoint3D)newValue);
				return;
			case DatamodelPackage.HLINEAR_LOFT_PATH__LOFT_PATH_XI:
				setLoftPathXi((HPoint3D)newValue);
				return;
			case DatamodelPackage.HLINEAR_LOFT_PATH__LOFT_PATH_ETA:
				setLoftPathEta((HPoint3D)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DatamodelPackage.HLINEAR_LOFT_PATH__ORIGIN:
				setOrigin((HPoint3D)null);
				return;
			case DatamodelPackage.HLINEAR_LOFT_PATH__LOFT_PATH_VECTOR:
				setLoftPathVector((HPoint3D)null);
				return;
			case DatamodelPackage.HLINEAR_LOFT_PATH__LOFT_PATH_XI:
				setLoftPathXi((HPoint3D)null);
				return;
			case DatamodelPackage.HLINEAR_LOFT_PATH__LOFT_PATH_ETA:
				setLoftPathEta((HPoint3D)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DatamodelPackage.HLINEAR_LOFT_PATH__ORIGIN:
				return origin != null;
			case DatamodelPackage.HLINEAR_LOFT_PATH__LOFT_PATH_VECTOR:
				return loftPathVector != null;
			case DatamodelPackage.HLINEAR_LOFT_PATH__LOFT_PATH_XI:
				return loftPathXi != null;
			case DatamodelPackage.HLINEAR_LOFT_PATH__LOFT_PATH_ETA:
				return loftPathEta != null;
		}
		return super.eIsSet(featureID);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getDenominator() == null) ? 0 : getDenominator().hashCode());
		result = prime * result + ((getOrigin() == null) ? 0 : getOrigin().hashCode());
		result = prime * result + ((getHLoftCoordinateSystem() == null) ? 0 : getHLoftCoordinateSystem().hashCode());
		result = prime * result + ((getLoftPathEta() == null) ? 0 : getLoftPathEta().hashCode());
		result = prime * result + ((getLoftPathVector() == null) ? 0 : getLoftPathVector().hashCode());
		result = prime * result + ((getLoftPathXi() == null) ? 0 : getLoftPathXi().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HLinearLoftPathImpl other = (HLinearLoftPathImpl) obj;
		if (getDenominator() == null) {
			if (other.getDenominator() != null)
				return false;
		} else if (!getDenominator().equals(other.getDenominator()))
			return false;
		if (getOrigin() == null) {
			if (other.getOrigin() != null)
				return false;
		} else if (!getOrigin().equals(other.getOrigin()))
			return false;
		if (getHLoftCoordinateSystem() == null) {
			if (other.getHLoftCoordinateSystem() != null)
				return false;
		} else if (!getHLoftCoordinateSystem().equals(other.getHLoftCoordinateSystem()))
			return false;
		if (getLoftPathEta() == null) {
			if (other.getLoftPathEta() != null)
				return false;
		} else if (!getLoftPathEta().equals(other.getLoftPathEta()))
			return false;
		if (getLoftPathVector() == null) {
			if (other.getLoftPathVector() != null)
				return false;
		} else if (!getLoftPathVector().equals(other.getLoftPathVector()))
			return false;
		if (getLoftPathXi() == null) {
			if (other.getLoftPathXi() != null)
				return false;
		} else if (!getLoftPathXi().equals(other.getLoftPathXi()))
			return false;
		return true;
	}

} //HLinearLoftPathImpl
