/**
 */
package de.hykops.jna.datamodel.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import com.sun.jna.NativeLong;

import de.hykops.lpd.datamodel.DatamodelPackage;
import de.hykops.lpd.datamodel.HPoint2D;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>HPoint2 D</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.hykops.lpd.datamodel.impl.HPoint2DImpl#getX1 <em>X1</em>}</li>
 *   <li>{@link de.hykops.lpd.datamodel.impl.HPoint2DImpl#getX2 <em>X2</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HPoint2DImpl extends HParentImpl implements HPoint2D {
	/**
	 * The default value of the '{@link #getX1() <em>X1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getX1()
	 * @generated
	 * @ordered
	 */
	protected static final Double X1_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getX1() <em>X1</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getX1()
	 * @generated
	 * @ordered
	 */
	protected Double x1;

	/**
	 * The default value of the '{@link #getX2() <em>X2</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getX2()
	 * @generated
	 * @ordered
	 */
	protected static final Double X2_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getX2() <em>X2</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getX2()
	 * @generated
	 * @ordered
	 */
	protected Double x2;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HPoint2DImpl(NativeLong handle) {
		super();
		this.handle = handle;
	}

	public HPoint2DImpl() {
		this(CORE.create_object(HPoint2D.CLASS_NAME));
	}
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DatamodelPackage.Literals.HPOINT2_D;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getX1() {
		if (this.x1 == null) {
			this.x1 = CORE.get_double(getHandle(), PROPERTY_X1);
		}
		return x1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setX1(Double newX1) {
		CORE.set_double(getHandle(), PROPERTY_X1, newX1);
		Double oldX1 = x1;
		x1 = newX1;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DatamodelPackage.HPOINT2_D__X1, oldX1.doubleValue(), x1.doubleValue()));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getX2() {
		if (this.x2 == null) {
			this.x2 = CORE.get_double(getHandle(), PROPERTY_X2);
		}
		return x2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setX2(Double newX2) {
		CORE.set_double(getHandle(), PROPERTY_X2, newX2);
		Double oldX2 = x2;
		x2 = newX2;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DatamodelPackage.HPOINT2_D__X2, oldX2.doubleValue(), x2.doubleValue()));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DatamodelPackage.HPOINT2_D__X1:
				return getX1();
			case DatamodelPackage.HPOINT2_D__X2:
				return getX2();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DatamodelPackage.HPOINT2_D__X1:
				setX1((Double)newValue);
				return;
			case DatamodelPackage.HPOINT2_D__X2:
				setX2((Double)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DatamodelPackage.HPOINT2_D__X1:
				setX1(X1_EDEFAULT);
				return;
			case DatamodelPackage.HPOINT2_D__X2:
				setX2(X2_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DatamodelPackage.HPOINT2_D__X1:
				return x1 != X1_EDEFAULT;
			case DatamodelPackage.HPOINT2_D__X2:
				return x2 != X2_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (x1: ");
		result.append(x1);
		result.append(", x2: ");
		result.append(x2);
		result.append(')');
		return result.toString();
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getX1() == null) ? 0 : getX1().hashCode());
		result = prime * result + ((getX2() == null) ? 0 : getX2().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HPoint2DImpl other = (HPoint2DImpl) obj;
		if (getX1() == null) {
			if (other.getX1() != null)
				return false;
		} else if (!getX1().equals(other.getX1()))
			return false;
		if (getX2() == null) {
			if (other.getX2() != null)
				return false;
		} else if (!getX2().equals(other.getX2()))
			return false;
		return true;
	}

} //HPoint2DImpl
