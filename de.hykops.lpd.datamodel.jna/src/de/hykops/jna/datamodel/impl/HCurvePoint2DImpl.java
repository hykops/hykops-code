/**
 */
package de.hykops.jna.datamodel.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import com.sun.jna.NativeLong;

import de.hykops.lpd.datamodel.DatamodelPackage;
import de.hykops.lpd.datamodel.HCurvePoint2D;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>HCurve Point2 D</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.hykops.lpd.datamodel.impl.HCurvePoint2DImpl#getCurveCoordinate <em>Curve Coordinate</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HCurvePoint2DImpl extends HPoint2DImpl implements HCurvePoint2D {
	/**
	 * The default value of the '{@link #getCurveCoordinate() <em>Curve Coordinate</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCurveCoordinate()
	 * @generated
	 * @ordered
	 */
	protected static final double CURVE_COORDINATE_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getCurveCoordinate() <em>Curve Coordinate</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCurveCoordinate()
	 * @generated
	 * @ordered
	 */
	protected Double curveCoordinate;;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HCurvePoint2DImpl(NativeLong handle) {
		super();
		this.handle = handle;
	}

	public HCurvePoint2DImpl() {
		this(CORE.create_object(HCurvePoint2D.CLASS_NAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DatamodelPackage.Literals.HCURVE_POINT2_D;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getCurveCoordinate() {
		if (this.curveCoordinate == null) {
			this.curveCoordinate = CORE.get_double(getHandle(), PROPERTY_CURVE_COORDINATE);
		}
		return this.curveCoordinate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCurveCoordinate(Double newCurveCoordinate) {
		CORE.set_double(getHandle(), PROPERTY_CURVE_COORDINATE, newCurveCoordinate);
		Double oldCurveCoordinate = curveCoordinate;
		curveCoordinate = newCurveCoordinate;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DatamodelPackage.HCURVE_POINT2_D__CURVE_COORDINATE, oldCurveCoordinate.doubleValue(), curveCoordinate.doubleValue()));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DatamodelPackage.HCURVE_POINT2_D__CURVE_COORDINATE:
				return getCurveCoordinate();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DatamodelPackage.HCURVE_POINT2_D__CURVE_COORDINATE:
				setCurveCoordinate((Double)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DatamodelPackage.HCURVE_POINT2_D__CURVE_COORDINATE:
				setCurveCoordinate(CURVE_COORDINATE_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DatamodelPackage.HCURVE_POINT2_D__CURVE_COORDINATE:
				return curveCoordinate != CURVE_COORDINATE_EDEFAULT;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (curveCoordinate: ");
		result.append(curveCoordinate);
		result.append(')');
		return result.toString();
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getCurveCoordinate() == null) ? 0 : getCurveCoordinate().hashCode());
		result = prime * result + ((getX1() == null) ? 0 : getX1().hashCode());
		result = prime * result + ((getX2() == null) ? 0 : getX2().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HCurvePoint2DImpl other = (HCurvePoint2DImpl) obj;
		if (getCurveCoordinate() == null) {
			if (other.getCurveCoordinate() != null)
				return false;
		} else if (!getCurveCoordinate().equals(other.getCurveCoordinate()))
			return false;
		if (getX1() == null) {
			if (other.getX1() != null)
				return false;
		} else if (!getX1().equals(other.getX1()))
			return false;
		if (getX2() == null) {
			if (other.getX2() != null)
				return false;
		} else if (!getX2().equals(other.getX2()))
			return false;
		return true;
	}
	

} //HCurvePoint2DImpl
