/**
 */
package de.hykops.jna.datamodel.impl;

import de.hykops.jna.datamodel.factory.HykopsObjectFactory;
import de.hykops.lpd.datamodel.DatamodelPackage;
import de.hykops.lpd.datamodel.HCoordinateSystem3DCone;
import de.hykops.lpd.datamodel.HPoint3D;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.omg.CORBA.IntHolder;

import com.sun.jna.NativeLong;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>HCoordinate System3 DCone</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.hykops.lpd.datamodel.impl.HCoordinateSystem3DConeImpl#getAxialVector <em>Axial Vector</em>}</li>
 *   <li>{@link de.hykops.lpd.datamodel.impl.HCoordinateSystem3DConeImpl#getRadialVector <em>Radial Vector</em>}</li>
 *   <li>{@link de.hykops.lpd.datamodel.impl.HCoordinateSystem3DConeImpl#getAngle <em>Angle</em>}</li>
 * </ul>

 * @generated
 */
public class HCoordinateSystem3DConeImpl extends HCoordinateSystem3DImpl implements HCoordinateSystem3DCone {
	/**
	 * The cached value of the '{@link #getAxialVector() <em>Axial Vector</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAxialVector()
	 * @generated
	 * @ordered
	 */
	protected HPoint3D axialVector;

	/**
	 * The cached value of the '{@link #getRadialVector() <em>Radial Vector</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRadialVector()
	 * @generated
	 * @ordered
	 */
	protected HPoint3D radialVector;

	/**
	 * The cached value of the '{@link #getAngle() <em>Angle</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getAngle()
	 * @generated
	 * @ordered
	 */
	protected HPoint3D angle;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HCoordinateSystem3DConeImpl() {
		this(CORE.create_object(CLASS_NAME));
	}

	public HCoordinateSystem3DConeImpl(NativeLong handle) {
		this.handle = handle;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DatamodelPackage.Literals.HCOORDINATE_SYSTEM3_DCONE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HPoint3D getAxialVector() {
		if (this.axialVector == null) {
			NativeLong objHandle = CORE.get_object(getHandle(), PROPERTY_AXIAL_VECTOR);
			if (objHandle.intValue() != 0) {
				this.axialVector = (HPoint3D) HykopsObjectFactory.createObject(objHandle);
			}
		}
		return axialVector;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAxialVector(HPoint3D newAxialVector, NotificationChain msgs) {
		HPoint3D oldAxialVector = axialVector;
		axialVector = newAxialVector;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DatamodelPackage.HCOORDINATE_SYSTEM3_DCONE__AXIAL_VECTOR, oldAxialVector, newAxialVector);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAxialVector(HPoint3D newAxialVector) {
		CORE.set_object(getHandle(), PROPERTY_AXIAL_VECTOR, newAxialVector.getHandle());
		if (newAxialVector != axialVector) {
			NotificationChain msgs = null;
			if (axialVector != null)
				msgs = ((InternalEObject)axialVector).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DatamodelPackage.HCOORDINATE_SYSTEM3_DCONE__AXIAL_VECTOR, null, msgs);
			if (newAxialVector != null)
				msgs = ((InternalEObject)newAxialVector).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DatamodelPackage.HCOORDINATE_SYSTEM3_DCONE__AXIAL_VECTOR, null, msgs);
			msgs = basicSetAxialVector(newAxialVector, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DatamodelPackage.HCOORDINATE_SYSTEM3_DCONE__AXIAL_VECTOR, newAxialVector, newAxialVector));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HPoint3D getRadialVector() {
		if (this.radialVector == null) {
			NativeLong objHandle = CORE.get_object(getHandle(), PROPERTY_RADIAL_VECTOR);
			if (objHandle.intValue() != 0) {
				this.radialVector = (HPoint3D) HykopsObjectFactory.createObject(objHandle);
			}
		}
		return radialVector;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetRadialVector(HPoint3D newRadialVector, NotificationChain msgs) {
		HPoint3D oldRadialVector = radialVector;
		radialVector = newRadialVector;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DatamodelPackage.HCOORDINATE_SYSTEM3_DCONE__RADIAL_VECTOR, oldRadialVector, newRadialVector);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRadialVector(HPoint3D newRadialVector) {
		CORE.set_object(getHandle(), PROPERTY_RADIAL_VECTOR, newRadialVector.getHandle());
		if (newRadialVector != radialVector) {
			NotificationChain msgs = null;
			if (radialVector != null)
				msgs = ((InternalEObject)radialVector).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DatamodelPackage.HCOORDINATE_SYSTEM3_DCONE__RADIAL_VECTOR, null, msgs);
			if (newRadialVector != null)
				msgs = ((InternalEObject)newRadialVector).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DatamodelPackage.HCOORDINATE_SYSTEM3_DCONE__RADIAL_VECTOR, null, msgs);
			msgs = basicSetRadialVector(newRadialVector, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DatamodelPackage.HCOORDINATE_SYSTEM3_DCONE__RADIAL_VECTOR, newRadialVector, newRadialVector));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HPoint3D getAngle() {
		if (this.angle == null) {
			NativeLong objHandle = CORE.get_object(getHandle(), PROPERTY_ANGLE);
			if (objHandle.intValue() != 0) {
				this.angle = (HPoint3D) HykopsObjectFactory.createObject(objHandle);
			}
		}
		return angle;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetAngle(HPoint3D newAngle, NotificationChain msgs) {
		HPoint3D oldAngle = angle;
		angle = newAngle;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DatamodelPackage.HCOORDINATE_SYSTEM3_DCONE__ANGLE, oldAngle, newAngle);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAngle(HPoint3D newAngle) {
		CORE.set_object(getHandle(), PROPERTY_ANGLE, newAngle.getHandle());
		if (newAngle != angle) {
			NotificationChain msgs = null;
			if (angle != null)
				msgs = ((InternalEObject)angle).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DatamodelPackage.HCOORDINATE_SYSTEM3_DCONE__ANGLE, null, msgs);
			if (newAngle != null)
				msgs = ((InternalEObject)newAngle).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DatamodelPackage.HCOORDINATE_SYSTEM3_DCONE__ANGLE, null, msgs);
			msgs = basicSetAngle(newAngle, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DatamodelPackage.HCOORDINATE_SYSTEM3_DCONE__ANGLE, newAngle, newAngle));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DatamodelPackage.HCOORDINATE_SYSTEM3_DCONE__AXIAL_VECTOR:
				return basicSetAxialVector(null, msgs);
			case DatamodelPackage.HCOORDINATE_SYSTEM3_DCONE__RADIAL_VECTOR:
				return basicSetRadialVector(null, msgs);
			case DatamodelPackage.HCOORDINATE_SYSTEM3_DCONE__ANGLE:
				return basicSetAngle(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DatamodelPackage.HCOORDINATE_SYSTEM3_DCONE__AXIAL_VECTOR:
				return getAxialVector();
			case DatamodelPackage.HCOORDINATE_SYSTEM3_DCONE__RADIAL_VECTOR:
				return getRadialVector();
			case DatamodelPackage.HCOORDINATE_SYSTEM3_DCONE__ANGLE:
				return getAngle();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DatamodelPackage.HCOORDINATE_SYSTEM3_DCONE__AXIAL_VECTOR:
				setAxialVector((HPoint3D)newValue);
				return;
			case DatamodelPackage.HCOORDINATE_SYSTEM3_DCONE__RADIAL_VECTOR:
				setRadialVector((HPoint3D)newValue);
				return;
			case DatamodelPackage.HCOORDINATE_SYSTEM3_DCONE__ANGLE:
				setAngle((HPoint3D)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DatamodelPackage.HCOORDINATE_SYSTEM3_DCONE__AXIAL_VECTOR:
				setAxialVector((HPoint3D)null);
				return;
			case DatamodelPackage.HCOORDINATE_SYSTEM3_DCONE__RADIAL_VECTOR:
				setRadialVector((HPoint3D)null);
				return;
			case DatamodelPackage.HCOORDINATE_SYSTEM3_DCONE__ANGLE:
				setAngle((HPoint3D)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DatamodelPackage.HCOORDINATE_SYSTEM3_DCONE__AXIAL_VECTOR:
				return axialVector != null;
			case DatamodelPackage.HCOORDINATE_SYSTEM3_DCONE__RADIAL_VECTOR:
				return radialVector != null;
			case DatamodelPackage.HCOORDINATE_SYSTEM3_DCONE__ANGLE:
				return angle != null;
		}
		return super.eIsSet(featureID);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getAngle() == null) ? 0 : getAngle().hashCode());
		result = prime * result + ((getAxialVector() == null) ? 0 : getAxialVector().hashCode());
		result = prime * result + ((getHHinges() == null) ? 0 : getHHinges().hashCode());
		result = prime * result + ((getOrigin() == null) ? 0 : getOrigin().hashCode());
		result = prime * result + ((getRadialVector() == null) ? 0 : getRadialVector().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HCoordinateSystem3DConeImpl other = (HCoordinateSystem3DConeImpl) obj;
		if (getAngle() == null) {
			if (other.getAngle() != null)
				return false;
		} else if (!getAngle().equals(other.getAngle()))
			return false;
		if (getAxialVector() == null) {
			if (other.getAxialVector() != null)
				return false;
		} else if (!getAxialVector().equals(other.getAxialVector()))
			return false;
		if (getHHinges() == null) {
			if (other.getHHinges() != null)
				return false;
		} else if (!getHHinges().equals(other.getHHinges()))
			return false;
		if (getOrigin() == null) {
			if (other.getOrigin() != null)
				return false;
		} else if (!getOrigin().equals(other.getOrigin()))
			return false;
		if (getRadialVector() == null) {
			if (other.getRadialVector() != null)
				return false;
		} else if (!getRadialVector().equals(other.getRadialVector()))
			return false;
		return true;
	}
} //HCoordinateSystem3DConeImpl
