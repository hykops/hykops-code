/**
 */
package de.hykops.jna.datamodel.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.InternalEList;

import com.sun.jna.NativeLong;

import de.hykops.jna.datamodel.core.InteropUtil;
import de.hykops.lpd.datamodel.DatamodelPackage;
import de.hykops.lpd.datamodel.HCurvePoint2D;
import de.hykops.lpd.datamodel.HDiscreteCurve2D;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>HDiscrete Curve2 D</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.hykops.lpd.datamodel.impl.HDiscreteCurve2DImpl#getInterpolationScheme <em>Interpolation Scheme</em>}</li>
 *   <li>{@link de.hykops.lpd.datamodel.impl.HDiscreteCurve2DImpl#getCurvePoints <em>Curve Points</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HDiscreteCurve2DImpl extends HCurve2DImpl implements HDiscreteCurve2D {
	
	
	
	/**
	 * The default value of the '{@link #getInterpolationScheme() <em>Interpolation Scheme</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInterpolationScheme()
	 * @generated
	 * @ordered
	 */
	protected static final String INTERPOLATION_SCHEME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getInterpolationScheme() <em>Interpolation Scheme</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInterpolationScheme()
	 * @generated
	 * @ordered
	 */
	protected String interpolationScheme = INTERPOLATION_SCHEME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getCurvePoints() <em>Curve Points</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCurvePoints()
	 * @generated
	 * @ordered
	 */
	protected EList<HCurvePoint2D> curvePoints;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HDiscreteCurve2DImpl(NativeLong handle) {
		this.handle = handle;
	}

	public HDiscreteCurve2DImpl() {
		this(CORE.create_object(HDiscreteCurve2D.CLASS_NAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DatamodelPackage.Literals.HDISCRETE_CURVE2_D;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getInterpolationScheme() {
		if (this.interpolationScheme == null) {
			this.interpolationScheme = InteropUtil.getString(getHandle(), PROPERTY_INTERPOLATION_SCHEME);
		}
		return interpolationScheme;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInterpolationScheme(String newInterpolationScheme) {
		CORE.set_string(getHandle(), PROPERTY_INTERPOLATION_SCHEME, newInterpolationScheme);
		String oldInterpolationScheme = interpolationScheme;
		interpolationScheme = newInterpolationScheme;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DatamodelPackage.HDISCRETE_CURVE2_D__INTERPOLATION_SCHEME, oldInterpolationScheme, interpolationScheme));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<HCurvePoint2D> getCurvePoints() {
		if (curvePoints == null) {
			this.curvePoints = new InteropUtil<HCurvePoint2D>(getHandle()).getList(PROPERTY_CURVE_POINTS, HCurvePoint2D.class, this, DatamodelPackage.HDISCRETE_CURVE2_D__CURVE_POINTS, false);
		}
		return curvePoints;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DatamodelPackage.HDISCRETE_CURVE2_D__CURVE_POINTS:
				return ((InternalEList<?>)getCurvePoints()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DatamodelPackage.HDISCRETE_CURVE2_D__INTERPOLATION_SCHEME:
				return getInterpolationScheme();
			case DatamodelPackage.HDISCRETE_CURVE2_D__CURVE_POINTS:
				return getCurvePoints();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DatamodelPackage.HDISCRETE_CURVE2_D__INTERPOLATION_SCHEME:
				setInterpolationScheme((String)newValue);
				return;
			case DatamodelPackage.HDISCRETE_CURVE2_D__CURVE_POINTS:
				getCurvePoints().clear();
				getCurvePoints().addAll((Collection<? extends HCurvePoint2D>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DatamodelPackage.HDISCRETE_CURVE2_D__INTERPOLATION_SCHEME:
				setInterpolationScheme(INTERPOLATION_SCHEME_EDEFAULT);
				return;
			case DatamodelPackage.HDISCRETE_CURVE2_D__CURVE_POINTS:
				getCurvePoints().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DatamodelPackage.HDISCRETE_CURVE2_D__INTERPOLATION_SCHEME:
				return INTERPOLATION_SCHEME_EDEFAULT == null ? interpolationScheme != null : !INTERPOLATION_SCHEME_EDEFAULT.equals(interpolationScheme);
			case DatamodelPackage.HDISCRETE_CURVE2_D__CURVE_POINTS:
				return curvePoints != null && !curvePoints.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (interpolationScheme: ");
		result.append(interpolationScheme);
		result.append(')');
		return result.toString();
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getCurvePoints() == null) ? 0 : getCurvePoints().hashCode());
		result = prime * result + ((getInterpolationScheme() == null) ? 0 : getInterpolationScheme().hashCode());
		result = prime * result + ((getHSurfaceIdentifiers() == null) ? 0 : getHSurfaceIdentifiers().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HDiscreteCurve2DImpl other = (HDiscreteCurve2DImpl) obj;
		if (getCurvePoints() == null) {
			if (other.getCurvePoints() != null)
				return false;
		} else if (!getCurvePoints().equals(other.getCurvePoints()))
			return false;
		if (getInterpolationScheme() == null) {
			if (other.getInterpolationScheme() != null)
				return false;
		} else if (!getInterpolationScheme().equals(other.getInterpolationScheme()))
			return false;
		if (getHSurfaceIdentifiers() == null) {
			if (other.getHSurfaceIdentifiers() != null)
				return false;
		} else if (!getHSurfaceIdentifiers().equals(other.getHSurfaceIdentifiers()))
			return false;
		return true;
	}

} //HDiscreteCurve2DImpl
