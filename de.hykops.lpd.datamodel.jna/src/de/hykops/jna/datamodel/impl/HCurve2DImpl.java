/**
 */
package de.hykops.jna.datamodel.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;

import com.sun.jna.NativeLong;

import de.hykops.jna.datamodel.core.InteropUtil;
import de.hykops.lpd.datamodel.DatamodelPackage;
import de.hykops.lpd.datamodel.HCurve2D;
import de.hykops.lpd.datamodel.HLoftElement;
import de.hykops.lpd.datamodel.HSurfaceIdentifier;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>HCurve2 D</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.hykops.lpd.datamodel.impl.HCurve2DImpl#getHSurfaceIdentifiers <em>HSurface Identifiers</em>}</li>
 *   <li>{@link de.hykops.lpd.datamodel.impl.HCurve2DImpl#getHLoftElement <em>HLoft Element</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class HCurve2DImpl extends HParentImpl implements HCurve2D {
	
	
	
	/**
	 * The cached value of the '{@link #getHSurfaceIdentifiers() <em>HSurface Identifiers</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHSurfaceIdentifiers()
	 * @generated
	 * @ordered
	 */
	protected EList<HSurfaceIdentifier> hSurfaceIdentifiers;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HCurve2DImpl() {
		this(CORE.create_object(CLASS_NAME));
	}

	public HCurve2DImpl(NativeLong handle) {
		super();
		this.handle = handle;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DatamodelPackage.Literals.HCURVE2_D;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<HSurfaceIdentifier> getHSurfaceIdentifiers() {
		if (hSurfaceIdentifiers == null) {
			hSurfaceIdentifiers = new InteropUtil<HSurfaceIdentifier>(getHandle()).getList(PROPERTY_SURFACE_IDENTIFIERS, HSurfaceIdentifier.class, this, DatamodelPackage.HCURVE2_D__HSURFACE_IDENTIFIERS, true);
		}
		return hSurfaceIdentifiers;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HLoftElement getHLoftElement() {
		if (eContainerFeatureID() != DatamodelPackage.HCURVE2_D__HLOFT_ELEMENT) return null;
		return (HLoftElement)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHLoftElement(HLoftElement newHLoftElement, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newHLoftElement, DatamodelPackage.HCURVE2_D__HLOFT_ELEMENT, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHLoftElement(HLoftElement newHLoftElement) {
		CORE.set_object(getHandle(), PROPERTY_LOFT_ELEMENT, newHLoftElement.getHandle());
		if (newHLoftElement != eInternalContainer() || (eContainerFeatureID() != DatamodelPackage.HCURVE2_D__HLOFT_ELEMENT && newHLoftElement != null)) {
			if (EcoreUtil.isAncestor(this, newHLoftElement))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newHLoftElement != null)
				msgs = ((InternalEObject)newHLoftElement).eInverseAdd(this, DatamodelPackage.HLOFT_ELEMENT__HCURVE2_DS, HLoftElement.class, msgs);
			msgs = basicSetHLoftElement(newHLoftElement, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DatamodelPackage.HCURVE2_D__HLOFT_ELEMENT, newHLoftElement, newHLoftElement));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DatamodelPackage.HCURVE2_D__HLOFT_ELEMENT:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetHLoftElement((HLoftElement)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DatamodelPackage.HCURVE2_D__HLOFT_ELEMENT:
				return basicSetHLoftElement(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case DatamodelPackage.HCURVE2_D__HLOFT_ELEMENT:
				return eInternalContainer().eInverseRemove(this, DatamodelPackage.HLOFT_ELEMENT__HCURVE2_DS, HLoftElement.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DatamodelPackage.HCURVE2_D__HSURFACE_IDENTIFIERS:
				return getHSurfaceIdentifiers();
			case DatamodelPackage.HCURVE2_D__HLOFT_ELEMENT:
				return getHLoftElement();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DatamodelPackage.HCURVE2_D__HSURFACE_IDENTIFIERS:
				getHSurfaceIdentifiers().clear();
				getHSurfaceIdentifiers().addAll((Collection<? extends HSurfaceIdentifier>)newValue);
				return;
			case DatamodelPackage.HCURVE2_D__HLOFT_ELEMENT:
				setHLoftElement((HLoftElement)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DatamodelPackage.HCURVE2_D__HSURFACE_IDENTIFIERS:
				getHSurfaceIdentifiers().clear();
				return;
			case DatamodelPackage.HCURVE2_D__HLOFT_ELEMENT:
				setHLoftElement((HLoftElement)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DatamodelPackage.HCURVE2_D__HSURFACE_IDENTIFIERS:
				return hSurfaceIdentifiers != null && !hSurfaceIdentifiers.isEmpty();
			case DatamodelPackage.HCURVE2_D__HLOFT_ELEMENT:
				return getHLoftElement() != null;
		}
		return super.eIsSet(featureID);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getHSurfaceIdentifiers() == null) ? 0 : getHSurfaceIdentifiers().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HCurve2DImpl other = (HCurve2DImpl) obj;
		if (getHSurfaceIdentifiers() == null) {
			if (other.getHSurfaceIdentifiers() != null)
				return false;
		} else if (!getHSurfaceIdentifiers().equals(other.getHSurfaceIdentifiers()))
			return false;
		return true;
	}

} //HCurve2DImpl
