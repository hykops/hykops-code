/**
 */
package de.hykops.jna.datamodel.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import com.sun.jna.NativeLong;

import de.hykops.jna.datamodel.core.InteropUtil;
import de.hykops.jna.datamodel.factory.CoordinateSystemFactory;
import de.hykops.lpd.datamodel.DatamodelPackage;
import de.hykops.lpd.datamodel.HComponent;
import de.hykops.lpd.datamodel.HComposition;
import de.hykops.lpd.datamodel.HCoordinateSystem3D;
import de.hykops.lpd.datamodel.HLoft;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>HComponent</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.hykops.lpd.datamodel.impl.HComponentImpl#getName <em>Name</em>}</li>
 *   <li>{@link de.hykops.lpd.datamodel.impl.HComponentImpl#getType <em>Type</em>}</li>
 *   <li>{@link de.hykops.lpd.datamodel.impl.HComponentImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link de.hykops.lpd.datamodel.impl.HComponentImpl#getParameters <em>Parameters</em>}</li>
 *   <li>{@link de.hykops.lpd.datamodel.impl.HComponentImpl#getHCoordinateSystem3D <em>HCoordinate System3 D</em>}</li>
 *   <li>{@link de.hykops.lpd.datamodel.impl.HComponentImpl#getHLofts <em>HLofts</em>}</li>
 *   <li>{@link de.hykops.lpd.datamodel.impl.HComponentImpl#getHComposition <em>HComposition</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HComponentImpl extends HParentImpl implements HComponent {
	
	public HComponentImpl() {
		this(CORE.create_object(CLASS_NAME));
	}

	public HComponentImpl(NativeLong handle) {
		super();
		this.handle = handle;
	}
	
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected static final String TYPE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getType() <em>Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getType()
	 * @generated
	 * @ordered
	 */
	protected String type = TYPE_EDEFAULT;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getParameters() <em>Parameters</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameters()
	 * @generated
	 * @ordered
	 */
	protected EMap<String, String> parameters;

	/**
	 * The cached value of the '{@link #getHCoordinateSystem3D() <em>HCoordinate System3 D</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHCoordinateSystem3D()
	 * @generated
	 * @ordered
	 */
	protected HCoordinateSystem3D hCoordinateSystem3D;

	/**
	 * The cached value of the '{@link #getHLofts() <em>HLofts</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHLofts()
	 * @generated
	 * @ordered
	 */
	protected EList<HLoft> hLofts;


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DatamodelPackage.Literals.HCOMPONENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		if (this.name == null) { 
			this.name = InteropUtil.getString(getHandle(), PROPERTY_NAME);
		}
		return this.name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		CORE.set_string(getHandle(), PROPERTY_NAME, newName);
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DatamodelPackage.HCOMPONENT__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getType() {
		if (this.type == null) {
			this.type = InteropUtil.getString(getHandle(), PROPERTY_TYPE);
		}
		return type;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(String newType) {
		CORE.set_string(getHandle(), PROPERTY_TYPE, newType);
		String oldType = type;
		type = newType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DatamodelPackage.HCOMPONENT__TYPE, oldType, type));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDescription() {
		if (this.description == null) {
			this.description = InteropUtil.getString(getHandle(), PROPERTY_DESCRIPTION);
		}
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(String newDescription) {
		CORE.set_string(getHandle(), PROPERTY_DESCRIPTION, newDescription);
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DatamodelPackage.HCOMPONENT__DESCRIPTION, oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<String, String> getParameters() {
		if(parameters == null) {
			parameters = new InteropUtil<>(getHandle()).getMap(PROPERTY_PARAMTER, DatamodelPackage.Literals.STRING_TO_STRING_MAP, StringToStringMapImpl.class, this, DatamodelPackage.HCOMPONENT__PARAMETERS);
		}
		return parameters;
	}



	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HCoordinateSystem3D getHCoordinateSystem3D() {
		if (this.hCoordinateSystem3D == null) {
			NativeLong cosHandle = CORE.get_object(getHandle(), PROPERTY_COORDINATESYSTEM);
			if (cosHandle.intValue() != 0) {
				String cosType = InteropUtil.getType(cosHandle);
				this.hCoordinateSystem3D = CoordinateSystemFactory.create(cosType, cosHandle);
			}
		}
		return hCoordinateSystem3D;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHCoordinateSystem3D(HCoordinateSystem3D newHCoordinateSystem3D, NotificationChain msgs) {
		HCoordinateSystem3D oldHCoordinateSystem3D = hCoordinateSystem3D;
		hCoordinateSystem3D = newHCoordinateSystem3D;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DatamodelPackage.HCOMPONENT__HCOORDINATE_SYSTEM3_D, oldHCoordinateSystem3D, newHCoordinateSystem3D);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHCoordinateSystem3D(HCoordinateSystem3D newHCoordinateSystem3D) {
		CORE.set_object(getHandle(), PROPERTY_COORDINATESYSTEM, newHCoordinateSystem3D.getHandle());
		if (newHCoordinateSystem3D != hCoordinateSystem3D) {
			NotificationChain msgs = null;
			if (hCoordinateSystem3D != null)
				msgs = ((InternalEObject)hCoordinateSystem3D).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DatamodelPackage.HCOMPONENT__HCOORDINATE_SYSTEM3_D, null, msgs);
			if (newHCoordinateSystem3D != null)
				msgs = ((InternalEObject)newHCoordinateSystem3D).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DatamodelPackage.HCOMPONENT__HCOORDINATE_SYSTEM3_D, null, msgs);
			msgs = basicSetHCoordinateSystem3D(newHCoordinateSystem3D, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DatamodelPackage.HCOMPONENT__HCOORDINATE_SYSTEM3_D, newHCoordinateSystem3D, newHCoordinateSystem3D));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<HLoft> getHLofts() {
		if (hLofts == null) {
			this.hLofts = new InteropUtil<HLoft>(getHandle()).getList(PROPERTY_LOFTS, HLoft.class, this, DatamodelPackage.HCOMPONENT__HLOFTS, DatamodelPackage.HLOFT__HCOMPONENT);
		}
		return hLofts;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HComposition getHComposition() {
		if (eContainerFeatureID() != DatamodelPackage.HCOMPONENT__HCOMPOSITION) return null;
		return (HComposition)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHComposition(HComposition newHComposition, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newHComposition, DatamodelPackage.HCOMPONENT__HCOMPOSITION, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHComposition(HComposition newHComposition) {
		CORE.set_object(getHandle(), PROPERTY_COMPOSITION, newHComposition.getHandle());
		if (newHComposition != eInternalContainer() || (eContainerFeatureID() != DatamodelPackage.HCOMPONENT__HCOMPOSITION && newHComposition != null)) {
			if (EcoreUtil.isAncestor(this, newHComposition))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newHComposition != null)
				msgs = ((InternalEObject)newHComposition).eInverseAdd(this, DatamodelPackage.HCOMPOSITION__HCOMPONENTS, HComposition.class, msgs);
			msgs = basicSetHComposition(newHComposition, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DatamodelPackage.HCOMPONENT__HCOMPOSITION, newHComposition, newHComposition));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DatamodelPackage.HCOMPONENT__HLOFTS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getHLofts()).basicAdd(otherEnd, msgs);
			case DatamodelPackage.HCOMPONENT__HCOMPOSITION:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetHComposition((HComposition)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DatamodelPackage.HCOMPONENT__PARAMETERS:
				return ((InternalEList<?>)getParameters()).basicRemove(otherEnd, msgs);
			case DatamodelPackage.HCOMPONENT__HCOORDINATE_SYSTEM3_D:
				return basicSetHCoordinateSystem3D(null, msgs);
			case DatamodelPackage.HCOMPONENT__HLOFTS:
				return ((InternalEList<?>)getHLofts()).basicRemove(otherEnd, msgs);
			case DatamodelPackage.HCOMPONENT__HCOMPOSITION:
				return basicSetHComposition(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case DatamodelPackage.HCOMPONENT__HCOMPOSITION:
				return eInternalContainer().eInverseRemove(this, DatamodelPackage.HCOMPOSITION__HCOMPONENTS, HComposition.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DatamodelPackage.HCOMPONENT__NAME:
				return getName();
			case DatamodelPackage.HCOMPONENT__TYPE:
				return getType();
			case DatamodelPackage.HCOMPONENT__DESCRIPTION:
				return getDescription();
			case DatamodelPackage.HCOMPONENT__PARAMETERS:
				if (coreType) return getParameters();
				else return getParameters().map();
			case DatamodelPackage.HCOMPONENT__HCOORDINATE_SYSTEM3_D:
				return getHCoordinateSystem3D();
			case DatamodelPackage.HCOMPONENT__HLOFTS:
				return getHLofts();
			case DatamodelPackage.HCOMPONENT__HCOMPOSITION:
				return getHComposition();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DatamodelPackage.HCOMPONENT__NAME:
				setName((String)newValue);
				return;
			case DatamodelPackage.HCOMPONENT__TYPE:
				setType((String)newValue);
				return;
			case DatamodelPackage.HCOMPONENT__DESCRIPTION:
				setDescription((String)newValue);
				return;
			case DatamodelPackage.HCOMPONENT__PARAMETERS:
				((EStructuralFeature.Setting)getParameters()).set(newValue);
				return;
			case DatamodelPackage.HCOMPONENT__HCOORDINATE_SYSTEM3_D:
				setHCoordinateSystem3D((HCoordinateSystem3D)newValue);
				return;
			case DatamodelPackage.HCOMPONENT__HLOFTS:
				getHLofts().clear();
				getHLofts().addAll((Collection<? extends HLoft>)newValue);
				return;
			case DatamodelPackage.HCOMPONENT__HCOMPOSITION:
				setHComposition((HComposition)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DatamodelPackage.HCOMPONENT__NAME:
				setName(NAME_EDEFAULT);
				return;
			case DatamodelPackage.HCOMPONENT__TYPE:
				setType(TYPE_EDEFAULT);
				return;
			case DatamodelPackage.HCOMPONENT__DESCRIPTION:
				setDescription(DESCRIPTION_EDEFAULT);
				return;
			case DatamodelPackage.HCOMPONENT__PARAMETERS:
				getParameters().clear();
				return;
			case DatamodelPackage.HCOMPONENT__HCOORDINATE_SYSTEM3_D:
				setHCoordinateSystem3D((HCoordinateSystem3D)null);
				return;
			case DatamodelPackage.HCOMPONENT__HLOFTS:
				getHLofts().clear();
				return;
			case DatamodelPackage.HCOMPONENT__HCOMPOSITION:
				setHComposition((HComposition)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DatamodelPackage.HCOMPONENT__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case DatamodelPackage.HCOMPONENT__TYPE:
				return TYPE_EDEFAULT == null ? type != null : !TYPE_EDEFAULT.equals(type);
			case DatamodelPackage.HCOMPONENT__DESCRIPTION:
				return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
			case DatamodelPackage.HCOMPONENT__PARAMETERS:
				return parameters != null && !parameters.isEmpty();
			case DatamodelPackage.HCOMPONENT__HCOORDINATE_SYSTEM3_D:
				return hCoordinateSystem3D != null;
			case DatamodelPackage.HCOMPONENT__HLOFTS:
				return hLofts != null && !hLofts.isEmpty();
			case DatamodelPackage.HCOMPONENT__HCOMPOSITION:
				return getHComposition() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", type: ");
		result.append(type);
		result.append(", description: ");
		result.append(description);
		result.append(')');
		return result.toString();
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getHCoordinateSystem3D() == null) ? 0 : getHCoordinateSystem3D().hashCode());
		result = prime * result + ((getDescription() == null) ? 0 : getDescription().hashCode());
		result = prime * result + ((getParameters() == null) ? 0 : getParameters().hashCode());
		result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
		result = prime * result + ((getHLofts() == null) ? 0 : getHLofts().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HComponentImpl other = (HComponentImpl) obj;
		if (getHCoordinateSystem3D() == null) {
			if (other.getHCoordinateSystem3D() != null)
				return false;
		} else if (!getHCoordinateSystem3D().equals(other.getHCoordinateSystem3D()))
			return false;
		if (getParameters() == null) {
			if (other.getParameters() != null)
				return false;
		} else if (!getParameters().equals(other.getParameters()))
			return false;
		if (getDescription() == null) {
			if (other.getDescription() != null)
				return false;
		} else if (!getDescription().equals(other.getDescription()))
			return false;
		if (getName() == null) {
			if (other.getName() != null)
				return false;
		} else if (!getName().equals(other.getName()))
			return false;
		if (getType() == null) {
			if (other.getType() != null)
				return false;
		} else if (!getType().equals(other.getType()))
			return false;
		if (getHLofts() == null) {
			if (other.getHLofts() != null)
				return false;
		} else if (!getHLofts().equals(other.getHLofts()))
			return false;
		return true;
	}

} //HComponentImpl
