/**
 */
package de.hykops.jna.datamodel.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import com.sun.jna.NativeLong;

import de.hykops.jna.datamodel.factory.HykopsObjectFactory;
import de.hykops.lpd.datamodel.DatamodelPackage;
import de.hykops.lpd.datamodel.HDiscreteLoftPathPoint3D;
import de.hykops.lpd.datamodel.HPoint3D;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>HDiscrete Loft Path Point3 D</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.hykops.lpd.datamodel.impl.HDiscreteLoftPathPoint3DImpl#getLoftCurvePoint <em>Loft Curve Point</em>}</li>
 *   <li>{@link de.hykops.lpd.datamodel.impl.HDiscreteLoftPathPoint3DImpl#getLoftCoordinate <em>Loft Coordinate</em>}</li>
 *   <li>{@link de.hykops.lpd.datamodel.impl.HDiscreteLoftPathPoint3DImpl#getLoftPathVector <em>Loft Path Vector</em>}</li>
 *   <li>{@link de.hykops.lpd.datamodel.impl.HDiscreteLoftPathPoint3DImpl#getLoftPathXi <em>Loft Path Xi</em>}</li>
 *   <li>{@link de.hykops.lpd.datamodel.impl.HDiscreteLoftPathPoint3DImpl#getLoftPathEta <em>Loft Path Eta</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HDiscreteLoftPathPoint3DImpl extends HPoint3DImpl implements HDiscreteLoftPathPoint3D {
	
	
	/**
	 * The cached value of the '{@link #getLoftCurvePoint() <em>Loft Curve Point</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLoftCurvePoint()
	 * @generated
	 * @ordered
	 */
	protected HPoint3D loftCurvePoint;

	/**
	 * The default value of the '{@link #getLoftCoordinate() <em>Loft Coordinate</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLoftCoordinate()
	 * @generated
	 * @ordered
	 */
	protected static final double LOFT_COORDINATE_EDEFAULT = 0.0;

	/**
	 * The cached value of the '{@link #getLoftCoordinate() <em>Loft Coordinate</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLoftCoordinate()
	 * @generated
	 * @ordered
	 */
	protected Double loftCoordinate = LOFT_COORDINATE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getLoftPathVector() <em>Loft Path Vector</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLoftPathVector()
	 * @generated
	 * @ordered
	 */
	protected HPoint3D loftPathVector;

	/**
	 * The cached value of the '{@link #getLoftPathXi() <em>Loft Path Xi</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLoftPathXi()
	 * @generated
	 * @ordered
	 */
	protected HPoint3D loftPathXi;

	/**
	 * The cached value of the '{@link #getLoftPathEta() <em>Loft Path Eta</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLoftPathEta()
	 * @generated
	 * @ordered
	 */
	protected HPoint3D loftPathEta;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HDiscreteLoftPathPoint3DImpl() {
		this(CORE.create_object(HDiscreteLoftPathPoint3D.CLASS_NAME));
	}

	public HDiscreteLoftPathPoint3DImpl(NativeLong handle) {
		this.handle = handle;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DatamodelPackage.Literals.HDISCRETE_LOFT_PATH_POINT3_D;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HPoint3D getLoftCurvePoint() {
		if (this.loftCurvePoint == null) {
			NativeLong objHandle = CORE.get_object(getHandle(), PROPERTY_LOFT_CURVE_POINT);
			if (objHandle.intValue() != 0) {
				this.loftCurvePoint = (HPoint3D) HykopsObjectFactory.createObject(objHandle);
			}
		}
		return loftCurvePoint;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLoftCurvePoint(HPoint3D newLoftCurvePoint, NotificationChain msgs) {
		HPoint3D oldLoftCurvePoint = loftCurvePoint;
		loftCurvePoint = newLoftCurvePoint;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DatamodelPackage.HDISCRETE_LOFT_PATH_POINT3_D__LOFT_CURVE_POINT, oldLoftCurvePoint, newLoftCurvePoint);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLoftCurvePoint(HPoint3D newLoftCurvePoint) {
		CORE.set_object(getHandle(), PROPERTY_LOFT_CURVE_POINT, newLoftCurvePoint.getHandle());
		this.loftCurvePoint = newLoftCurvePoint;
		if (newLoftCurvePoint != loftCurvePoint) {
			NotificationChain msgs = null;
			if (loftCurvePoint != null)
				msgs = ((InternalEObject)loftCurvePoint).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DatamodelPackage.HDISCRETE_LOFT_PATH_POINT3_D__LOFT_CURVE_POINT, null, msgs);
			if (newLoftCurvePoint != null)
				msgs = ((InternalEObject)newLoftCurvePoint).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DatamodelPackage.HDISCRETE_LOFT_PATH_POINT3_D__LOFT_CURVE_POINT, null, msgs);
			msgs = basicSetLoftCurvePoint(newLoftCurvePoint, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DatamodelPackage.HDISCRETE_LOFT_PATH_POINT3_D__LOFT_CURVE_POINT, newLoftCurvePoint, newLoftCurvePoint));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Double getLoftCoordinate() {
		if (this.loftCoordinate == null) {
			this.loftCoordinate = CORE.get_double(getHandle(), PROPERTY_LOFT_COORDINATE);
		}
		return loftCoordinate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLoftCoordinate(Double newLoftCoordinate) {
		CORE.set_double(getHandle(), PROPERTY_LOFT_COORDINATE, newLoftCoordinate);
		double oldLoftCoordinate = loftCoordinate;
		loftCoordinate = newLoftCoordinate;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DatamodelPackage.HDISCRETE_LOFT_PATH_POINT3_D__LOFT_COORDINATE, oldLoftCoordinate, loftCoordinate.doubleValue()));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HPoint3D getLoftPathVector() {
		if (this.loftPathVector == null) {
			NativeLong objHandle = CORE.get_object(getHandle(), PROPERTY_LOFT_PATH_VECTOR);
			if (objHandle.intValue() != 0) {
				this.loftPathVector = (HPoint3D) HykopsObjectFactory.createObject(objHandle);
			}
		}
		return loftPathVector;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLoftPathVector(HPoint3D newLoftPathVector, NotificationChain msgs) {
		HPoint3D oldLoftPathVector = loftPathVector;
		loftPathVector = newLoftPathVector;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DatamodelPackage.HDISCRETE_LOFT_PATH_POINT3_D__LOFT_PATH_VECTOR, oldLoftPathVector, newLoftPathVector);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLoftPathVector(HPoint3D newLoftPathVector) {
		CORE.set_object(getHandle(), PROPERTY_LOFT_PATH_VECTOR, newLoftPathVector.getHandle());
		if (newLoftPathVector != loftPathVector) {
			NotificationChain msgs = null;
			if (loftPathVector != null)
				msgs = ((InternalEObject)loftPathVector).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DatamodelPackage.HDISCRETE_LOFT_PATH_POINT3_D__LOFT_PATH_VECTOR, null, msgs);
			if (newLoftPathVector != null)
				msgs = ((InternalEObject)newLoftPathVector).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DatamodelPackage.HDISCRETE_LOFT_PATH_POINT3_D__LOFT_PATH_VECTOR, null, msgs);
			msgs = basicSetLoftPathVector(newLoftPathVector, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DatamodelPackage.HDISCRETE_LOFT_PATH_POINT3_D__LOFT_PATH_VECTOR, newLoftPathVector, newLoftPathVector));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HPoint3D getLoftPathXi() {
		if (this.loftPathXi == null) {
			NativeLong objHandle = CORE.get_object(getHandle(), PROPERTY_LOFT_PATH_XI);
			if (objHandle.intValue() != 0) {
				this.loftPathXi = (HPoint3D) HykopsObjectFactory.createObject(objHandle);
			}
		}
		return loftPathXi;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLoftPathXi(HPoint3D newLoftPathXi, NotificationChain msgs) {
		HPoint3D oldLoftPathXi = loftPathXi;
		loftPathXi = newLoftPathXi;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DatamodelPackage.HDISCRETE_LOFT_PATH_POINT3_D__LOFT_PATH_XI, oldLoftPathXi, newLoftPathXi);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLoftPathXi(HPoint3D newLoftPathXi) {
		CORE.set_object(getHandle(), PROPERTY_LOFT_PATH_XI, newLoftPathXi.getHandle());
		if (newLoftPathXi != loftPathXi) {
			NotificationChain msgs = null;
			if (loftPathXi != null)
				msgs = ((InternalEObject)loftPathXi).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DatamodelPackage.HDISCRETE_LOFT_PATH_POINT3_D__LOFT_PATH_XI, null, msgs);
			if (newLoftPathXi != null)
				msgs = ((InternalEObject)newLoftPathXi).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DatamodelPackage.HDISCRETE_LOFT_PATH_POINT3_D__LOFT_PATH_XI, null, msgs);
			msgs = basicSetLoftPathXi(newLoftPathXi, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DatamodelPackage.HDISCRETE_LOFT_PATH_POINT3_D__LOFT_PATH_XI, newLoftPathXi, newLoftPathXi));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HPoint3D getLoftPathEta() {
		if (this.loftPathEta == null) {
			NativeLong objHandle = CORE.get_object(getHandle(), PROPERTY_LOFT_PATH_ETA);
			if (objHandle.intValue() != 0) {
				this.loftPathEta = (HPoint3D) HykopsObjectFactory.createObject(objHandle);
			}
		}
		return loftPathEta;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLoftPathEta(HPoint3D newLoftPathEta, NotificationChain msgs) {
		HPoint3D oldLoftPathEta = loftPathEta;
		loftPathEta = newLoftPathEta;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DatamodelPackage.HDISCRETE_LOFT_PATH_POINT3_D__LOFT_PATH_ETA, oldLoftPathEta, newLoftPathEta);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLoftPathEta(HPoint3D newLoftPathEta) {
		CORE.set_object(getHandle(), PROPERTY_LOFT_PATH_ETA, newLoftPathEta.getHandle());
		if (newLoftPathEta != loftPathEta) {
			NotificationChain msgs = null;
			if (loftPathEta != null)
				msgs = ((InternalEObject)loftPathEta).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DatamodelPackage.HDISCRETE_LOFT_PATH_POINT3_D__LOFT_PATH_ETA, null, msgs);
			if (newLoftPathEta != null)
				msgs = ((InternalEObject)newLoftPathEta).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DatamodelPackage.HDISCRETE_LOFT_PATH_POINT3_D__LOFT_PATH_ETA, null, msgs);
			msgs = basicSetLoftPathEta(newLoftPathEta, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DatamodelPackage.HDISCRETE_LOFT_PATH_POINT3_D__LOFT_PATH_ETA, newLoftPathEta, newLoftPathEta));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DatamodelPackage.HDISCRETE_LOFT_PATH_POINT3_D__LOFT_CURVE_POINT:
				return basicSetLoftCurvePoint(null, msgs);
			case DatamodelPackage.HDISCRETE_LOFT_PATH_POINT3_D__LOFT_PATH_VECTOR:
				return basicSetLoftPathVector(null, msgs);
			case DatamodelPackage.HDISCRETE_LOFT_PATH_POINT3_D__LOFT_PATH_XI:
				return basicSetLoftPathXi(null, msgs);
			case DatamodelPackage.HDISCRETE_LOFT_PATH_POINT3_D__LOFT_PATH_ETA:
				return basicSetLoftPathEta(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DatamodelPackage.HDISCRETE_LOFT_PATH_POINT3_D__LOFT_CURVE_POINT:
				return getLoftCurvePoint();
			case DatamodelPackage.HDISCRETE_LOFT_PATH_POINT3_D__LOFT_COORDINATE:
				return getLoftCoordinate();
			case DatamodelPackage.HDISCRETE_LOFT_PATH_POINT3_D__LOFT_PATH_VECTOR:
				return getLoftPathVector();
			case DatamodelPackage.HDISCRETE_LOFT_PATH_POINT3_D__LOFT_PATH_XI:
				return getLoftPathXi();
			case DatamodelPackage.HDISCRETE_LOFT_PATH_POINT3_D__LOFT_PATH_ETA:
				return getLoftPathEta();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DatamodelPackage.HDISCRETE_LOFT_PATH_POINT3_D__LOFT_CURVE_POINT:
				setLoftCurvePoint((HPoint3D)newValue);
				return;
			case DatamodelPackage.HDISCRETE_LOFT_PATH_POINT3_D__LOFT_COORDINATE:
				setLoftCoordinate((Double)newValue);
				return;
			case DatamodelPackage.HDISCRETE_LOFT_PATH_POINT3_D__LOFT_PATH_VECTOR:
				setLoftPathVector((HPoint3D)newValue);
				return;
			case DatamodelPackage.HDISCRETE_LOFT_PATH_POINT3_D__LOFT_PATH_XI:
				setLoftPathXi((HPoint3D)newValue);
				return;
			case DatamodelPackage.HDISCRETE_LOFT_PATH_POINT3_D__LOFT_PATH_ETA:
				setLoftPathEta((HPoint3D)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DatamodelPackage.HDISCRETE_LOFT_PATH_POINT3_D__LOFT_CURVE_POINT:
				setLoftCurvePoint((HPoint3D)null);
				return;
			case DatamodelPackage.HDISCRETE_LOFT_PATH_POINT3_D__LOFT_COORDINATE:
				setLoftCoordinate(LOFT_COORDINATE_EDEFAULT);
				return;
			case DatamodelPackage.HDISCRETE_LOFT_PATH_POINT3_D__LOFT_PATH_VECTOR:
				setLoftPathVector((HPoint3D)null);
				return;
			case DatamodelPackage.HDISCRETE_LOFT_PATH_POINT3_D__LOFT_PATH_XI:
				setLoftPathXi((HPoint3D)null);
				return;
			case DatamodelPackage.HDISCRETE_LOFT_PATH_POINT3_D__LOFT_PATH_ETA:
				setLoftPathEta((HPoint3D)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DatamodelPackage.HDISCRETE_LOFT_PATH_POINT3_D__LOFT_CURVE_POINT:
				return loftCurvePoint != null;
			case DatamodelPackage.HDISCRETE_LOFT_PATH_POINT3_D__LOFT_COORDINATE:
				return loftCoordinate != LOFT_COORDINATE_EDEFAULT;
			case DatamodelPackage.HDISCRETE_LOFT_PATH_POINT3_D__LOFT_PATH_VECTOR:
				return loftPathVector != null;
			case DatamodelPackage.HDISCRETE_LOFT_PATH_POINT3_D__LOFT_PATH_XI:
				return loftPathXi != null;
			case DatamodelPackage.HDISCRETE_LOFT_PATH_POINT3_D__LOFT_PATH_ETA:
				return loftPathEta != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (loftCoordinate: ");
		result.append(loftCoordinate);
		result.append(')');
		return result.toString();
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getLoftCoordinate() == null) ? 0 : getLoftCoordinate().hashCode());
		result = prime * result + ((getLoftCurvePoint() == null) ? 0 : getLoftCurvePoint().hashCode());
		result = prime * result + ((getLoftPathEta() == null) ? 0 : getLoftPathEta().hashCode());
		result = prime * result + ((getLoftPathVector() == null) ? 0 : getLoftPathVector().hashCode());
		result = prime * result + ((getLoftPathXi() == null) ? 0 : getLoftPathXi().hashCode());
		result = prime * result + ((getX1() == null) ? 0 : getX1().hashCode());
		result = prime * result + ((getX2() == null) ? 0 : getX2().hashCode());
		result = prime * result + ((getX3() == null) ? 0 : getX3().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HDiscreteLoftPathPoint3DImpl other = (HDiscreteLoftPathPoint3DImpl) obj;
		if (getLoftCoordinate() == null) {
			if (other.getLoftCoordinate() != null)
				return false;
		} else if (!getLoftCoordinate().equals(other.getLoftCoordinate()))
			return false;
		if (getLoftCurvePoint() == null) {
			if (other.getLoftCurvePoint() != null)
				return false;
		} else if (!getLoftCurvePoint().equals(other.getLoftCurvePoint()))
			return false;
		if (getLoftPathEta() == null) {
			if (other.getLoftPathEta() != null)
				return false;
		} else if (!getLoftPathEta().equals(other.getLoftPathEta()))
			return false;
		if (getLoftPathVector() == null) {
			if (other.getLoftPathVector() != null)
				return false;
		} else if (!getLoftPathVector().equals(other.getLoftPathVector()))
			return false;
		if (getLoftPathXi() == null) {
			if (other.getLoftPathXi() != null)
				return false;
		} else if (!getLoftPathXi().equals(other.getLoftPathXi()))
			return false;
		if (getX1() == null) {
			if (other.getX1() != null)
				return false;
		} else if (!getX1().equals(other.getX1()))
			return false;
		if (getX2() == null) {
			if (other.getX2() != null)
				return false;
		} else if (!getX2().equals(other.getX2()))
			return false;
		if (getX3() == null) {
			if (other.getX3() != null)
				return false;
		} else if (!getX3().equals(other.getX3()))
			return false;
		return true;
	}

} //HDiscreteLoftPathPoint3DImpl
