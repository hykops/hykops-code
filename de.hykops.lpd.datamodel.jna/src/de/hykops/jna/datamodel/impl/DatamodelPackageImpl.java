/**
 */
package de.hykops.jna.datamodel.impl;

import java.util.Map;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EGenericType;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.ETypeParameter;
import org.eclipse.emf.ecore.impl.EPackageImpl;

import de.hykops.lpd.datamodel.DatamodelFactory;
import de.hykops.lpd.datamodel.DatamodelPackage;
import de.hykops.lpd.datamodel.HComponent;
import de.hykops.lpd.datamodel.HComposition;
import de.hykops.lpd.datamodel.HCoordinateSystem3D;
import de.hykops.lpd.datamodel.HCoordinateSystem3DAffine;
import de.hykops.lpd.datamodel.HCoordinateSystem3DCartesian;
import de.hykops.lpd.datamodel.HCoordinateSystem3DCone;
import de.hykops.lpd.datamodel.HCoordinateSystem3DPolar;
import de.hykops.lpd.datamodel.HCurve2D;
import de.hykops.lpd.datamodel.HCurvePoint2D;
import de.hykops.lpd.datamodel.HDiscreteCurve2D;
import de.hykops.lpd.datamodel.HDiscreteLoftPath;
import de.hykops.lpd.datamodel.HDiscreteLoftPathPoint3D;
import de.hykops.lpd.datamodel.HHinge;
import de.hykops.lpd.datamodel.HLinearLoftPath;
import de.hykops.lpd.datamodel.HLoft;
import de.hykops.lpd.datamodel.HLoftElement;
import de.hykops.lpd.datamodel.HLoftPath;
import de.hykops.lpd.datamodel.HPair;
import de.hykops.lpd.datamodel.HParent;
import de.hykops.lpd.datamodel.HPoint2D;
import de.hykops.lpd.datamodel.HPoint3D;
import de.hykops.lpd.datamodel.HSeriesProfileCurve2D;
import de.hykops.lpd.datamodel.HSurfaceIdentifier;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class DatamodelPackageImpl extends EPackageImpl implements DatamodelPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hComponentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hCompositionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hCoordinateSystem3DEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hCoordinateSystem3DAffineEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hCoordinateSystem3DCartesianEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hCoordinateSystem3DConeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hCoordinateSystem3DPolarEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hCurve2DEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hCurvePoint2DEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hDiscreteCurve2DEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hDiscreteLoftPathEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hDiscreteLoftPathPoint3DEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hHingeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hLinearLoftPathEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hLoftEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hLoftElementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hLoftPathEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hPairEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hParentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hPoint2DEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hPoint3DEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hSeriesProfileCurve2DEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hSurfaceIdentifierEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mapEClass = null;
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stringToStringMapEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see de.hykops.lpd.datamodel.de.hykops.lpd.datamodel.test.DatamodelPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private DatamodelPackageImpl() {
		super(eNS_URI, DatamodelFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link DatamodelPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static DatamodelPackage init() {
		if (isInited) return (DatamodelPackage)EPackage.Registry.INSTANCE.getEPackage(DatamodelPackage.eNS_URI);

		// Obtain or create and register package
		DatamodelPackageImpl theDatamodelPackage = (DatamodelPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof DatamodelPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new DatamodelPackageImpl());

		isInited = true;

		// Create package meta-data objects
		theDatamodelPackage.createPackageContents();

		// Initialize created meta-data
		theDatamodelPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theDatamodelPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(DatamodelPackage.eNS_URI, theDatamodelPackage);
		return theDatamodelPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHComponent() {
		return hComponentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHComponent_Name() {
		return (EAttribute)hComponentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHComponent_Type() {
		return (EAttribute)hComponentEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHComponent_Description() {
		return (EAttribute)hComponentEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHComponent_Parameters() {
		return (EReference)hComponentEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHComponent_HCoordinateSystem3D() {
		return (EReference)hComponentEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHComponent_HLofts() {
		return (EReference)hComponentEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHComponent_HComposition() {
		return (EReference)hComponentEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHComposition() {
		return hCompositionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHComposition_Version() {
		return (EAttribute)hCompositionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHComposition_Name() {
		return (EAttribute)hCompositionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHComposition_Description() {
		return (EAttribute)hCompositionEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHComposition_UnitSystem() {
		return (EAttribute)hCompositionEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHComposition_HCoordinateSystem3D() {
		return (EReference)hCompositionEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHComposition_HComponents() {
		return (EReference)hCompositionEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHCoordinateSystem3D() {
		return hCoordinateSystem3DEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHCoordinateSystem3D_Origin() {
		return (EReference)hCoordinateSystem3DEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHCoordinateSystem3D_HHinges() {
		return (EReference)hCoordinateSystem3DEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHCoordinateSystem3DAffine() {
		return hCoordinateSystem3DAffineEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHCoordinateSystem3DAffine_X1() {
		return (EReference)hCoordinateSystem3DAffineEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHCoordinateSystem3DAffine_X2() {
		return (EReference)hCoordinateSystem3DAffineEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHCoordinateSystem3DAffine_X3() {
		return (EReference)hCoordinateSystem3DAffineEClass.getEStructuralFeatures().get(2);
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHCoordinateSystem3DCartesian() {
		return hCoordinateSystem3DCartesianEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHCoordinateSystem3DCartesian_X1() {
		return (EReference)hCoordinateSystem3DCartesianEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHCoordinateSystem3DCartesian_E1() {
		return (EReference)hCoordinateSystem3DCartesianEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHCoordinateSystem3DCone() {
		return hCoordinateSystem3DConeEClass;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHCoordinateSystem3DCone_AxialVector() {
		return (EReference)hCoordinateSystem3DConeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHCoordinateSystem3DCone_RadialVector() {
		return (EReference)hCoordinateSystem3DConeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHCoordinateSystem3DCone_Angle() {
		return (EReference)hCoordinateSystem3DConeEClass.getEStructuralFeatures().get(2);
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHCoordinateSystem3DPolar() {
		return hCoordinateSystem3DPolarEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHCoordinateSystem3DPolar_AxialVector() {
		return (EReference)hCoordinateSystem3DPolarEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHCoordinateSystem3DPolar_RadialVector() {
		return (EReference)hCoordinateSystem3DPolarEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHCoordinateSystem3DPolar_Angle() {
		return (EReference)hCoordinateSystem3DPolarEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHCurve2D() {
		return hCurve2DEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHCurve2D_HSurfaceIdentifiers() {
		return (EReference)hCurve2DEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHCurve2D_HLoftElement() {
		return (EReference)hCurve2DEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHCurvePoint2D() {
		return hCurvePoint2DEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHCurvePoint2D_CurveCoordinate() {
		return (EAttribute)hCurvePoint2DEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHDiscreteCurve2D() {
		return hDiscreteCurve2DEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHDiscreteCurve2D_InterpolationScheme() {
		return (EAttribute)hDiscreteCurve2DEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHDiscreteCurve2D_CurvePoints() {
		return (EReference)hDiscreteCurve2DEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHDiscreteLoftPath() {
		return hDiscreteLoftPathEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHDiscreteLoftPath_LoftCurvePoints() {
		return (EReference)hDiscreteLoftPathEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHDiscreteLoftPath_InterpolationScheme() {
		return (EAttribute)hDiscreteLoftPathEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHDiscreteLoftPathPoint3D() {
		return hDiscreteLoftPathPoint3DEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHDiscreteLoftPathPoint3D_LoftCurvePoint() {
		return (EReference)hDiscreteLoftPathPoint3DEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHDiscreteLoftPathPoint3D_LoftCoordinate() {
		return (EAttribute)hDiscreteLoftPathPoint3DEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHDiscreteLoftPathPoint3D_LoftPathVector() {
		return (EReference)hDiscreteLoftPathPoint3DEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHDiscreteLoftPathPoint3D_LoftPathXi() {
		return (EReference)hDiscreteLoftPathPoint3DEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHDiscreteLoftPathPoint3D_LoftPathEta() {
		return (EReference)hDiscreteLoftPathPoint3DEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHHinge() {
		return hHingeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHHinge_HingeAxis() {
		return (EReference)hHingeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHHinge_HingePositions() {
		return (EReference)hHingeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHHinge_HingeLimited() {
		return (EAttribute)hHingeEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHHinge_HCoordinateSystem3D() {
		return (EReference)hHingeEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHLinearLoftPath() {
		return hLinearLoftPathEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHLinearLoftPath_Origin() {
		return (EReference)hLinearLoftPathEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHLinearLoftPath_LoftPathVector() {
		return (EReference)hLinearLoftPathEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHLinearLoftPath_LoftPathXi() {
		return (EReference)hLinearLoftPathEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHLinearLoftPath_LoftPathEta() {
		return (EReference)hLinearLoftPathEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHLoft() {
		return hLoftEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHLoft_Name() {
		return (EAttribute)hLoftEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHLoft_Description() {
		return (EAttribute)hLoftEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHLoft_Parameters() {
		return (EReference)hLoftEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHLoft_HCoordinateSystems3D() {
		return (EReference)hLoftEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHLoft_HLoftPath() {
		return (EReference)hLoftEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHLoft_HLoftElements() {
		return (EReference)hLoftEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHLoft_HSurfaceIdentifications() {
		return (EReference)hLoftEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHLoft_HComponent() {
		return (EReference)hLoftEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHLoftElement() {
		return hLoftElementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHLoftElement_LoftPlaneDefiningCoordinateSystem() {
		return (EReference)hLoftElementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHLoftElement_HCurve2Ds() {
		return (EReference)hLoftElementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHLoftElement_HLoft() {
		return (EReference)hLoftElementEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHLoftPath() {
		return hLoftPathEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHLoftPath_Denominator() {
		return (EAttribute)hLoftPathEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHLoftPath_HLoftCoordinateSystem() {
		return (EReference)hLoftPathEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHLoftPath_HLoft() {
		return (EReference)hLoftPathEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHPair() {
		return hPairEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHPair_A() {
		return (EReference)hPairEClass.getEStructuralFeatures().get(0);
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStringToStringMap() {
		return stringToStringMapEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHPair_B() {
		return (EReference)hPairEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHParent() {
		return hParentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHParent_HParent() {
		return (EReference)hParentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHPoint2D() {
		return hPoint2DEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHPoint2D_X1() {
		return (EAttribute)hPoint2DEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHPoint2D_X2() {
		return (EAttribute)hPoint2DEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHPoint3D() {
		return hPoint3DEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHPoint3D_X1() {
		return (EAttribute)hPoint3DEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHPoint3D_X2() {
		return (EAttribute)hPoint3DEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHPoint3D_X3() {
		return (EAttribute)hPoint3DEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHSeriesProfileCurve2D() {
		return hSeriesProfileCurve2DEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHSeriesProfileCurve2D_Denomination() {
		return (EAttribute)hSeriesProfileCurve2DEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHSeriesProfileCurve2D_Side() {
		return (EAttribute)hSeriesProfileCurve2DEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHSeriesProfileCurve2D_Series() {
		return (EAttribute)hSeriesProfileCurve2DEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHSurfaceIdentifier() {
		return hSurfaceIdentifierEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHSurfaceIdentifier_Name() {
		return (EAttribute)hSurfaceIdentifierEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMap() {
		return mapEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DatamodelFactory getDatamodelFactory() {
		return (DatamodelFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		hComponentEClass = createEClass(HCOMPONENT);
		createEAttribute(hComponentEClass, HCOMPONENT__NAME);
		createEAttribute(hComponentEClass, HCOMPONENT__TYPE);
		createEAttribute(hComponentEClass, HCOMPONENT__DESCRIPTION);
		createEReference(hComponentEClass, HCOMPONENT__PARAMETERS);
		createEReference(hComponentEClass, HCOMPONENT__HCOORDINATE_SYSTEM3_D);
		createEReference(hComponentEClass, HCOMPONENT__HLOFTS);
		createEReference(hComponentEClass, HCOMPONENT__HCOMPOSITION);

		hCompositionEClass = createEClass(HCOMPOSITION);
		createEAttribute(hCompositionEClass, HCOMPOSITION__VERSION);
		createEAttribute(hCompositionEClass, HCOMPOSITION__NAME);
		createEAttribute(hCompositionEClass, HCOMPOSITION__DESCRIPTION);
		createEAttribute(hCompositionEClass, HCOMPOSITION__UNIT_SYSTEM);
		createEReference(hCompositionEClass, HCOMPOSITION__HCOORDINATE_SYSTEM3_D);
		createEReference(hCompositionEClass, HCOMPOSITION__HCOMPONENTS);

		hCoordinateSystem3DEClass = createEClass(HCOORDINATE_SYSTEM3_D);
		createEReference(hCoordinateSystem3DEClass, HCOORDINATE_SYSTEM3_D__ORIGIN);
		createEReference(hCoordinateSystem3DEClass, HCOORDINATE_SYSTEM3_D__HHINGES);

		hCoordinateSystem3DAffineEClass = createEClass(HCOORDINATE_SYSTEM3_DAFFINE);
		createEReference(hCoordinateSystem3DAffineEClass, HCOORDINATE_SYSTEM3_DAFFINE__X1);
		createEReference(hCoordinateSystem3DAffineEClass, HCOORDINATE_SYSTEM3_DAFFINE__X2);
		createEReference(hCoordinateSystem3DAffineEClass, HCOORDINATE_SYSTEM3_DAFFINE__X3);

		hCoordinateSystem3DCartesianEClass = createEClass(HCOORDINATE_SYSTEM3_DCARTESIAN);
		createEReference(hCoordinateSystem3DCartesianEClass, HCOORDINATE_SYSTEM3_DCARTESIAN__X1);
		createEReference(hCoordinateSystem3DCartesianEClass, HCOORDINATE_SYSTEM3_DCARTESIAN__E1);

		hCoordinateSystem3DConeEClass = createEClass(HCOORDINATE_SYSTEM3_DCONE);
		createEReference(hCoordinateSystem3DConeEClass, HCOORDINATE_SYSTEM3_DCONE__AXIAL_VECTOR);
		createEReference(hCoordinateSystem3DConeEClass, HCOORDINATE_SYSTEM3_DCONE__RADIAL_VECTOR);
		createEReference(hCoordinateSystem3DConeEClass, HCOORDINATE_SYSTEM3_DCONE__ANGLE);

		hCoordinateSystem3DPolarEClass = createEClass(HCOORDINATE_SYSTEM3_DPOLAR);
		createEReference(hCoordinateSystem3DPolarEClass, HCOORDINATE_SYSTEM3_DPOLAR__AXIAL_VECTOR);
		createEReference(hCoordinateSystem3DPolarEClass, HCOORDINATE_SYSTEM3_DPOLAR__RADIAL_VECTOR);
		createEReference(hCoordinateSystem3DPolarEClass, HCOORDINATE_SYSTEM3_DPOLAR__ANGLE);

		hCurve2DEClass = createEClass(HCURVE2_D);
		createEReference(hCurve2DEClass, HCURVE2_D__HSURFACE_IDENTIFIERS);
		createEReference(hCurve2DEClass, HCURVE2_D__HLOFT_ELEMENT);

		hCurvePoint2DEClass = createEClass(HCURVE_POINT2_D);
		createEAttribute(hCurvePoint2DEClass, HCURVE_POINT2_D__CURVE_COORDINATE);

		hDiscreteCurve2DEClass = createEClass(HDISCRETE_CURVE2_D);
		createEAttribute(hDiscreteCurve2DEClass, HDISCRETE_CURVE2_D__INTERPOLATION_SCHEME);
		createEReference(hDiscreteCurve2DEClass, HDISCRETE_CURVE2_D__CURVE_POINTS);

		hDiscreteLoftPathEClass = createEClass(HDISCRETE_LOFT_PATH);
		createEReference(hDiscreteLoftPathEClass, HDISCRETE_LOFT_PATH__LOFT_CURVE_POINTS);
		createEAttribute(hDiscreteLoftPathEClass, HDISCRETE_LOFT_PATH__INTERPOLATION_SCHEME);

		hDiscreteLoftPathPoint3DEClass = createEClass(HDISCRETE_LOFT_PATH_POINT3_D);
		createEReference(hDiscreteLoftPathPoint3DEClass, HDISCRETE_LOFT_PATH_POINT3_D__LOFT_CURVE_POINT);
		createEAttribute(hDiscreteLoftPathPoint3DEClass, HDISCRETE_LOFT_PATH_POINT3_D__LOFT_COORDINATE);
		createEReference(hDiscreteLoftPathPoint3DEClass, HDISCRETE_LOFT_PATH_POINT3_D__LOFT_PATH_VECTOR);
		createEReference(hDiscreteLoftPathPoint3DEClass, HDISCRETE_LOFT_PATH_POINT3_D__LOFT_PATH_XI);
		createEReference(hDiscreteLoftPathPoint3DEClass, HDISCRETE_LOFT_PATH_POINT3_D__LOFT_PATH_ETA);

		hHingeEClass = createEClass(HHINGE);
		createEReference(hHingeEClass, HHINGE__HINGE_AXIS);
		createEReference(hHingeEClass, HHINGE__HINGE_POSITIONS);
		createEAttribute(hHingeEClass, HHINGE__HINGE_LIMITED);
		createEReference(hHingeEClass, HHINGE__HCOORDINATE_SYSTEM3_D);

		hLinearLoftPathEClass = createEClass(HLINEAR_LOFT_PATH);
		createEReference(hLinearLoftPathEClass, HLINEAR_LOFT_PATH__ORIGIN);
		createEReference(hLinearLoftPathEClass, HLINEAR_LOFT_PATH__LOFT_PATH_VECTOR);
		createEReference(hLinearLoftPathEClass, HLINEAR_LOFT_PATH__LOFT_PATH_XI);
		createEReference(hLinearLoftPathEClass, HLINEAR_LOFT_PATH__LOFT_PATH_ETA);

		hLoftEClass = createEClass(HLOFT);
		createEAttribute(hLoftEClass, HLOFT__NAME);
		createEAttribute(hLoftEClass, HLOFT__DESCRIPTION);
		createEReference(hLoftEClass, HLOFT__PARAMETERS);
		createEReference(hLoftEClass, HLOFT__HCOORDINATE_SYSTEMS3_D);
		createEReference(hLoftEClass, HLOFT__HLOFT_PATH);
		createEReference(hLoftEClass, HLOFT__HLOFT_ELEMENTS);
		createEReference(hLoftEClass, HLOFT__HSURFACE_IDENTIFICATIONS);
		createEReference(hLoftEClass, HLOFT__HCOMPONENT);

		hLoftElementEClass = createEClass(HLOFT_ELEMENT);
		createEReference(hLoftElementEClass, HLOFT_ELEMENT__LOFT_PLANE_DEFINING_COORDINATE_SYSTEM);
		createEReference(hLoftElementEClass, HLOFT_ELEMENT__HCURVE2_DS);
		createEReference(hLoftElementEClass, HLOFT_ELEMENT__HLOFT);

		hLoftPathEClass = createEClass(HLOFT_PATH);
		createEAttribute(hLoftPathEClass, HLOFT_PATH__DENOMINATOR);
		createEReference(hLoftPathEClass, HLOFT_PATH__HLOFT_COORDINATE_SYSTEM);
		createEReference(hLoftPathEClass, HLOFT_PATH__HLOFT);

		hPairEClass = createEClass(HPAIR);
		createEReference(hPairEClass, HPAIR__A);
		createEReference(hPairEClass, HPAIR__B);

		hParentEClass = createEClass(HPARENT);
		createEReference(hParentEClass, HPARENT__HPARENT);

		hPoint2DEClass = createEClass(HPOINT2_D);
		createEAttribute(hPoint2DEClass, HPOINT2_D__X1);
		createEAttribute(hPoint2DEClass, HPOINT2_D__X2);

		hPoint3DEClass = createEClass(HPOINT3_D);
		createEAttribute(hPoint3DEClass, HPOINT3_D__X1);
		createEAttribute(hPoint3DEClass, HPOINT3_D__X2);
		createEAttribute(hPoint3DEClass, HPOINT3_D__X3);

		hSeriesProfileCurve2DEClass = createEClass(HSERIES_PROFILE_CURVE2_D);
		createEAttribute(hSeriesProfileCurve2DEClass, HSERIES_PROFILE_CURVE2_D__DENOMINATION);
		createEAttribute(hSeriesProfileCurve2DEClass, HSERIES_PROFILE_CURVE2_D__SIDE);
		createEAttribute(hSeriesProfileCurve2DEClass, HSERIES_PROFILE_CURVE2_D__SERIES);

		hSurfaceIdentifierEClass = createEClass(HSURFACE_IDENTIFIER);
		createEAttribute(hSurfaceIdentifierEClass, HSURFACE_IDENTIFIER__NAME);

		mapEClass = createEClass(MAP);
		
		stringToStringMapEClass = createEClass(STRING_TO_STRING_MAP);
		createEAttribute(stringToStringMapEClass, STRING_TO_STRING_MAP__KEY);
		createEAttribute(stringToStringMapEClass, STRING_TO_STRING_MAP__VALUE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Create type parameters
		ETypeParameter hPairEClass_A = addETypeParameter(hPairEClass, "A");
		ETypeParameter hPairEClass_B = addETypeParameter(hPairEClass, "B");
		addETypeParameter(mapEClass, "T");
		addETypeParameter(mapEClass, "T1");

		// Set bounds for type parameters

		// Add supertypes to classes
		hComponentEClass.getESuperTypes().add(this.getHParent());
		hCompositionEClass.getESuperTypes().add(this.getHParent());
		hCoordinateSystem3DEClass.getESuperTypes().add(this.getHParent());
		hCoordinateSystem3DAffineEClass.getESuperTypes().add(this.getHCoordinateSystem3D());
		hCoordinateSystem3DCartesianEClass.getESuperTypes().add(this.getHCoordinateSystem3D());
		hCoordinateSystem3DConeEClass.getESuperTypes().add(this.getHCoordinateSystem3D());
		hCoordinateSystem3DPolarEClass.getESuperTypes().add(this.getHCoordinateSystem3D());
		hCurve2DEClass.getESuperTypes().add(this.getHParent());
		hCurvePoint2DEClass.getESuperTypes().add(this.getHPoint2D());
		hDiscreteCurve2DEClass.getESuperTypes().add(this.getHCurve2D());
		hDiscreteLoftPathEClass.getESuperTypes().add(this.getHLoftPath());
		hDiscreteLoftPathPoint3DEClass.getESuperTypes().add(this.getHPoint3D());
		hHingeEClass.getESuperTypes().add(this.getHParent());
		hLinearLoftPathEClass.getESuperTypes().add(this.getHLoftPath());
		hLoftEClass.getESuperTypes().add(this.getHParent());
		hLoftElementEClass.getESuperTypes().add(this.getHParent());
		hLoftPathEClass.getESuperTypes().add(this.getHParent());
		hPairEClass.getESuperTypes().add(this.getHParent());
		hPoint2DEClass.getESuperTypes().add(this.getHParent());
		hPoint3DEClass.getESuperTypes().add(this.getHParent());
		hSeriesProfileCurve2DEClass.getESuperTypes().add(this.getHCurve2D());
		hSurfaceIdentifierEClass.getESuperTypes().add(this.getHParent());

		// Initialize classes, features, and operations; add parameters
		initEClass(hComponentEClass, HComponent.class, "HComponent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getHComponent_Name(), ecorePackage.getEString(), "name", null, 0, 1, HComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getHComponent_Type(), ecorePackage.getEString(), "type", null, 0, 1, HComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getHComponent_Description(), ecorePackage.getEString(), "description", null, 0, 1, HComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		EGenericType g1 = createEGenericType(this.getMap());
		EGenericType g2 = createEGenericType(ecorePackage.getEString());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEString());
		g1.getETypeArguments().add(g2);
		initEReference(getHComponent_Parameters(), g1, null, "parameters", null, 0, -1, HComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHComponent_HCoordinateSystem3D(), this.getHCoordinateSystem3D(), null, "hCoordinateSystem3D", null, 0, 1, HComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHComponent_HLofts(), this.getHLoft(), this.getHLoft_HComponent(), "hLofts", null, 0, -1, HComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHComponent_HComposition(), this.getHComposition(), this.getHComposition_HComponents(), "hComposition", null, 0, 1, HComponent.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(hCompositionEClass, HComposition.class, "HComposition", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getHComposition_Version(), ecorePackage.getEInt(), "version", null, 0, 1, HComposition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getHComposition_Name(), ecorePackage.getEString(), "name", null, 0, 1, HComposition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getHComposition_Description(), ecorePackage.getEString(), "description", null, 0, 1, HComposition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getHComposition_UnitSystem(), ecorePackage.getEString(), "unitSystem", null, 0, 1, HComposition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHComposition_HCoordinateSystem3D(), this.getHCoordinateSystem3D(), null, "hCoordinateSystem3D", null, 0, 1, HComposition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHComposition_HComponents(), this.getHComponent(), this.getHComponent_HComposition(), "hComponents", null, 0, -1, HComposition.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(hCoordinateSystem3DEClass, HCoordinateSystem3D.class, "HCoordinateSystem3D", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getHCoordinateSystem3D_Origin(), this.getHPoint3D(), null, "origin", null, 0, 1, HCoordinateSystem3D.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHCoordinateSystem3D_HHinges(), this.getHHinge(), this.getHHinge_HCoordinateSystem3D(), "hHinges", null, 0, -1, HCoordinateSystem3D.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(hCoordinateSystem3DAffineEClass, HCoordinateSystem3DAffine.class, "HCoordinateSystem3DAffine", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getHCoordinateSystem3DAffine_X1(), this.getHPoint3D(), null, "x1", null, 0, 1, HCoordinateSystem3DAffine.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHCoordinateSystem3DAffine_X2(), this.getHPoint3D(), null, "x2", null, 0, 1, HCoordinateSystem3DAffine.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHCoordinateSystem3DAffine_X3(), this.getHPoint3D(), null, "x3", null, 0, 1, HCoordinateSystem3DAffine.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(hCoordinateSystem3DCartesianEClass, HCoordinateSystem3DCartesian.class, "HCoordinateSystem3DCartesian", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getHCoordinateSystem3DCartesian_X1(), this.getHPoint3D(), null, "x1", null, 0, 1, HCoordinateSystem3DCartesian.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHCoordinateSystem3DCartesian_E1(), this.getHPoint3D(), null, "e1", null, 0, 1, HCoordinateSystem3DCartesian.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(hCoordinateSystem3DConeEClass, HCoordinateSystem3DCone.class, "HCoordinateSystem3DCone", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getHCoordinateSystem3DCone_AxialVector(), this.getHPoint3D(), null, "axialVector", null, 0, 1, HCoordinateSystem3DCone.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHCoordinateSystem3DCone_RadialVector(), this.getHPoint3D(), null, "radialVector", null, 0, 1, HCoordinateSystem3DCone.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHCoordinateSystem3DCone_Angle(), this.getHPoint3D(), null, "angle", null, 0, 1, HCoordinateSystem3DCone.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(hCoordinateSystem3DPolarEClass, HCoordinateSystem3DPolar.class, "HCoordinateSystem3DPolar", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getHCoordinateSystem3DPolar_AxialVector(), this.getHPoint3D(), null, "axialVector", null, 0, 1, HCoordinateSystem3DPolar.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHCoordinateSystem3DPolar_RadialVector(), this.getHPoint3D(), null, "radialVector", null, 0, 1, HCoordinateSystem3DPolar.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHCoordinateSystem3DPolar_Angle(), this.getHPoint3D(), null, "angle", null, 0, 1, HCoordinateSystem3DPolar.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(hCurve2DEClass, HCurve2D.class, "HCurve2D", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getHCurve2D_HSurfaceIdentifiers(), this.getHSurfaceIdentifier(), null, "hSurfaceIdentifiers", null, 0, -1, HCurve2D.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHCurve2D_HLoftElement(), this.getHLoftElement(), this.getHLoftElement_HCurve2Ds(), "hLoftElement", null, 0, 1, HCurve2D.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(hCurvePoint2DEClass, HCurvePoint2D.class, "HCurvePoint2D", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getHCurvePoint2D_CurveCoordinate(), ecorePackage.getEDouble(), "curveCoordinate", null, 0, 1, HCurvePoint2D.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(hDiscreteCurve2DEClass, HDiscreteCurve2D.class, "HDiscreteCurve2D", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getHDiscreteCurve2D_InterpolationScheme(), ecorePackage.getEString(), "interpolationScheme", null, 0, 1, HDiscreteCurve2D.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHDiscreteCurve2D_CurvePoints(), this.getHCurvePoint2D(), null, "curvePoints", null, 0, -1, HDiscreteCurve2D.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(hDiscreteLoftPathEClass, HDiscreteLoftPath.class, "HDiscreteLoftPath", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getHDiscreteLoftPath_LoftCurvePoints(), this.getHDiscreteLoftPathPoint3D(), null, "loftCurvePoints", null, 0, -1, HDiscreteLoftPath.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getHDiscreteLoftPath_InterpolationScheme(), ecorePackage.getEString(), "interpolationScheme", null, 0, 1, HDiscreteLoftPath.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(hDiscreteLoftPathPoint3DEClass, HDiscreteLoftPathPoint3D.class, "HDiscreteLoftPathPoint3D", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getHDiscreteLoftPathPoint3D_LoftCurvePoint(), this.getHPoint3D(), null, "loftCurvePoint", null, 0, 1, HDiscreteLoftPathPoint3D.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getHDiscreteLoftPathPoint3D_LoftCoordinate(), ecorePackage.getEDouble(), "loftCoordinate", null, 0, 1, HDiscreteLoftPathPoint3D.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHDiscreteLoftPathPoint3D_LoftPathVector(), this.getHPoint3D(), null, "loftPathVector", null, 0, 1, HDiscreteLoftPathPoint3D.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHDiscreteLoftPathPoint3D_LoftPathXi(), this.getHPoint3D(), null, "loftPathXi", null, 0, 1, HDiscreteLoftPathPoint3D.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHDiscreteLoftPathPoint3D_LoftPathEta(), this.getHPoint3D(), null, "loftPathEta", null, 0, 1, HDiscreteLoftPathPoint3D.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(hHingeEClass, HHinge.class, "HHinge", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getHHinge_HingeAxis(), this.getHPoint3D(), null, "hingeAxis", null, 0, 1, HHinge.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(this.getHPair());
		g2 = createEGenericType(ecorePackage.getEString());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEDoubleObject());
		g1.getETypeArguments().add(g2);
		initEReference(getHHinge_HingePositions(), g1, null, "hingePositions", null, 0, -1, HHinge.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getHHinge_HingeLimited(), ecorePackage.getEBoolean(), "hingeLimited", null, 0, 1, HHinge.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHHinge_HCoordinateSystem3D(), this.getHCoordinateSystem3D(), this.getHCoordinateSystem3D_HHinges(), "hCoordinateSystem3D", null, 0, 1, HHinge.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(hLinearLoftPathEClass, HLinearLoftPath.class, "HLinearLoftPath", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getHLinearLoftPath_Origin(), this.getHPoint3D(), null, "origin", null, 0, 1, HLinearLoftPath.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHLinearLoftPath_LoftPathVector(), this.getHPoint3D(), null, "loftPathVector", null, 0, 1, HLinearLoftPath.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHLinearLoftPath_LoftPathXi(), this.getHPoint3D(), null, "loftPathXi", null, 0, 1, HLinearLoftPath.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHLinearLoftPath_LoftPathEta(), this.getHPoint3D(), null, "loftPathEta", null, 0, 1, HLinearLoftPath.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(hLoftEClass, HLoft.class, "HLoft", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getHLoft_Name(), ecorePackage.getEString(), "name", null, 0, 1, HLoft.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getHLoft_Description(), ecorePackage.getEString(), "description", null, 0, 1, HLoft.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(this.getMap());
		g2 = createEGenericType(ecorePackage.getEString());
		g1.getETypeArguments().add(g2);
		g2 = createEGenericType(ecorePackage.getEString());
		g1.getETypeArguments().add(g2);
		initEReference(getHLoft_Parameters(), g1, null, "parameters", null, 0, -1, HLoft.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHLoft_HCoordinateSystems3D(), this.getHCoordinateSystem3D(), null, "hCoordinateSystems3D", null, 0, -1, HLoft.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHLoft_HLoftPath(), this.getHLoftPath(), this.getHLoftPath_HLoft(), "hLoftPath", null, 0, 1, HLoft.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHLoft_HLoftElements(), this.getHLoftElement(), this.getHLoftElement_HLoft(), "hLoftElements", null, 0, -1, HLoft.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHLoft_HSurfaceIdentifications(), this.getHSurfaceIdentifier(), null, "hSurfaceIdentifications", null, 0, -1, HLoft.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHLoft_HComponent(), this.getHComponent(), this.getHComponent_HLofts(), "hComponent", null, 0, 1, HLoft.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(hLoftElementEClass, HLoftElement.class, "HLoftElement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getHLoftElement_LoftPlaneDefiningCoordinateSystem(), this.getHCoordinateSystem3D(), null, "loftPlaneDefiningCoordinateSystem", null, 0, 1, HLoftElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHLoftElement_HCurve2Ds(), this.getHCurve2D(), this.getHCurve2D_HLoftElement(), "hCurve2Ds", null, 0, -1, HLoftElement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHLoftElement_HLoft(), this.getHLoft(), this.getHLoft_HLoftElements(), "hLoft", null, 0, 1, HLoftElement.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(hLoftPathEClass, HLoftPath.class, "HLoftPath", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getHLoftPath_Denominator(), ecorePackage.getEString(), "denominator", null, 0, 1, HLoftPath.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHLoftPath_HLoftCoordinateSystem(), this.getHCoordinateSystem3D(), null, "hLoftCoordinateSystem", null, 0, 1, HLoftPath.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHLoftPath_HLoft(), this.getHLoft(), this.getHLoft_HLoftPath(), "hLoft", null, 0, 1, HLoftPath.class, IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(hPairEClass, HPair.class, "HPair", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		g1 = createEGenericType(hPairEClass_A);
		initEReference(getHPair_A(), g1, null, "a", null, 0, 1, HPair.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		g1 = createEGenericType(hPairEClass_B);
		initEReference(getHPair_B(), g1, null, "b", null, 0, 1, HPair.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(hParentEClass, HParent.class, "HParent", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getHParent_HParent(), this.getHParent(), null, "hParent", null, 0, 1, HParent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(hPoint2DEClass, HPoint2D.class, "HPoint2D", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getHPoint2D_X1(), ecorePackage.getEDouble(), "x1", null, 0, 1, HPoint2D.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getHPoint2D_X2(), ecorePackage.getEDouble(), "x2", null, 0, 1, HPoint2D.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(hPoint3DEClass, HPoint3D.class, "HPoint3D", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getHPoint3D_X1(), ecorePackage.getEDouble(), "x1", null, 0, 1, HPoint3D.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getHPoint3D_X2(), ecorePackage.getEDouble(), "x2", null, 0, 1, HPoint3D.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getHPoint3D_X3(), ecorePackage.getEDouble(), "x3", null, 0, 1, HPoint3D.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(hSeriesProfileCurve2DEClass, HSeriesProfileCurve2D.class, "HSeriesProfileCurve2D", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getHSeriesProfileCurve2D_Denomination(), ecorePackage.getEString(), "denomination", null, 0, 1, HSeriesProfileCurve2D.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getHSeriesProfileCurve2D_Side(), ecorePackage.getEString(), "side", null, 0, 1, HSeriesProfileCurve2D.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getHSeriesProfileCurve2D_Series(), ecorePackage.getEString(), "series", null, 0, 1, HSeriesProfileCurve2D.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(hSurfaceIdentifierEClass, HSurfaceIdentifier.class, "HSurfaceIdentifier", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getHSurfaceIdentifier_Name(), ecorePackage.getEString(), "name", null, 0, 1, HSurfaceIdentifier.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(mapEClass, Map.class, "Map", IS_ABSTRACT, IS_INTERFACE, !IS_GENERATED_INSTANCE_CLASS);

		// Create resource
		createResource(eNS_URI);
	}

} //DatamodelPackageImpl
