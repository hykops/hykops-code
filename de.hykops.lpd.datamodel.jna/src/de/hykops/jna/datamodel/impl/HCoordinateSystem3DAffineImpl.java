/**
 */
package de.hykops.jna.datamodel.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

import com.sun.jna.NativeLong;

import de.hykops.jna.datamodel.factory.HykopsObjectFactory;
import de.hykops.lpd.datamodel.DatamodelPackage;
import de.hykops.lpd.datamodel.HCoordinateSystem3DAffine;
import de.hykops.lpd.datamodel.HPoint3D;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>HCoordinate System3 DAffine</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.hykops.lpd.datamodel.impl.HCoordinateSystem3DAffineImpl#getX1 <em>X1</em>}</li>
 *   <li>{@link de.hykops.lpd.datamodel.impl.HCoordinateSystem3DAffineImpl#getX2 <em>X2</em>}</li>
 *   <li>{@link de.hykops.lpd.datamodel.impl.HCoordinateSystem3DAffineImpl#getX3 <em>X3</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HCoordinateSystem3DAffineImpl extends HCoordinateSystem3DImpl implements HCoordinateSystem3DAffine {
	
	/**
	 * The cached value of the '{@link #getX1() <em>X1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getX1()
	 * @generated
	 * @ordered
	 */
	protected HPoint3D x1;

	/**
	 * The cached value of the '{@link #getX2() <em>X2</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getX2()
	 * @generated
	 * @ordered
	 */
	protected HPoint3D x2;

	/**
	 * The cached value of the '{@link #getX3() <em>X3</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getX3()
	 * @generated
	 * @ordered
	 */
	protected HPoint3D x3;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	
	public HCoordinateSystem3DAffineImpl(NativeLong handle) {
		this.handle = handle;
	}

	public HCoordinateSystem3DAffineImpl() {
		this(CORE.create_object(CLASS_NAME));
	}
	


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DatamodelPackage.Literals.HCOORDINATE_SYSTEM3_DAFFINE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HPoint3D getX1() {
		if (this.x1 == null) {
			NativeLong objHandle = CORE.get_object(getHandle(), PROPERTY_X1);
			if (objHandle.intValue() != 0) {
				this.x1 = (HPoint3D) HykopsObjectFactory.createObject(objHandle);
			}
		}
		return x1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetX1(HPoint3D newX1, NotificationChain msgs) {
		HPoint3D oldX1 = x1;
		x1 = newX1;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DatamodelPackage.HCOORDINATE_SYSTEM3_DAFFINE__X1, oldX1, newX1);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setX1(HPoint3D newX1) {
		CORE.set_object(getHandle(), PROPERTY_X1, newX1.getHandle());
		if (newX1 != x1) {
			NotificationChain msgs = null;
			if (x1 != null)
				msgs = ((InternalEObject)x1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DatamodelPackage.HCOORDINATE_SYSTEM3_DAFFINE__X1, null, msgs);
			if (newX1 != null)
				msgs = ((InternalEObject)newX1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DatamodelPackage.HCOORDINATE_SYSTEM3_DAFFINE__X1, null, msgs);
			msgs = basicSetX1(newX1, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DatamodelPackage.HCOORDINATE_SYSTEM3_DAFFINE__X1, newX1, newX1));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HPoint3D getX2() {
		if (this.x2 == null) {
			NativeLong objHandle = CORE.get_object(getHandle(), PROPERTY_X2);
			if (objHandle.intValue() != 0) {
				this.x2 = (HPoint3D) HykopsObjectFactory.createObject(objHandle);
			}
		}
		return x2;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetX2(HPoint3D newX2, NotificationChain msgs) {
		HPoint3D oldX2 = x2;
		x2 = newX2;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DatamodelPackage.HCOORDINATE_SYSTEM3_DAFFINE__X2, oldX2, newX2);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setX2(HPoint3D newX2) {
		CORE.set_object(getHandle(), PROPERTY_X2, newX2.getHandle());
		if (newX2 != x2) {
			NotificationChain msgs = null;
			if (x2 != null)
				msgs = ((InternalEObject)x2).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DatamodelPackage.HCOORDINATE_SYSTEM3_DAFFINE__X2, null, msgs);
			if (newX2 != null)
				msgs = ((InternalEObject)newX2).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DatamodelPackage.HCOORDINATE_SYSTEM3_DAFFINE__X2, null, msgs);
			msgs = basicSetX2(newX2, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DatamodelPackage.HCOORDINATE_SYSTEM3_DAFFINE__X2, newX2, newX2));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HPoint3D getX3() {
		if (this.x3 == null) {
			NativeLong objHandle = CORE.get_object(getHandle(), PROPERTY_X3);
			if (objHandle.intValue() != 0) {
				this.x3 = (HPoint3D) HykopsObjectFactory.createObject(objHandle);
			}
		}
		return x3;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetX3(HPoint3D newX3, NotificationChain msgs) {
		HPoint3D oldX3 = x3;
		x3 = newX3;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DatamodelPackage.HCOORDINATE_SYSTEM3_DAFFINE__X3, oldX3, newX3);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setX3(HPoint3D newX3) {
		CORE.set_object(getHandle(), PROPERTY_X3, newX3.getHandle());

		if (newX3 != x3) {
			NotificationChain msgs = null;
			if (x3 != null)
				msgs = ((InternalEObject)x3).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DatamodelPackage.HCOORDINATE_SYSTEM3_DAFFINE__X3, null, msgs);
			if (newX3 != null)
				msgs = ((InternalEObject)newX3).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DatamodelPackage.HCOORDINATE_SYSTEM3_DAFFINE__X3, null, msgs);
			msgs = basicSetX3(newX3, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DatamodelPackage.HCOORDINATE_SYSTEM3_DAFFINE__X3, newX3, newX3));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DatamodelPackage.HCOORDINATE_SYSTEM3_DAFFINE__X1:
				return basicSetX1(null, msgs);
			case DatamodelPackage.HCOORDINATE_SYSTEM3_DAFFINE__X2:
				return basicSetX2(null, msgs);
			case DatamodelPackage.HCOORDINATE_SYSTEM3_DAFFINE__X3:
				return basicSetX3(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DatamodelPackage.HCOORDINATE_SYSTEM3_DAFFINE__X1:
				return getX1();
			case DatamodelPackage.HCOORDINATE_SYSTEM3_DAFFINE__X2:
				return getX2();
			case DatamodelPackage.HCOORDINATE_SYSTEM3_DAFFINE__X3:
				return getX3();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DatamodelPackage.HCOORDINATE_SYSTEM3_DAFFINE__X1:
				setX1((HPoint3D)newValue);
				return;
			case DatamodelPackage.HCOORDINATE_SYSTEM3_DAFFINE__X2:
				setX2((HPoint3D)newValue);
				return;
			case DatamodelPackage.HCOORDINATE_SYSTEM3_DAFFINE__X3:
				setX3((HPoint3D)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DatamodelPackage.HCOORDINATE_SYSTEM3_DAFFINE__X1:
				setX1((HPoint3D)null);
				return;
			case DatamodelPackage.HCOORDINATE_SYSTEM3_DAFFINE__X2:
				setX2((HPoint3D)null);
				return;
			case DatamodelPackage.HCOORDINATE_SYSTEM3_DAFFINE__X3:
				setX3((HPoint3D)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DatamodelPackage.HCOORDINATE_SYSTEM3_DAFFINE__X1:
				return x1 != null;
			case DatamodelPackage.HCOORDINATE_SYSTEM3_DAFFINE__X2:
				return x2 != null;
			case DatamodelPackage.HCOORDINATE_SYSTEM3_DAFFINE__X3:
				return x3 != null;
		}
		return super.eIsSet(featureID);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getHHinges() == null) ? 0 : getHHinges().hashCode());
		result = prime * result + ((getOrigin() == null) ? 0 : getOrigin().hashCode());
		result = prime * result + ((getX1() == null) ? 0 : getX1().hashCode());
		result = prime * result + ((getX2() == null) ? 0 : getX2().hashCode());
		result = prime * result + ((getX3() == null) ? 0 : getX3().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HCoordinateSystem3DAffineImpl other = (HCoordinateSystem3DAffineImpl) obj;
		if (getHHinges() == null) {
			if (other.getHHinges() != null)
				return false;
		} else if (!getHHinges().equals(other.getHHinges()))
			return false;
		if (getOrigin() == null) {
			if (other.getOrigin() != null)
				return false;
		} else if (!getOrigin().equals(other.getOrigin()))
			return false;
		if (getX1() == null) {
			if (other.getX1() != null)
				return false;
		} else if (!getX1().equals(other.getX1()))
			return false;
		if (getX2() == null) {
			if (other.getX2() != null)
				return false;
		} else if (!getX2().equals(other.getX2()))
			return false;
		if (getX3() == null) {
			if (other.getX3() != null)
				return false;
		} else if (!getX3().equals(other.getX3()))
			return false;
		return true;
	}

} //HCoordinateSystem3DAffineImpl
