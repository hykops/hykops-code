/**
 */
package de.hykops.jna.datamodel.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.InternalEList;

import com.sun.jna.NativeLong;

import de.hykops.jna.datamodel.core.InteropUtil;
import de.hykops.lpd.datamodel.DatamodelPackage;
import de.hykops.lpd.datamodel.HDiscreteLoftPath;
import de.hykops.lpd.datamodel.HDiscreteLoftPathPoint3D;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>HDiscrete Loft Path</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.hykops.lpd.datamodel.impl.HDiscreteLoftPathImpl#getLoftCurvePoints <em>Loft Curve Points</em>}</li>
 *   <li>{@link de.hykops.lpd.datamodel.impl.HDiscreteLoftPathImpl#getInterpolationScheme <em>Interpolation Scheme</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HDiscreteLoftPathImpl extends HLoftPathImpl implements HDiscreteLoftPath {
	/**
	 * The cached value of the '{@link #getLoftCurvePoints() <em>Loft Curve Points</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLoftCurvePoints()
	 * @generated
	 * @ordered
	 */
	protected EList<HDiscreteLoftPathPoint3D> loftCurvePoints;

	/**
	 * The default value of the '{@link #getInterpolationScheme() <em>Interpolation Scheme</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInterpolationScheme()
	 * @generated
	 * @ordered
	 */
	protected static final String INTERPOLATION_SCHEME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getInterpolationScheme() <em>Interpolation Scheme</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getInterpolationScheme()
	 * @generated
	 * @ordered
	 */
	protected String interpolationScheme = INTERPOLATION_SCHEME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HDiscreteLoftPathImpl() {
		this(CORE.create_object(HDiscreteLoftPath.CLASS_NAME));
	}

	public HDiscreteLoftPathImpl(NativeLong handle) {
		this.handle = handle;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DatamodelPackage.Literals.HDISCRETE_LOFT_PATH;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<HDiscreteLoftPathPoint3D> getLoftCurvePoints() {
		if (loftCurvePoints == null) {
			loftCurvePoints = new InteropUtil<HDiscreteLoftPathPoint3D>(getHandle()).getList(PROPERTY_LOFT_CURVE_POINTS, HDiscreteLoftPathPoint3D.class, this, DatamodelPackage.HDISCRETE_LOFT_PATH__LOFT_CURVE_POINTS, false);
		}
		return loftCurvePoints;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getInterpolationScheme() {
		if (this.interpolationScheme == null) {
			this.interpolationScheme = InteropUtil.getString(getHandle(), PROPERTY_INTERPOLATIONS_SCHEME);
		}
		return interpolationScheme;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInterpolationScheme(String newInterpolationScheme) {
		CORE.set_string(getHandle(), PROPERTY_INTERPOLATIONS_SCHEME, newInterpolationScheme);
		String oldInterpolationScheme = interpolationScheme;
		interpolationScheme = newInterpolationScheme;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DatamodelPackage.HDISCRETE_LOFT_PATH__INTERPOLATION_SCHEME, oldInterpolationScheme, interpolationScheme));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DatamodelPackage.HDISCRETE_LOFT_PATH__LOFT_CURVE_POINTS:
				return ((InternalEList<?>)getLoftCurvePoints()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DatamodelPackage.HDISCRETE_LOFT_PATH__LOFT_CURVE_POINTS:
				return getLoftCurvePoints();
			case DatamodelPackage.HDISCRETE_LOFT_PATH__INTERPOLATION_SCHEME:
				return getInterpolationScheme();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DatamodelPackage.HDISCRETE_LOFT_PATH__LOFT_CURVE_POINTS:
				getLoftCurvePoints().clear();
				getLoftCurvePoints().addAll((Collection<? extends HDiscreteLoftPathPoint3D>)newValue);
				return;
			case DatamodelPackage.HDISCRETE_LOFT_PATH__INTERPOLATION_SCHEME:
				setInterpolationScheme((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DatamodelPackage.HDISCRETE_LOFT_PATH__LOFT_CURVE_POINTS:
				getLoftCurvePoints().clear();
				return;
			case DatamodelPackage.HDISCRETE_LOFT_PATH__INTERPOLATION_SCHEME:
				setInterpolationScheme(INTERPOLATION_SCHEME_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DatamodelPackage.HDISCRETE_LOFT_PATH__LOFT_CURVE_POINTS:
				return loftCurvePoints != null && !loftCurvePoints.isEmpty();
			case DatamodelPackage.HDISCRETE_LOFT_PATH__INTERPOLATION_SCHEME:
				return INTERPOLATION_SCHEME_EDEFAULT == null ? interpolationScheme != null : !INTERPOLATION_SCHEME_EDEFAULT.equals(interpolationScheme);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (interpolationScheme: ");
		result.append(interpolationScheme);
		result.append(')');
		return result.toString();
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getHLoftCoordinateSystem() == null) ? 0 : getHLoftCoordinateSystem().hashCode());
		result = prime * result + ((getDenominator() == null) ? 0 : getDenominator().hashCode());
		result = prime * result + ((getInterpolationScheme() == null) ? 0 : getInterpolationScheme().hashCode());
		result = prime * result + ((getLoftCurvePoints() == null) ? 0 : getLoftCurvePoints().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HDiscreteLoftPathImpl other = (HDiscreteLoftPathImpl) obj;
		if (getHLoftCoordinateSystem() == null) {
			if (other.getHLoftCoordinateSystem() != null)
				return false;
		} else if (!getHLoftCoordinateSystem().equals(other.getHLoftCoordinateSystem()))
			return false;
		if (getDenominator() == null) {
			if (other.getDenominator() != null)
				return false;
		} else if (!getDenominator().equals(other.getDenominator()))
			return false;
		if (getInterpolationScheme() == null) {
			if (other.getInterpolationScheme() != null)
				return false;
		} else if (!getInterpolationScheme().equals(other.getInterpolationScheme()))
			return false;
		if (getLoftCurvePoints() == null) {
			if (other.getLoftCurvePoints() != null)
				return false;
		} else if (!getLoftCurvePoints().equals(other.getLoftCurvePoints()))
			return false;
		return true;
	}

} //HDiscreteLoftPathImpl
