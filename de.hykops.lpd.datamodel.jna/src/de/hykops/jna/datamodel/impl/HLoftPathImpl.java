/**
 */
package de.hykops.jna.datamodel.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;

import com.sun.jna.NativeLong;

import de.hykops.jna.datamodel.core.InteropUtil;
import de.hykops.jna.datamodel.factory.HykopsObjectFactory;
import de.hykops.lpd.datamodel.DatamodelPackage;
import de.hykops.lpd.datamodel.HCoordinateSystem3D;
import de.hykops.lpd.datamodel.HLoft;
import de.hykops.lpd.datamodel.HLoftPath;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>HLoft Path</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.hykops.lpd.datamodel.impl.HLoftPathImpl#getDenominator <em>Denominator</em>}</li>
 *   <li>{@link de.hykops.lpd.datamodel.impl.HLoftPathImpl#getHLoftCoordinateSystem <em>HLoft Coordinate System</em>}</li>
 *   <li>{@link de.hykops.lpd.datamodel.impl.HLoftPathImpl#getHLoft <em>HLoft</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HLoftPathImpl extends HParentImpl implements HLoftPath {
	/**
	 * The default value of the '{@link #getDenominator() <em>Denominator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDenominator()
	 * @generated
	 * @ordered
	 */
	protected static final String DENOMINATOR_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDenominator() <em>Denominator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDenominator()
	 * @generated
	 * @ordered
	 */
	protected String denominator = DENOMINATOR_EDEFAULT;

	/**
	 * The cached value of the '{@link #getHLoftCoordinateSystem() <em>HLoft Coordinate System</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHLoftCoordinateSystem()
	 * @generated
	 * @ordered
	 */
	protected HCoordinateSystem3D hLoftCoordinateSystem;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HLoftPathImpl() {
		this.handle = CORE.create_object(CLASS_NAME);
	}

	public HLoftPathImpl(NativeLong handle) {
		this.handle = handle;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DatamodelPackage.Literals.HLOFT_PATH;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDenominator() {
		if (this.denominator == null) {
			this.denominator = InteropUtil.getString(getHandle(), PROPERTY_DENOMINATOR);
		}
		return denominator;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDenominator(String newDenominator) {
		CORE.set_string(getHandle(), PROPERTY_DENOMINATOR, newDenominator);
		String oldDenominator = denominator;
		denominator = newDenominator;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DatamodelPackage.HLOFT_PATH__DENOMINATOR, oldDenominator, denominator));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HCoordinateSystem3D getHLoftCoordinateSystem() {
		if (this.hLoftCoordinateSystem == null) {
			NativeLong objHandle = CORE.get_object(getHandle(), PROPERTY_LOFT_COORDINATE_SYSTEM);
			if (objHandle.intValue() != 0) {
				this.hLoftCoordinateSystem = (HCoordinateSystem3D) HykopsObjectFactory.createObject(objHandle);
			}
		}
		return hLoftCoordinateSystem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHLoftCoordinateSystem(HCoordinateSystem3D newHLoftCoordinateSystem, NotificationChain msgs) {
		HCoordinateSystem3D oldHLoftCoordinateSystem = hLoftCoordinateSystem;
		hLoftCoordinateSystem = newHLoftCoordinateSystem;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DatamodelPackage.HLOFT_PATH__HLOFT_COORDINATE_SYSTEM, oldHLoftCoordinateSystem, newHLoftCoordinateSystem);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHLoftCoordinateSystem(HCoordinateSystem3D newHLoftCoordinateSystem) {
		CORE.set_object(getHandle(), PROPERTY_LOFT_COORDINATE_SYSTEM, newHLoftCoordinateSystem.getHandle());
		if (newHLoftCoordinateSystem != hLoftCoordinateSystem) {
			NotificationChain msgs = null;
			if (hLoftCoordinateSystem != null)
				msgs = ((InternalEObject)hLoftCoordinateSystem).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DatamodelPackage.HLOFT_PATH__HLOFT_COORDINATE_SYSTEM, null, msgs);
			if (newHLoftCoordinateSystem != null)
				msgs = ((InternalEObject)newHLoftCoordinateSystem).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DatamodelPackage.HLOFT_PATH__HLOFT_COORDINATE_SYSTEM, null, msgs);
			msgs = basicSetHLoftCoordinateSystem(newHLoftCoordinateSystem, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DatamodelPackage.HLOFT_PATH__HLOFT_COORDINATE_SYSTEM, newHLoftCoordinateSystem, newHLoftCoordinateSystem));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HLoft getHLoft() {
		if (eContainerFeatureID() != DatamodelPackage.HLOFT_PATH__HLOFT) return null;
		return (HLoft)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHLoft(HLoft newHLoft, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newHLoft, DatamodelPackage.HLOFT_PATH__HLOFT, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHLoft(HLoft newHLoft) {
		CORE.set_object(getHandle(), PROPERTY_LOFT, newHLoft.getHandle());
		if (newHLoft != eInternalContainer() || (eContainerFeatureID() != DatamodelPackage.HLOFT_PATH__HLOFT && newHLoft != null)) {
			if (EcoreUtil.isAncestor(this, newHLoft))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newHLoft != null)
				msgs = ((InternalEObject)newHLoft).eInverseAdd(this, DatamodelPackage.HLOFT__HLOFT_PATH, HLoft.class, msgs);
			msgs = basicSetHLoft(newHLoft, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DatamodelPackage.HLOFT_PATH__HLOFT, newHLoft, newHLoft));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DatamodelPackage.HLOFT_PATH__HLOFT:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetHLoft((HLoft)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DatamodelPackage.HLOFT_PATH__HLOFT_COORDINATE_SYSTEM:
				return basicSetHLoftCoordinateSystem(null, msgs);
			case DatamodelPackage.HLOFT_PATH__HLOFT:
				return basicSetHLoft(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case DatamodelPackage.HLOFT_PATH__HLOFT:
				return eInternalContainer().eInverseRemove(this, DatamodelPackage.HLOFT__HLOFT_PATH, HLoft.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DatamodelPackage.HLOFT_PATH__DENOMINATOR:
				return getDenominator();
			case DatamodelPackage.HLOFT_PATH__HLOFT_COORDINATE_SYSTEM:
				return getHLoftCoordinateSystem();
			case DatamodelPackage.HLOFT_PATH__HLOFT:
				return getHLoft();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DatamodelPackage.HLOFT_PATH__DENOMINATOR:
				setDenominator((String)newValue);
				return;
			case DatamodelPackage.HLOFT_PATH__HLOFT_COORDINATE_SYSTEM:
				setHLoftCoordinateSystem((HCoordinateSystem3D)newValue);
				return;
			case DatamodelPackage.HLOFT_PATH__HLOFT:
				setHLoft((HLoft)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DatamodelPackage.HLOFT_PATH__DENOMINATOR:
				setDenominator(DENOMINATOR_EDEFAULT);
				return;
			case DatamodelPackage.HLOFT_PATH__HLOFT_COORDINATE_SYSTEM:
				setHLoftCoordinateSystem((HCoordinateSystem3D)null);
				return;
			case DatamodelPackage.HLOFT_PATH__HLOFT:
				setHLoft((HLoft)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DatamodelPackage.HLOFT_PATH__DENOMINATOR:
				return DENOMINATOR_EDEFAULT == null ? denominator != null : !DENOMINATOR_EDEFAULT.equals(denominator);
			case DatamodelPackage.HLOFT_PATH__HLOFT_COORDINATE_SYSTEM:
				return hLoftCoordinateSystem != null;
			case DatamodelPackage.HLOFT_PATH__HLOFT:
				return getHLoft() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (denominator: ");
		result.append(denominator);
		result.append(')');
		return result.toString();
	}
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getDenominator() == null) ? 0 : getDenominator().hashCode());
		result = prime * result + ((getHLoftCoordinateSystem() == null) ? 0 : getHLoftCoordinateSystem().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HLoftPathImpl other = (HLoftPathImpl) obj;
		if (getDenominator() == null) {
			if (other.getDenominator() != null)
				return false;
		} else if (!getDenominator().equals(other.getDenominator()))
			return false;
		if (getHLoft() == null) {
			if (other.getHLoft() != null)
				return false;
		} else if (!getHLoft().equals(other.getHLoft()))
			return false;
		if (getHLoftCoordinateSystem() == null) {
			if (other.getHLoftCoordinateSystem() != null)
				return false;
		} else if (!getHLoftCoordinateSystem().equals(other.getHLoftCoordinateSystem()))
			return false;
		return true;
	}

} //HLoftPathImpl
