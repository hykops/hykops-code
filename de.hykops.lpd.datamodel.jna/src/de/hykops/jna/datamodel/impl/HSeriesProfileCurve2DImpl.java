/**
 */
package de.hykops.jna.datamodel.impl;

import de.hykops.jna.datamodel.core.InteropUtil;
import de.hykops.lpd.datamodel.DatamodelPackage;
import de.hykops.lpd.datamodel.HSeriesProfileCurve2D;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.ecore.EClass;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import com.sun.jna.NativeLong;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>HSeries Profile Curve2 D</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.hykops.lpd.datamodel.impl.HSeriesProfileCurve2DImpl#getDenomination <em>Denomination</em>}</li>
 *   <li>{@link de.hykops.lpd.datamodel.impl.HSeriesProfileCurve2DImpl#getSide <em>Side</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HSeriesProfileCurve2DImpl extends HCurve2DImpl implements HSeriesProfileCurve2D {
	/**
	 * The default value of the '{@link #getDenomination() <em>Denomination</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDenomination()
	 * @generated
	 * @ordered
	 */
	protected static final String DENOMINATION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDenomination() <em>Denomination</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDenomination()
	 * @generated
	 * @ordered
	 */
	protected String denomination = DENOMINATION_EDEFAULT;

	/**
	 * The default value of the '{@link #getSide() <em>Side</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSide()
	 * @generated
	 * @ordered
	 */
	protected static final String SIDE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSide() <em>Side</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSide()
	 * @generated
	 * @ordered
	 */
	protected String side = SIDE_EDEFAULT;

	/**
	 * The default value of the '{@link #getSeries() <em>Series</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSeries()
	 * @generated
	 * @ordered
	 */
	protected static final String SERIES_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getSeries() <em>Series</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSeries()
	 * @generated
	 * @ordered
	 */
	protected String series = SERIES_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HSeriesProfileCurve2DImpl(NativeLong handle) {
		this.handle = handle;
	}

	public HSeriesProfileCurve2DImpl() {
		this(CORE.create_object(HSeriesProfileCurve2D.CLASS_NAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DatamodelPackage.Literals.HSERIES_PROFILE_CURVE2_D;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDenomination() {
		if (this.denomination == null) {
			this.denomination = InteropUtil.getString(getHandle(), PROPERTY_DENOMINATION);
		}
		return denomination;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDenomination(String newDenomination) {
		CORE.set_string(getHandle(), PROPERTY_DENOMINATION, newDenomination);
		String oldDenomination = denomination;
		denomination = newDenomination;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DatamodelPackage.HSERIES_PROFILE_CURVE2_D__DENOMINATION, oldDenomination, denomination));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSide() {
		if (this.side == null) {
			this.side = InteropUtil.getString(getHandle(), PROPERTY_SIDE);
		}
		return side;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSide(String newSide) {
		CORE.set_string(getHandle(), PROPERTY_SIDE, newSide);
		String oldSide = side;
		side = newSide;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DatamodelPackage.HSERIES_PROFILE_CURVE2_D__SIDE, oldSide, side));
	}

	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSeries() {
		if (this.series == null) {
			this.series = InteropUtil.getString(getHandle(), PROPERTY_SERIES);
		}
		return series;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSeries(String newSeries) {
		CORE.set_string(getHandle(), PROPERTY_SERIES, newSeries);
		String oldSeries = series;
		series = newSeries;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DatamodelPackage.HSERIES_PROFILE_CURVE2_D__SERIES, oldSeries, series));
	}

	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DatamodelPackage.HSERIES_PROFILE_CURVE2_D__DENOMINATION:
				return getDenomination();
			case DatamodelPackage.HSERIES_PROFILE_CURVE2_D__SIDE:
				return getSide();
			case DatamodelPackage.HSERIES_PROFILE_CURVE2_D__SERIES:
				return getSeries();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DatamodelPackage.HSERIES_PROFILE_CURVE2_D__DENOMINATION:
				setDenomination((String)newValue);
				return;
			case DatamodelPackage.HSERIES_PROFILE_CURVE2_D__SIDE:
				setSide((String)newValue);
				return;
			case DatamodelPackage.HSERIES_PROFILE_CURVE2_D__SERIES:
				setSeries((String)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DatamodelPackage.HSERIES_PROFILE_CURVE2_D__DENOMINATION:
				setDenomination(DENOMINATION_EDEFAULT);
				return;
			case DatamodelPackage.HSERIES_PROFILE_CURVE2_D__SIDE:
				setSide(SIDE_EDEFAULT);
				return;
			case DatamodelPackage.HSERIES_PROFILE_CURVE2_D__SERIES:
				setSeries(SERIES_EDEFAULT);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DatamodelPackage.HSERIES_PROFILE_CURVE2_D__DENOMINATION:
				return DENOMINATION_EDEFAULT == null ? denomination != null : !DENOMINATION_EDEFAULT.equals(denomination);
			case DatamodelPackage.HSERIES_PROFILE_CURVE2_D__SIDE:
				return SIDE_EDEFAULT == null ? side != null : !SIDE_EDEFAULT.equals(side);
			case DatamodelPackage.HSERIES_PROFILE_CURVE2_D__SERIES:
				return SERIES_EDEFAULT == null ? series != null : !SERIES_EDEFAULT.equals(series);
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (denomination: ");
		result.append(denomination);
		result.append(", side: ");
		result.append(side);
		result.append(", series: ");
		result.append(series);
		result.append(')');
		return result.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getDenomination() == null) ? 0 : getDenomination().hashCode());
		result = prime * result + ((getSide() == null) ? 0 : getSide().hashCode());
		result = prime * result + ((getSeries() == null) ? 0 : getSeries().hashCode());
		result = prime * result + ((getHSurfaceIdentifiers() == null) ? 0 : getHSurfaceIdentifiers().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HSeriesProfileCurve2DImpl other = (HSeriesProfileCurve2DImpl) obj;
		if (getDenomination() == null) {
			if (other.getDenomination() != null)
				return false;
		} else if (!getDenomination().equals(other.getDenomination()))
			return false;
		if (getSide() == null) {
			if (other.getSide() != null)
				return false;
		} else if (!getSide().equals(other.getSide()))
			return false;
		if (getSeries() == null) {
			if (other.getSeries() != null)
				return false;
		} else if (!getSeries().equals(other.getSeries()))
			return false;		
		if (getHSurfaceIdentifiers() == null) {
			if (other.getHSurfaceIdentifiers() != null)
				return false;
		} else if (!getHSurfaceIdentifiers().equals(other.getHSurfaceIdentifiers()))
			return false;
		return true;
	}
	
} //HSeriesProfileCurve2DImpl
