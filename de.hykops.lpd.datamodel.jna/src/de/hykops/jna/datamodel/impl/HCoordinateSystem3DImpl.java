/**
 */
package de.hykops.jna.datamodel.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.InternalEList;

import com.sun.jna.NativeLong;

import de.hykops.jna.datamodel.core.InteropUtil;
import de.hykops.jna.datamodel.factory.HykopsObjectFactory;
import de.hykops.lpd.datamodel.DatamodelPackage;
import de.hykops.lpd.datamodel.HCoordinateSystem3D;
import de.hykops.lpd.datamodel.HHinge;
import de.hykops.lpd.datamodel.HPoint3D;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>HCoordinate System3 D</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.hykops.lpd.datamodel.impl.HCoordinateSystem3DImpl#getOrigin <em>Origin</em>}</li>
 *   <li>{@link de.hykops.lpd.datamodel.impl.HCoordinateSystem3DImpl#getHHinges <em>HHinges</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class HCoordinateSystem3DImpl extends HParentImpl implements HCoordinateSystem3D {
	
	/**
	 * The cached value of the '{@link #getOrigin() <em>Origin</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getOrigin()
	 * @generated
	 * @ordered
	 */
	protected HPoint3D origin;

	/**
	 * The cached value of the '{@link #getHHinges() <em>HHinges</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHHinges()
	 * @generated
	 * @ordered
	 */
	protected EList<HHinge> hHinges;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected HCoordinateSystem3DImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DatamodelPackage.Literals.HCOORDINATE_SYSTEM3_D;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HPoint3D getOrigin() {
		if (this.origin == null) {
			NativeLong objHandle = CORE.get_object(getHandle(), PROPERTY_ORIGIN);
			if (objHandle.intValue() != 0) {
				this.origin = (HPoint3D) HykopsObjectFactory.createObject(objHandle);
			}
		}
		return origin;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetOrigin(HPoint3D newOrigin, NotificationChain msgs) {
		HPoint3D oldOrigin = origin;
		origin = newOrigin;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DatamodelPackage.HCOORDINATE_SYSTEM3_D__ORIGIN, oldOrigin, newOrigin);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOrigin(HPoint3D newOrigin) {
		CORE.set_object(getHandle(), PROPERTY_ORIGIN, newOrigin.getHandle());
		if (newOrigin != origin) {
			NotificationChain msgs = null;
			if (origin != null)
				msgs = ((InternalEObject)origin).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DatamodelPackage.HCOORDINATE_SYSTEM3_D__ORIGIN, null, msgs);
			if (newOrigin != null)
				msgs = ((InternalEObject)newOrigin).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DatamodelPackage.HCOORDINATE_SYSTEM3_D__ORIGIN, null, msgs);
			msgs = basicSetOrigin(newOrigin, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DatamodelPackage.HCOORDINATE_SYSTEM3_D__ORIGIN, newOrigin, newOrigin));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<HHinge> getHHinges() {
		if (hHinges == null) {
			hHinges = new InteropUtil<HHinge>(getHandle()).getList(PROPERTY_HINGES, HHinge.class, this, DatamodelPackage.HCOORDINATE_SYSTEM3_D__HHINGES, DatamodelPackage.HHINGE__HCOORDINATE_SYSTEM3_D);
		}
		return hHinges;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DatamodelPackage.HCOORDINATE_SYSTEM3_D__HHINGES:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getHHinges()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DatamodelPackage.HCOORDINATE_SYSTEM3_D__ORIGIN:
				return basicSetOrigin(null, msgs);
			case DatamodelPackage.HCOORDINATE_SYSTEM3_D__HHINGES:
				return ((InternalEList<?>)getHHinges()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DatamodelPackage.HCOORDINATE_SYSTEM3_D__ORIGIN:
				return getOrigin();
			case DatamodelPackage.HCOORDINATE_SYSTEM3_D__HHINGES:
				return getHHinges();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DatamodelPackage.HCOORDINATE_SYSTEM3_D__ORIGIN:
				setOrigin((HPoint3D)newValue);
				return;
			case DatamodelPackage.HCOORDINATE_SYSTEM3_D__HHINGES:
				getHHinges().clear();
				getHHinges().addAll((Collection<? extends HHinge>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DatamodelPackage.HCOORDINATE_SYSTEM3_D__ORIGIN:
				setOrigin((HPoint3D)null);
				return;
			case DatamodelPackage.HCOORDINATE_SYSTEM3_D__HHINGES:
				getHHinges().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DatamodelPackage.HCOORDINATE_SYSTEM3_D__ORIGIN:
				return origin != null;
			case DatamodelPackage.HCOORDINATE_SYSTEM3_D__HHINGES:
				return hHinges != null && !hHinges.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //HCoordinateSystem3DImpl
