/**
 */
package de.hykops.jna.datamodel.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import com.sun.jna.NativeLong;

import de.hykops.jna.datamodel.core.InteropUtil;
import de.hykops.jna.datamodel.factory.HykopsObjectFactory;
import de.hykops.lpd.datamodel.DatamodelPackage;
import de.hykops.lpd.datamodel.HComponent;
import de.hykops.lpd.datamodel.HCoordinateSystem3D;
import de.hykops.lpd.datamodel.HLoft;
import de.hykops.lpd.datamodel.HLoftElement;
import de.hykops.lpd.datamodel.HLoftPath;
import de.hykops.lpd.datamodel.HSurfaceIdentifier;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>HLoft</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.hykops.lpd.datamodel.impl.HLoftImpl#getName <em>Name</em>}</li>
 *   <li>{@link de.hykops.lpd.datamodel.impl.HLoftImpl#getDescription <em>Description</em>}</li>
 *   <li>{@link de.hykops.lpd.datamodel.impl.HLoftImpl#getParameters <em>Parameters</em>}</li>
 *   <li>{@link de.hykops.lpd.datamodel.impl.HLoftImpl#getHCoordinateSystems3D <em>HCoordinate Systems3 D</em>}</li>
 *   <li>{@link de.hykops.lpd.datamodel.impl.HLoftImpl#getHLoftPath <em>HLoft Path</em>}</li>
 *   <li>{@link de.hykops.lpd.datamodel.impl.HLoftImpl#getHLoftElements <em>HLoft Elements</em>}</li>
 *   <li>{@link de.hykops.lpd.datamodel.impl.HLoftImpl#getHSurfaceIdentifications <em>HSurface Identifications</em>}</li>
 *   <li>{@link de.hykops.lpd.datamodel.impl.HLoftImpl#getHComponent <em>HComponent</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HLoftImpl extends HParentImpl implements HLoft {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The cached value of the '{@link #getParameters() <em>Parameters</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getParameters()
	 * @generated
	 * @ordered
	 */
	protected EMap<String, String> parameters;

	/**
	 * The cached value of the '{@link #getHCoordinateSystems3D() <em>HCoordinate Systems3 D</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHCoordinateSystems3D()
	 * @generated
	 * @ordered
	 */
	protected EList<HCoordinateSystem3D> hCoordinateSystems3D;

	/**
	 * The cached value of the '{@link #getHLoftPath() <em>HLoft Path</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHLoftPath()
	 * @generated
	 * @ordered
	 */
	protected HLoftPath hLoftPath;

	/**
	 * The cached value of the '{@link #getHLoftElements() <em>HLoft Elements</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHLoftElements()
	 * @generated
	 * @ordered
	 */
	protected EList<HLoftElement> hLoftElements;

	/**
	 * The cached value of the '{@link #getHSurfaceIdentifications() <em>HSurface Identifications</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHSurfaceIdentifications()
	 * @generated
	 * @ordered
	 */
	protected EList<HSurfaceIdentifier> hSurfaceIdentifications;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HLoftImpl(NativeLong handle) {
		super();
		this.handle = handle;
	}

	public HLoftImpl() {
		this(CORE.create_object(CLASS_NAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DatamodelPackage.Literals.HLOFT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		if (this.name == null) {
			this.name = InteropUtil.getString(getHandle(), PROPERTY_NAME);
		}
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		CORE.set_string(getHandle(), PROPERTY_NAME, newName);
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DatamodelPackage.HLOFT__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDescription() {
		if (this.description == null) {
			this.description = InteropUtil.getString(getHandle(), PROPERTY_DESCRIPTION);
		}
		return description;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDescription(String newDescription) {
		CORE.set_string(handle, PROPERTY_DESCRIPTION, newDescription);
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DatamodelPackage.HLOFT__DESCRIPTION, oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<String, String> getParameters() {
		if(parameters == null) {
			parameters = new InteropUtil<>(getHandle()).getMap(PROPERTY_PARAMETER, DatamodelPackage.Literals.STRING_TO_STRING_MAP, StringToStringMapImpl.class, this, DatamodelPackage.HLOFT__PARAMETERS);
		}
		return parameters;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetParameters(EMap<String, String> newParameters, NotificationChain msgs) {
		EMap<String, String> oldParameters = parameters;
		parameters = newParameters;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DatamodelPackage.HLOFT__PARAMETERS, oldParameters, newParameters);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<HCoordinateSystem3D> getHCoordinateSystems3D() {
		if (hCoordinateSystems3D == null) {
			hCoordinateSystems3D = new InteropUtil<HCoordinateSystem3D>(getHandle()).getList(PROPERTY_COORDINATE_SYSTEMS, HCoordinateSystem3D.class, this, DatamodelPackage.HLOFT__HCOORDINATE_SYSTEMS3_D, false);
		}
		return hCoordinateSystems3D;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HLoftPath getHLoftPath() {
		if (this.hLoftPath == null) {
			NativeLong objHandle = CORE.get_object(getHandle(), PROPERTY_LOFT_PATH);
			if (objHandle.intValue() != 0) {
				this.hLoftPath = (HLoftPath) HykopsObjectFactory.createObject(objHandle);
			}
		}
		return hLoftPath;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHLoftPath(HLoftPath newHLoftPath, NotificationChain msgs) {
		HLoftPath oldHLoftPath = hLoftPath;
		hLoftPath = newHLoftPath;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DatamodelPackage.HLOFT__HLOFT_PATH, oldHLoftPath, newHLoftPath);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHLoftPath(HLoftPath newHLoftPath) {
		CORE.set_object(getHandle(), PROPERTY_LOFT_PATH, newHLoftPath.getHandle());
		if (newHLoftPath != hLoftPath) {
			NotificationChain msgs = null;
			if (hLoftPath != null)
				msgs = ((InternalEObject)hLoftPath).eInverseRemove(this, DatamodelPackage.HLOFT_PATH__HLOFT, HLoftPath.class, msgs);
			if (newHLoftPath != null)
				msgs = ((InternalEObject)newHLoftPath).eInverseAdd(this, DatamodelPackage.HLOFT_PATH__HLOFT, HLoftPath.class, msgs);
			msgs = basicSetHLoftPath(newHLoftPath, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DatamodelPackage.HLOFT__HLOFT_PATH, newHLoftPath, newHLoftPath));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<HLoftElement> getHLoftElements() {
		if (hLoftElements == null) {
			hLoftElements = new InteropUtil<HLoftElement>(getHandle()).getList(PROPERTY_SURFACE_LOFT_ELEMENTS, HLoftElement.class, this, DatamodelPackage.HLOFT__HLOFT_ELEMENTS, DatamodelPackage.HLOFT_ELEMENT__HLOFT);
		}
		return hLoftElements;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<HSurfaceIdentifier> getHSurfaceIdentifications() {
		if (hSurfaceIdentifications == null) {
			hSurfaceIdentifications = new InteropUtil<HSurfaceIdentifier>(getHandle()).getList(PROPERTY_SURFACE_IDENTIFICATIONS, HSurfaceIdentifier.class, this, DatamodelPackage.HLOFT__HSURFACE_IDENTIFICATIONS, false);
		}
		return hSurfaceIdentifications;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HComponent getHComponent() {
		if (eContainerFeatureID() != DatamodelPackage.HLOFT__HCOMPONENT) return null;
		return (HComponent)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHComponent(HComponent newHComponent, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newHComponent, DatamodelPackage.HLOFT__HCOMPONENT, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHComponent(HComponent newHComponent) {
		CORE.set_object(getHandle(), PROPERTY_COMPONENT, newHComponent.getHandle());
		if (newHComponent != eInternalContainer() || (eContainerFeatureID() != DatamodelPackage.HLOFT__HCOMPONENT && newHComponent != null)) {
			if (EcoreUtil.isAncestor(this, newHComponent))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newHComponent != null)
				msgs = ((InternalEObject)newHComponent).eInverseAdd(this, DatamodelPackage.HCOMPONENT__HLOFTS, HComponent.class, msgs);
			msgs = basicSetHComponent(newHComponent, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DatamodelPackage.HLOFT__HCOMPONENT, newHComponent, newHComponent));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DatamodelPackage.HLOFT__HLOFT_PATH:
				if (hLoftPath != null)
					msgs = ((InternalEObject)hLoftPath).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DatamodelPackage.HLOFT__HLOFT_PATH, null, msgs);
				return basicSetHLoftPath((HLoftPath)otherEnd, msgs);
			case DatamodelPackage.HLOFT__HLOFT_ELEMENTS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getHLoftElements()).basicAdd(otherEnd, msgs);
			case DatamodelPackage.HLOFT__HCOMPONENT:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetHComponent((HComponent)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DatamodelPackage.HLOFT__PARAMETERS:
				return basicSetParameters(null, msgs);
			case DatamodelPackage.HLOFT__HCOORDINATE_SYSTEMS3_D:
				return ((InternalEList<?>)getHCoordinateSystems3D()).basicRemove(otherEnd, msgs);
			case DatamodelPackage.HLOFT__HLOFT_PATH:
				return basicSetHLoftPath(null, msgs);
			case DatamodelPackage.HLOFT__HLOFT_ELEMENTS:
				return ((InternalEList<?>)getHLoftElements()).basicRemove(otherEnd, msgs);
			case DatamodelPackage.HLOFT__HSURFACE_IDENTIFICATIONS:
				return ((InternalEList<?>)getHSurfaceIdentifications()).basicRemove(otherEnd, msgs);
			case DatamodelPackage.HLOFT__HCOMPONENT:
				return basicSetHComponent(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case DatamodelPackage.HLOFT__HCOMPONENT:
				return eInternalContainer().eInverseRemove(this, DatamodelPackage.HCOMPONENT__HLOFTS, HComponent.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DatamodelPackage.HLOFT__NAME:
				return getName();
			case DatamodelPackage.HLOFT__DESCRIPTION:
				return getDescription();
			case DatamodelPackage.HLOFT__PARAMETERS:
				return getParameters();
			case DatamodelPackage.HLOFT__HCOORDINATE_SYSTEMS3_D:
				return getHCoordinateSystems3D();
			case DatamodelPackage.HLOFT__HLOFT_PATH:
				return getHLoftPath();
			case DatamodelPackage.HLOFT__HLOFT_ELEMENTS:
				return getHLoftElements();
			case DatamodelPackage.HLOFT__HSURFACE_IDENTIFICATIONS:
				return getHSurfaceIdentifications();
			case DatamodelPackage.HLOFT__HCOMPONENT:
				return getHComponent();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DatamodelPackage.HLOFT__NAME:
				setName((String)newValue);
				return;
			case DatamodelPackage.HLOFT__DESCRIPTION:
				setDescription((String)newValue);
				return;
			case DatamodelPackage.HLOFT__HCOORDINATE_SYSTEMS3_D:
				getHCoordinateSystems3D().clear();
				getHCoordinateSystems3D().addAll((Collection<? extends HCoordinateSystem3D>)newValue);
				return;
			case DatamodelPackage.HLOFT__HLOFT_PATH:
				setHLoftPath((HLoftPath)newValue);
				return;
			case DatamodelPackage.HLOFT__HLOFT_ELEMENTS:
				getHLoftElements().clear();
				getHLoftElements().addAll((Collection<? extends HLoftElement>)newValue);
				return;
			case DatamodelPackage.HLOFT__HSURFACE_IDENTIFICATIONS:
				getHSurfaceIdentifications().clear();
				getHSurfaceIdentifications().addAll((Collection<? extends HSurfaceIdentifier>)newValue);
				return;
			case DatamodelPackage.HLOFT__HCOMPONENT:
				setHComponent((HComponent)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DatamodelPackage.HLOFT__NAME:
				setName(NAME_EDEFAULT);
				return;
			case DatamodelPackage.HLOFT__DESCRIPTION:
				setDescription(DESCRIPTION_EDEFAULT);
				return;
			case DatamodelPackage.HLOFT__HCOORDINATE_SYSTEMS3_D:
				getHCoordinateSystems3D().clear();
				return;
			case DatamodelPackage.HLOFT__HLOFT_PATH:
				setHLoftPath((HLoftPath)null);
				return;
			case DatamodelPackage.HLOFT__HLOFT_ELEMENTS:
				getHLoftElements().clear();
				return;
			case DatamodelPackage.HLOFT__HSURFACE_IDENTIFICATIONS:
				getHSurfaceIdentifications().clear();
				return;
			case DatamodelPackage.HLOFT__HCOMPONENT:
				setHComponent((HComponent)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DatamodelPackage.HLOFT__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case DatamodelPackage.HLOFT__DESCRIPTION:
				return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
			case DatamodelPackage.HLOFT__PARAMETERS:
				return parameters != null;
			case DatamodelPackage.HLOFT__HCOORDINATE_SYSTEMS3_D:
				return hCoordinateSystems3D != null && !hCoordinateSystems3D.isEmpty();
			case DatamodelPackage.HLOFT__HLOFT_PATH:
				return hLoftPath != null;
			case DatamodelPackage.HLOFT__HLOFT_ELEMENTS:
				return hLoftElements != null && !hLoftElements.isEmpty();
			case DatamodelPackage.HLOFT__HSURFACE_IDENTIFICATIONS:
				return hSurfaceIdentifications != null && !hSurfaceIdentifications.isEmpty();
			case DatamodelPackage.HLOFT__HCOMPONENT:
				return getHComponent() != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(", description: ");
		result.append(description);
		result.append(')');
		return result.toString();
	}
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getHCoordinateSystems3D() == null) ? 0 : getHCoordinateSystems3D().hashCode());
		result = prime * result + ((getDescription() == null) ? 0 : getDescription().hashCode());
		result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HLoftImpl other = (HLoftImpl) obj;
		if (getHCoordinateSystems3D() == null) {
			if (other.getHCoordinateSystems3D() != null)
				return false;
		} else if (!getHCoordinateSystems3D().equals(other.getHCoordinateSystems3D()))
			return false;
		if (getDescription() == null) {
			if (other.getDescription() != null)
				return false;
		} else if (!getDescription().equals(other.getDescription()))
			return false;
		if (getName() == null) {
			if (other.getName() != null)
				return false;
		} else if (!getName().equals(other.getName()))
			return false;
		if (getHLoftPath() == null) {
			if (other.getHLoftPath() != null)
				return false;
		} else if (!getHLoftPath().equals(other.getHLoftPath()))
			return false;
		if (getHLoftElements() == null) {
			if (other.getHLoftElements() != null)
				return false;
		} else if (!getHLoftElements().equals(other.getHLoftElements()))
			return false;
		return true;
	}

} //HLoftImpl
