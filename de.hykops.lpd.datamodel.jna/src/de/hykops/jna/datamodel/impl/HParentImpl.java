/**
 */
package de.hykops.jna.datamodel.impl;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import com.sun.jna.NativeLong;

import de.hykops.jna.datamodel.core.HykopsCore;
import de.hykops.jna.datamodel.factory.HykopsObjectFactory;
import de.hykops.lpd.datamodel.DatamodelPackage;
import de.hykops.lpd.datamodel.HParent;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>HParent</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.hykops.lpd.datamodel.impl.HParentImpl#getHParent
 * <em>HParent</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class HParentImpl extends MinimalEObjectImpl.Container implements HParent {

	protected static HykopsCore CORE = HykopsCore.INSTANCE;

	protected NativeLong handle = new NativeLong(0);

	@Override
	public NativeLong getHandle() {
		return this.handle;
	}
	

	/**
	 * The cached value of the '{@link #getHParent() <em>HParent</em>}'
	 * reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getHParent()
	 * @generated
	 * @ordered
	 */
	protected HParent hParent;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected HParentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DatamodelPackage.Literals.HPARENT;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public HParent getHParent() {
		if (hParent == null) {
			NativeLong objHandle = CORE.get_object(getHandle(), PROPERTY_HPARENT);
			if (objHandle.intValue() != 0) {
				this.hParent = HykopsObjectFactory.createObject(objHandle);
			}
		}
		if (hParent != null && hParent.eIsProxy()) {
			InternalEObject oldHParent = (InternalEObject) hParent;
			hParent = (HParent) eResolveProxy(oldHParent);

			if (hParent != oldHParent) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, DatamodelPackage.HPARENT__HPARENT,
							oldHParent, hParent));
			}
		}
		return hParent;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public HParent basicGetHParent() {
		return hParent;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setHParent(HParent newHParent) {
		CORE.set_object(getHandle(), PROPERTY_HPARENT, newHParent.getHandle());
		HParent oldHParent = hParent;
		hParent = newHParent;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DatamodelPackage.HPARENT__HPARENT, oldHParent,
					hParent));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case DatamodelPackage.HPARENT__HPARENT:
			if (resolve)
				return getHParent();
			return basicGetHParent();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case DatamodelPackage.HPARENT__HPARENT:
			setHParent((HParent) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case DatamodelPackage.HPARENT__HPARENT:
			setHParent((HParent) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case DatamodelPackage.HPARENT__HPARENT:
			return hParent != null;
		}
		return super.eIsSet(featureID);
	}

} // HParentImpl
