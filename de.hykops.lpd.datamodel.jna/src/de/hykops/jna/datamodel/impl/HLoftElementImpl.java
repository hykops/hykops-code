/**
 */
package de.hykops.jna.datamodel.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

import com.sun.jna.NativeLong;

import de.hykops.jna.datamodel.core.InteropUtil;
import de.hykops.jna.datamodel.factory.HykopsObjectFactory;
import de.hykops.lpd.datamodel.DatamodelPackage;
import de.hykops.lpd.datamodel.HCoordinateSystem3D;
import de.hykops.lpd.datamodel.HCurve2D;
import de.hykops.lpd.datamodel.HLoft;
import de.hykops.lpd.datamodel.HLoftElement;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>HLoft Element</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.hykops.lpd.datamodel.impl.HLoftElementImpl#getLoftPlaneDefiningCoordinateSystem <em>Loft Plane Defining Coordinate System</em>}</li>
 *   <li>{@link de.hykops.lpd.datamodel.impl.HLoftElementImpl#getHCurve2Ds <em>HCurve2 Ds</em>}</li>
 *   <li>{@link de.hykops.lpd.datamodel.impl.HLoftElementImpl#getHLoft <em>HLoft</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HLoftElementImpl extends HParentImpl implements HLoftElement {
	/**
	 * The cached value of the '{@link #getLoftPlaneDefiningCoordinateSystem() <em>Loft Plane Defining Coordinate System</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLoftPlaneDefiningCoordinateSystem()
	 * @generated
	 * @ordered
	 */
	protected HCoordinateSystem3D loftPlaneDefiningCoordinateSystem;

	/**
	 * The cached value of the '{@link #getHCurve2Ds() <em>HCurve2 Ds</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getHCurve2Ds()
	 * @generated
	 * @ordered
	 */
	protected EList<HCurve2D> hCurve2Ds;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HLoftElementImpl(NativeLong handle) {
		super();
		this.handle = handle;
	}

	public HLoftElementImpl() {
		this(CORE.create_object(CLASS_NAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DatamodelPackage.Literals.HLOFT_ELEMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HCoordinateSystem3D getLoftPlaneDefiningCoordinateSystem() {
		if (this.loftPlaneDefiningCoordinateSystem == null) {
			NativeLong objHandle = CORE.get_object(getHandle(), PROPERTY_LOFT_PLANE_DEFINING_COORDINATE_SYSTEM);
			if (objHandle.intValue() != 0) {
				this.loftPlaneDefiningCoordinateSystem = (HCoordinateSystem3D) HykopsObjectFactory.createObject(objHandle);
			}
		}
		return loftPlaneDefiningCoordinateSystem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetLoftPlaneDefiningCoordinateSystem(HCoordinateSystem3D newLoftPlaneDefiningCoordinateSystem, NotificationChain msgs) {
		HCoordinateSystem3D oldLoftPlaneDefiningCoordinateSystem = loftPlaneDefiningCoordinateSystem;
		loftPlaneDefiningCoordinateSystem = newLoftPlaneDefiningCoordinateSystem;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DatamodelPackage.HLOFT_ELEMENT__LOFT_PLANE_DEFINING_COORDINATE_SYSTEM, oldLoftPlaneDefiningCoordinateSystem, newLoftPlaneDefiningCoordinateSystem);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLoftPlaneDefiningCoordinateSystem(HCoordinateSystem3D newLoftPlaneDefiningCoordinateSystem) {
		CORE.set_object(getHandle(), PROPERTY_LOFT_PLANE_DEFINING_COORDINATE_SYSTEM, newLoftPlaneDefiningCoordinateSystem.getHandle());
		if (newLoftPlaneDefiningCoordinateSystem != loftPlaneDefiningCoordinateSystem) {
			NotificationChain msgs = null;
			if (loftPlaneDefiningCoordinateSystem != null)
				msgs = ((InternalEObject)loftPlaneDefiningCoordinateSystem).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DatamodelPackage.HLOFT_ELEMENT__LOFT_PLANE_DEFINING_COORDINATE_SYSTEM, null, msgs);
			if (newLoftPlaneDefiningCoordinateSystem != null)
				msgs = ((InternalEObject)newLoftPlaneDefiningCoordinateSystem).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DatamodelPackage.HLOFT_ELEMENT__LOFT_PLANE_DEFINING_COORDINATE_SYSTEM, null, msgs);
			msgs = basicSetLoftPlaneDefiningCoordinateSystem(newLoftPlaneDefiningCoordinateSystem, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DatamodelPackage.HLOFT_ELEMENT__LOFT_PLANE_DEFINING_COORDINATE_SYSTEM, newLoftPlaneDefiningCoordinateSystem, newLoftPlaneDefiningCoordinateSystem));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<HCurve2D> getHCurve2Ds() {
		if (hCurve2Ds == null) {
			hCurve2Ds = new InteropUtil<HCurve2D>(getHandle()).getList(PROPERTY_CURVES_2D, HCurve2D.class, this, DatamodelPackage.HLOFT_ELEMENT__HCURVE2_DS, DatamodelPackage.HCURVE2_D__HLOFT_ELEMENT);
		}
		return hCurve2Ds;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HLoft getHLoft() {
		if (eContainerFeatureID() != DatamodelPackage.HLOFT_ELEMENT__HLOFT) return null;
		return (HLoft)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetHLoft(HLoft newHLoft, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newHLoft, DatamodelPackage.HLOFT_ELEMENT__HLOFT, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHLoft(HLoft newHLoft) {
		CORE.set_object(getHandle(), PROPERTY_LOFT, newHLoft.getHandle());
		if (newHLoft != eInternalContainer() || (eContainerFeatureID() != DatamodelPackage.HLOFT_ELEMENT__HLOFT && newHLoft != null)) {
			if (EcoreUtil.isAncestor(this, newHLoft))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newHLoft != null)
				msgs = ((InternalEObject)newHLoft).eInverseAdd(this, DatamodelPackage.HLOFT__HLOFT_ELEMENTS, HLoft.class, msgs);
			msgs = basicSetHLoft(newHLoft, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DatamodelPackage.HLOFT_ELEMENT__HLOFT, newHLoft, newHLoft));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DatamodelPackage.HLOFT_ELEMENT__HCURVE2_DS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getHCurve2Ds()).basicAdd(otherEnd, msgs);
			case DatamodelPackage.HLOFT_ELEMENT__HLOFT:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetHLoft((HLoft)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DatamodelPackage.HLOFT_ELEMENT__LOFT_PLANE_DEFINING_COORDINATE_SYSTEM:
				return basicSetLoftPlaneDefiningCoordinateSystem(null, msgs);
			case DatamodelPackage.HLOFT_ELEMENT__HCURVE2_DS:
				return ((InternalEList<?>)getHCurve2Ds()).basicRemove(otherEnd, msgs);
			case DatamodelPackage.HLOFT_ELEMENT__HLOFT:
				return basicSetHLoft(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case DatamodelPackage.HLOFT_ELEMENT__HLOFT:
				return eInternalContainer().eInverseRemove(this, DatamodelPackage.HLOFT__HLOFT_ELEMENTS, HLoft.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DatamodelPackage.HLOFT_ELEMENT__LOFT_PLANE_DEFINING_COORDINATE_SYSTEM:
				return getLoftPlaneDefiningCoordinateSystem();
			case DatamodelPackage.HLOFT_ELEMENT__HCURVE2_DS:
				return getHCurve2Ds();
			case DatamodelPackage.HLOFT_ELEMENT__HLOFT:
				return getHLoft();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DatamodelPackage.HLOFT_ELEMENT__LOFT_PLANE_DEFINING_COORDINATE_SYSTEM:
				setLoftPlaneDefiningCoordinateSystem((HCoordinateSystem3D)newValue);
				return;
			case DatamodelPackage.HLOFT_ELEMENT__HCURVE2_DS:
				getHCurve2Ds().clear();
				getHCurve2Ds().addAll((Collection<? extends HCurve2D>)newValue);
				return;
			case DatamodelPackage.HLOFT_ELEMENT__HLOFT:
				setHLoft((HLoft)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DatamodelPackage.HLOFT_ELEMENT__LOFT_PLANE_DEFINING_COORDINATE_SYSTEM:
				setLoftPlaneDefiningCoordinateSystem((HCoordinateSystem3D)null);
				return;
			case DatamodelPackage.HLOFT_ELEMENT__HCURVE2_DS:
				getHCurve2Ds().clear();
				return;
			case DatamodelPackage.HLOFT_ELEMENT__HLOFT:
				setHLoft((HLoft)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DatamodelPackage.HLOFT_ELEMENT__LOFT_PLANE_DEFINING_COORDINATE_SYSTEM:
				return loftPlaneDefiningCoordinateSystem != null;
			case DatamodelPackage.HLOFT_ELEMENT__HCURVE2_DS:
				return hCurve2Ds != null && !hCurve2Ds.isEmpty();
			case DatamodelPackage.HLOFT_ELEMENT__HLOFT:
				return getHLoft() != null;
		}
		return super.eIsSet(featureID);
	}
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getLoftPlaneDefiningCoordinateSystem() == null) ? 0 : getLoftPlaneDefiningCoordinateSystem().hashCode());
		result = prime * result + ((getHCurve2Ds() == null) ? 0 : getHCurve2Ds().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HLoftElementImpl other = (HLoftElementImpl) obj;
		if (getLoftPlaneDefiningCoordinateSystem() == null) {
			if (other.getLoftPlaneDefiningCoordinateSystem() != null)
				return false;
		} else if (!getLoftPlaneDefiningCoordinateSystem().equals(other.getLoftPlaneDefiningCoordinateSystem()))
			return false;
		if (getHCurve2Ds() == null) {
			if (other.getHCurve2Ds() != null)
				return false;
		} else if (!getHCurve2Ds().equals(other.getHCurve2Ds()))
			return false;
		return true;
	}

} //HLoftElementImpl
