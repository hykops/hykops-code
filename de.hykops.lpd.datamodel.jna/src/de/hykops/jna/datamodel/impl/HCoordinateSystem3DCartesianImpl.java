/**
 */
package de.hykops.jna.datamodel.impl;

import de.hykops.jna.datamodel.factory.HykopsObjectFactory;
import de.hykops.lpd.datamodel.DatamodelPackage;
import de.hykops.lpd.datamodel.HCoordinateSystem3DCartesian;
import de.hykops.lpd.datamodel.HPoint3D;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import com.sun.jna.NativeLong;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>HCoordinate System3 DCartesian</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link de.hykops.lpd.datamodel.impl.HCoordinateSystem3DCartesianImpl#getX1 <em>X1</em>}</li>
 *   <li>{@link de.hykops.lpd.datamodel.impl.HCoordinateSystem3DCartesianImpl#getE1 <em>E1</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HCoordinateSystem3DCartesianImpl extends HCoordinateSystem3DImpl implements HCoordinateSystem3DCartesian {	
	/**
	 * The cached value of the '{@link #getX1() <em>X1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getX1()
	 * @generated
	 * @ordered
	 */
	protected HPoint3D x1;

	/**
	 * The cached value of the '{@link #getE1() <em>E1</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getE1()
	 * @generated
	 * @ordered
	 */
	protected HPoint3D e1;
	
	public HCoordinateSystem3DCartesianImpl(NativeLong handle) {
		this.handle = handle;
	}

	public HCoordinateSystem3DCartesianImpl() {
		this(CORE.create_object(CLASS_NAME));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DatamodelPackage.Literals.HCOORDINATE_SYSTEM3_DCARTESIAN;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HPoint3D getX1() {
		if (this.x1 == null) {
			NativeLong objHandle = CORE.get_object(getHandle(), PROPERTY_X1);
			if (objHandle.intValue() != 0) {
				this.x1 = (HPoint3D) HykopsObjectFactory.createObject(objHandle);
			}
		}
		return x1;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetX1(HPoint3D newX1, NotificationChain msgs) {
		HPoint3D oldX1 = x1;
		x1 = newX1;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DatamodelPackage.HCOORDINATE_SYSTEM3_DCARTESIAN__X1, oldX1, newX1);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setX1(HPoint3D newX1) {
		CORE.set_object(getHandle(), PROPERTY_X1, newX1.getHandle());
		if (newX1 != x1) {
			NotificationChain msgs = null;
			if (x1 != null)
				msgs = ((InternalEObject)x1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DatamodelPackage.HCOORDINATE_SYSTEM3_DCARTESIAN__X1, null, msgs);
			if (newX1 != null)
				msgs = ((InternalEObject)newX1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DatamodelPackage.HCOORDINATE_SYSTEM3_DCARTESIAN__X1, null, msgs);
			msgs = basicSetX1(newX1, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DatamodelPackage.HCOORDINATE_SYSTEM3_DCARTESIAN__X1, newX1, newX1));
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HPoint3D getE1() {
		if (this.e1 == null) {
			NativeLong objHandle = CORE.get_object(getHandle(), PROPERTY_E1);
			if (objHandle.intValue() != 0) {
				this.e1 = (HPoint3D) HykopsObjectFactory.createObject(objHandle);
			}
		}
		return e1;
	}

	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetE1(HPoint3D newE1, NotificationChain msgs) {
		HPoint3D oldE1 = e1;
		e1 = newE1;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, DatamodelPackage.HCOORDINATE_SYSTEM3_DCARTESIAN__E1, oldE1, newE1);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setE1(HPoint3D newE1) {
		CORE.set_object(getHandle(), PROPERTY_E1, newE1.getHandle());

		if (newE1 != e1) {
			NotificationChain msgs = null;
			if (e1 != null)
				msgs = ((InternalEObject)e1).eInverseRemove(this, EOPPOSITE_FEATURE_BASE - DatamodelPackage.HCOORDINATE_SYSTEM3_DCARTESIAN__E1, null, msgs);
			if (newE1 != null)
				msgs = ((InternalEObject)newE1).eInverseAdd(this, EOPPOSITE_FEATURE_BASE - DatamodelPackage.HCOORDINATE_SYSTEM3_DCARTESIAN__E1, null, msgs);
			msgs = basicSetE1(newE1, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DatamodelPackage.HCOORDINATE_SYSTEM3_DCARTESIAN__E1, newE1, newE1));

	}


	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case DatamodelPackage.HCOORDINATE_SYSTEM3_DCARTESIAN__X1:
				return basicSetX1(null, msgs);
			case DatamodelPackage.HCOORDINATE_SYSTEM3_DCARTESIAN__E1:
				return basicSetE1(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case DatamodelPackage.HCOORDINATE_SYSTEM3_DCARTESIAN__X1:
				return getX1();
			case DatamodelPackage.HCOORDINATE_SYSTEM3_DCARTESIAN__E1:
				return getE1();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case DatamodelPackage.HCOORDINATE_SYSTEM3_DCARTESIAN__X1:
				setX1((HPoint3D)newValue);
				return;
			case DatamodelPackage.HCOORDINATE_SYSTEM3_DCARTESIAN__E1:
				setE1((HPoint3D)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case DatamodelPackage.HCOORDINATE_SYSTEM3_DCARTESIAN__X1:
				setX1((HPoint3D)null);
				return;
			case DatamodelPackage.HCOORDINATE_SYSTEM3_DCARTESIAN__E1:
				setE1((HPoint3D)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case DatamodelPackage.HCOORDINATE_SYSTEM3_DCARTESIAN__X1:
				return x1 != null;
			case DatamodelPackage.HCOORDINATE_SYSTEM3_DCARTESIAN__E1:
				return e1 != null;
		}
		return super.eIsSet(featureID);
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getHHinges() == null) ? 0 : getHHinges().hashCode());
		result = prime * result + ((getOrigin() == null) ? 0 : getOrigin().hashCode());
		result = prime * result + ((getX1() == null) ? 0 : getX1().hashCode());
		result = prime * result + ((getE1() == null) ? 0 : getE1().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HCoordinateSystem3DCartesianImpl other = (HCoordinateSystem3DCartesianImpl) obj;
		if (getHHinges() == null) {
			if (other.getHHinges() != null)
				return false;
		} else if (!getHHinges().equals(other.getHHinges()))
			return false;
		if (getOrigin() == null) {
			if (other.getOrigin() != null)
				return false;
		} else if (!getOrigin().equals(other.getOrigin()))
			return false;
		if (getX1() == null) {
			if (other.getX1() != null)
				return false;
		} else if (!getX1().equals(other.getX1()))
			return false;
		if (getE1() == null) {
			if (other.getE1() != null)
				return false;
		} else if (!getE1().equals(other.getE1()))
			return false;
		return true;
	}

} //HCoordinateSystem3DCartesianImpl
