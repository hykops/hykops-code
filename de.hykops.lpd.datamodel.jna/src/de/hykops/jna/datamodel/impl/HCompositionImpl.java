/**
 */
package de.hykops.jna.datamodel.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.util.InternalEList;

import com.sun.jna.NativeLong;

import de.hykops.jna.datamodel.core.InteropUtil;
import de.hykops.jna.datamodel.factory.CoordinateSystemFactory;
import de.hykops.lpd.datamodel.DatamodelPackage;
import de.hykops.lpd.datamodel.HComponent;
import de.hykops.lpd.datamodel.HComposition;
import de.hykops.lpd.datamodel.HCoordinateSystem3D;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>HComposition</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 * <li>{@link de.hykops.lpd.datamodel.impl.HCompositionImpl#getVersion
 * <em>Version</em>}</li>
 * <li>{@link de.hykops.lpd.datamodel.impl.HCompositionImpl#getName
 * <em>Name</em>}</li>
 * <li>{@link de.hykops.lpd.datamodel.impl.HCompositionImpl#getDescription
 * <em>Description</em>}</li>
 * <li>{@link de.hykops.lpd.datamodel.impl.HCompositionImpl#getUnitSystem
 * <em>Unit System</em>}</li>
 * <li>
 * {@link de.hykops.lpd.datamodel.impl.HCompositionImpl#getHCoordinateSystem3D
 * <em>HCoordinate System3 D</em>}</li>
 * <li>{@link de.hykops.lpd.datamodel.impl.HCompositionImpl#getHComponents
 * <em>HComponents</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HCompositionImpl extends HParentImpl implements HComposition {

	/**
	 * The default value of the '{@link #getVersion() <em>Version</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getVersion()
	 * @generated
	 * @ordered
	 */
	protected static final int VERSION_EDEFAULT = 0;

	/**
	 * The cached value of the '{@link #getVersion() <em>Version</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getVersion()
	 * @generated
	 * @ordered
	 */
	protected Integer version = VERSION_EDEFAULT;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getDescription() <em>Description</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected static final String DESCRIPTION_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDescription() <em>Description</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getDescription()
	 * @generated
	 * @ordered
	 */
	protected String description = DESCRIPTION_EDEFAULT;

	/**
	 * The default value of the '{@link #getUnitSystem() <em>Unit System</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getUnitSystem()
	 * @generated
	 * @ordered
	 */
	protected static final String UNIT_SYSTEM_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getUnitSystem() <em>Unit System</em>}'
	 * attribute. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getUnitSystem()
	 * @generated
	 * @ordered
	 */
	protected String unitSystem = UNIT_SYSTEM_EDEFAULT;

	/**
	 * The cached value of the '{@link #getHCoordinateSystem3D()
	 * <em>HCoordinate System3 D</em>}' containment reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getHCoordinateSystem3D()
	 * @generated
	 * @ordered
	 */
	protected HCoordinateSystem3D hCoordinateSystem3D;

	/**
	 * The cached value of the '{@link #getHComponents() <em>HComponents</em>}'
	 * containment reference list. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getHComponents()
	 * @generated
	 * @ordered
	 */
	protected EList<HComponent> hComponents;

	public HCompositionImpl(NativeLong handle) {
		super();
		this.handle = handle;
	}

	public HCompositionImpl() {
		this(CORE.create_object(CLASS_NAME));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DatamodelPackage.Literals.HCOMPOSITION;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Integer getVersion() {
		if (this.version == null) {
			this.version = CORE.get_int(getHandle(), PROPERTY_VERSION);
		}
		return version;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setVersion(Integer newVersion) {
		CORE.set_int(getHandle(), PROPERTY_VERSION, newVersion);
		int oldVersion = version;
		version = newVersion;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DatamodelPackage.HCOMPOSITION__VERSION, oldVersion,
					version.intValue()));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getName() {
		if (this.name == null) {
			this.name = InteropUtil.getString(getHandle(), PROPERTY_NAME);
		}
		return name;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setName(String newName) {
		CORE.set_string(getHandle(), PROPERTY_NAME, newName);
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DatamodelPackage.HCOMPOSITION__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getDescription() {
		if (this.description == null) {
			this.description = InteropUtil.getString(getHandle(), PROPERTY_DESCRIPTION);
		}
		return description;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setDescription(String newDescription) {
		CORE.set_string(getHandle(), PROPERTY_DESCRIPTION, newDescription);
		String oldDescription = description;
		description = newDescription;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DatamodelPackage.HCOMPOSITION__DESCRIPTION,
					oldDescription, description));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getUnitSystem() {
		if (this.unitSystem == null) {
			this.unitSystem = InteropUtil.getString(getHandle(), PROPERTY_UNITSYSTEM);
		}
		return unitSystem;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setUnitSystem(String newUnitSystem) {
		CORE.set_string(getHandle(), PROPERTY_UNITSYSTEM, newUnitSystem);
		String oldUnitSystem = unitSystem;
		unitSystem = newUnitSystem;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DatamodelPackage.HCOMPOSITION__UNIT_SYSTEM,
					oldUnitSystem, unitSystem));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public HCoordinateSystem3D getHCoordinateSystem3D() {
		if (this.hCoordinateSystem3D == null) {
			NativeLong objHandle = CORE.get_object(getHandle(), PROPERTY_COORDINATESYSTEM);
			if (objHandle.intValue() != 0) {
				String cosType = InteropUtil.getType(objHandle);
				this.hCoordinateSystem3D = CoordinateSystemFactory.create(cosType, objHandle);
			}
		}
		return hCoordinateSystem3D;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NotificationChain basicSetHCoordinateSystem3D(HCoordinateSystem3D newHCoordinateSystem3D,
			NotificationChain msgs) {
		HCoordinateSystem3D oldHCoordinateSystem3D = hCoordinateSystem3D;
		hCoordinateSystem3D = newHCoordinateSystem3D;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET,
					DatamodelPackage.HCOMPOSITION__HCOORDINATE_SYSTEM3_D, oldHCoordinateSystem3D,
					newHCoordinateSystem3D);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setHCoordinateSystem3D(HCoordinateSystem3D newHCoordinateSystem3D) {
		CORE.set_object(getHandle(), PROPERTY_COORDINATESYSTEM, newHCoordinateSystem3D.getHandle());
		if (newHCoordinateSystem3D != hCoordinateSystem3D) {
			NotificationChain msgs = null;
			if (hCoordinateSystem3D != null)
				msgs = ((InternalEObject) hCoordinateSystem3D).eInverseRemove(this,
						EOPPOSITE_FEATURE_BASE - DatamodelPackage.HCOMPOSITION__HCOORDINATE_SYSTEM3_D, null, msgs);
			if (newHCoordinateSystem3D != null)
				msgs = ((InternalEObject) newHCoordinateSystem3D).eInverseAdd(this,
						EOPPOSITE_FEATURE_BASE - DatamodelPackage.HCOMPOSITION__HCOORDINATE_SYSTEM3_D, null, msgs);
			msgs = basicSetHCoordinateSystem3D(newHCoordinateSystem3D, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, DatamodelPackage.HCOMPOSITION__HCOORDINATE_SYSTEM3_D,
					newHCoordinateSystem3D, newHCoordinateSystem3D));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public EList<HComponent> getHComponents() {
		if (hComponents == null) {
			hComponents = new InteropUtil<HComponent>(getHandle()).getList(PROPERTY_COMPONENTS, HComponent.class, this,
					DatamodelPackage.HCOMPOSITION__HCOMPONENTS, DatamodelPackage.HCOMPONENT__HCOMPOSITION);
		}
		return hComponents;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case DatamodelPackage.HCOMPOSITION__HCOMPONENTS:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getHComponents()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case DatamodelPackage.HCOMPOSITION__HCOORDINATE_SYSTEM3_D:
			return basicSetHCoordinateSystem3D(null, msgs);
		case DatamodelPackage.HCOMPOSITION__HCOMPONENTS:
			return ((InternalEList<?>) getHComponents()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case DatamodelPackage.HCOMPOSITION__VERSION:
			return getVersion();
		case DatamodelPackage.HCOMPOSITION__NAME:
			return getName();
		case DatamodelPackage.HCOMPOSITION__DESCRIPTION:
			return getDescription();
		case DatamodelPackage.HCOMPOSITION__UNIT_SYSTEM:
			return getUnitSystem();
		case DatamodelPackage.HCOMPOSITION__HCOORDINATE_SYSTEM3_D:
			return getHCoordinateSystem3D();
		case DatamodelPackage.HCOMPOSITION__HCOMPONENTS:
			return getHComponents();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case DatamodelPackage.HCOMPOSITION__VERSION:
			setVersion((Integer) newValue);
			return;
		case DatamodelPackage.HCOMPOSITION__NAME:
			setName((String) newValue);
			return;
		case DatamodelPackage.HCOMPOSITION__DESCRIPTION:
			setDescription((String) newValue);
			return;
		case DatamodelPackage.HCOMPOSITION__UNIT_SYSTEM:
			setUnitSystem((String) newValue);
			return;
		case DatamodelPackage.HCOMPOSITION__HCOORDINATE_SYSTEM3_D:
			setHCoordinateSystem3D((HCoordinateSystem3D) newValue);
			return;
		case DatamodelPackage.HCOMPOSITION__HCOMPONENTS:
			getHComponents().clear();
			getHComponents().addAll((Collection<? extends HComponent>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case DatamodelPackage.HCOMPOSITION__VERSION:
			setVersion(VERSION_EDEFAULT);
			return;
		case DatamodelPackage.HCOMPOSITION__NAME:
			setName(NAME_EDEFAULT);
			return;
		case DatamodelPackage.HCOMPOSITION__DESCRIPTION:
			setDescription(DESCRIPTION_EDEFAULT);
			return;
		case DatamodelPackage.HCOMPOSITION__UNIT_SYSTEM:
			setUnitSystem(UNIT_SYSTEM_EDEFAULT);
			return;
		case DatamodelPackage.HCOMPOSITION__HCOORDINATE_SYSTEM3_D:
			setHCoordinateSystem3D((HCoordinateSystem3D) null);
			return;
		case DatamodelPackage.HCOMPOSITION__HCOMPONENTS:
			getHComponents().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case DatamodelPackage.HCOMPOSITION__VERSION:
			return version != VERSION_EDEFAULT;
		case DatamodelPackage.HCOMPOSITION__NAME:
			return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
		case DatamodelPackage.HCOMPOSITION__DESCRIPTION:
			return DESCRIPTION_EDEFAULT == null ? description != null : !DESCRIPTION_EDEFAULT.equals(description);
		case DatamodelPackage.HCOMPOSITION__UNIT_SYSTEM:
			return UNIT_SYSTEM_EDEFAULT == null ? unitSystem != null : !UNIT_SYSTEM_EDEFAULT.equals(unitSystem);
		case DatamodelPackage.HCOMPOSITION__HCOORDINATE_SYSTEM3_D:
			return hCoordinateSystem3D != null;
		case DatamodelPackage.HCOMPOSITION__HCOMPONENTS:
			return hComponents != null && !hComponents.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuffer result = new StringBuffer(super.toString());
		result.append(" (version: ");
		result.append(version);
		result.append(", name: ");
		result.append(name);
		result.append(", description: ");
		result.append(description);
		result.append(", unitSystem: ");
		result.append(unitSystem);
		result.append(')');
		return result.toString();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getHComponents() == null) ? 0 : getHComponents().hashCode());
		result = prime * result + ((getHCoordinateSystem3D() == null) ? 0 : getHCoordinateSystem3D().hashCode());
		result = prime * result + ((getDescription() == null) ? 0 : getDescription().hashCode());
		result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
		result = prime * result + ((getUnitSystem() == null) ? 0 : getUnitSystem().hashCode());
		result = prime * result + ((getVersion() == null) ? 0 : getVersion().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		HCompositionImpl other = (HCompositionImpl) obj;
		if (getHComponents() == null) {
			if (other.getHComponents() != null)
				return false;
		} else if (!getHComponents().equals(other.getHComponents())) {
			return false;
		}
		if (getHCoordinateSystem3D() == null) {
			if (other.getHCoordinateSystem3D() != null)
				return false;
		} else if (!getHCoordinateSystem3D().equals(other.getHCoordinateSystem3D()))
			return false;
		if (getDescription() == null) {
			if (other.getDescription() != null)
				return false;
		} else if (!getDescription().equals(other.getDescription()))
			return false;
		if (getName() == null) {
			if (other.getName() != null)
				return false;
		} else if (!getName().equals(other.getName()))
			return false;
		if (getUnitSystem() == null) {
			if (other.getUnitSystem() != null)
				return false;
		} else if (!getUnitSystem().equals(other.getUnitSystem()))
			return false;
		if (getVersion() == null) {
			if (other.getVersion() != null)
				return false;
		} else if (!getVersion().equals(other.getVersion()))
			return false;
		return true;
	}

} // HCompositionImpl
