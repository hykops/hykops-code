/**
 */
package de.hykops.jna.datamodel.impl;

import de.hykops.lpd.datamodel.*;

import java.util.Map;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class DatamodelFactoryImpl extends EFactoryImpl implements DatamodelFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static DatamodelFactory init() {
		try {
			DatamodelFactory theDatamodelFactory = (DatamodelFactory)EPackage.Registry.INSTANCE.getEFactory(DatamodelPackage.eNS_URI);
			if (theDatamodelFactory != null) {
				return theDatamodelFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new DatamodelFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DatamodelFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {

		switch (eClass.getClassifierID()) {
			case DatamodelPackage.HCOMPONENT: return createHComponent();
			case DatamodelPackage.HCOMPOSITION: return createHComposition();
			case DatamodelPackage.HCOORDINATE_SYSTEM3_DAFFINE: return createHCoordinateSystem3DAffine();
			case DatamodelPackage.HCOORDINATE_SYSTEM3_DCARTESIAN: return createHCoordinateSystem3DCartesian();
			case DatamodelPackage.HCOORDINATE_SYSTEM3_DCONE: return createHCoordinateSystem3DCone();
			case DatamodelPackage.HCOORDINATE_SYSTEM3_DPOLAR: return createHCoordinateSystem3DPolar();
			case DatamodelPackage.HCURVE_POINT2_D: return createHCurvePoint2D();
			case DatamodelPackage.HDISCRETE_CURVE2_D: return createHDiscreteCurve2D();
			case DatamodelPackage.HDISCRETE_LOFT_PATH: return createHDiscreteLoftPath();
			case DatamodelPackage.HDISCRETE_LOFT_PATH_POINT3_D: return createHDiscreteLoftPathPoint3D();
			case DatamodelPackage.HHINGE: return createHHinge();
			case DatamodelPackage.HLINEAR_LOFT_PATH: return createHLinearLoftPath();
			case DatamodelPackage.HLOFT: return createHLoft();
			case DatamodelPackage.HLOFT_ELEMENT: return createHLoftElement();
			case DatamodelPackage.HLOFT_PATH: return createHLoftPath();
			case DatamodelPackage.HPAIR: return createHPair();
			case DatamodelPackage.HPOINT2_D: return createHPoint2D();
			case DatamodelPackage.HPOINT3_D: return createHPoint3D();
			case DatamodelPackage.HSERIES_PROFILE_CURVE2_D: return createHSeriesProfileCurve2D();
			case DatamodelPackage.HSURFACE_IDENTIFIER: return createHSurfaceIdentifier();
			case DatamodelPackage.STRING_TO_STRING_MAP: return (EObject)createStringToStringMap();

			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Map.Entry<String, String> createStringToStringMap() {
		StringToStringMapImpl stringToStringMap = new StringToStringMapImpl();
		return stringToStringMap;
	}
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HComponent createHComponent() {
		HComponentImpl hComponent = new HComponentImpl();
		return hComponent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HComposition createHComposition() {
		HCompositionImpl hComposition = new HCompositionImpl();
		return hComposition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HCoordinateSystem3DAffine createHCoordinateSystem3DAffine() {
		HCoordinateSystem3DAffineImpl hCoordinateSystem3DAffine = new HCoordinateSystem3DAffineImpl();
		return hCoordinateSystem3DAffine;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HCoordinateSystem3DCartesian createHCoordinateSystem3DCartesian() {
		HCoordinateSystem3DCartesianImpl hCoordinateSystem3DCartesian = new HCoordinateSystem3DCartesianImpl();
		return hCoordinateSystem3DCartesian;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HCoordinateSystem3DCone createHCoordinateSystem3DCone() {
		HCoordinateSystem3DConeImpl hCoordinateSystem3DCone = new HCoordinateSystem3DConeImpl();
		return hCoordinateSystem3DCone;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HCoordinateSystem3DPolar createHCoordinateSystem3DPolar() {
		HCoordinateSystem3DPolarImpl hCoordinateSystem3DPolar = new HCoordinateSystem3DPolarImpl();
		return hCoordinateSystem3DPolar;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HCurvePoint2D createHCurvePoint2D() {
		HCurvePoint2DImpl hCurvePoint2D = new HCurvePoint2DImpl();
		return hCurvePoint2D;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HDiscreteCurve2D createHDiscreteCurve2D() {
		HDiscreteCurve2DImpl hDiscreteCurve2D = new HDiscreteCurve2DImpl();
		return hDiscreteCurve2D;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HDiscreteLoftPath createHDiscreteLoftPath() {
		HDiscreteLoftPathImpl hDiscreteLoftPath = new HDiscreteLoftPathImpl();
		return hDiscreteLoftPath;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HDiscreteLoftPathPoint3D createHDiscreteLoftPathPoint3D() {
		HDiscreteLoftPathPoint3DImpl hDiscreteLoftPathPoint3D = new HDiscreteLoftPathPoint3DImpl();
		return hDiscreteLoftPathPoint3D;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HHinge createHHinge() {
		HHingeImpl hHinge = new HHingeImpl();
		return hHinge;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HLinearLoftPath createHLinearLoftPath() {
		HLinearLoftPathImpl hLinearLoftPath = new HLinearLoftPathImpl();
		return hLinearLoftPath;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HLoft createHLoft() {
		HLoftImpl hLoft = new HLoftImpl();
		return hLoft;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HLoftElement createHLoftElement() {
		HLoftElementImpl hLoftElement = new HLoftElementImpl();
		return hLoftElement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HLoftPath createHLoftPath() {
		HLoftPathImpl hLoftPath = new HLoftPathImpl();
		return hLoftPath;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public <A, B> HPair<A, B> createHPair() {
		HPairImpl<A, B> hPair = new HPairImpl<A, B>();
		return hPair;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HPoint2D createHPoint2D() {
		HPoint2DImpl hPoint2D = new HPoint2DImpl();
		return hPoint2D;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HPoint3D createHPoint3D() {
		HPoint3DImpl hPoint3D = new HPoint3DImpl();
		return hPoint3D;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HSeriesProfileCurve2D createHSeriesProfileCurve2D() {
		HSeriesProfileCurve2DImpl hSeriesProfileCurve2D = new HSeriesProfileCurve2DImpl();
		return hSeriesProfileCurve2D;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HSurfaceIdentifier createHSurfaceIdentifier() {
		HSurfaceIdentifierImpl hSurfaceIdentifier = new HSurfaceIdentifierImpl();
		return hSurfaceIdentifier;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DatamodelPackage getDatamodelPackage() {
		return (DatamodelPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static DatamodelPackage getPackage() {
		return DatamodelPackage.eINSTANCE;
	}

} //DatamodelFactoryImpl
