package de.hykops.jna.datamodel.serialize;

import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;

import com.sun.jna.ptr.NativeLongByReference;

import de.fsg.tfe.rde.logging.LogLevel;
import de.fsg.tfe.rde.logging.RDELogger;
import de.hykops.jna.datamodel.Activator;
import de.hykops.jna.datamodel.core.HykopsCore;
import de.hykops.jna.datamodel.impl.HCompositionImpl;
import de.hykops.lpd.core.serialize.IDeserializer;
import de.hykops.lpd.datamodel.HComposition;
import de.hykops.lpd.datamodel.HParent;

public class Deserializer implements IDeserializer {

	private static HykopsCore CORE = HykopsCore.INSTANCE;

	@Override
	public HComposition deserialize(String content) {
		NativeLongByReference handleRef = new NativeLongByReference();
		CORE.deserialize(handleRef, content);

		HComposition composition = new HCompositionImpl(handleRef.getValue());
		init(composition, null);
		System.out.println("Done");
		return composition;
	}

	/**
	 * @Author: Timo Lange
	 * 
	 *          This method invokes all getters of the object/bean to initialize
	 *          them.
	 * 
	 *          This is useful after deserializing because the java object are
	 *          only "lazy-initialized" while syncing with the C++ model.
	 * 
	 *          Without object pre-initializing the EMF transactional system
	 *          would raise errors when drawing the composition due changes on
	 *          the models without write transactions.
	 */
	private void init(Object object, Object root) {
		try {
			for (PropertyDescriptor descriptor : Introspector.getBeanInfo(object.getClass()).getPropertyDescriptors()) {
				Method getter = descriptor.getReadMethod();

				String name = getter.getName();
				// We want to run the object tree downwards, not upwards
				if (name.contains(HParent.class.getSimpleName()) || name.equals("getClass"))
					continue;

				Object getterObj = getter.invoke(object);
				if (getterObj == null)
					continue;

				if (getterObj instanceof EMap<?, ?>)
					continue;

				if (getterObj instanceof HParent) {
					// Otherwise we run into a endless recursion
					if (root != null && root.getClass() == ((HParent) getterObj).getClass())
						continue;

					init(getterObj, object);

				} else if (getterObj instanceof EList<?>) {
					((EList<?>) getterObj).forEach(obj -> init(obj, object));
				}
			}
		} catch (Exception e) {
			RDELogger.log(LogLevel.ERROR, Activator.getContext(), "Init failed.");
		}
	}

}
