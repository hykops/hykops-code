package de.hykops.jna.datamodel.serialize;

import com.sun.jna.Native;
import com.sun.jna.NativeLong;

import de.hykops.jna.datamodel.core.HykopsCore;
import de.hykops.lpd.core.serialize.ISerializer;
import de.hykops.lpd.datamodel.HComposition;

public class Serializer implements ISerializer {

	private static HykopsCore CORE = HykopsCore.INSTANCE;

	@Override
	public String serialize(HComposition composition) {

		NativeLong serializedHandler = CORE.serialize(composition.getHandle());
		int size = CORE.get_serialized_text_size(serializedHandler);

		byte[] buffer = new byte[size + 1];
		CORE.get_serialized_string(serializedHandler, buffer);
		CORE.delete_object(serializedHandler);

		return Native.toString(buffer);
	}

}
