package de.hykops.jna.datamodel;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;

import de.fsg.tfe.rde.logging.LogLevel;
import de.fsg.tfe.rde.logging.RDELogger;
import de.fsg.tfe.rde.ui.support.BundleSupport;
import de.hykops.jna.datamodel.core.HykopsCore;
import de.hykops.jna.datamodel.core.LibraryUtils;

public class Activator implements BundleActivator {


	// Order matters
	private static final String[] LIB_NAMES_WIN = { HykopsCore.LIB_HYKOPS_CORE };
	private static final String[] LIB_NAMES_LINUX = { "hykops_data", "serializeemf", HykopsCore.LIB_HYKOPS_CORE };
	private static final String[] QT_LIB_NAMES = { "Qt5Core", "Qt5Xml", "Qt5Gui" };

	private static BundleContext context;

	public static BundleContext getContext() {
		return context;
	}

	public void start(BundleContext bundleContext) throws Exception {
		Activator.context = bundleContext;
		loadLibraries();
	}

	public void stop(BundleContext bundleContext) throws Exception {
		Activator.context = null;
	}

	private void loadLibraries() {
		try {
			String libDirectory = LibraryUtils.getOSGiLibraryDirectoryName();
			String path = BundleSupport.extractToUserStorage(context.getBundle(), libDirectory);

			loadQtLibraries(path);
			loadHykopsLibraries(path);
			HykopsCore.INSTANCE.init();

		} catch (Exception e) {
			RDELogger.log(LogLevel.ERROR, context, "Failed loading shared libraries: " + e.getMessage());
		}
	}

	private void loadQtLibraries(String path) {
		String os = System.getProperty("os.name").toLowerCase();
		if (os.startsWith("win")) {
			for (String qtLib : QT_LIB_NAMES) {
				System.load(path + LibraryUtils.buildLibName(qtLib));
			}
		}
	}

	private void loadHykopsLibraries(String path) {
		String os = System.getProperty("os.name").toLowerCase();
		String[] libs = os.startsWith("win") ? LIB_NAMES_WIN : LIB_NAMES_LINUX;
		for (String lib : libs) {
			String absPath = path + LibraryUtils.buildLibName(lib);
			RDELogger.log(LogLevel.TRACE, context, "Loading shared library: " + absPath, true, false, true);
			System.load(absPath);
		}
	}

}
