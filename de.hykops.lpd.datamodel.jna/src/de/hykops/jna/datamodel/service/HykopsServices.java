package de.hykops.jna.datamodel.service;

import de.hykops.jna.datamodel.factory.HykopsFactory;
import de.hykops.jna.datamodel.impl.DatamodelFactoryImpl;
import de.hykops.jna.datamodel.impl.DatamodelPackageImpl;
import de.hykops.lpd.core.factory.IHykopsFactory;
import de.hykops.lpd.core.factory.IHykopsFactoryService;
import de.hykops.lpd.datamodel.DatamodelFactory;
import de.hykops.lpd.datamodel.DatamodelPackage;
import de.hykops.lpd.datamodel.IEMFService;

public class HykopsServices implements IHykopsFactoryService, IEMFService {

	@Override
	public IHykopsFactory getFactory() {
		return new HykopsFactory();
	}

	@Override
	public DatamodelFactory getEMFDataModelFactory() {
		return DatamodelFactoryImpl.init();
	}

	@Override
	public DatamodelPackage getEMFDatamodelPackage() {
		return DatamodelPackageImpl.init();
	}

}
