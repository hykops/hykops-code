/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.nozzle.core.sdk;

import de.hykops.lpd.core.sdk.Point3DSupport;
import de.hykops.lpd.datamodel.HComponent;
import de.hykops.lpd.datamodel.HLinearLoftPath;
import de.hykops.lpd.datamodel.HLoftElement;
import de.hykops.lpd.datamodel.HLoftPath;
import de.hykops.lpd.datamodel.HPoint3D;

/**
 * 
 *
 * @author stoye
 * @since Jul 27, 2016
 */
public class NozzleDataSupport {

	@SuppressWarnings("boxing")
	public static double getNozzleRadius(HComponent component) {
		if (component.getHLofts().size() == 1) {
			HLoftPath path = component.getHLofts().get(0).getHLoftPath();
			if (path instanceof HLinearLoftPath) {
				HPoint3D orig = ((HLinearLoftPath) path).getOrigin();
				return orig.getX3();
			}
		}
		return 0.;
	}

	public static void setNozzleRadius(HComponent component, double radius) {
		if (component.getHLofts().size() == 1) {
			HLoftPath path = component.getHLofts().get(0).getHLoftPath();
			if (path instanceof HLinearLoftPath) {
				HPoint3D orig = Point3DSupport.createPoint3D(path, 0., 0., (0. + radius));
				((HLinearLoftPath) path).setOrigin(orig);
			}
		}
	}

	/**
	 * Circumferential averaged angle
	 * 
	 * @param component
	 * @return
	 *
	 * @author stoye
	 * @since Jul 27, 2016
	 */
	public static double getDiffusorAngle(HComponent component) {
		if (component.getHLofts().size() == 1) {
			HLoftPath blade = component.getHLofts().get(0).getHLoftPath();
			if (blade instanceof HLinearLoftPath) {
				{
					int np = component.getHLofts().get(0).getHLoftElements().size();
					double angle = 0.;

					if (np > 0) {
						for (int i = 0; i < np; i++) {
							HLoftElement element = component.getHLofts().get(0).getHLoftElements().get(i);
							angle += ProfileDataSupport.getDiffusorAngle(element);
						}
						angle /= np;
					}
					return angle;

				}
			}
		}
		return 0.;
	}

	public static void setDiffusorAngle(HComponent component, double alpha) {
		double oldAlpha = getDiffusorAngle(component);
		double scale = alpha / oldAlpha;
		for (HLoftElement element : component.getHLofts().get(0).getHLoftElements()) {
			double angle = ProfileDataSupport.getDiffusorAngle(element);
			ProfileDataSupport.setDiffusorAngle(element, (angle * scale));
		}
	}

	public static double getChordlength(HComponent component) {
		if (component.getHLofts().size() == 1) {
			HLoftPath blade = component.getHLofts().get(0).getHLoftPath();
			if (blade instanceof HLinearLoftPath) {
				{
					int np = component.getHLofts().get(0).getHLoftElements().size();
					double chord = 0.;

					if (np > 0) {
						for (int i = 0; i < np; i++) {
							HLoftElement element = component.getHLofts().get(0).getHLoftElements().get(i);
							double l = ProfileDataSupport.getChordlength(element);
							chord += l;
						}
						chord /= np;
					}
					return chord;

				}
			}
		}
		return 0.;
	}

	public static void setChordlength(HComponent component, double chord) {
		double oldChord = getChordlength(component);
		double f = chord / oldChord;
		for (int i = 0; i < component.getHLofts().get(0).getHLoftElements().size(); i++) {
			HLoftElement element = component.getHLofts().get(0).getHLoftElements().get(i);
			double lChord = ProfileDataSupport.getChordlength(element);
			double newChord = lChord * f;
			ProfileDataSupport.setChordlength(element, newChord);
		}
	}


}
