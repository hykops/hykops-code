/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.nozzle.core.sdk;

import de.fsg.tk.math.constants.Epsilon;
import de.hykops.lpd.core.sdk.CoordinateSystem3DSupport;
import de.hykops.lpd.datamodel.HComponent;
import de.hykops.lpd.datamodel.HCoordinateSystem3D;
import de.hykops.lpd.datamodel.HCoordinateSystem3DAffine;
import de.hykops.lpd.datamodel.HLoftElement;
import de.hykops.lpd.datamodel.HPoint3D;

/**
 * 
 *
 * @author stoye
 * @since Dec 1, 2016
 */
public class ProfileDataSupport {
	@SuppressWarnings("boxing")
	public static double getRelRadius(HLoftElement element) {
		HComponent component = element.getHLoft().getHComponent();
		double rNozzle = NozzleDataSupport.getNozzleRadius(component);
		double x2OrigProfile = element.getLoftPlaneDefiningCoordinateSystem().getOrigin().getX2();
		if (Math.abs(rNozzle) > Epsilon.NANO) {
			return (rNozzle + x2OrigProfile) / rNozzle;
		}
		return 1.;
	}

	@SuppressWarnings("boxing")
	public static void setRelRadius(HLoftElement element, double relRadius) {
		HComponent component = element.getHLoft().getHComponent();
		double rNozzle = NozzleDataSupport.getNozzleRadius(component);
		double x2OrigProfile = relRadius * rNozzle - rNozzle;
		element.getLoftPlaneDefiningCoordinateSystem().getOrigin().setX2(x2OrigProfile);
	}

	@SuppressWarnings({ "boxing" })
	public static double getProfileAxialShift(HLoftElement element) {
		double x1OrigProfile = element.getLoftPlaneDefiningCoordinateSystem().getOrigin().getX1();
		return x1OrigProfile;
	}

	@SuppressWarnings("boxing")
	public static void setProfileAxialShift(HLoftElement element, double axialShift) {
		element.getLoftPlaneDefiningCoordinateSystem().getOrigin().setX1(axialShift);
	}

	@SuppressWarnings("boxing")
	public static double getDiffusorAngle(HLoftElement element) {
		HCoordinateSystem3D cos = element.getLoftPlaneDefiningCoordinateSystem();
		if (cos instanceof HCoordinateSystem3DAffine) {
			HPoint3D xi = ((HCoordinateSystem3DAffine) cos).getX1();
			double alpha = Math.atan2(xi.getX2(), xi.getX1());
			return alpha;
		}
		return 0.;
	}

	@SuppressWarnings("boxing")
	public static void setDiffusorAngle(HLoftElement element, double alpha) {
		double chord = getChordlength(element);

		HCoordinateSystem3D cos = element.getLoftPlaneDefiningCoordinateSystem();
		HPoint3D horig = ((HCoordinateSystem3DAffine) cos).getOrigin();

		double xi1 = chord * Math.cos(alpha);
		double xi2 = chord * Math.sin(alpha);
		double xi3 = 0.;

		double z1 = 0.;
		double z2 = 0.;
		double z3 = 1.; // circum direction

		// length of eta: =CHORD!
		double eta[] = de.fsg.tk.math.vec.VecOp.cross(new double[] { xi1, xi2, xi3 }, new double[] { z1, z2, z3 });

		HCoordinateSystem3D planeCos = CoordinateSystem3DSupport.createCoordinateSystem3DAffine(null, horig.getX1(), horig.getX2(), horig.getX3(), xi1, xi2, xi3, eta[0], eta[1], eta[2], z1, z2, z3);
		planeCos.setHParent(element);
		element.setLoftPlaneDefiningCoordinateSystem(planeCos);

	}

	@SuppressWarnings("boxing")
	public static double getChordlength(HLoftElement element) {
		HCoordinateSystem3D cos = element.getLoftPlaneDefiningCoordinateSystem();
		if (cos instanceof HCoordinateSystem3DAffine) {
			HPoint3D xi = ((HCoordinateSystem3DAffine) cos).getX1();
			double chord = Math.sqrt(Math.pow(xi.getX1(), 2.) + Math.pow(xi.getX2(), 2.));
			return chord;
		}
		return 0.;
	}

	@SuppressWarnings("boxing")
	public static void setChordlength(HLoftElement element, double chord) {
		double oldChord = getChordlength(element);
		double f = chord / oldChord;

		HCoordinateSystem3D cos = element.getLoftPlaneDefiningCoordinateSystem();
		HPoint3D horig = ((HCoordinateSystem3DAffine) cos).getOrigin();

		HPoint3D xi = ((HCoordinateSystem3DAffine) cos).getX1();
		double xi1 = xi.getX1() * f;
		double xi2 = xi.getX2() * f;
		double xi3 = xi.getX3() * f;

		HPoint3D eta = ((HCoordinateSystem3DAffine) cos).getX2();
		double eta1 = eta.getX1() * f;
		double eta2 = eta.getX2() * f;
		double eta3 = eta.getX3() * f;

		HPoint3D z = ((HCoordinateSystem3DAffine) cos).getX3();
		double z1 = z.getX1();
		double z2 = z.getX2();
		double z3 = z.getX3();

		HCoordinateSystem3D planeCos = CoordinateSystem3DSupport.createCoordinateSystem3DAffine(null, horig.getX1(), horig.getX2(), horig.getX3(), xi1, xi2, xi3, eta1, eta2, eta3, z1, z2, z3);
		planeCos.setHParent(element);
		element.setLoftPlaneDefiningCoordinateSystem(planeCos);

	}

}
