/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.nozzle.core.sdk;

import de.hykops.lpd.core.Activator;
import de.hykops.lpd.core.factory.IHykopsFactory;
import de.hykops.lpd.core.sdk.CoordinateSystem3DSupport;
import de.hykops.lpd.core.sdk.Point3DSupport;
import de.hykops.lpd.datamodel.HComponent;
import de.hykops.lpd.datamodel.HComposition;
import de.hykops.lpd.datamodel.HCoordinateSystem3D;
import de.hykops.lpd.datamodel.HLinearLoftPath;
import de.hykops.lpd.datamodel.HLoft;
import de.hykops.lpd.datamodel.HPoint3D;
import de.hykops.nozzle.core.TypeID;

/**
 * 
 *
 * @author stoye
 * @since Jul 13, 2016
 */
public class NozzleSupport {

	public static HComponent createEmptyNozzle(String description, HComposition parent, double xref, double yref, double zref, double radius) {

		IHykopsFactory factory = Activator.getFactoryService().getFactory();

		HComponent nozzle = factory.createComponent();
		nozzle.setHParent(parent);
		nozzle.setName("Nozzle");
		nozzle.setType(TypeID.TYPE_ID);
		nozzle.setDescription(description);
		nozzle.setHComposition(parent);
		// Coordinate system of the whole nozzle component: cartesian
		HCoordinateSystem3D cos = CoordinateSystem3DSupport.createCoordinateSystem3DAffine(nozzle, /*-*/
				xref, yref, zref, /*-origin*/
				1., 0., 0., /*-x1 axis*/
				0., 1., 0., /*-x2 axis*/
				0., 0., 1.);/*-x3 axis*/
		nozzle.setHCoordinateSystem3D(cos);

		// create blade loft
		HLoft ring = factory.createLoft();
		nozzle.getHLofts().add(ring);
		ring.setHParent(nozzle);
		ring.setName("nozzle profile loft");
		ring.setDescription("Default nozzle profile loft");
		ring.setHComponent(nozzle);

		HCoordinateSystem3D cos2 = CoordinateSystem3DSupport.createCoordinateSystem3DPolar(ring, /*-*/
				0., 0., 0., /*-origin*/
				1., 0., 0., /*-axial*/
				0., 0., 1. /*-radial*/);
		ring.getHCoordinateSystems3D().add(cos2);
		
		HLinearLoftPath path = factory.createLinearLoftPath();
		path.setHParent(ring);
		path.setHLoft(ring);
		path.setDenominator("NOZZLE_LOFT");
// default loft coordinate system: unit-length no-rotated		
		HCoordinateSystem3D cos3 = CoordinateSystem3DSupport.createCoordinateSystem3DAffine(path, /*-*/
				0., 0., 0., /*-origin*/
				1., 0., 0., /*-x1-axis*/
				0., 1., 0., /*-x2-axis*/
				0., 0., 1.); /*-x3-axis*/
		path.setHLoftCoordinateSystem(cos3);
		

		HPoint3D orig = Point3DSupport.createPoint3D(path, 0., 0., (0. + radius));
		path.setOrigin(orig);
		double circum = (2. * Math.PI);
		HPoint3D axis = Point3DSupport.createPoint3D(path, 0., circum, 0.); /*- circumferential axis*/
		path.setLoftPathVector(axis);

		HPoint3D xi = Point3DSupport.createPoint3D(path, 1., 0., 0.);
		path.setLoftPathXi(xi);
		HPoint3D eta = Point3DSupport.createPoint3D(path, 0., 0., 1.);
		path.setLoftPathEta(eta);

		ring.setHLoftPath(path);

		return nozzle;
	}
}
