/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.rudder.ui.toolbox;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import de.fsg.tk.sdk.values.AbstractPhysicalDoubleValue;
import de.fsg.tk.ui.views.operationToolBox.ConfigurationPage;
import de.fsg.tk.ui.views.operationToolBox.DefaultConfigurationPage;
import de.fsg.tk.ui.widgets.IValueWidget;
import de.fsg.tk.ui.widgets.PhysicalValueWidget;
import de.fsg.tk.ui.widgets.StringValueWidget;
import de.hykops.lpd.datamodel.HComposition;
import de.hykops.lpd.ui.editor.transformations.HYKOPSOperationProvider;
import de.hykops.profile.sdk.ProfileSupport;
import de.hykops.profile.series.impl.NACA_ONE;
import de.hykops.rudder.ui.sdk.AddExampleRudder;

/**
 * 
 *
 * @author stoye
 * @since Sep 6, 2016
 */
@SuppressWarnings("restriction")
public class AddRudder extends HYKOPSOperationProvider {
	private class OperationConfigurationPage extends DefaultConfigurationPage {

		private static final String																DEFAULT_PROFILE_NAME	= "NACA 16-020";
		private static final double																DEFAULT_HEIGHT			= 5.;
		private static final double																DEFAULT_CHORDLENGTH		= 2.5;
		private static final String																DEFAULT_SERIES_NAME		= NACA_ONE.SERIESNAME;

		private String																			seriesName;
		private String																			profileName;
		private double																			height;
		private double																			chordlength;

		private de.fsg.tk.sdk.values.AbstractStringValue<OperationConfigurationPage>			profileValue;
		private de.fsg.tk.sdk.values.AbstractPhysicalDoubleValue<OperationConfigurationPage>	heightValue;
		private de.fsg.tk.sdk.values.AbstractPhysicalDoubleValue<OperationConfigurationPage>	chordLengthValue;

		private StringValueWidget<OperationConfigurationPage>									profileWidget;
		private PhysicalValueWidget<OperationConfigurationPage>									heightWidget;
		private PhysicalValueWidget<OperationConfigurationPage>									chordLengthWidget;

		List<IValueWidget<?>>																	widgets;

		private boolean																			uiInitialized			= false;

		public OperationConfigurationPage() {
		}

		@Override
		public void createControl(Composite parent) {
			this.uiInitialized = false;
			this.widgets = new ArrayList<IValueWidget<?>>();
			Composite composite = new Composite(parent, SWT.NONE);
			composite.setLayout(new GridLayout(1, false));

			this.profileValue = new de.fsg.tk.sdk.values.AbstractStringValue<OperationConfigurationPage>("Profile", "Profile", "Profile", true) {
				@Override
				public String getValue(OperationConfigurationPage model) {
					return model.getProfileName();
				}

				@Override
				public void setValueImpl(OperationConfigurationPage model, String value) {
					if (ProfileSupport.isValidProfile(value)) {
						model.setProfileName(value);
					}
					refresh();
				}
			};
			this.profileWidget = new StringValueWidget<OperationConfigurationPage>(composite, this.profileValue, null);
			this.widgets.add(this.profileWidget);

			this.heightValue = new AbstractPhysicalDoubleValue<OperationConfigurationPage>(de.fsg.tk.sdk.units.Length.meter, "height", "rudder height", "rudder height", true) {

				@Override
				public double getValue(OperationConfigurationPage model) {
					return model.getHeight();
				}

				@Override
				public void setValueImpl(OperationConfigurationPage model, double value) {
					model.setHeight(value);
					refresh();
				}
			};
			this.heightWidget = new PhysicalValueWidget<OperationConfigurationPage>(composite, this.heightValue, null);
			this.widgets.add(this.heightWidget);

			this.chordLengthValue = new AbstractPhysicalDoubleValue<OperationConfigurationPage>(de.fsg.tk.sdk.units.Length.meter, "chord", "chord length", "chord length", true) {

				@Override
				public double getValue(OperationConfigurationPage model) {
					return model.getChordlength();
				}

				@Override
				public void setValueImpl(OperationConfigurationPage model, double value) {
					model.setChordlength(value);
					refresh();
				}
			};
			this.chordLengthWidget = new PhysicalValueWidget<OperationConfigurationPage>(composite, this.chordLengthValue, null);
			this.widgets.add(this.chordLengthWidget);

			composite.layout();
			restoreDefaults();
			this.uiInitialized = true;
			refresh();
		}

		@SuppressWarnings({ "rawtypes", "unchecked" })
		public void refresh() {
			if (this.uiInitialized) {
				for (IValueWidget w : this.widgets) {
					w.setModel(this);
					w.refresh();
				}
			}
		}

		@Override
		public void restoreDefaults() {
			setProfileName(DEFAULT_PROFILE_NAME);
			setHeight(DEFAULT_HEIGHT);
			setChordlength(DEFAULT_CHORDLENGTH);
			setSeriesName(DEFAULT_SERIES_NAME);
			refresh();
		}

		/**
		 * @return the profileName
		 */
		public String getProfileName() {
			return this.profileName;
		}

		/**
		 * @param profileName
		 *            the profileName to set
		 */
		public void setProfileName(String profileName_) {
			this.profileName = profileName_;
		}

		/**
		 * @return the height
		 */
		public double getHeight() {
			return this.height;
		}

		/**
		 * @param height
		 *            the height to set
		 */
		public void setHeight(double height_) {
			this.height = height_;
		}

		/**
		 * @return the chordlength
		 */
		public double getChordlength() {
			return this.chordlength;
		}

		/**
		 * @param chordlength
		 *            the chordlength to set
		 */
		public void setChordlength(double chordlength_) {
			this.chordlength = chordlength_;
		}

		/**
		 * @return the seriesName
		 */
		public String getSeriesName() {
			return this.seriesName;
		}

		/**
		 * @param seriesName the seriesName to set
		 */
		public void setSeriesName(String seriesName_) {
			this.seriesName = seriesName_;
		}
	}

	private OperationConfigurationPage page;

	@Override
	public ConfigurationPage getConfigurationPage() {
		if (this.page == null) {
			this.page = new OperationConfigurationPage();
		}
		return this.page;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hykops.lpd.ui.editor.transformations.HYKOPSOperationProvider#
	 * performImpl(de.hykops.lpd.datamodel.HComposition)
	 */
	@Override
	public void performImpl(HComposition composition) {
		OperationConfigurationPage page_ = (OperationConfigurationPage) getConfigurationPage();
		String series = page_.getSeriesName();
		String profile = page_.getProfileName();
		double height = page_.getHeight();
		double chord = page_.getChordlength();

		AddExampleRudder.createExampleRudder(composition, 0., 0., 0., height, chord, series, profile);
	}

}
