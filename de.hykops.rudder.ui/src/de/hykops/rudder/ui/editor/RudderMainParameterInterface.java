/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.rudder.ui.editor;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.NotificationImpl;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import de.fsg.tk.sdk.values.AbstractPhysicalDoubleValue;
import de.fsg.tk.ui.basic.ColorChooser;
import de.fsg.tk.ui.basic.UniqueColorProvider;
import de.fsg.tk.ui.widgets.BooleanValueWidget;
import de.fsg.tk.ui.widgets.IValueWidget;
import de.fsg.tk.ui.widgets.PhysicalValueWidget;
import de.fsg.tk.ui.widgets.ValueWidgetManager;
import de.hykops.lpd.datamodel.HComponent;
import de.hykops.lpd.datamodel.HLoftElement;
import de.hykops.lpd.ui.editor.CompositionMainDataPage;
import de.hykops.lpd.ui.editor.component.ComponentMainParameterUIInterface;
import de.hykops.lpd.ui.editor.transformations.TransformationHelper;
import de.hykops.rudder.core.TypeID;
import de.hykops.rudder.core.sdk.RudderDataSupport;

/**
 * 
 *
 * @author stoye
 * @since Sep 7, 2016
 */
@SuppressWarnings("restriction")
public class RudderMainParameterInterface extends ComponentMainParameterUIInterface {

	de.fsg.tk.sdk.values.IPhysicalDoubleValue<HComponent>	heightValue;
	PhysicalValueWidget<HComponent>							heightWidget;
	de.fsg.tk.sdk.values.IPhysicalDoubleValue<HComponent>	chordValue;
	PhysicalValueWidget<HComponent>							chordWidget;

	de.fsg.tk.sdk.values.IBooleanValue<HComponent> mirrorYValue;
	BooleanValueWidget<HComponent> mirrorYWidget;

	private ColorChooser									colorChooser;

	public RudderMainParameterInterface(Composite parent_) {
		super(parent_);
	}

	/* (non-Javadoc)
	 * @see de.hykops.lpd.ui.editor.component.IComponentMainParameterUIInterface#getHRComponentType()
	 */
	@Override
	public String getHRComponentType() {
		return TypeID.TYPE;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.hykops.lpd.ui.editor.component.IComponentMainParameterUIInterface#createControl()
	 */
	@Override
	public void createMainParameterControl() {
		Composite rudderComposite = new Composite(this.mainParameterComposite, SWT.BORDER);
		rudderComposite.setLayout(new GridLayout(2, true));
		rudderComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		this.heightValue = new AbstractPhysicalDoubleValue<HComponent>(de.fsg.tk.sdk.units.Length.meter, "H", "Rudder height",
				"Rudder height", true) {
			@Override
			public double getValue(HComponent model) {
				return RudderDataSupport.getRudderHeight(model);
			}

			@Override
			public void setValueImpl(HComponent model, double value) {
				EditingDomain eD = TransformationHelper.getEditingDomain();
				eD.getCommandStack().execute(new RecordingCommand((TransactionalEditingDomain) eD, "Set rudder height") {

					@Override
					protected void doExecute() {
						RudderDataSupport.setRudderHeight(model, value);
						refreshMainParameterComposite();
					}
				});
			}
		};
		this.heightWidget = new PhysicalValueWidget<HComponent>(rudderComposite, this.heightValue, null);
		this.mainParameterWidgets.add(this.heightWidget);

		this.chordValue = new AbstractPhysicalDoubleValue<HComponent>(de.fsg.tk.sdk.units.Length.meter, "C", "Rudder chord length",
				"Rudder chord length", true) {
			@Override
			public double getValue(HComponent model) {
				return RudderDataSupport.getChordlength(model);
			}

			@Override
			public void setValueImpl(HComponent model, double chord) {

				double oldChord = RudderDataSupport.getChordlength(model);
				double f = chord / oldChord;
				for (int i = 0; i < model.getHLofts().get(0).getHLoftElements().size(); i++) {
					HLoftElement element = model.getHLofts().get(0).getHLoftElements().get(i);
					double lChord = RudderDataSupport.getChordlength(element);
					double newChord = lChord * f;
					EditingDomain eD = TransformationHelper.getEditingDomain();
					eD.getCommandStack().execute(new RecordingCommand((TransactionalEditingDomain) eD,
							"Set rudder chord length for profile " + i + "/" + model.getHLofts().get(0).getHLoftElements().size()) {

						@Override
						protected void doExecute() {
							RudderDataSupport.setChordlength(element, newChord);
						}
					});
				}
				refreshMainParameterComposite();
			}
		};
		this.chordWidget = new PhysicalValueWidget<HComponent>(rudderComposite, this.chordValue, null);
		this.mainParameterWidgets.add(this.chordWidget);

		this.mirrorYValue = de.hykops.lpd.ui.dataaccess.ComponentDataAdaptor.getMirrorY();
		this.mirrorYWidget = new BooleanValueWidget<HComponent>(rudderComposite, this.mirrorYValue, null);
		this.mainParameterWidgets.add(this.mirrorYWidget);
		
		this.colorChooser = new ColorChooser(rudderComposite, HComponent.class.toString()) {
			@Override
			protected void refreshAllViews() {
				((EObject) getReference()).eNotify(new NotificationImpl(Notification.NO_FEATURE_ID, 0, 0));
			}
		};

		ValueWidgetManager.align(this.mainParameterWidgets);

		this.mainParameterComposite.pack();
		this.mainParameterComposite.layout();
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void refreshMainParameterComposite() {
		super.refreshMainParameterComposite();
		HComponent model = getModel();
		Color col = UniqueColorProvider.getSWTColor(model, HComponent.class.toString());
		this.colorChooser.setColor(col, model);

		for (IValueWidget<?> w : this.mainParameterWidgets) {
			IValueWidget w2 = w;
			w2.setModel(model);
			w2.refresh();
		}
	}

}
