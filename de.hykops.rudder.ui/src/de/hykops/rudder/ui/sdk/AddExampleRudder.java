/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.rudder.ui.sdk;

import de.hykops.lpd.core.nomenclature.ESurfaceIdentifiers;
import de.hykops.lpd.core.sdk.CoordinateSystem3DSupport;
import de.hykops.lpd.core.sdk.LoftElementSupport;
import de.hykops.lpd.core.sdk.SurfaceIdentifierSupport;
import de.hykops.lpd.datamodel.HComponent;
import de.hykops.lpd.datamodel.HComposition;
import de.hykops.lpd.datamodel.HCoordinateSystem3D;
import de.hykops.lpd.datamodel.HCurve2D;
import de.hykops.lpd.datamodel.HLoft;
import de.hykops.lpd.datamodel.HLoftElement;
import de.hykops.lpd.datamodel.HSurfaceIdentifier;
import de.hykops.profile.sdk.HCurveSupport;
import de.hykops.rudder.core.sdk.RudderSupport;

/**
 * 
 *
 * @author stoye
 * @since Sep 6, 2016
 */
public class AddExampleRudder {

	public static HComponent createExampleRudder(HComposition parent, double xref, double yref, double zref, double height, double chord, String seriesName, String profileName) {
		
		double balance = 0.5;

		HComponent rudder = RudderSupport.createEmptyRudder("Default example rudder", parent, xref, yref, zref, height);
		parent.getHComponents().add(rudder);

		HLoft shaft = rudder.getHLofts().get(0);
		shaft.getHLoftPath();

		for (ESurfaceIdentifiers ei : ESurfaceIdentifiers.values()) {
			HSurfaceIdentifier hident = SurfaceIdentifierSupport.createHSurfaceIdentifier(shaft, ei);
			shaft.getHSurfaceIdentifications().add(hident); // containment
		}

		int np = 20;
		for (int ip = 0; ip < np; ip++) {
			double coord = (((double) ip) / ((double) (np - 1)));

			double origX = 0. + balance * chord;
			double origY = 0.;
			double origZ = coord;

			double xi1 = -chord;
			double xi2 = 0.;
			double xi3 = 0.;

			double z1 = 0.;
			double z2 = 0.;
			double z3 = 1.;

			// length of eta: =CHORD!
			@SuppressWarnings("restriction")
			double eta[] = de.fsg.tk.math.vec.VecOp.cross(new double[] { xi1, xi2, xi3 }, new double[] { z1, z2, z3 });

			HCoordinateSystem3D planeCos = CoordinateSystem3DSupport.createCoordinateSystem3DAffine(null, origX, origY, origZ, xi1, xi2, xi3, eta[0], eta[1], eta[2], z1, z2, z3);
			HLoftElement plane = LoftElementSupport.createLoftElement(shaft, planeCos);
			planeCos.setHParent(plane);

			for (ESurfaceIdentifiers side : ESurfaceIdentifiers.values()) {
				HCurve2D curve = HCurveSupport.createSeriesProfile(plane, seriesName, profileName, side);

				HSurfaceIdentifier ident = SurfaceIdentifierSupport.getSurfaceIdentifierInstance(shaft, side);
				if (ident != null) {
					shaft.getHSurfaceIdentifications().add(ident); // containment
					curve.getHSurfaceIdentifiers().add(ident); // reference
					plane.getHCurve2Ds().add(curve);
				} else {
					System.out.println("No Surface identifier");
				}
			}
		}
		return rudder;
	}
}
