/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.propeller.core.sdk;

import de.fsg.tk.math.constants.Epsilon;
import de.hykops.lpd.core.sdk.CoordinateSystem3DSupport;
import de.hykops.lpd.datamodel.HComponent;
import de.hykops.lpd.datamodel.HCoordinateSystem3D;
import de.hykops.lpd.datamodel.HCoordinateSystem3DAffine;
import de.hykops.lpd.datamodel.HLoftElement;
import de.hykops.lpd.datamodel.HPoint3D;

/**
 * 
 *
 * @author stoye
 * @since Nov 30, 2016
 */
public class ProfileDataSupport {

	public static double getRelRadius(HLoftElement element) {
		HCoordinateSystem3D cos = element.getLoftPlaneDefiningCoordinateSystem();
		if (cos instanceof HCoordinateSystem3DAffine) {
			HPoint3D orig = ((HCoordinateSystem3DAffine) cos).getOrigin();
			return orig.getX3();
		}
		return 0.;
	}

	public static void setRelRadius(HLoftElement element, double relRadius) {
		HCoordinateSystem3D cos = element.getLoftPlaneDefiningCoordinateSystem();
		if (cos instanceof HCoordinateSystem3DAffine) {
			HPoint3D orig = ((HCoordinateSystem3DAffine) cos).getOrigin();
			orig.setX3(relRadius);
		}
	}

	public static double getPitchAngle(HLoftElement element) {
		HCoordinateSystem3D cos = element.getLoftPlaneDefiningCoordinateSystem();
		if (cos instanceof HCoordinateSystem3DAffine) {
			HPoint3D xi = ((HCoordinateSystem3DAffine) cos).getX1();
			double dx = xi.getX1();
			double du = xi.getX2() * getRelRadius(element);
			return Math.atan2(-dx, -du);
		}
		return 0.;
	}

	public static void setPitchAngle(HLoftElement element, double newPitchAngle) {
		double chord = ProfileDataSupport.getRelChordLength(element);
		double pitch = ProfileDataSupport.getPitchAngle(element);
		double rake = ProfileDataSupport.getRelProfileRake(element);
		double chi = ProfileDataSupport.getRelChi(element);
		double relRadius = ProfileDataSupport.getRelRadius(element);
		// TODO: CLOCKWISE identifizieren/persistieren
		boolean clockwise = true;

		pitch = newPitchAngle;

		double[] polarOrig = PropellerSupport.convertPointToPolar3D(/*-*/
				relRadius, /*-*/
				rake, /*-*/
				pitch, /*-*/
				chi, /*-*/
				chord, /*-*/
				0., /*-*/
				0., /*-*/
				clockwise);

		double[] polarXi = PropellerSupport.convertPointToPolar3D(/*-*/
				relRadius, /*-*/
				rake, /*-*/
				pitch, /*-*/
				chi, /*-*/
				chord, /*-*/
				chord, /*-*/
				0., /*-*/
				clockwise);

		double[] polarEta = PropellerSupport.convertPointToPolar3D(/*-*/
				relRadius, /*-*/
				rake, /*-*/
				pitch, /*-*/
				chi, /*-*/
				chord, /*-*/
				0., /*-*/
				-chord, /*-*/
				clockwise);

		// xi = chordline
		double xi[] = de.fsg.tk.math.vec.VecOp.sub(polarXi, polarOrig);
		// eta
		double eta[] = de.fsg.tk.math.vec.VecOp.sub(polarEta, polarOrig);
		// perpendicular vector
		double z1 = 0.;
		double z2 = 0.;
		double z3 = 1.;

		// exchange loft element (2D-)coordinate system
		HCoordinateSystem3D planeCos = CoordinateSystem3DSupport.createCoordinateSystem3DAffine(null, /*-*/
				polarOrig[0], polarOrig[1], polarOrig[2], /*-*/
				xi[0], xi[1], xi[2], /*-*/ //
				eta[0], eta[1], eta[2], /*-*/
				z1, z2, z3);/*-*/
		element.setLoftPlaneDefiningCoordinateSystem(planeCos);
		planeCos.setHParent(element);
	}

	public static double getRelChordLength(HLoftElement element) {
		HCoordinateSystem3D cos = element.getLoftPlaneDefiningCoordinateSystem();
		if (cos instanceof HCoordinateSystem3DAffine) {
			HPoint3D xi = ((HCoordinateSystem3DAffine) cos).getX1();
			double dx = xi.getX1(); // dx in laenge,
			double du = xi.getX2() * getRelRadius(element); // du in radiant!
			double chord = Math.sqrt(Math.pow(dx, 2.) + Math.pow(du, 2.));
			return chord;
		}
		return 0.;
	}

	public static void setRelChordLength(HLoftElement element, double newChord) {
		HCoordinateSystem3D cos = element.getLoftPlaneDefiningCoordinateSystem();
		if (cos instanceof HCoordinateSystem3DAffine) {
			HPoint3D xi = ((HCoordinateSystem3DAffine) cos).getX1();
			HPoint3D eta = ((HCoordinateSystem3DAffine) cos).getX2();
			double dx = xi.getX1();// dx in laenge,
			double du = xi.getX2() * getRelRadius(element);// du in radiant!
			double chord = Math.sqrt(Math.pow(dx, 2.) + Math.pow(du, 2.));
			if (Math.abs(chord) > Epsilon.NANO) {
				double f = newChord / chord;
				xi.setX1(xi.getX1() * f);
				xi.setX2(xi.getX2() * f);
				eta.setX1(eta.getX1() * f);
				eta.setX2(eta.getX2() * f);
			}
		}
	}

	public static double getChordLength(HLoftElement element) {
		HComponent component = element.getHLoft().getHComponent();
		double bladeRadius = PropellerDataSupport.getBladeRadius(component);
		return bladeRadius * getRelChordLength(element);
	}

	public static void setChordLength(HLoftElement element, double newChord) {
		double bladeRadius = PropellerDataSupport.getBladeRadius(element.getHLoft().getHComponent());
		if (Math.abs(bladeRadius) > Epsilon.NANO) {
			setRelChordLength(element, newChord / bladeRadius);
		}
	}

	public static double getRelProfileRake(HLoftElement element) {
		HCoordinateSystem3D cos = element.getLoftPlaneDefiningCoordinateSystem();
		if (cos instanceof HCoordinateSystem3DAffine) {
			HPoint3D orig = ((HCoordinateSystem3DAffine) cos).getOrigin();
			double chi = getRelChi(element);
			double pitch = getPitchAngle(element);
			double xorig = orig.getX1();
			double rake = -xorig + chi * Math.sin(pitch);

			return rake;
		}
		return 0.;
	}

	public static void setRelProfileRake(HLoftElement element, double newRake)
	{
		HCoordinateSystem3D cos = element.getLoftPlaneDefiningCoordinateSystem();
		if (cos instanceof HCoordinateSystem3DAffine) {
			HPoint3D orig = ((HCoordinateSystem3DAffine) cos).getOrigin();
			double chi = getRelChi(element);
			double pitch = getPitchAngle(element);
			
			double newXOrig = chi * Math.sin(pitch)-newRake;
			orig.setX1(newXOrig);
		}
	}
	
	public static double getRake(HLoftElement element) {
		HComponent component = element.getHLoft().getHComponent();
		double bladeRadius = PropellerDataSupport.getBladeRadius(component);
		return bladeRadius * getRelProfileRake(element);
	}

	public static void setRake(HLoftElement element, double newRake) {
		HComponent component = element.getHLoft().getHComponent();
		double bladeRadius = PropellerDataSupport.getBladeRadius(component);
		if (Math.abs(bladeRadius) > Epsilon.NANO) {
			setRelProfileRake(element, newRake / bladeRadius);			
		}		
	}
	
	public static double getRelChi(HLoftElement element) {
		HCoordinateSystem3D cos = element.getLoftPlaneDefiningCoordinateSystem();
		if (cos instanceof HCoordinateSystem3DAffine) {
			HPoint3D orig = ((HCoordinateSystem3DAffine) cos).getOrigin();
			double pitch = getPitchAngle(element);
			double relRadius = getRelRadius(element);
			double chi = relRadius * (orig.getX2()) / Math.cos(pitch);
			return chi;
		}
		return 0.;
	}
	
	public static double getChi(HLoftElement element) {

		HComponent component = element.getHLoft().getHComponent();
		double bladeRadius = PropellerDataSupport.getBladeRadius(component);
		return bladeRadius * getRelChi(element);
	}

	public static double getSkewAngle(HLoftElement element) {
		HCoordinateSystem3D cos = element.getLoftPlaneDefiningCoordinateSystem();
		if (cos instanceof HCoordinateSystem3DAffine) {

			double pitch = getPitchAngle(element);
			double relRadius = getRelRadius(element);
			double chi = getRelChi(element);
			double chord = getRelChordLength(element);
			double rake = getRelProfileRake(element);

			double p[] = PropellerSupport.convertPointToPolar3D(relRadius, rake, pitch, chi, chord, 0.5, 0., true);
			// TODO: Dies stimmt nicht mit der PFF-Ansicht überein!
			return p[1];

		}
		return 0.;
	}

}
