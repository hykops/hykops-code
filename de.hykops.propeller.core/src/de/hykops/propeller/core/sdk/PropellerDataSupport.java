/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.propeller.core.sdk;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.StringTokenizer;

import de.fsg.tk.math.constants.Epsilon;
import de.fsg.tk.math.maps.Pair;
import de.fsg.tk.math.maps.SegmentIterator;
import de.hykops.lpd.core.nomenclature.EMapKeys;
import de.hykops.lpd.core.sdk.CoordinateSystem3DSupport;
import de.hykops.lpd.datamodel.HComponent;
import de.hykops.lpd.datamodel.HCoordinateSystem3D;
import de.hykops.lpd.datamodel.HCoordinateSystem3DAffine;
import de.hykops.lpd.datamodel.HLinearLoftPath;
import de.hykops.lpd.datamodel.HLoft;
import de.hykops.lpd.datamodel.HLoftElement;
import de.hykops.lpd.datamodel.HLoftPath;
import de.hykops.lpd.datamodel.HPoint3D;

/**
 * 
 *
 * @author stoye
 * @since Jul 26, 2016
 */
public class PropellerDataSupport {

	public static double getBladeRadius(HComponent component) {
		if (component.getHLofts().size() == 1) {
			HLoftPath blade = component.getHLofts().get(0).getHLoftPath();
			if (blade instanceof HLinearLoftPath) {
				HPoint3D axis = ((HLinearLoftPath) blade).getLoftPathVector();
				return axis.getX3();
			}
		}
		return 0.;
	}

	public static void setBladeRadius(HComponent component, double radius) {
		if (component.getHLofts().size() == 1) {
			HLoftPath blade = component.getHLofts().get(0).getHLoftPath();
			if (blade instanceof HLinearLoftPath) {
				HPoint3D axis = ((HLinearLoftPath) blade).getLoftPathVector();
				HPoint3D xi = ((HLinearLoftPath) blade).getLoftPathXi();

				axis.setX3(radius);
				xi.setX1(radius);
				/*-eta nicht skalieren: Siehe PropellerSupport.createEmptyPropeller*/

			}
		}
	}

	@SuppressWarnings("boxing")
	public static double getPitchAngle(HComponent component, double relRadius) {
		if (component.getHLofts().size() == 1) {
			HLoftPath blade = component.getHLofts().get(0).getHLoftPath();
			if (blade instanceof HLinearLoftPath) {
				{
					int np = component.getHLofts().get(0).getHLoftElements().size();

					List<Pair<Double, Double>> curve = new ArrayList<Pair<Double, Double>>();

					double x[] = new double[np];
					double y[] = new double[np];
					for (int i = 0; i < np; i++) {
						HLoftElement element = component.getHLofts().get(0).getHLoftElements().get(i);
						x[i] = ProfileDataSupport.getRelRadius(element);
						y[i] = ProfileDataSupport.getPitchAngle(element);
						curve.add(new Pair<Double, Double>(x[i], y[i]));
					}
					Collections.sort(curve, new Comparator<Pair<Double, Double>>() {
						@Override
						public int compare(Pair<Double, Double> o1, Pair<Double, Double> o2) {
							if (o1.getT1() < o2.getT1())
								return -1;
							if (o1.getT1() > o2.getT1())
								return 1;
							return 0;
						}
					});
					// piecewise linear interpolation
					for (SegmentIterator<Pair<Double, Double>> it = new SegmentIterator<Pair<Double, Double>>(curve, false); it.hasNext();) {
						Pair<Pair<Double, Double>, Pair<Double, Double>> p = it.next();
						double r1 = p.getT1().getT1();
						double p1 = p.getT1().getT2();
						double r2 = p.getT2().getT1();
						double p2 = p.getT2().getT2();

						if (relRadius >= r1 && relRadius <= r2) {
							if (Math.abs(r2 - r1) > Epsilon.NANO) {
								double c = (relRadius - r1) / (r2 - r1);
								return (p1 * (1. - c)) + (p2 * c);
							}
							return (p1 + p2) / 2.;

						}
					}

				}
			}
		}
		return 0.;
	}

	public static void setPitchAngle(HComponent component, double relRadius, double newPitch) {
		double oldPitch = getPitchAngle(component, relRadius);
		double scale = newPitch / oldPitch;
		for (HLoftElement element : component.getHLofts().get(0).getHLoftElements()) {
			double profilePitch = ProfileDataSupport.getPitchAngle(element);
			ProfileDataSupport.setPitchAngle(element, (profilePitch * scale));
		}
	}

	public static double getPitchRatio(HComponent component, double relRadius) {
		double phi = getPitchAngle(component, relRadius);
		return 0.7 * Math.PI * Math.tan(phi);
	}

	public static void setPitchRatio(HComponent component, double relRadius, double pdRatio) {
		double d = 2. * getBladeRadius(component);
		double p = pdRatio * d;

		double phi = Math.atan2(p, (0.7 * Math.PI * d));
		setPitchAngle(component, relRadius, phi);
	}

	@SuppressWarnings("boxing")
	public static double getChordLength(HComponent component, double relRadius) {
		if (component.getHLofts().size() == 1) {
			HLoftPath blade = component.getHLofts().get(0).getHLoftPath();
			if (blade instanceof HLinearLoftPath) {
				{
					int np = component.getHLofts().get(0).getHLoftElements().size();

					List<Pair<Double, Double>> curve = new ArrayList<Pair<Double, Double>>();

					double x[] = new double[np];
					double y[] = new double[np];
					for (int i = 0; i < np; i++) {
						HLoftElement element = component.getHLofts().get(0).getHLoftElements().get(i);
						x[i] = ProfileDataSupport.getRelRadius(element);
						y[i] = ProfileDataSupport.getChordLength(element);
						curve.add(new Pair<Double, Double>(x[i], y[i]));
					}
					Collections.sort(curve, new Comparator<Pair<Double, Double>>() {
						@Override
						public int compare(Pair<Double, Double> o1, Pair<Double, Double> o2) {
							if (o1.getT1() < o2.getT1())
								return -1;
							if (o1.getT1() > o2.getT1())
								return 1;
							return 0;
						}
					});
					// piecewise linear interpolation
					for (SegmentIterator<Pair<Double, Double>> it = new SegmentIterator<Pair<Double, Double>>(curve, false); it.hasNext();) {
						Pair<Pair<Double, Double>, Pair<Double, Double>> p = it.next();
						double r1 = p.getT1().getT1();
						double c1 = p.getT1().getT2();
						double r2 = p.getT2().getT1();
						double c2 = p.getT2().getT2();
						if (relRadius >= r1 && relRadius <= r2) {
							if (Math.abs(r2 - r1) > Epsilon.NANO) {
								double c = (relRadius - r1) / (r2 - r1);
								return (c1 * (1. - c)) + (c2 * c);
							}
							return (c1 + c2) / 2.;

						}
					}

				}
			}
		}
		return 0.;
	}

	@SuppressWarnings("boxing")
	public static double getRake(HComponent component, double relRadius) {
		if (component.getHLofts().size() == 1) {
			HLoftPath blade = component.getHLofts().get(0).getHLoftPath();
			if (blade instanceof HLinearLoftPath) {
				{
					int np = component.getHLofts().get(0).getHLoftElements().size();

					List<Pair<Double, Double>> curve = new ArrayList<Pair<Double, Double>>();

					double x[] = new double[np];
					double y[] = new double[np];
					for (int i = 0; i < np; i++) {
						HLoftElement element = component.getHLofts().get(0).getHLoftElements().get(i);
						x[i] = ProfileDataSupport.getRelRadius(element);
						y[i] = ProfileDataSupport.getRake(element);
						curve.add(new Pair<Double, Double>(x[i], y[i]));
					}
					Collections.sort(curve, new Comparator<Pair<Double, Double>>() {
						@Override
						public int compare(Pair<Double, Double> o1, Pair<Double, Double> o2) {
							if (o1.getT1() < o2.getT1())
								return -1;
							if (o1.getT1() > o2.getT1())
								return 1;
							return 0;
						}
					});

					// piecewise linear interpolation
					for (SegmentIterator<Pair<Double, Double>> it = new SegmentIterator<Pair<Double, Double>>(curve, false); it.hasNext();) {
						Pair<Pair<Double, Double>, Pair<Double, Double>> p = it.next();
						double r1 = p.getT1().getT1();
						double rake1 = p.getT1().getT2();
						double r2 = p.getT2().getT1();
						double rake2 = p.getT2().getT2();

						if (relRadius >= r1 && relRadius <= r2) {
							if (Math.abs(r2 - r1) > Epsilon.NANO) {
								double c = (relRadius - r1) / (r2 - r1);
								return (rake1 * (1. - c)) + (rake2 * c);
							}
							return (rake1 + rake2) / 2.;

						}
					}

				}
			}
		}
		return 0.;
	}

	public static void setRake(HComponent component, double refRelRadius, double newRake) {
		double oldRake = getRake(component, refRelRadius);
		double dRake = newRake-oldRake;

		HLoftPath blade = component.getHLofts().get(0).getHLoftPath();
		if (blade instanceof HLinearLoftPath) {
			{
				int np = component.getHLofts().get(0).getHLoftElements().size();
				for (int i = 0; i < np; i++) {
					HLoftElement element = component.getHLofts().get(0).getHLoftElements().get(i);
					ProfileDataSupport.setRake(element, dRake+ProfileDataSupport.getRake(element));
				}
			}
		}
	}

	@SuppressWarnings("boxing")
	public static double getChi(HComponent component, double relRadius) {
		if (component.getHLofts().size() == 1) {
			HLoftPath blade = component.getHLofts().get(0).getHLoftPath();
			if (blade instanceof HLinearLoftPath) {
				{
					int np = component.getHLofts().get(0).getHLoftElements().size();

					List<Pair<Double, Double>> curve = new ArrayList<Pair<Double, Double>>();

					double x[] = new double[np];
					double y[] = new double[np];
					for (int i = 0; i < np; i++) {
						HLoftElement element = component.getHLofts().get(0).getHLoftElements().get(i);
						x[i] = ProfileDataSupport.getRelRadius(element);
						y[i] = ProfileDataSupport.getChi(element);
						curve.add(new Pair<Double, Double>(x[i], y[i]));
					}
					Collections.sort(curve, new Comparator<Pair<Double, Double>>() {
						@Override
						public int compare(Pair<Double, Double> o1, Pair<Double, Double> o2) {
							if (o1.getT1() < o2.getT1())
								return -1;
							if (o1.getT1() > o2.getT1())
								return 1;
							return 0;
						}
					});

					// piecewise linear interpolation
					for (SegmentIterator<Pair<Double, Double>> it = new SegmentIterator<Pair<Double, Double>>(curve, false); it.hasNext();) {
						Pair<Pair<Double, Double>, Pair<Double, Double>> p = it.next();
						double r1 = p.getT1().getT1();
						double chi1 = p.getT1().getT2();
						double r2 = p.getT2().getT1();
						double chi2 = p.getT2().getT2();

						if (relRadius >= r1 && relRadius <= r2) {
							if (Math.abs(r2 - r1) > Epsilon.NANO) {
								double c = (relRadius - r1) / (r2 - r1);
								return (chi1 * (1. - c)) + (chi2 * c);
							}
							return (chi1 + chi2) / 2.;
						}
					}

				}
			}
		}
		return 0.;
	}

	@SuppressWarnings("unused")
	public static double getSkew(HComponent component, double relRadius) {
		if (component.getHLofts().size() == 1) {
			HLoftPath blade = component.getHLofts().get(0).getHLoftPath();
			if (blade instanceof HLinearLoftPath) {
				{
					int np = component.getHLofts().get(0).getHLoftElements().size();

					double min = Double.MAX_VALUE;
					double max = -Double.MAX_VALUE;

					for (int i = 0; i < np; i++) {
						HLoftElement element = component.getHLofts().get(0).getHLoftElements().get(i);
						double s = ProfileDataSupport.getSkewAngle(element);

						if (s > max) {
							max = s;
						}
						if (s < min) {
							min = s;
						}
					}
					return max - min;
				}
			}
		}
		return 0.;
	}

	public static int getBladeCount(HComponent component) {
		if (component.getHLofts().size() > 0) {
			HLoft loft = component.getHLofts().get(0);
			if (loft.getParameters().containsKey(EMapKeys.ROTFREQ.toString())) {
				double rotAxis[] = new double[3];
				int z = 1;
				String value = (loft.getParameters().get(EMapKeys.ROTFREQ.toString()));
				if (value != null && value.length() > 0) {
					StringTokenizer st = new StringTokenizer(value);
					if (st.countTokens() == 4) {
						rotAxis[0] = Double.parseDouble(st.nextToken());
						rotAxis[1] = Double.parseDouble(st.nextToken());
						rotAxis[2] = Double.parseDouble(st.nextToken());
						z = Integer.parseInt(st.nextToken());
					}
				}
				return z;
			}
		}
		return 1;
	}

	@SuppressWarnings("boxing")
	public static void setBladeCount(HComponent component, int bladeCount) {
		if (component.getHLofts().size() > 0) {
			HLoft loft = component.getHLofts().get(0);
			String value = String.format("%3.1f %3.1f %3.1f %03d", 1., 0., 0., bladeCount);
			loft.getParameters().put(EMapKeys.ROTFREQ.toString(), value);
		}
	}
}
