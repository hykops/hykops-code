/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.tiprake.ui.toolbox;

import de.fsg.tfe.rde.ui.support.AbstractEditorModelUISupport;
import de.fsg.tk.propeller.datamodel.Propeller;
import de.fsg.tk.propeller.ui.editor.transformations.PropellerOperationProvider;
import de.hykops.lpd.core.sdk.CompositionSupport;
import de.hykops.lpd.datamodel.HComposition;
import de.hykops.lpd.ui.Perspective;
import de.hykops.lpd.ui.editor.CompositionMainEditor;
import de.hykops.lpd.ui.support.CompositionUISupport;
import de.hykops.tiprake.ui.sdk.TipRakeToHYKOPS;

/**
 * 
 *
 * @author stoye
 * @since Nov 22, 2016
 */
public class ToHYKOPSSeries extends PropellerOperationProvider{
	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fsg.tk.propeller.ui.editor.transformations.PropellerOperationProvider#
	 * performImpl(de.fsg.tk.propeller.datamodel.Propeller)
	 */
	@Override
	public void performImpl(Propeller propeller) {
		String name = "Propeller import";
		HComposition composition = CompositionSupport.createEmptyComposition(name, propeller.getDescription());
		TipRakeToHYKOPS.convertPropellerWithTipRakeAndSeriesProfiles(name, composition, propeller);

		AbstractEditorModelUISupport.openEditor(new CompositionUISupport().createEditorModelInput(composition, "Example HYKOPS lofted profile series data"), CompositionMainEditor.ID,
				Perspective.ID);
	}

}
