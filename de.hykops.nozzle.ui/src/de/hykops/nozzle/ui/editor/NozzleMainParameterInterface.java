/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.nozzle.ui.editor;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.NotificationImpl;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import de.fsg.tk.sdk.values.AbstractPhysicalDoubleValue;
import de.fsg.tk.ui.basic.ColorChooser;
import de.fsg.tk.ui.basic.UniqueColorProvider;
import de.fsg.tk.ui.widgets.BooleanValueWidget;
import de.fsg.tk.ui.widgets.IValueWidget;
import de.fsg.tk.ui.widgets.PhysicalValueWidget;
import de.fsg.tk.ui.widgets.ValueWidgetManager;
import de.hykops.lpd.datamodel.HComponent;
import de.hykops.lpd.datamodel.HLoftElement;
import de.hykops.lpd.ui.editor.component.ComponentMainParameterUIInterface;
import de.hykops.lpd.ui.editor.transformations.TransformationHelper;
import de.hykops.nozzle.core.sdk.NozzleDataSupport;
import de.hykops.nozzle.core.sdk.ProfileDataSupport;

/**
 * 
 *
 * @author stoye
 * @since Jul 26, 2016
 */
@SuppressWarnings("restriction")
public class NozzleMainParameterInterface extends ComponentMainParameterUIInterface {

	de.fsg.tk.sdk.values.IPhysicalDoubleValue<HComponent>	diameterValue;
	PhysicalValueWidget<HComponent>							diameterWidget;
	de.fsg.tk.sdk.values.IPhysicalDoubleValue<HComponent>	diffusorValue;
	PhysicalValueWidget<HComponent>							diffusorWidget;
	de.fsg.tk.sdk.values.IPhysicalDoubleValue<HComponent>	chordValue;
	PhysicalValueWidget<HComponent>							chordWidget;

	de.fsg.tk.sdk.values.IBooleanValue<HComponent> mirrorYValue;
	BooleanValueWidget<HComponent> mirrorYWidget;

	private ColorChooser									colorChooser;

	public NozzleMainParameterInterface(Composite parent_) {
		super(parent_);
	}

	/* (non-Javadoc)
	 * @see de.hykops.lpd.ui.editor.component.IComponentMainParameterUIInterface#getHRComponentType()
	 */
	@Override
	public String getHRComponentType() {
		return de.hykops.nozzle.core.TypeID.TYPE;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.hykops.lpd.ui.editor.component.IComponentMainParameterUIInterface#
	 * createControl()
	 */
	@Override
	public void createMainParameterControl() {

		Composite nozzleComposite = new Composite(this.mainParameterComposite, SWT.BORDER);
		nozzleComposite.setLayout(new GridLayout(2, true));
		nozzleComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		this.diameterValue = new AbstractPhysicalDoubleValue<HComponent>(de.fsg.tk.sdk.units.Length.meter, "D", "Nozzle diameter",
				"Nozzle diameter", true) {
			@Override
			public double getValue(HComponent model) {
				return 2. * NozzleDataSupport.getNozzleRadius(model);
			}

			@Override
			public void setValueImpl(HComponent model, double value) {
				EditingDomain eD = TransformationHelper.getEditingDomain();
				eD.getCommandStack().execute(new RecordingCommand((TransactionalEditingDomain) eD, "Set nozzle diameter") {

					@Override
					protected void doExecute() {
						NozzleDataSupport.setNozzleRadius(model, value / 2.);
						refreshMainParameterComposite();
					}
				});
			}
		};

		this.diameterWidget = new PhysicalValueWidget<HComponent>(nozzleComposite, this.diameterValue, null);
		this.mainParameterWidgets.add(this.diameterWidget);

		this.diffusorValue = new AbstractPhysicalDoubleValue<HComponent>(de.fsg.tk.sdk.units.Angle.radian, "A", "Nozzle diffusor angle",
				"Nozzle diffusor angle", true) {
			@Override
			public double getValue(HComponent model) {
				return NozzleDataSupport.getDiffusorAngle(model);
			}

			@Override
			public void setValueImpl(HComponent model, double value) {
				EditingDomain eD = TransformationHelper.getEditingDomain();
				eD.getCommandStack().execute(new RecordingCommand((TransactionalEditingDomain) eD, "Set nozzle diffusor angle") {

					@Override
					protected void doExecute() {
						NozzleDataSupport.setDiffusorAngle(model, value);
						refreshMainParameterComposite();
					}
				});
			}
		};
		this.diffusorWidget = new PhysicalValueWidget<HComponent>(nozzleComposite, this.diffusorValue, null);
		this.diffusorWidget.setUnit(de.fsg.tk.sdk.units.Angle.degree);
		this.mainParameterWidgets.add(this.diffusorWidget);

		this.chordValue = new AbstractPhysicalDoubleValue<HComponent>(de.fsg.tk.sdk.units.Length.meter, "C", "Nozzle chord length",
				"Nozzle chord length", true) {
			@Override
			public double getValue(HComponent model) {
				return NozzleDataSupport.getChordlength(model);
			}

			@Override
			public void setValueImpl(HComponent model, double chord) {

				double oldChord = NozzleDataSupport.getChordlength(model);
				double f = chord / oldChord;
				for (int i = 0; i < model.getHLofts().get(0).getHLoftElements().size(); i++) {
					HLoftElement element = model.getHLofts().get(0).getHLoftElements().get(i);
					double lChord = ProfileDataSupport.getChordlength(element);
					double newChord = lChord * f;
					EditingDomain eD = TransformationHelper.getEditingDomain();
					eD.getCommandStack().execute(new RecordingCommand((TransactionalEditingDomain) eD,
							"Set nozzle chord length for profile " + i + "/" + model.getHLofts().get(0).getHLoftElements().size()) {

						@Override
						protected void doExecute() {
							ProfileDataSupport.setChordlength(element, newChord);
						}
					});
				}
				refreshMainParameterComposite();
			}
		};
		this.chordWidget = new PhysicalValueWidget<HComponent>(nozzleComposite, this.chordValue, null);
		this.mainParameterWidgets.add(this.chordWidget);

		this.mirrorYValue = de.hykops.lpd.ui.dataaccess.ComponentDataAdaptor.getMirrorY();
		this.mirrorYWidget = new BooleanValueWidget<HComponent>(nozzleComposite, this.mirrorYValue, null);
		this.mainParameterWidgets.add(this.mirrorYWidget);
		
		this.colorChooser = new ColorChooser(nozzleComposite, HComponent.class.toString()) {
			@Override
			protected void refreshAllViews() {
				((EObject) getReference()).eNotify(new NotificationImpl(Notification.NO_FEATURE_ID, 0, 0));
			}
		};

		ValueWidgetManager.align(this.mainParameterWidgets);

		this.mainParameterComposite.pack();
		this.mainParameterComposite.layout();
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void refreshMainParameterComposite() {
		super.refreshMainParameterComposite();

		HComponent model = getModel();

		Color col = UniqueColorProvider.getSWTColor(model, HComponent.class.toString());
		this.colorChooser.setColor(col, model);

		for (IValueWidget<?> w : this.mainParameterWidgets) {
			IValueWidget w2 = w;
			w2.setModel(model);
			w2.refresh();
		}
	}

}
