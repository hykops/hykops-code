/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.nozzle.ui.editor;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.swt.widgets.Composite;

import de.hykops.lpd.ui.editor.component.ComponentMainParameterUIInterface;
import de.hykops.lpd.ui.editor.component.GeneralComponentProfileTableInterface;
import de.hykops.lpd.ui.editor.component.IComponentInterfaceFactory;
import de.hykops.lpd.ui.editor.component.IComponentProfileTableUIInterface;

/**
 * 
 *
 * @author stoye
 * @since Jul 26, 2016
 */
public class NozzleInterfaceFactory implements IComponentInterfaceFactory {

	@Override
	public ComponentMainParameterUIInterface getNewMainParameterComposite(Composite parent) {
		return new NozzleMainParameterInterface(parent);
	}

	@Override
	public IComponentProfileTableUIInterface getNewProfileTableComposite(Composite parent, AdapterFactory adapterFactory_) {
		return new GeneralComponentProfileTableInterface(parent, new NozzleDataAdaptor(), adapterFactory_);
	}
}
