/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.nozzle.ui.editor;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;

import de.fsg.tk.sdk.values.IPhysicalDoubleValue;
import de.fsg.tk.sdk.values.IValue;
import de.fsg.tk.sdk.values.IValueAdapter;
import de.fsg.tk.ui.values.AbstractEMFPhysicalDoubleValue;
import de.hykops.lpd.datamodel.HLoftElement;
import de.hykops.lpd.ui.dataaccess.ProfileDataAdapter;
import de.hykops.lpd.ui.editor.transformations.TransformationHelper;
import de.hykops.nozzle.core.sdk.ProfileDataSupport;

/**
 * 
 *
 * @author stoye
 * @since Dec 1, 2016
 */
public class NozzleDataAdaptor implements IValueAdapter<HLoftElement> {
	public static IPhysicalDoubleValue<HLoftElement> getProfileRelRadius() {
		return new AbstractEMFPhysicalDoubleValue<HLoftElement>(de.fsg.tk.sdk.units.One.one, "r/R", "Rel. radius",
				"Relative profile radius to nozzle radius", true) {
			@Override
			public double getValue(HLoftElement model) {
				return ProfileDataSupport.getRelRadius(model);
			}

			@Override
			public void setValueImpl(HLoftElement model, double value) {
				EditingDomain eD = TransformationHelper.getEditingDomain();
				eD.getCommandStack().execute(new RecordingCommand((TransactionalEditingDomain) eD, "Set rel. profile radius") {

					@Override
					protected void doExecute() {
						ProfileDataSupport.setRelRadius(model, value);
					}
				});
			}
		};
	}

	public static IPhysicalDoubleValue<HLoftElement> getProfileAxialShift() {
		return new AbstractEMFPhysicalDoubleValue<HLoftElement>(de.fsg.tk.sdk.units.Length.meter, "x", "Axial shift",
				"Axial location of origin relative to loft curve", true) {
			@Override
			public double getValue(HLoftElement model) {
				return ProfileDataSupport.getProfileAxialShift(model);
			}

			@Override
			public void setValueImpl(HLoftElement model, double value) {
				EditingDomain eD = TransformationHelper.getEditingDomain();
				eD.getCommandStack().execute(new RecordingCommand((TransactionalEditingDomain) eD, "Set axial shift") {

					@Override
					protected void doExecute() {
						ProfileDataSupport.setProfileAxialShift(model, value);
					}
				});
			}
		};
	}

	public static IPhysicalDoubleValue<HLoftElement> getChordLength() {
		return new AbstractEMFPhysicalDoubleValue<HLoftElement>(de.fsg.tk.sdk.units.Length.meter, "c", "Chord length",
				"Axial location of origin relative to loft curve", true) {
			@Override
			public double getValue(HLoftElement model) {
				return ProfileDataSupport.getChordlength(model);
			}

			@Override
			public void setValueImpl(HLoftElement model, double value) {
				EditingDomain eD = TransformationHelper.getEditingDomain();
				eD.getCommandStack().execute(new RecordingCommand((TransactionalEditingDomain) eD, "Set chord length") {

					@Override
					protected void doExecute() {
						ProfileDataSupport.setChordlength(model, value);
					}
				});
			}
		};
	}

	public static IPhysicalDoubleValue<HLoftElement> getDiffusorAngle() {
		return new AbstractEMFPhysicalDoubleValue<HLoftElement>(de.fsg.tk.sdk.units.Angle.radian, "alpha", "Diffusor angle",
				"Profile diffusor angle", true) {
			@Override
			public double getValue(HLoftElement model) {
				return ProfileDataSupport.getDiffusorAngle(model);
			}

			@Override
			public void setValueImpl(HLoftElement model, double value) {
				EditingDomain eD = TransformationHelper.getEditingDomain();
				eD.getCommandStack().execute(new RecordingCommand((TransactionalEditingDomain) eD, "Set chord length") {

					@Override
					protected void doExecute() {
						ProfileDataSupport.setDiffusorAngle(model, value);
					}
				});
			}
		};
	}

	private static final String SERIES	= "Series";
	private static final String	NAME	= "Name";
	private static final String	RREL	= "r/R";
	private static final String	XORIG	= "x";
	private static final String	CHORD	= "c";
	private static final String	ALPHA	= "alpha";

	@Override
	public List<String> getValueNames() {
		List<String> list = new ArrayList<String>();
		list.add(SERIES);
		list.add(NAME);
		list.add(RREL);
		list.add(XORIG);
		list.add(CHORD);
		list.add(ALPHA);
		return list;
	}

	public IValue<HLoftElement> getValue(String string) {
		if (SERIES.equals(string)) {
			return ProfileDataAdapter.getProfileSeries();
		} else if (NAME.equals(string)) {
			return ProfileDataAdapter.getProfileName();
		} else if (RREL.equals(string)) {
			return NozzleDataAdaptor.getProfileRelRadius();
		} else if (XORIG.equals(string)) {
			return NozzleDataAdaptor.getProfileAxialShift();
		} else if (CHORD.equals(string)) {
			return NozzleDataAdaptor.getChordLength();
		} else if (ALPHA.equals(string)) {
			return NozzleDataAdaptor.getDiffusorAngle();
		}
		return null;
	}

}
