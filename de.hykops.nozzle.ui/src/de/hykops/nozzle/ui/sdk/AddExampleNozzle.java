/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.nozzle.ui.sdk;

import de.hykops.lpd.core.nomenclature.ESurfaceIdentifiers;
import de.hykops.lpd.core.sdk.CoordinateSystem3DSupport;
import de.hykops.lpd.core.sdk.LoftElementSupport;
import de.hykops.lpd.core.sdk.SurfaceIdentifierSupport;
import de.hykops.lpd.datamodel.HComponent;
import de.hykops.lpd.datamodel.HComposition;
import de.hykops.lpd.datamodel.HCoordinateSystem3D;
import de.hykops.lpd.datamodel.HCurve2D;
import de.hykops.lpd.datamodel.HLoft;
import de.hykops.lpd.datamodel.HLoftElement;
import de.hykops.lpd.datamodel.HSurfaceIdentifier;
import de.hykops.nozzle.core.sdk.NozzleSupport;
import de.hykops.profile.sdk.HCurveSupport;

/**
 * 
 *
 * @author stoye
 * @since Jul 13, 2016
 */
public class AddExampleNozzle {

	public static HComponent createExampleNozzle1(HComposition parent, double xref, double yref, double zref, double radius, double chord, String profileSeries, String profileName) {
		HComponent nozzle = NozzleSupport.createEmptyNozzle("Default example nozzle", parent, xref, yref, zref, radius);
		parent.getHComponents().add(nozzle);

		HLoft ring = nozzle.getHLofts().get(0);
		//HLoftPath axis =
				ring.getHLoftPath();
		
		
		for (ESurfaceIdentifiers ei : ESurfaceIdentifiers.values()) {
			HSurfaceIdentifier hident = SurfaceIdentifierSupport.createHSurfaceIdentifier(ring, ei);
			ring.getHSurfaceIdentifications().add(hident); // containment
		}


		double f = .95; //1= vollkreis
		int np = 73;
		for (int ip = 0; ip < np; ip++) {
			double coord = (((double)ip) / ((double) (np - 1)));
			coord *= f;

			
			double origX = 0. + chord / 2.;
			double origY = 0.;
			double origCircum = coord;

			double xi1 = -chord;
			double xi2 = 0.;
			double xi3 = 0.;

			double z1 = 0.;
			double z2 = 0.;
			double z3 = 1.; // circum direction

// length of eta: =CHORD!			
			@SuppressWarnings("restriction")
			double eta[] = de.fsg.tk.math.vec.VecOp.cross(new double[]{xi1, xi2, xi3}, new double[]{z1, z2, z3});
			
			
			HCoordinateSystem3D planeCos = CoordinateSystem3DSupport.createCoordinateSystem3DAffine(null, origX, origY, origCircum, xi1, xi2, xi3, eta[0], eta[1], eta[2], z1, z2, z3);
			HLoftElement plane = LoftElementSupport.createLoftElement(ring, planeCos);
			planeCos.setHParent(plane);

			for (ESurfaceIdentifiers side : ESurfaceIdentifiers.values()) {
				HCurve2D curve = HCurveSupport.createSeriesProfile(plane, profileSeries, profileName, side);
				
				
				HSurfaceIdentifier ident = SurfaceIdentifierSupport.getSurfaceIdentifierInstance(ring, side);
				if (ident != null) {
					ring.getHSurfaceIdentifications().add(ident); // containment
					curve.getHSurfaceIdentifiers().add(ident); // reference
					plane.getHCurve2Ds().add(curve);
				}
				else
				{
					System.out.println("No Surface identifier");
				}
			}
		}
		return nozzle;
	}

}
