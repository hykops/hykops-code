/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.nozzle.ui.toolbox;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import de.fsg.tk.sdk.values.AbstractPhysicalDoubleValue;
import de.fsg.tk.ui.views.operationToolBox.ConfigurationPage;
import de.fsg.tk.ui.views.operationToolBox.DefaultConfigurationPage;
import de.fsg.tk.ui.widgets.IValueWidget;
import de.fsg.tk.ui.widgets.PhysicalValueWidget;
import de.fsg.tk.ui.widgets.StringValueWidget;
import de.hykops.lpd.datamodel.HComposition;
import de.hykops.lpd.ui.editor.transformations.HYKOPSOperationProvider;
import de.hykops.nozzle.ui.sdk.AddExampleNozzle;
import de.hykops.profile.sdk.ProfileSupport;
import de.hykops.profile.series.impl.NACA_ONE;

/**
 * 
 *
 * @author stoye
 * @since Jul 13, 2016
 */
@SuppressWarnings("restriction")
public class AddNozzle extends HYKOPSOperationProvider {

	private class OperationConfigurationPage extends DefaultConfigurationPage {

		private static final String																		DEFAULT_PROFILE_SERIES	= NACA_ONE.SERIESNAME;
		private static final String																		DEFAULT_PROFILE_NAME	= "NACA 16-1620";		/*- eigentlich bis 16-900*/
		private static final double																		DEFAULT_RADIUS			= 2.55;
		private static final double																		DEFAULT_CHORDLENGTH		= 2.5;

		private String profileSeries;
		private String																					profileName;
		private double																					radius;
		private double																					chordlength;

		private de.fsg.tk.sdk.values.AbstractStringValue<AddNozzle.OperationConfigurationPage>			profileValue;
		private de.fsg.tk.sdk.values.AbstractPhysicalDoubleValue<AddNozzle.OperationConfigurationPage>	radiusValue;
		private de.fsg.tk.sdk.values.AbstractPhysicalDoubleValue<AddNozzle.OperationConfigurationPage>	chordLengthValue;

		private StringValueWidget<AddNozzle.OperationConfigurationPage>									profileWidget;
		private PhysicalValueWidget<AddNozzle.OperationConfigurationPage>								radiusWidget;
		private PhysicalValueWidget<AddNozzle.OperationConfigurationPage>								chordLengthWidget;

		List<IValueWidget<?>>																			widgets;

		private boolean																					uiInitialized			= false;

		public OperationConfigurationPage() {
		}

		@Override
		public void createControl(Composite parent) {
			this.uiInitialized = false;
			this.widgets = new ArrayList<IValueWidget<?>>();
			Composite composite = new Composite(parent, SWT.NONE);
			composite.setLayout(new GridLayout(1, false));

			this.profileValue = new de.fsg.tk.sdk.values.AbstractStringValue<AddNozzle.OperationConfigurationPage>("Profile", "Profile", "Profile", true) {
				@Override
				public String getValue(AddNozzle.OperationConfigurationPage model) {
					return model.getProfileName();
				}

				@Override
				public void setValueImpl(AddNozzle.OperationConfigurationPage model, String value) {
					if (ProfileSupport.isValidProfile(value)) {
						model.setProfileName(value);
					}
					refresh();
				}
			};
			this.profileWidget = new StringValueWidget<OperationConfigurationPage>(composite, this.profileValue, null);
			this.widgets.add(this.profileWidget);

			this.radiusValue = new AbstractPhysicalDoubleValue<AddNozzle.OperationConfigurationPage>(de.fsg.tk.sdk.units.Length.meter, "radius", "nozzle radius", "nozzle radius", true) {

				@Override
				public double getValue(AddNozzle.OperationConfigurationPage model) {
					return model.getRadius();
				}

				@Override
				public void setValueImpl(AddNozzle.OperationConfigurationPage model, double value) {
					model.setRadius(value);
					refresh();
				}
			};
			this.radiusWidget = new PhysicalValueWidget<AddNozzle.OperationConfigurationPage>(composite, this.radiusValue, null);
			this.widgets.add(this.radiusWidget);

			this.chordLengthValue = new AbstractPhysicalDoubleValue<AddNozzle.OperationConfigurationPage>(de.fsg.tk.sdk.units.Length.meter, "chord", "chord length", "chord length", true) {

				@Override
				public double getValue(AddNozzle.OperationConfigurationPage model) {
					return model.getChordlength();
				}

				@Override
				public void setValueImpl(AddNozzle.OperationConfigurationPage model, double value) {
					model.setChordlength(value);
					refresh();
				}
			};
			this.chordLengthWidget = new PhysicalValueWidget<AddNozzle.OperationConfigurationPage>(composite, this.chordLengthValue, null);
			this.widgets.add(this.chordLengthWidget);

			composite.layout();
			restoreDefaults();
			this.uiInitialized = true;
			refresh();
		}

		@SuppressWarnings({ "rawtypes", "unchecked" })
		public void refresh() {
			if (this.uiInitialized) {
				for (IValueWidget w : this.widgets) {
					w.setModel(this);
					w.refresh();
				}
			}
		}

		@Override
		public void restoreDefaults() {
			setProfileSeries(DEFAULT_PROFILE_SERIES);
			setProfileName(DEFAULT_PROFILE_NAME);
			setRadius(DEFAULT_RADIUS);
			setChordlength(DEFAULT_CHORDLENGTH);
			refresh();
		}

		/**
		 * @return the profileName
		 */
		public String getProfileName() {
			return this.profileName;
		}

		/**
		 * @param profileName
		 *            the profileName to set
		 */
		public void setProfileName(String profileName_) {
			this.profileName = profileName_;
		}

		/**
		 * @return the radius
		 */
		public double getRadius() {
			return this.radius;
		}

		/**
		 * @param radius
		 *            the radius to set
		 */
		public void setRadius(double radius_) {
			this.radius = radius_;
		}

		/**
		 * @return the chordlength
		 */
		public double getChordlength() {
			return this.chordlength;
		}

		/**
		 * @param chordlength
		 *            the chordlength to set
		 */
		public void setChordlength(double chordlength_) {
			this.chordlength = chordlength_;
		}

		/**
		 * @return the profileSeries
		 */
		public String getProfileSeries() {
			return this.profileSeries;
		}

		/**
		 * @param profileSeries the profileSeries to set
		 */
		public void setProfileSeries(String profileSeries_) {
			this.profileSeries = profileSeries_;
		}
	}

	private OperationConfigurationPage page;

	@Override
	public ConfigurationPage getConfigurationPage() {
		if (this.page == null) {
			this.page = new OperationConfigurationPage();
		}
		return this.page;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hykops.lpd.ui.editor.transformations.HYKOPSOperationProvider#
	 * performImpl(de.hykops.lpd.datamodel.HComposition)
	 */
	@Override
	public void performImpl(HComposition composition) {
		AddNozzle.OperationConfigurationPage page_ = (AddNozzle.OperationConfigurationPage) getConfigurationPage();
		String series = page_.getProfileSeries();
		String profile = page_.getProfileName();
		double radius = page_.getRadius();
		double chord = page_.getChordlength();

		AddExampleNozzle.createExampleNozzle1(composition, 0., 0., 0., radius, chord, series, profile);
		// AddExampleNozzle.createExampleNozzle1("Example nozzle", composition,
		// 0., 0., 0., 1., 1.,"NACA 0012");
	}

}
