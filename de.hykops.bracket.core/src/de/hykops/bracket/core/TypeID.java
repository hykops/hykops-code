/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.bracket.core;

/**
 * 
 *
 * @author stoye
 * @since Nov 11, 2016
 */
public class TypeID {

	public final static String TYPE_ID = "de.hykops.bracket.default";
	public final static String TYPE ="shaft bracket";
}
