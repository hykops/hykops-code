/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.bracket.core.sdk;

import de.hykops.bracket.core.TypeID;
import de.hykops.lpd.core.Activator;
import de.hykops.lpd.core.factory.IHykopsFactory;
import de.hykops.lpd.core.sdk.CoordinateSystem3DSupport;
import de.hykops.lpd.core.sdk.Point3DSupport;
import de.hykops.lpd.datamodel.HComponent;
import de.hykops.lpd.datamodel.HComposition;
import de.hykops.lpd.datamodel.HCoordinateSystem3D;
import de.hykops.lpd.datamodel.HLinearLoftPath;
import de.hykops.lpd.datamodel.HLoft;
import de.hykops.lpd.datamodel.HPoint3D;

/**
 * 
 *
 * @author stoye
 * @since Nov 11, 2016
 */
public class BracketSupport {

	public static HComponent createEmptyBracket(String description, HComposition parent, double xref, double yref, double zref, double lLoft, double chord, double circumAngle, double rakeAngle,
			double attackAngle) {
		IHykopsFactory factory = Activator.getFactoryService().getFactory();
		HComponent bracket = factory.createComponent();
		bracket.setHParent(parent);
		bracket.setName("Bracket");
		bracket.setType(TypeID.TYPE_ID);
		bracket.setDescription(description);
		// Coordinate system of the whole bracket component: cartesian
		HCoordinateSystem3D cos = CoordinateSystem3DSupport.createCoordinateSystem3DAffine(bracket, /*-*/
				xref, yref, zref, /*-origin*/
				-1., 0., 0., /*-x1 axis: pos. downstream*/
				0., -1., 0., /*-x2 axis: pos. to stbd.*/
				0., 0., 1.);/*-x3 axis: pos. up*/
		bracket.setHCoordinateSystem3D(cos);
		HLoft shaft = factory.createLoft();
		bracket.getHLofts().add(shaft);

		shaft.setHParent(bracket);
		shaft.setName("bracket profile loft");
		shaft.setDescription("Default bracket profile loft");

		// Set blade loft axis: Linear, z upwards
		HLinearLoftPath path = factory.createLinearLoftPath();
		path.setHParent(shaft);
		path.setHLoft(shaft);
		path.setDenominator("BRACKET_LOFT");

		HCoordinateSystem3D cos2 = CoordinateSystem3DSupport.createCoordinateSystem3DCartesian(path, /*-*/
				xref, yref, zref, /*-origin*/
				chord, chord, lLoft, /*-*/
				circumAngle, rakeAngle, attackAngle);

		path.setHLoftCoordinateSystem(cos2);

		// origin of blade loft axis
		HPoint3D orig = Point3DSupport.createPoint3D(path, 0., 0., 0.);
		path.setOrigin(orig);
		// blade loft axis vector
		HPoint3D axis = Point3DSupport.createPoint3D(path, 0., 0., 1.);
		path.setLoftPathVector(axis);
		// blade loft perpendicular coordinate 1
		HPoint3D xi = Point3DSupport.createPoint3D(path, 1., 0., 0.);
		path.setLoftPathXi(xi);
		// blade loft perpendicular coordinate 2
		HPoint3D eta = Point3DSupport.createPoint3D(path, 0., 1., 0.);
		path.setLoftPathEta(eta);

		shaft.setHLoftPath(path);

		return bracket;
	}
}
