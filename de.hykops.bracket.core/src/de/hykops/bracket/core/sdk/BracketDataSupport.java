/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.bracket.core.sdk;

import de.fsg.tk.math.constants.Epsilon;
import de.hykops.lpd.datamodel.HComponent;
import de.hykops.lpd.datamodel.HCoordinateSystem3DCartesian;
import de.hykops.lpd.datamodel.HLinearLoftPath;
import de.hykops.lpd.datamodel.HLoftPath;

/**
 * 
 *
 * @author stoye
 * @since Nov 17, 2016
 */
public class BracketDataSupport {

	@SuppressWarnings("boxing")
	public static double getBracketLength(HComponent component) {
		if (component.getHLofts().size() == 1) {
			HLoftPath path = component.getHLofts().get(0).getHLoftPath();
			if (path instanceof HLinearLoftPath) {
				double l = ((HCoordinateSystem3DCartesian) ((HLinearLoftPath) path).getHLoftCoordinateSystem()).getX1().getX3();
				return l;
			}
			return 0.;
		}
		return 0.;
	}

	@SuppressWarnings("boxing")
	public static void setBracketLength(HComponent component, double newLength) {
		double length = getBracketLength(component);
		if (length > 0. && component.getHLofts().size() == 1) {
			HLoftPath path = component.getHLofts().get(0).getHLoftPath();
			if (path instanceof HLinearLoftPath) {
				((HCoordinateSystem3DCartesian) ((HLinearLoftPath) path).getHLoftCoordinateSystem()).getX1().setX3(newLength);
			}
		}
	}

	@SuppressWarnings("boxing")
	public static double getChordLength(HComponent component) {
		if (component.getHLofts().size() == 1) {
			HLoftPath path = component.getHLofts().get(0).getHLoftPath();
			if (path instanceof HLinearLoftPath) {
				double c = ((HCoordinateSystem3DCartesian) ((HLinearLoftPath) path).getHLoftCoordinateSystem()).getX1().getX1();
				return c;
			}
			return 0.;
		}
		return 0.;
	}

	@SuppressWarnings("boxing")
	public static void setChordLength(HComponent component, double newChord) {
		double chord = getChordLength(component);
		if (chord > 0. && component.getHLofts().size() == 1) {
			HLoftPath path = component.getHLofts().get(0).getHLoftPath();
			if (path instanceof HLinearLoftPath) {
				double xiOld = ((HCoordinateSystem3DCartesian) ((HLinearLoftPath) path).getHLoftCoordinateSystem()).getX1().getX1();
				double etaOld = ((HCoordinateSystem3DCartesian) ((HLinearLoftPath) path).getHLoftCoordinateSystem()).getX1().getX2();
				if (Math.abs(xiOld) > Epsilon.NANO) {
					double f = newChord / xiOld;
					((HCoordinateSystem3DCartesian) ((HLinearLoftPath) path).getHLoftCoordinateSystem()).getX1().setX1(f * xiOld);
					((HCoordinateSystem3DCartesian) ((HLinearLoftPath) path).getHLoftCoordinateSystem()).getX1().setX2(f * etaOld);
				}
			}
		}
	}

	@SuppressWarnings("boxing")
	public static double getCircumAngle(HComponent component) {
		if (component.getHLofts().size() == 1) {
			HLoftPath path = component.getHLofts().get(0).getHLoftPath();
			if (path instanceof HLinearLoftPath) {
				HCoordinateSystem3DCartesian c3 = ((HCoordinateSystem3DCartesian) ((HLinearLoftPath) path).getHLoftCoordinateSystem());
				return c3.getE1().getX1(); // alpha
			}
			return 0.;
		}
		return 0.;
	}

	@SuppressWarnings("boxing")
	public static void setCircumAngle(HComponent component, double newAngle) {
		if (component.getHLofts().size() == 1) {
			HLoftPath path = component.getHLofts().get(0).getHLoftPath();
			if (path instanceof HLinearLoftPath) {
				HCoordinateSystem3DCartesian c3 = ((HCoordinateSystem3DCartesian) ((HLinearLoftPath) path).getHLoftCoordinateSystem());
				c3.getE1().setX1(newAngle); // alpha
			}
		}
	}

	@SuppressWarnings("boxing")
	public static double getRakeAngle(HComponent component) {
		if (component.getHLofts().size() == 1) {
			HLoftPath path = component.getHLofts().get(0).getHLoftPath();
			if (path instanceof HLinearLoftPath) {

				HCoordinateSystem3DCartesian c3 = ((HCoordinateSystem3DCartesian) ((HLinearLoftPath) path).getHLoftCoordinateSystem());
				return c3.getE1().getX2(); // beta
			}
			return 0.;
		}
		return 0.;
	}

	@SuppressWarnings("boxing")
	public static void setRakeAngle(HComponent component, double newCircum) {
		if (component.getHLofts().size() == 1) {
			HLoftPath path = component.getHLofts().get(0).getHLoftPath();
			if (path instanceof HLinearLoftPath) {
				HCoordinateSystem3DCartesian c3 = ((HCoordinateSystem3DCartesian) ((HLinearLoftPath) path).getHLoftCoordinateSystem());
				c3.getE1().setX2(newCircum); // beta
			}
		}
	}

	@SuppressWarnings("boxing")
	public static double getAttackAngle(HComponent component) {
		if (component.getHLofts().size() == 1) {
			HLoftPath path = component.getHLofts().get(0).getHLoftPath();
			if (path instanceof HLinearLoftPath) {
				HCoordinateSystem3DCartesian c3 = ((HCoordinateSystem3DCartesian) ((HLinearLoftPath) path).getHLoftCoordinateSystem());
				return c3.getE1().getX3(); // gamma
			}
			return 0.;
		}
		return 0.;
	}

	@SuppressWarnings("boxing")
	public static void setAttackAngle(HComponent component, double newRake) {
		if (component.getHLofts().size() == 1) {
			HLoftPath path = component.getHLofts().get(0).getHLoftPath();
			if (path instanceof HLinearLoftPath) {
				HCoordinateSystem3DCartesian c3 = ((HCoordinateSystem3DCartesian) ((HLinearLoftPath) path).getHLoftCoordinateSystem());
				c3.getE1().setX3(newRake); // gamma
			}
		}
	}
}
