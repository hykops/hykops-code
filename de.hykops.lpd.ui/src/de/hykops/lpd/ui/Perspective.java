/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.lpd.ui;

import org.eclipse.ui.IFolderLayout;
import org.eclipse.ui.IPageLayout;
import org.eclipse.ui.IPerspectiveFactory;

import de.fsg.tfe.rde.ui.projectexplorer.ProjectExplorer;
import de.fsg.tk.ui.views.operationToolBox.OperationToolBoxView;

/**
 * 
 *
 * @author stoye
 * @since Jun 28, 2016
 */
public class Perspective implements IPerspectiveFactory {

	public static final String ID = "de.hykops.lpd.ui.Perspective";

	@SuppressWarnings("unused")
	@Override
	public void createInitialLayout(IPageLayout layout) {
		IFolderLayout bottomLeft = layout.createFolder("bottomLeft", IPageLayout.BOTTOM, 0.50f, IPageLayout.ID_EDITOR_AREA);

		IFolderLayout bottomRight = layout.createFolder("bottomRightTop", IPageLayout.RIGHT, 0.40f, "bottomLeft");
		IFolderLayout topLeft = layout.createFolder("topLeft", IPageLayout.LEFT, 0.2f, IPageLayout.ID_EDITOR_AREA);
		IFolderLayout topRight = layout.createFolder("topRight", IPageLayout.RIGHT, 0.7f, IPageLayout.ID_EDITOR_AREA);

		topLeft.addView(ProjectExplorer.VIEW_ID);
		topLeft.addView("org.eclipse.ui.views.ContentOutline");
		topLeft.addView("org.eclipse.ui.views.PropertySheet");
		bottomRight.addView(OperationToolBoxView.ID);
		bottomRight.addView(IPageLayout.ID_PROBLEM_VIEW);
		bottomRight.addView("org.eclipse.pde.runtime.LogView");
		bottomRight.addView(org.eclipse.ui.console.IConsoleConstants.ID_CONSOLE_VIEW);

		bottomLeft.addView("de.hykops.lpd.ui.vtk.views.composition3D");
	}
}
