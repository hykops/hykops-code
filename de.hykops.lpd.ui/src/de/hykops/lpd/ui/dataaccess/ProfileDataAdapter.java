/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.lpd.ui.dataaccess;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;

import de.fsg.tk.sdk.units.Length;
import de.fsg.tk.sdk.values.AbstractPhysicalDoubleValue;
import de.fsg.tk.sdk.values.IDoubleValue;
import de.fsg.tk.sdk.values.IStringValue;
import de.fsg.tk.sdk.values.IValue;
import de.fsg.tk.sdk.values.IValueAdapter;
import de.fsg.tk.ui.values.AbstractEMFStringValue;
import de.hykops.lpd.core.sdk.LoftElementSupport;
import de.hykops.lpd.datamodel.HCurve2D;
import de.hykops.lpd.datamodel.HDiscreteCurve2D;
import de.hykops.lpd.datamodel.HLoftElement;
import de.hykops.lpd.datamodel.HSeriesProfileCurve2D;
import de.hykops.lpd.datamodel.HSurfaceIdentifier;
import de.hykops.lpd.ui.editor.transformations.TransformationHelper;

/**
 * 
 *
 * @author stoye
 * @since Nov 29, 2016
 */
public class ProfileDataAdapter implements IValueAdapter<HLoftElement> {

	public static IStringValue<HLoftElement> getProfileName() {
		return new AbstractEMFStringValue<HLoftElement>("profile", "Profile", "Profile denomination", true) {
			@Override
			public String getValue(HLoftElement model) {
				return LoftElementSupport.getSingleProfileInLoftElement(model);
			}
			@Override
			public void setValueImpl(HLoftElement element, String value)
			{
				LoftElementSupport.setSingleProfileInLoftElement(element, value);
			}
		};
	}

	public static IStringValue<HLoftElement> getProfileSeries() {
		return new AbstractEMFStringValue<HLoftElement>("series", "Series", "Profile family", true) {
			@Override
			public String getValue(HLoftElement model) {
				return LoftElementSupport.getSingleSeriesInLoftElement(model);
			}
			@Override
			public void setValueImpl(HLoftElement element, String value)
			{
				LoftElementSupport.setSingleSeriesInLoftElement(element, value);
			}
		};
	}

	public static IStringValue<HLoftElement> getConnectedSurfaces() {
		return new AbstractEMFStringValue<HLoftElement>("Surfaces", "Surfaces", "connected surfaces", false) {
			@Override
			public String getValue(HLoftElement model) {
				HSurfaceIdentifier idents[] = LoftElementSupport.getConnectedSurfaceIdentifiers(model);
				if (idents.length == 0) {
					return "";
				} else if (idents.length == 1) {
					return idents[0].getName();
				} else {
					String s = "";
					for (int i = 0; i < (idents.length - 1); i++) {
						s += idents[i].getName();
						s += " ";
					}
					s += idents[idents.length - 1].getName();
					return s;
				}
			}
		};
	}

	public static IDoubleValue<HLoftElement> getXiOrig() {
		return new AbstractPhysicalDoubleValue<HLoftElement>(Length.meter, "xi", "xi", "xi-origin of profile", true) {
			@Override
			public double getValue(HLoftElement model) {
				return model.getLoftPlaneDefiningCoordinateSystem().getOrigin().getX1();
			}

			@SuppressWarnings("boxing")
			@Override
			public void setValueImpl(HLoftElement model, double value) {
				EditingDomain eD = TransformationHelper.getEditingDomain();
				eD.getCommandStack().execute(new RecordingCommand((TransactionalEditingDomain) eD, String.format("Set the xi-origin of the profile to %9.3f", value)) {
					@Override
					protected void doExecute() {
						model.getLoftPlaneDefiningCoordinateSystem().getOrigin().setX1(value);
					}
				});
			}
		};
	}

	public static IDoubleValue<HLoftElement> getEtaOrig() {
		return new AbstractPhysicalDoubleValue<HLoftElement>(Length.meter, "eta", "eta", "eta-origin of profile", true) {
			@Override
			public double getValue(HLoftElement model) {
				return model.getLoftPlaneDefiningCoordinateSystem().getOrigin().getX2();
			}

			@SuppressWarnings("boxing")
			@Override
			public void setValueImpl(HLoftElement model, double value) {
				EditingDomain eD = TransformationHelper.getEditingDomain();
				eD.getCommandStack().execute(new RecordingCommand((TransactionalEditingDomain) eD, String.format("Set the eta-origin of the component to %9.3f", value)) {
					@Override
					protected void doExecute() {
						model.getLoftPlaneDefiningCoordinateSystem().getOrigin().setX2(value);
					}
				});
			}
		};
	}

	
	private static final String	SERIES	= "Series";
	private static final String	NAME	= "Name";
	private static final String	XORIG	= "x1ref";
	private static final String	YORIG	= "x2ref";
	private static final String	SURFS	= "surfaces";

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fsg.tk.sdk.values.IValueAdapter#getValueNames()
	 */
	@Override
	public List<String> getValueNames() {
		List<String> list = new ArrayList<String>();
		list.add(XORIG);
		list.add(YORIG);
		list.add(SURFS);
		list.add(SERIES);
		list.add(NAME);
		return list;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.fsg.tk.sdk.values.IValueAdapter#getValue(java.lang.String)
	 */
	@Override
	public IValue<HLoftElement> getValue(String string) {
		if (XORIG.equals(string)) {
			return getXiOrig();
		} else if (YORIG.equals(string)) {
			return getEtaOrig();
		} else if (SURFS.equals(string)) {
			return getConnectedSurfaces();
		} else if (SERIES.equals(string)) {
			return getProfileSeries();
		} else if (NAME.equals(string)) {
			return getProfileName();
		}
		return null;
	}

}
