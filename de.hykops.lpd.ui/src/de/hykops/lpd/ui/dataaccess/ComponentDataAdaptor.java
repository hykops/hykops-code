/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.lpd.ui.dataaccess;

import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;

import de.fsg.tk.sdk.units.Length;
import de.fsg.tk.sdk.values.AbstractBooleanValue;
import de.fsg.tk.sdk.values.AbstractPhysicalDoubleValue;
import de.fsg.tk.sdk.values.IBooleanValue;
import de.fsg.tk.sdk.values.IDoubleValue;
import de.fsg.tk.sdk.values.IStringValue;
import de.fsg.tk.sdk.values.IValue;
import de.fsg.tk.sdk.values.IValueAdapter;
import de.fsg.tk.ui.values.AbstractEMFStringValue;
import de.hykops.lpd.core.nomenclature.EMapKeys;
import de.hykops.lpd.datamodel.HComponent;
import de.hykops.lpd.ui.editor.transformations.TransformationHelper;

/**
 * 
 *
 * @author stoye
 * @since Jul 15, 2016
 */
public class ComponentDataAdaptor implements IValueAdapter<HComponent> {

	public static IStringValue<HComponent> getName() {
		return new AbstractEMFStringValue<HComponent>("name", "Name", "Name of the component", true) {
			@Override
			public String getValue(HComponent model) {
				return model.getName();
			}

			@Override
			public void setValueImpl(HComponent model, String value) {
				EditingDomain eD = TransformationHelper.getEditingDomain();
				eD.getCommandStack().execute(new RecordingCommand((TransactionalEditingDomain) eD, "Set the name of the component") {
					@Override
					protected void doExecute() {
						model.setName(value);
					}
				});
			}
		};
	}

	public static IDoubleValue<HComponent> getXRef() {
		return new AbstractPhysicalDoubleValue<HComponent>(Length.meter, "xref", "xref", "x-origin of component", true) {
			@Override
			public double getValue(HComponent model) {
				return model.getHCoordinateSystem3D().getOrigin().getX1();
			}

			@SuppressWarnings("boxing")
			@Override
			public void setValueImpl(HComponent model, double value) {
				EditingDomain eD = TransformationHelper.getEditingDomain();
				eD.getCommandStack().execute(new RecordingCommand((TransactionalEditingDomain) eD, String.format("Set the x-origin of the component to %9.3f", value)) {
					@Override
					protected void doExecute() {
						model.getHCoordinateSystem3D().getOrigin().setX1(value);
					}
				});
			}
		};
	}

	public static IDoubleValue<HComponent> getYRef() {
		return new AbstractPhysicalDoubleValue<HComponent>(Length.meter, "yref", "yref", "y-origin of component", true) {
			@Override
			public double getValue(HComponent model) {
				return model.getHCoordinateSystem3D().getOrigin().getX2();
			}

			@SuppressWarnings("boxing")
			@Override
			public void setValueImpl(HComponent model, double value) {
				EditingDomain eD = TransformationHelper.getEditingDomain();
				eD.getCommandStack().execute(new RecordingCommand((TransactionalEditingDomain) eD, String.format("Set the y-origin of the component to %9.3f", value)) {
					@Override
					protected void doExecute() {
						model.getHCoordinateSystem3D().getOrigin().setX2(value);
					}
				});
			}
		};
	}

	public static IDoubleValue<HComponent> getZRef() {
		return new AbstractPhysicalDoubleValue<HComponent>(Length.meter, "zref", "zref", "z-origin of component", true) {
			@Override
			public double getValue(HComponent model) {
				return model.getHCoordinateSystem3D().getOrigin().getX3();
			}

			@SuppressWarnings("boxing")
			@Override
			public void setValueImpl(HComponent model, double value) {
				EditingDomain eD = TransformationHelper.getEditingDomain();
				eD.getCommandStack().execute(new RecordingCommand((TransactionalEditingDomain) eD, String.format("Set the z-origin of the component to %9.3f", value)) {
					@Override
					protected void doExecute() {
						model.getHCoordinateSystem3D().getOrigin().setX3(value);
					}
				});
			}
		};
	}

	public static IBooleanValue<HComponent> getMirrorY() {
		return new AbstractBooleanValue<HComponent>("mirror", "mirror", "mirror by global y", true) {
			@Override
			public boolean getValue(HComponent model) {
				if (model.getParameters().containsKey(EMapKeys.MIRROR.toString())) {
					String value = model.getParameters().get(EMapKeys.MIRROR.toString());
					if (value != null && value.length() > 0) {
						StringTokenizer st = new StringTokenizer(value);
						if (st.countTokens() == 6) {
							st.nextToken(); // origX
							st.nextToken(); // origY
							st.nextToken(); // origZ
							st.nextToken(); // mirrX
							boolean mirrY = Boolean.parseBoolean(st.nextToken()); // mirrY
							st.nextToken(); // mirrZ
							return mirrY;
						}
					}
				}
				return false;
			}

			@SuppressWarnings("boxing")
			@Override
			public void setValueImpl(HComponent model, boolean newMirrorY) {
				EditingDomain eD = TransformationHelper.getEditingDomain();
				eD.getCommandStack().execute(new RecordingCommand((TransactionalEditingDomain) eD, String.format("Toggle mirror by y")) {
					@Override
					protected void doExecute() {
						double origX = 0.;
						double origY = 0.;
						double origZ = 0.;
						boolean mirrX = false;
						boolean mirrY = false;
						boolean mirrZ = false;
						if (model.getParameters().containsKey(EMapKeys.MIRROR.toString())) {
							String value = model.getParameters().get(EMapKeys.MIRROR.toString());
							if (value != null && value.length() > 0) {
								StringTokenizer st = new StringTokenizer(value);
								if (st.countTokens() == 6) {
									origX = Double.parseDouble(st.nextToken()); // origX
									origY = Double.parseDouble(st.nextToken()); // origY
									origZ = Double.parseDouble(st.nextToken()); // origZ
									mirrX = Boolean.parseBoolean(st.nextToken()); // mirrX
									mirrY = Boolean.parseBoolean(st.nextToken()); // mirrY
									mirrZ = Boolean.parseBoolean(st.nextToken()); // mirrY
								}
							}
						}
						mirrY = newMirrorY;
						double eps = de.fsg.tk.math.constants.Epsilon.NANO;
						if ((Math.abs(origX) < eps) && (Math.abs(origY) < eps) && (Math.abs(origZ) < eps) && (!mirrX) && (!mirrY) && (!mirrZ)) {
							model.getParameters().remove(EMapKeys.MIRROR.toString());
						} else {
							String value = String.format("%16.5f %16.5f %16.5f %b %b %b", origX, origY, origZ, mirrX, mirrY, mirrZ);
							model.getParameters().put(EMapKeys.MIRROR.toString(), value);
						}
					}
				});
			}
		};
	}

	private static final String	NAME	= "Name";
	private static final String	XREF	= "xref";
	private static final String	YREF	= "yref";
	private static final String	ZREF	= "zref";

	@Override
	public List<String> getValueNames() {
		List<String> list = new ArrayList<String>();
		list.add(NAME);
		list.add(XREF);
		list.add(YREF);
		list.add(ZREF);
		return list;
	}

	@Override
	public IValue<HComponent> getValue(String string) {
		if (NAME.equals(string)) {
			return getName();
		} else if (XREF.equals(string)) {
			return getXRef();
		} else if (YREF.equals(string)) {
			return getYRef();
		} else if (ZREF.equals(string)) {
			return getZRef();
		}
		return null;
	}
}
