/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.lpd.ui.dataaccess;

import java.util.ArrayList;
import java.util.List;

import de.fsg.tk.sdk.values.IStringValue;
import de.fsg.tk.sdk.values.IValue;
import de.fsg.tk.sdk.values.IValueAdapter;
import de.fsg.tk.ui.values.AbstractEMFStringValue;
import de.hykops.lpd.datamodel.HComposition;

/**
 * 
 *
 * @author stoye
 * @since Jun 28, 2016
 */
public class CompositionDataAdaptor implements IValueAdapter<HComposition>{
	public static IStringValue<HComposition> getDescription() {
		return new AbstractEMFStringValue<HComposition>("name", "Description", "Description of the composition", true) {
			@Override
			public String getValue(HComposition model) {
				return model.getDescription();
			}

			@Override
			public void setValueImpl(HComposition m, String value) {
				m.setDescription(value);
			}
		};
	}
	
	public static List<IValue<HComposition>> getAllProvidedValues() {
		List<IValue<HComposition>> list = new ArrayList<IValue<HComposition>>();

		list.add(getDescription());

		return list;
	}

	private static final String	DESCRIPTION							= "Desc";

	@Override
	public List<String> getValueNames() {
		List<String> list = new ArrayList<String>();
		list.add(DESCRIPTION);
		return list;
	}
	
	@Override
	public IValue<HComposition> getValue(String string) {
		if (DESCRIPTION.equals(string)) {
			return getDescription();
		} 
		return null;
	}

}
