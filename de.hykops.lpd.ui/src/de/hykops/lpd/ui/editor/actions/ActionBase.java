/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.lpd.ui.editor.actions;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;

import de.fsg.tk.ui.editor.actions.AbstractActionBase;
import de.hykops.lpd.datamodel.HComponent;
import de.hykops.lpd.datamodel.HComposition;
import de.hykops.lpd.datamodel.HLoftElement;
import de.hykops.lpd.datamodel.HLoftPath;

/**
 * 
 *
 * @author stoye
 * @since Jun 28, 2016
 */
public abstract class ActionBase extends AbstractActionBase<HComposition> {

	protected HComponent		activeComponent;
	protected HLoftPath	activeLoftPath;
	protected HLoftElement	activeLoftElement;

	@Override
	public void selectionChanged(IAction action, ISelection selection) {
		this.activeEditorModel = null;
		this.activeComponent = null;
		this.activeLoftPath = null;
		this.activeLoftElement = null;

		boolean selected = false;
		if (selection instanceof IStructuredSelection && ((IStructuredSelection) selection).size() == 1) {
			Object o = ((IStructuredSelection) selection).getFirstElement();
			if (o instanceof HComposition) {
				this.activeEditorModel = (HComposition) o;
				selected = true;
			} else if (o instanceof HComponent) {
				this.activeComponent = (HComponent) o;
				// this.activeEditorModel =
				// this.activeComponent.getComposition...;
				selected = true;
			} else if (o instanceof HLoftPath) {
				this.activeLoftPath = (HLoftPath) o;
				// this.activeEditorModel =
				// this.activeProfile.getBladeGeometry().getPropeller();
				selected = true;
			}
		}
		if (!selected && this.activeEditor != null) {
			this.activeEditorModel = this.activeEditor.getAdapter(HComposition.class);
		}

		validate(action);
	}

	public HComponent getComponent() {
		if (this.activeComponent != null) {
			return this.activeComponent;
		}
		return null;
	}

	public HLoftPath getLoftPath() {
		if (this.activeLoftPath != null) {
			return this.activeLoftPath;
		}
		return null;
	}

	public HLoftElement getLoftElement() {
		if (this.activeLoftElement != null) {
			return this.activeLoftElement;
		}
		return null;
	}

	@Override
	public Class<HComposition> getEditorModelClass() {
		return HComposition.class;
	}

	@Override
	public abstract void validate(IAction action);

}
