/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.lpd.ui.editor.actions;

import org.eclipse.jface.action.IAction;

/**
 * 
 *
 * @author stoye
 * @since Jun 28, 2016
 */
public class DoesNothingAction extends ActionBase {
	@Override
	public void run() {
		// TODO: Nothing
	}

	@Override
	public void validate(IAction action) {
		// TODO: Nothing
	}
}
