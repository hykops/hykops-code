/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.lpd.ui.editor;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.NotificationImpl;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import de.fsg.tk.ui.basic.ColorChooser;
import de.fsg.tk.ui.basic.EUniqueColorProviderPalettes;
import de.fsg.tk.ui.basic.UniqueColorProvider;
import de.fsg.tk.ui.widgets.IValueWidget;
import de.fsg.tk.ui.widgets.StringValueWidget;
import de.fsg.tk.ui.widgets.ValueWidgetManager;
import de.hykops.lpd.datamodel.HComponent;
import de.hykops.lpd.datamodel.HComposition;
import de.hykops.lpd.ui.dataaccess.CompositionDataAdaptor;
import de.hykops.lpd.ui.editor.component.IComponentMainParameterUIInterface;

/**
 * 
 *
 * @author stoye
 * @since Jun 28, 2016
 */
public class CompositionMainDataPage extends Composite {

	private HComposition							model;
	private StringValueWidget<HComposition>			descriptionWidget;

	private ColorChooser							colorChooser;

	@SuppressWarnings("unused")
	public CompositionMainDataPage(Composite parent, AdapterFactory adapterFactory) {
		super(parent, SWT.FILL);

		UniqueColorProvider.setPalette(HComponent.class.toString(), EUniqueColorProviderPalettes.MOTLEY);

		Composite outerComposite = new Composite(this, SWT.FILL) {
			@Override
			public void layout() {
				setSize(computeSize(SWT.DEFAULT, SWT.DEFAULT));
				super.layout();
			}
		};
		outerComposite.setLayout(new GridLayout(1, false));

		this.descriptionWidget = new StringValueWidget<HComposition>(outerComposite, CompositionDataAdaptor.getDescription(), null, 120, 5);

		this.colorChooser = new ColorChooser(outerComposite, HComposition.class.toString()) {
			@Override
			protected void refreshAllViews() {
				((EObject) getReference()).eNotify(new NotificationImpl(Notification.NO_FEATURE_ID, 0, 0));
			}
		};

		Composite composite = new Composite(outerComposite, SWT.FILL) {
			@Override
			public void layout() {
				setSize(computeSize(SWT.DEFAULT, SWT.DEFAULT));
				super.layout();
				getParent().layout();
			}
		};

		GridLayout layout = new GridLayout();
		layout.numColumns = 1;
		layout.horizontalSpacing = 20;
		layout.verticalSpacing = 0;
		composite.setLayout(layout);

		List<IValueWidget<?>> alignList = new ArrayList<IValueWidget<?>>();
		alignList.add(this.descriptionWidget);
		ValueWidgetManager.align(alignList);


		composite.layout();
	}

	public CTabFolder getComponentComposite() {
		return null;
	}

	public void refresh(HComposition composition) {
		if (composition != null) {
			this.setModel(composition);
			this.descriptionWidget.setModel(composition);
			this.descriptionWidget.refresh();

			Color col = UniqueColorProvider.getSWTColor(composition, HComposition.class.toString());
			this.colorChooser.setColor(col, composition);
		}
	}

	public HComponent getHComponent(IComponentMainParameterUIInterface interf) {
		return null;
	}

	/**
	 * @return the model
	 */
	public HComposition getModel() {
		return this.model;
	}

	/**
	 * @param model
	 *            the model to set
	 */
	public void setModel(HComposition model_) {
		this.model = model_;
	}
}
