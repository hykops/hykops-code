/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.lpd.ui.editor;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.action.MenuManager;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorSite;
import org.eclipse.ui.PartInitException;

import de.fsg.tfe.rde.ui.editor.DatamodelPreview;
import de.fsg.tfe.rde.ui.editor.EditorModelMainEditor;
import de.hykops.lpd.core.Activator;
import de.hykops.lpd.core.serialize.ISerializer;
import de.hykops.lpd.datamodel.HComponent;
import de.hykops.lpd.datamodel.HComposition;
import de.hykops.lpd.datamodel.HLoftPath;
import de.hykops.lpd.datamodel.provider.DatamodelItemProviderAdapterFactory;
import de.hykops.lpd.ui.editor.input.IHYKOPSEditorInput;

/**
 * 
 *
 * @author stoye
 * @since Jun 28, 2016
 */
public class CompositionMainEditor extends EditorModelMainEditor<HComposition> {

	public static String			ID								= "de.hykops.lpd.ui.editor.compositionMainEditor";

	private static final HComponent	EMPTY_COMPONENT_FOR_VALIDATOR	= Activator.getFactoryService().getFactory().createComponent();

	private CompositionMainDataPage	mainDataPage;
	private ComponentMainDataPage	componentMainDataPage;
	private ComponentProfilePage componentProfilePage;
	private DatamodelPreview		datamodelPreview;
	private MenuManager				menuManager;

	public CompositionMainEditor() {
		super();
	}

	private void createMainPropertiesPage() {
		this.mainDataPage = new CompositionMainDataPage(getContainer(), this.adapterFactory);
		setPageText(addPage(this.mainDataPage), "General");
		this.mainDataPage.refresh(this.editorModel);
	}

	private void createComponentMainDataPage() {
		this.componentMainDataPage = new ComponentMainDataPage(getContainer(), this.adapterFactory);

		this.componentMainDataPage.getComponentTable().addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				setSelection(event.getSelection());
			}
		});

		setPageText(addPage(this.componentMainDataPage.getComposite()), "main");
		this.componentMainDataPage.refresh();
	}

	private void createComponentProfilePage() {
		this.componentProfilePage = new ComponentProfilePage(getContainer(), this.adapterFactory);

		this.componentProfilePage.getComponentTable().addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(SelectionChangedEvent event) {
				setSelection(event.getSelection());
			}
		});

		setPageText(addPage(this.componentProfilePage.getComposite()), "profile");
		this.componentProfilePage.refresh();
	}

	private void createDatamodelEditorPage() {
		Composite parent = getContainer();
		parent.setLayout(new FillLayout());
		this.datamodelPreview = new DatamodelPreview(parent, this.editingDomain);
		int index = addPage(this.datamodelPreview);
		setPageText(index, "Datamodel");
	}

	@Override
	protected void createEditorPages() {
		createMainPropertiesPage();
		createComponentMainDataPage();
		createComponentProfilePage();
		createDatamodelEditorPage();
	}

	@Override
	public void init(IEditorSite site, IEditorInput input) throws PartInitException {
		super.editorInit(site, input);

		IMenuManager manager = site.getActionBars().getMenuManager();
		addLocalMenuEntries(manager);
	}

	public void addLocalMenuEntries(IMenuManager customisedMenu) {
		this.menuManager = new MenuManager("HComposition", ID);

		if (customisedMenu.find(this.menuManager.getId()) == null) {
			customisedMenu.add(this.menuManager);
		}
	}

	@Override
	public void refresh() {
		refresh(this.editorModel);
	}

	void refresh(HComposition composition) {
		firePropertyChange(IEditorPart.PROP_DIRTY);
		if (composition != null) {
			validateModel();
			// HComposition componentProfileTableComposite = composition;
			// if (componentProfileTableComposite.getHComponents() != null) {
			// if (this.profileTablePage != null) {
			// if (this.profileTablePage.getProfileTable() != null) {
			// this.profileTablePage.getProfileTable().setInput((propeller).getBladeGeometry());
			// this.profileTablePage.getProfileTable().refresh();
			// }
			// if (this.profileTablePage.getStationTable() != null) {
			// this.profileTablePage.getStationTable().setInput(null);
			// this.profileTablePage.getStationTable().refresh();
			// }
			// }
			// }
			if (this.mainDataPage != null) {
				// if (composition.getHComponents() != null) {
				// if (this.mainDataPage.getComponentTable() != null) {
				// this.mainDataPage.getComponentTable().setInput(composition);
				// this.mainDataPage.getComponentTable().refresh();
				// }
				// }
				this.mainDataPage.refresh(composition);
			}
			if (this.componentMainDataPage != null) {
				this.componentMainDataPage.getComponentTable().setInput(this.editorModel);
				this.componentMainDataPage.getComponentTable().refresh();
				this.componentMainDataPage.refresh();
			}
			if(this.componentProfilePage != null){
				this.componentProfilePage.getComponentTable().setInput(this.editorModel);
				this.componentProfilePage.getComponentTable().refresh();
				this.componentProfilePage.refresh();
			}
			if (this.datamodelPreview != null) {
				ISerializer serializer = Activator.getFactoryService().getFactory().createSerializer();
				this.datamodelPreview.refresh(serializer.serialize(this.editorModel));
			}
			if (this.propertySheetPage != null) {
				this.propertySheetPage.refresh();
			}
		}
	}

	@Override
	public void handleSelection(ISelection selection) {
		if (!selection.isEmpty() && selection instanceof IStructuredSelection) {
			Object firstElement = ((IStructuredSelection) selection).getFirstElement();
			if (firstElement instanceof HComposition) {
				setSelection(selection);
			} else if (firstElement instanceof HComponent) {
				// this.profileTablePage.getProfileTable().setSelection(new
				// StructuredSelection(firstElement));
			} else if (firstElement instanceof HLoftPath) {
				// this.profileTablePage.getProfileTable().setSelection(new
				// StructuredSelection(((Station) firstElement).getProfile()));
				// this.profileTablePage.getStationTable().setSelection(selection);
			}
		}

	}

	@Override
	protected Class<HComposition> getEditorModelClass() {
		return HComposition.class;
	}

	@Override
	protected String getEditorModelContext() {
		return "de.hykops.lpd.ui.editor.compositionContext";
	}

	@Override
	protected Class<?> getEditorModelEditorInputClass() {
		return IHYKOPSEditorInput.class;
	}

	@Override
	protected AdapterFactory getEditorModelItemProviderAdapterFactory() {
		return new DatamodelItemProviderAdapterFactory();
	}

	@Override
	protected String getEditorModelMainEditorName() {
		return CompositionMainEditor.ID;
	}

	@Override
	protected EObject getEmptyPointForValidator() {
		return CompositionMainEditor.EMPTY_COMPONENT_FOR_VALIDATOR;
	}

	@Override
	protected String getOperationsPath() {
		return "de.hykops";
	}

}
