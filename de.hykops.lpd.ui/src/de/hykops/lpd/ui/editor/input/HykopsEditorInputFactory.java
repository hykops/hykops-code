package de.hykops.lpd.ui.editor.input;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IAdaptable;
import org.eclipse.core.runtime.Path;
import org.eclipse.ui.IElementFactory;
import org.eclipse.ui.IMemento;

public class HykopsEditorInputFactory implements IElementFactory {

	@Override
	public IAdaptable createElement(IMemento memento) {
		String filePath = memento.getString(RDEFileHYKOPSEditorInput.MEMENTO_FILE_PATH);
		IFile file = ResourcesPlugin.getWorkspace().getRoot().getFileForLocation(new Path(filePath));
		return new RDEFileHYKOPSEditorInput(file);
	}
}
