/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.lpd.ui.editor.input;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.IAdapterFactory;

import de.fsg.tfe.rde.ui.editorinputs.RDEFileEditorInput;

/**
 * 
 *
 * @author stoye
 * @since Jun 28, 2016
 */
public class HYKOPSEditorInputAdapterFactory implements IAdapterFactory {

	@SuppressWarnings("rawtypes")
	private static final Class[] adapterList = new Class[] { IHYKOPSEditorInput.class };

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Object getAdapter(Object adaptableObject, Class adapterType) {
		if (adapterType.isAssignableFrom(IHYKOPSEditorInput.class)) {
			if (adaptableObject instanceof RDEFileEditorInput) {
				IFile rde = (((RDEFileEditorInput) adaptableObject).getFile());
				return new RDEFileHYKOPSEditorInput(rde);
			}
		}
		return null;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public Class[] getAdapterList() {
		return adapterList;
	}

}
