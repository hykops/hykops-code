/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.lpd.ui.editor.input;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;

import com.google.common.base.Charsets;
import com.google.common.io.CharStreams;

import de.fsg.tfe.rde.ui.editorinputs.IRepositoryIO;
import de.hykops.lpd.core.Activator;
import de.hykops.lpd.core.serialize.IDeserializer;
import de.hykops.lpd.core.serialize.ISerializer;
import de.hykops.lpd.datamodel.HComposition;
import de.hykops.lpd.ui.editor.transformations.TransformationHelper;

/**
 * 
 *
 * @author stoye
 * @since Jun 28, 2016
 */

public enum EHYKOPSExtensions implements IRepositoryIO<HComposition> {
	HYKOPS_EMF("HYKOPS", "Lofted Profile Data") {

		@Override
		public HComposition load(IFile file) throws IOException {

			try {
				IDeserializer deserializer = Activator.getFactoryService().getFactory().createDeserializer();
				String xml = CharStreams.toString(new InputStreamReader(file.getContents(), Charsets.UTF_8));
				EditingDomain eD = TransformationHelper.getEditingDomain();
				eD.getCommandStack().execute(new RecordingCommand((TransactionalEditingDomain) eD,
						"Loading composition") {
					@Override
					protected void doExecute() {
						HYKOPS_EMF.composition = deserializer.deserialize(xml);
					}
				});				
				return HYKOPS_EMF.getHComposition();
			} catch (CoreException e) {
				e.printStackTrace();
			}
			return null;
		}

		@Override
		public IStatus save(HComposition model, IFile file) throws IOException {

			ISerializer serializer = Activator.getFactoryService().getFactory().createSerializer();
			String xml = serializer.serialize(model);

			try {
				file.setContents(new ByteArrayInputStream(xml.getBytes()), IResource.FORCE, null);
			} catch (CoreException e) {
				e.printStackTrace();
			}
			return Status.OK_STATUS;
		}
	};

	HComposition composition;
	private String fileExtension;
	private String description;

	private EHYKOPSExtensions(String fileExtension_, String description_) {
		this.fileExtension = fileExtension_;
		this.description = description_;
	}

	@Override
	public String getOpenMethod() {
		return this.toString();
	}

	public static EHYKOPSExtensions getDefault() {
		return HYKOPS_EMF;
	}

	@Override
	public IRepositoryIO<HComposition> getRepositoryIOByFileExtension(String value) {
		for (EHYKOPSExtensions e : EHYKOPSExtensions.values()) {
			if (e.getFileExtension().equals(value)) {
				return e;
			}
		}
		return getDefault();
	}

	@Override
	public List<IRepositoryIO<HComposition>> getRepositoryIOs() {
		List<IRepositoryIO<HComposition>> l = new ArrayList<IRepositoryIO<HComposition>>();
		for (IRepositoryIO<HComposition> io : EHYKOPSExtensions.values()) {
			l.add(io);
		}
		return l;
	}

	@Override
	public String getFileExtension() {
		return this.fileExtension;
	}

	@Override
	public String getDescription() {
		return this.description;
	}

	private HComposition getHComposition()
	{
		return this.composition;
	}
}
