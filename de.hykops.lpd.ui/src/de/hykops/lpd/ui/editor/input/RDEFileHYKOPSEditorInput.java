/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.lpd.ui.editor.input;

import org.eclipse.core.resources.IFile;
import org.eclipse.ui.IMemento;
import org.eclipse.ui.IPersistableElement;

import de.fsg.tfe.rde.ui.editorinputs.AbstractEditorModelEditorInput;
import de.fsg.tfe.rde.ui.editorinputs.IRepositoryIO;
import de.hykops.lpd.datamodel.HComposition;

/**
 *
 * @author stoye
 * @since Jun 28, 2016
 */
public class RDEFileHYKOPSEditorInput extends AbstractEditorModelEditorInput<HComposition> implements IHYKOPSEditorInput, IPersistableElement {

	public static final String MEMENTO_FILE_PATH = "de.hykops.file.editor.input.memento.path";
	private static final String PERSIST_FACTORY_NAME = "de.hykops.file.editor.input.factory";

	public RDEFileHYKOPSEditorInput(IFile file_) {
		super(file_);
	}

	@Override
	public String getName() {
		return this.file.getName();
	}

	@Override
	public IRepositoryIO<HComposition> getRepositoryIO() {
		return EHYKOPSExtensions.getDefault();
	}

	@Override
	public void saveState(IMemento memento) {
		memento.putString(MEMENTO_FILE_PATH, getFile().getLocation().toPortableString());
	}

	@Override
	public String getFactoryId() {
		return PERSIST_FACTORY_NAME;
	}
}
