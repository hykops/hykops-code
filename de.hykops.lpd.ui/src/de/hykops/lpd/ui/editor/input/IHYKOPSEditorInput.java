/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.lpd.ui.editor.input;

import de.fsg.tfe.rde.ui.editorinputs.IEditorModelEditorInput;
import de.hykops.lpd.datamodel.HComposition;

/**
 * 
 *
 * @author stoye
 * @since Jun 28, 2016
 */
public interface IHYKOPSEditorInput extends IEditorModelEditorInput<HComposition> {
// TODO: Nothing
}
