/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.lpd.ui.editor.component;

import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.swt.widgets.Composite;

import de.fsg.tk.sdk.values.IValueAdapter;
import de.hykops.lpd.datamodel.HLoftElement;

/**
 * 
 *
 * @author stoye
 * @since Nov 29, 2016
 */
public abstract class ComponentProfileTableUIInterface implements IComponentProfileTableUIInterface {

	protected Composite						componentProfileTableComposite;
	protected IValueAdapter<HLoftElement>	componentProfileTableDataAdapter;
	protected ITreeContentProvider			loftElementContentProvider;

	private HLoftElement					selection;

	public ComponentProfileTableUIInterface(Composite parent_, IValueAdapter<HLoftElement> adapter, AdapterFactory adapterFactory) {
		this.componentProfileTableComposite = parent_;
		this.componentProfileTableDataAdapter = adapter;
		this.loftElementContentProvider = new AdapterFactoryContentProvider(adapterFactory) {
			@Override
			public boolean hasChildren(Object object) {
				return false;
			}

			@Override
			public Object[] getElements(Object object) {
				if (object instanceof List) {
					List<HLoftElement> elements = ((List<HLoftElement>) object);
					Object[] o3 = new Object[elements.size()];
					for (int i = 0; i < elements.size(); i++) {
						o3[i] = elements.get(i);
					}
					return o3;
				} else {
					return new Object[0];
				}
			}
		};

	}

	@Override
	public Composite getComponentProfileTableComposite() {
		return this.componentProfileTableComposite;
	}

	/**
	 * @return the selection
	 */
	public HLoftElement getSelection() {
		return this.selection;
	}

	/**
	 * @param selection
	 *            the selection to set
	 */
	public void setSelection(HLoftElement selection_) {
		this.selection = selection_;
	}

}
