/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.lpd.ui.editor.component;

/**
 * 
 *
 * @author stoye
 * @since Jul 22, 2016
 */
public class ComponentUI implements IComponentUI {

	private String	typeID		= "undefined";
	private String	libraryID	= "undefined";
	private String	provider	= "The HYKOPS consortium";
	private String	version		= "0.0";
	private String	description	= "undefined";
	private String	uiInterface	= "undefined";

	public ComponentUI(String typeID_, String libraryID_, String provider_, String version_, String description_, String uiInterface_) {
		this.typeID = typeID_;
		this.libraryID = libraryID_;
		this.provider = provider_;
		this.version = version_;
		this.description = description_;
		this.uiInterface = uiInterface_;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hykops.lpd.ui.editor.component.IComponentUI#getTypeID()
	 */
	@Override
	public String getTypeID() {
		return this.typeID;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hykops.lpd.ui.editor.component.IComponentUI#getLibraryID()
	 */
	@Override
	public String getLibraryID() {
		return this.libraryID;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hykops.lpd.ui.editor.component.IComponentUI#getProvider()
	 */
	@Override
	public String getProvider() {
		return this.provider;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hykops.lpd.ui.editor.component.IComponentUI#getVersion()
	 */
	@Override
	public String getVersion() {
		return this.version;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hykops.lpd.ui.editor.component.IComponentUI#getDescription()
	 */
	@Override
	public String getDescription() {
		return this.description;
	}


	
	public String getUIComponentInterface()
	{
		return this.uiInterface;
	}

}
