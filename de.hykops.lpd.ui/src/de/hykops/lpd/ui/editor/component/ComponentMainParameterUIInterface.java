/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.lpd.ui.editor.component;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.swt.widgets.Composite;

import de.fsg.tk.ui.widgets.IValueWidget;
import de.hykops.lpd.datamodel.HComponent;

/**
 * 
 *
 * @author stoye
 * @since Jul 22, 2016
 */
public abstract class ComponentMainParameterUIInterface implements IComponentMainParameterUIInterface {

	protected List<IValueWidget<?>>	mainParameterWidgets;
	protected Composite				mainParameterComposite;

	private HComponent				model;

	public ComponentMainParameterUIInterface(Composite parent_) {
		this.mainParameterComposite = parent_;
		this.mainParameterWidgets = new ArrayList<IValueWidget<?>>();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.hykops.lpd.ui.editor.component.IComponentMainParameterUIInterface#
	 * refresh()
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public void refreshMainParameterComposite() {
		if (this.mainParameterWidgets != null) {
			for (IValueWidget<?> widget : this.mainParameterWidgets) {
				IValueWidget w2 = widget;
				w2.setModel(getModel());
				w2.refresh();
			}
		}
	}

	public HComponent getModel() {
		return this.model;
	}

	/**
	 * @param model
	 *            the model to set
	 */
	@Override
	public void setModel(HComponent model_) {
		this.model = model_;
	}

	@Override
	public Composite getMainParameterComposite() {
		return this.mainParameterComposite;
	}
}
