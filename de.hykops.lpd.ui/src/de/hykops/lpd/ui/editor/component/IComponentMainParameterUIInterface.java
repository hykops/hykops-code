/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.lpd.ui.editor.component;

import org.eclipse.swt.widgets.Composite;

import de.hykops.lpd.datamodel.HComponent;

/**
 * 
 *
 * @author stoye
 * @since Jul 22, 2016
 */
public interface IComponentMainParameterUIInterface {
	public void setModel(HComponent model);
	public String getHRComponentType();
	public void createMainParameterControl();	
	public void refreshMainParameterComposite();
	public Composite getMainParameterComposite();
	
}
