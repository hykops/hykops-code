/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.lpd.ui.editor.component;

import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import de.fsg.tk.ui.model.EMFNotifier;
import de.fsg.tk.ui.table.ValueTable;
import de.hykops.lpd.datamodel.HComponent;
import de.hykops.lpd.datamodel.HLoftElement;

/**
 * 
 *
 * @author stoye
 * @since Nov 29, 2016
 */
public class GeneralComponentProfileTableInterface extends ComponentProfileTableUIInterface {

	ValueTable<HLoftElement>	loftElementTable;
	private HComponent			model;

	protected EMFNotifier		notifier	= new EMFNotifier(500, Job.LONG) {
												@Override
												protected void notifyListeners() {
													if (!GeneralComponentProfileTableInterface.this.getComponentProfileTableComposite().isDisposed()) {
														GeneralComponentProfileTableInterface.this.loftElementTable.refresh();
													}
												}
											};

	/**
	 * @param parent_
	 *
	 * @author stoye
	 * @since Nov 29, 2016
	 */
	public GeneralComponentProfileTableInterface(Composite parent_, de.fsg.tk.sdk.values.IValueAdapter<HLoftElement> dataAdapter_, AdapterFactory adapterFactory_) {
		super(parent_, dataAdapter_, adapterFactory_);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hykops.lpd.ui.editor.component.IComponentProfileTableUIInterface#
	 * createComponentProfileTableControl()
	 */
	@Override
	public void createComponentProfileTableControl() {
		Composite generalComposite = new Composite(this.componentProfileTableComposite, SWT.BORDER);
		generalComposite.setLayout(new GridLayout(2, true));
		generalComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		createLoftElementTable();

		this.componentProfileTableComposite.pack();
		this.componentProfileTableComposite.layout();

	}

	private void createLoftElementTable() {
		this.loftElementTable = new ValueTable<HLoftElement>(this.componentProfileTableComposite);
		this.loftElementTable.setValueAdapter(this.componentProfileTableDataAdapter);
		this.loftElementTable.setContentProvider(this.loftElementContentProvider);
		GridData gd = new GridData(GridData.FILL_BOTH);
		gd.heightHint = 200;
		gd.widthHint = 800;
		this.loftElementTable.setLayoutData(gd);
		this.loftElementTable.addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(SelectionChangedEvent selEvent) {
				if (selEvent.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection ssel = (IStructuredSelection) selEvent.getSelection();
					setSelection((HLoftElement) ssel.getFirstElement());
				}
			}

		});

	}

	@Override
	public void refreshComponentProfileTableComposite() {
		this.componentProfileTableComposite.layout();
		this.componentProfileTableComposite.redraw();
		if (this.model != null) {
			this.loftElementTable.setInput(this.model.getHLofts().get(0).getHLoftElements());
			this.loftElementTable.refresh();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hykops.lpd.ui.editor.component.IComponentProfileTableUIInterface#
	 * setModel(de.hykops.lpd.datamodel.HComponent)
	 */
	@Override
	public void setModel(HComponent model_) {
		this.model = model_;
		this.notifier.removeAllObservedModels();
		this.notifier.addObservedModel((EObject) this.model);

	}

}
