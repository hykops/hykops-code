/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.lpd.ui.editor.component;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;

/**
 * 
 *
 * @author stoye
 * @since Jul 26, 2016
 */
public class GeneralComponentMainParameterInterface extends ComponentMainParameterUIInterface {


	
	public GeneralComponentMainParameterInterface(Composite parent_)
	{
		super(parent_);
	}

	/* (non-Javadoc)
	 * @see de.hykops.lpd.ui.editor.component.IComponentMainParameterUIInterface#getHRComponentType()
	 */
	@Override
	public String getHRComponentType() {
		return "general component";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.hykops.lpd.ui.editor.component.IComponentMainParameterUIInterface#createControl()
	 */
	@Override
	public void createMainParameterControl() {
		Composite generalComposite = new Composite(this.mainParameterComposite, SWT.BORDER);
		generalComposite.setLayout(new GridLayout(2, true));
		generalComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		Label label = new Label(generalComposite, SWT.NONE);
		label.setText("Unknown component");
		
		this.mainParameterComposite.pack();
		this.mainParameterComposite.layout();
	}

	@Override
	public void refreshMainParameterComposite() {
		super.refreshMainParameterComposite();
	}


}
