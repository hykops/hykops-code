/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.lpd.ui.editor.component;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtension;
import org.eclipse.core.runtime.IExtensionPoint;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;

import de.fsg.tfe.rde.logging.LogLevel;
import de.fsg.tfe.rde.logging.RDELogger;
import de.fsg.tk.math.maps.Pair;
import de.hykops.lpd.ui.Activator;

/**
 * 
 *
 * @author stoye
 * @since Jul 22, 2016
 */
@SuppressWarnings("restriction")
public class ComponentUIProvider {

	private static final String	PATTERN_ID				= "de.hykops.component.libraryprovider";

	private static final String	ATT_TYPE_ID				= "type_ID";
	@SuppressWarnings("unused")
	private static final String	ATT_LIBRARY_ID			= "library_ID";
	@SuppressWarnings("unused")
	private static final String	ATT_PROVIDER			= "provider";
	@SuppressWarnings("unused")
	private static final String	ATT_VERSION				= "version";
	@SuppressWarnings("unused")
	private static final String	ATT_DESCRIPTION			= "description";
	private static final String	ATT_COMPONENT_INTERFACE	= "ui_interface";

	private static List<Pair<IConfigurationElement, IComponentInterfaceFactory>> getAvailableUIInterfaceFactories() {
		List<Pair<IConfigurationElement, IComponentInterfaceFactory>> components = new ArrayList<Pair<IConfigurationElement, IComponentInterfaceFactory>>();

		IExtensionRegistry ier = Platform.getExtensionRegistry();
		IExtensionPoint iep = ier.getExtensionPoint(PATTERN_ID);
		IExtension[] extensions = iep.getExtensions();

		for (int i = 0; i < extensions.length; i++) {
			IConfigurationElement[] configElements = extensions[i].getConfigurationElements();
			for (int j = 0; j < configElements.length; j++) {
				final IConfigurationElement conf = configElements[j];
				IComponentInterfaceFactory f;

				try {
					Class<IComponentInterfaceFactory> providerClass = getComponentInterfaceFactoryClass(conf);
					f = providerClass.newInstance();
					components.add(new Pair<IConfigurationElement, IComponentInterfaceFactory>(conf, f));
				} catch (InstantiationException e) {
					RDELogger.log(LogLevel.ERROR, Activator.getBundleContext(), "Cannot instantiate class " + conf.getAttribute(ATT_TYPE_ID), e);
				} catch (IllegalAccessException e) {
					RDELogger.log(LogLevel.ERROR, Activator.getBundleContext(), "Cannot access class " + conf.getAttribute(ATT_TYPE_ID), e);
				} catch (ClassNotFoundException e) {
					RDELogger.log(LogLevel.ERROR, Activator.getBundleContext(), "Cannot find class " + conf.getAttribute(ATT_TYPE_ID), e);
				}
			}
		}
		return components;
	}

	@SuppressWarnings("unchecked")
	private static Class<IComponentInterfaceFactory> getComponentInterfaceFactoryClass(IConfigurationElement conf) throws ClassNotFoundException {
		String providerName = conf.getAttribute(ATT_COMPONENT_INTERFACE);
		Class<IComponentInterfaceFactory> providerClass = (Class<IComponentInterfaceFactory>) Platform.getBundle(conf.getContributor().getName()).loadClass(providerName)
				.asSubclass(IComponentInterfaceFactory.class);
		return providerClass;
	}

	private static Pair<IConfigurationElement, IComponentInterfaceFactory> getComponentOfName(String typeID) {
		List<Pair<IConfigurationElement, IComponentInterfaceFactory>> list = getAvailableUIInterfaceFactories();
		for (Pair<IConfigurationElement, IComponentInterfaceFactory> p : list) {
			if (p.getT1().getAttribute(ATT_TYPE_ID).equals(typeID)) {
				return p;
			}
		}
		RDELogger.log(LogLevel.ERROR, Activator.getBundleContext(), "Unknown UI interface for HYKOPS component  of type " + typeID, null);
		return null;
	}

	public static IComponentInterfaceFactory getComponentInterfaceFactory(String typeID) {
		Pair<IConfigurationElement, IComponentInterfaceFactory> p = getComponentOfName(typeID);
		if (p != null) {
			return p.getT2();
		}
		return null;
	}

}
