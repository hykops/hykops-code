/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.lpd.ui.editor.transformations;

import de.hykops.lpd.datamodel.HComponent;
import de.hykops.lpd.datamodel.HComposition;
import de.hykops.lpd.datamodel.HCurve2D;
import de.hykops.lpd.datamodel.HLoft;
import de.hykops.lpd.datamodel.HLoftElement;
import de.hykops.lpd.datamodel.HSurfaceIdentifier;
import de.hykops.lpd.ui.Activator;
import de.hykops.profile.sdk.ProfileSupport;

/**
 * 
 *
 * @author stoye
 * @since Jul 4, 2016
 */
public class PrintCartesianCoordinates extends HYKOPSOperationProvider {

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fsg.tk.propeller.ui.editor.transformations.PropellerOperationProvider#
	 * performImpl(de.fsg.tk.propeller.datamodel.Propeller)
	 */
	@SuppressWarnings({ "boxing", "restriction" })
	@Override
	public void performImpl(HComposition composition) {

		int nc = 20;
		double coord[] = new double[nc];
		for (int i = 0; i < nc; i++) {
			double c = ((double) i) / ((double) (nc - 1));
			c = c * c;
			coord[i] = c;
		}

		for (HComponent component : composition.getHComponents()) {
			de.fsg.tfe.rde.logging.RDELogger.log(de.fsg.tfe.rde.logging.LogLevel.INFO, Activator.getBundleContext(), "# Component: " + component.getName());
			for (HLoft loft : component.getHLofts()) {
				de.fsg.tfe.rde.logging.RDELogger.log(de.fsg.tfe.rde.logging.LogLevel.INFO, Activator.getBundleContext(), "# Loft: " + loft.getName());
//				HLoftPath path = loft.getHLoftPath();
				for (HLoftElement element : loft.getHLoftElements()) {
					for (HCurve2D curve : element.getHCurve2Ds()) {
						for (HSurfaceIdentifier id : curve.getHSurfaceIdentifiers()) {
							de.fsg.tfe.rde.logging.RDELogger.log(de.fsg.tfe.rde.logging.LogLevel.INFO, Activator.getBundleContext(), "\n\n# Curve: " + id.getName());
						}
						for (int i = 0; i < nc; i++) {
							double c = coord[i];
							double xiEta[] = ProfileSupport.getXiEta(curve, c);
							double p[] = ProfileSupport.getCartesianCoordinate(curve, c);
							if ((xiEta != null) && (p != null)) {
								de.fsg.tfe.rde.logging.RDELogger.log(de.fsg.tfe.rde.logging.LogLevel.INFO, Activator.getBundleContext(), String.format("%12.4f %12.4f %12.4f %12.4f %12.4f", xiEta[0], xiEta[1], p[0], p[1], p[2]));
							}
						}
					}
				}
			}
		}
	}

}
