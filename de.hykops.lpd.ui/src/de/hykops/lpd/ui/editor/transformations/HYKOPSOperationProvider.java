/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.lpd.ui.editor.transformations;

import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.IWorkbenchPart;

import de.fsg.tfe.rde.ui.editor.transformations.EditorModelOperationProvider;
import de.fsg.tfe.rde.ui.editorinputs.IEditorModelEditorInput;
import de.hykops.lpd.datamodel.HComponent;
import de.hykops.lpd.datamodel.HComposition;
import de.hykops.lpd.ui.editor.input.MosHYKOPSEditorInput;

/**
 * 
 *
 * @author stoye
 * @since Jun 28, 2016
 */
public abstract class HYKOPSOperationProvider extends EditorModelOperationProvider<HComposition> {
	
	private HComponent component = null;

	@Override
	public abstract void performImpl(HComposition composition);

	@Override
	public Class<HComposition> getEditorModelClass() {
		return HComposition.class;
	}

	@Override
	public String getMainEditorID() {
		return "de.hykops.lpd.ui.editor.hykopsMainEditor";
	}

	@Override
	public String getEditorModelName() {
		return "HYKOPS";
	}

	@Override
	public IEditorModelEditorInput<HComposition> getEditorModelEditorInput(HComposition composition, String editorName) {
		return new MosHYKOPSEditorInput(composition);
	}

	@Override
	public void selectionChanged(IWorkbenchPart part, ISelection selection) {
		super.selectionChanged(part, selection);

		if (selection instanceof IStructuredSelection) {
			IStructuredSelection ssel = (IStructuredSelection) selection;
			Object firstElement = ssel.getFirstElement();
			if (firstElement instanceof HComponent) {
				HComponent component_ = (HComponent) firstElement;
				setComponent(component_);
			}
		}
	}

	public void setComponent(HComponent component_) {
		this.component = component_;
	}

	public HComponent getComponent() {
		return this.component;
	}

}
