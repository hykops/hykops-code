/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.lpd.ui.editor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.eclipse.emf.common.notify.AdapterFactory;
import org.eclipse.emf.edit.ui.provider.AdapterFactoryContentProvider;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.forms.widgets.ExpandableComposite;
import org.eclipse.ui.forms.widgets.Form;
import org.eclipse.ui.forms.widgets.FormToolkit;
import org.eclipse.ui.forms.widgets.Section;

import de.fsg.tk.ui.table.ValueTable;
import de.hykops.lpd.datamodel.HComponent;
import de.hykops.lpd.ui.dataaccess.ComponentDataAdaptor;
import de.hykops.lpd.ui.editor.component.ComponentUIProvider;
import de.hykops.lpd.ui.editor.component.IComponentInterfaceFactory;
import de.hykops.lpd.ui.editor.component.IComponentMainParameterUIInterface;

/**
 * 
 *
 * @author stoye
 * @since Nov 21, 2016
 */
public class ComponentMainDataPage {

	private Composite				parent;
	private Form					form;
	private SashForm				sash;
	private Composite				cup, cdn;

	private ComponentDataAdaptor	dataAdaptor;
	private ITreeContentProvider	contentProvider;

	private ValueTable<HComponent>	componentTable;

	HComponent						selection;
	Section							componentSection;
	Composite						activeComposite;
	Label							label;

	public ComponentMainDataPage(Composite parent_, AdapterFactory adapterFactory) {
		this.parent = parent_;

		this.dataAdaptor = new ComponentDataAdaptor();

		FormToolkit toolkit = new FormToolkit(this.parent.getDisplay());
		this.form = toolkit.createForm(this.parent);
		this.form.getBody().setLayout(new FillLayout());

		this.contentProvider = new AdapterFactoryContentProvider(adapterFactory) {
			@Override
			public boolean hasChildren(Object object) {
				return false;
			}

			@Override
			public Object[] getElements(Object object) {
				List<Object> o1 = Arrays.asList(super.getElements(object));
				List<HComponent> o2 = new ArrayList<HComponent>();
				for (Object o : o1) {
					if (o instanceof HComponent) {
						o2.add((HComponent) o);
					}
				}
				Object[] o3 = new Object[o2.size()];
				for (int i = 0; i < o2.size(); i++) {
					o3[i] = o2.get(i);
				}
				return o3;
			}
		};

		this.sash = new SashForm(this.form.getBody(), SWT.VERTICAL | SWT.SMOOTH);
		this.sash.setLayout(new FillLayout(SWT.HORIZONTAL));

		this.cup = new Composite(this.sash, SWT.BORDER);
		this.cup.setLayout(new GridLayout(1, false));
		Section cupSection = toolkit.createSection(this.cup, ExpandableComposite.TITLE_BAR);
		cupSection.clientVerticalSpacing = 10;
		cupSection.setText("Component");
		cupSection.setDescription("Component name and location");

		toolkit.createCompositeSeparator(cupSection);
		makeComponentSelectionTable();

		this.cdn = new Composite(this.sash, SWT.BORDER);
		this.cdn.setLayout(new GridLayout(1, false));
		this.componentSection = toolkit.createSection(this.cdn, ExpandableComposite.TITLE_BAR);
		this.componentSection.clientVerticalSpacing = 10;
		this.componentSection.setText("Component-specific main data");
		this.componentSection.setDescription("Component-specific main data sheet");
		this.label = new Label(this.cdn, SWT.NONE);
		this.label.setText("Nothing selected");

		this.sash.setWeights(new int[] { 45, 55 });
	}

	private void makeComponentSelectionTable() {
		this.componentTable = new ValueTable<HComponent>(this.cup);
		this.componentTable.setValueAdapter(this.dataAdaptor);
		this.componentTable.setContentProvider(this.contentProvider);
		GridData gd = new GridData(GridData.FILL_BOTH);
		gd.heightHint = 100;
		gd.widthHint = 200;
		this.componentTable.setLayoutData(gd);
		this.componentTable.addSelectionChangedListener(new ISelectionChangedListener() {
			@Override
			public void selectionChanged(SelectionChangedEvent selEvent) {
				if (selEvent.getSelection() instanceof IStructuredSelection) {
					IStructuredSelection ssel = (IStructuredSelection) selEvent.getSelection();
					ComponentMainDataPage.this.selection = (HComponent) ssel.getFirstElement();
					ComponentMainDataPage.this.updateUIInterfaceSection();
				}
			}
		});
	}

	public void updateUIInterfaceSection() {
		if (this.selection != null) {
			String typeID = this.selection.getType();
			IComponentInterfaceFactory cif = ComponentUIProvider.getComponentInterfaceFactory(typeID);
			if (cif != null) {

				this.label.dispose();
				if (this.activeComposite != null) {
					this.activeComposite.dispose();
				}
				Composite comp = new Composite(this.cdn, SWT.NONE);
				comp.setLayout(new GridLayout(1, false));

				IComponentMainParameterUIInterface ci = cif.getNewMainParameterComposite(comp);
				ci.createMainParameterControl();
				this.cdn.layout();
				ci.setModel(this.selection);
				ci.refreshMainParameterComposite();
				this.activeComposite = ci.getMainParameterComposite();
				this.componentSection.setText("Interface for " + ci.getHRComponentType());
			}
		}
	}

	public void refresh() {
		this.parent.layout();
		this.parent.redraw();
	}

	public ValueTable<HComponent> getComponentTable() {
		return this.componentTable;
	}

	public Composite getComposite() {
		return this.form;
	}
}
