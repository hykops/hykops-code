/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.lpd.ui.support;

import org.eclipse.ui.IEditorPart;

import de.fsg.tfe.rde.ui.editorinputs.IEditorModelEditorInput;
import de.fsg.tfe.rde.ui.support.AbstractEditorModelUISupport;
import de.hykops.lpd.datamodel.HComposition;
import de.hykops.lpd.ui.editor.input.MosHYKOPSEditorInput;

/**
 * 
 *
 * @author stoye
 * @since Jun 28, 2016
 */
public class CompositionUISupport extends AbstractEditorModelUISupport<HComposition> {

	public CompositionUISupport() {
		super(HComposition.class, null, null);
	}

	/**
	 * 
	 * @param model
	 * @param maxChars
	 * @return
	 */
	public String getSuitableName(HComposition model, int maxChars) {
		IEditorPart part = getEditorPartFromDatamodel(model);
		if (part == null) // from SHIPDATA
		{
			return getSuitableName(model, true, maxChars);
		}
		return getSuitableName(model, false, maxChars);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fsg.tfe.rde.ui.support.IEditorModelUISupport#getSuitableName(java.lang
	 * .Object, boolean)
	 */
	@Override
	public String getSuitableName(HComposition model, boolean fromShipData, int maxChars) {
		if (fromShipData) {
			return "undefined";
		}
		IEditorPart part = getEditorPartFromDatamodel(model);
		if (part != null) {
			String name = EDITOR_LABEL + part.getTitle();
			if (name.length() > maxChars) {
				name = name.substring(0, maxChars - 1);
			}
			return name;
		} /*- if part.getTitle() is called before the editor is instanciated:*/
		return EDITOR_LABEL + "NOT YET OPENED";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fsg.tfe.rde.ui.support.IEditorModelUISupport#getSuitableDescription
	 * (java .lang.Object)
	 */
	@Override
	public String getSuitableDescription(HComposition model) {
		return model.getDescription().trim();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.fsg.tfe.rde.ui.support.IEditorModelUISupport#createEditorModelInput
	 * (java.lang.Object, java.lang.String)
	 */
	@Override
	public IEditorModelEditorInput<HComposition> createEditorModelInput(HComposition model, String name) {
		return new MosHYKOPSEditorInput(model);
	}
	
}
