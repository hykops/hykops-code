/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.lpd.ui.wizard;

import org.eclipse.jface.viewers.ArrayContentProvider;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.jface.viewers.ISelectionChangedListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.LabelProvider;
import org.eclipse.jface.viewers.SelectionChangedEvent;
import org.eclipse.jface.viewers.TableViewer;
import org.eclipse.jface.wizard.WizardPage;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Text;

import de.hykops.lpd.datamodel.HComposition;

/**
 * 
 *
 * @author stoye
 * @since Jun 28, 2016
 */
public class TemplatePage extends WizardPage implements ISelectionChangedListener {
	private TableViewer			templateList;
	private Text				description;
	private CompositionTemplate	actualSelected	= null;

	public TemplatePage(String pageName) {
		super(pageName);
		setTitle(pageName);
		setDescription("Select one of the templates for the new file.");
		setPageComplete(false); // disable the finish-button
	}

	@Override
	public void createControl(Composite parent) {
		Composite pageContainer = new Composite(parent, SWT.NULL);
		pageContainer.setLayout(new GridLayout(1, true));

		Label label = new Label(pageContainer, SWT.NONE);
		label.setText("Available templates:");
		label.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		Composite templateContainer = new Composite(pageContainer, SWT.NULL);
		templateContainer.setLayoutData(new GridData(GridData.FILL_BOTH));
		templateContainer.setLayout(new GridLayout(2, true));
		this.templateList = new TableViewer(templateContainer);
		this.templateList.getTable().setLayoutData(new GridData(GridData.FILL_BOTH));
		this.description = new Text(templateContainer, SWT.MULTI | SWT.WRAP | SWT.BORDER | SWT.READ_ONLY);
		this.description.setLayoutData(new GridData(GridData.FILL_BOTH));

		this.templateList.setContentProvider(new ArrayContentProvider());
		this.templateList.setLabelProvider(new LabelProvider() {
			@Override
			public String getText(Object element) {
				if (element instanceof CompositionTemplate) {
					return ((CompositionTemplate) element).getName();
				}
				return super.getText(element);
			}

			@Override
			public Image getImage(Object element) {
				if (element instanceof CompositionTemplate) {
					return ((CompositionTemplate) element).getIcon();
				}
				return super.getImage(element);
			}
		});

		this.templateList.setInput(new CompositionTemplateFactory().getTemplates().toArray());
		this.templateList.addSelectionChangedListener(this);

		setControl(pageContainer);
	}

	@Override
	public void selectionChanged(SelectionChangedEvent e) {
		IStructuredSelection selection = (IStructuredSelection) this.templateList.getSelection();
		Object element = selection.getFirstElement();
		if (element != null && element instanceof CompositionTemplate) {
			this.actualSelected = (CompositionTemplate) element;
			this.description.setText(this.actualSelected.getDescription());
			setPageComplete(true); // enable finish-button
		} else {
			this.actualSelected = null;
			this.description.setText("");
			setPageComplete(false); // diable finish-button
		}
	}

	public HComposition getComposition()
	{
		if(this.actualSelected == null)
		{
			return EWizardTemplates.DEFAULT.getComposition();
		}
		return this.actualSelected.getComposition();
	}

	@SuppressWarnings("unused")
	private static CompositionTemplate toCompositionTemplate(ISelection selection) {
		if (selection == null)
			return null;
		IStructuredSelection sselection = (IStructuredSelection) selection;
		Object element = sselection.getFirstElement();
		if (element instanceof CompositionTemplate)
			return (CompositionTemplate) element;
		return null;
	}

}
