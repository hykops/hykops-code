/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.lpd.ui.wizard;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.IExtension;
import org.eclipse.core.runtime.IExtensionRegistry;
import org.eclipse.core.runtime.Platform;

/**
 * 
 *
 * @author stoye
 * @since Jun 28, 2016
 */
public class CompositionTemplateFactory {

	private static final String	TAG_TEMPLATE			= "template";

	private final String		TEMPLATEEXTENSIONPOINT	= "de.hykops.lpd.ui.templates";

	public List<CompositionTemplate> getTemplates() {
		IExtensionRegistry extensionRegistry = Platform.getExtensionRegistry();
		IExtension[] extensions = extensionRegistry.getExtensionPoint(this.TEMPLATEEXTENSIONPOINT).getExtensions();
		List<CompositionTemplate> results = new ArrayList<CompositionTemplate>();
		for (IExtension extension : extensions) {
			IConfigurationElement[] configElements = extension.getConfigurationElements();
			for (IConfigurationElement configurationElement : configElements) {
				if (configurationElement.getName().equals(TAG_TEMPLATE)) {
					results.add(new CompositionTemplate(configurationElement));
				}
			}
		}
		return Collections.unmodifiableList(results);
	}

}
