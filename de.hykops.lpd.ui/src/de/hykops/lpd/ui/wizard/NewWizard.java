/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.lpd.ui.wizard;

import org.eclipse.ui.wizards.newresource.BasicNewFileResourceWizard;

import de.fsg.tfe.rde.ui.support.AbstractEditorModelUISupport;
import de.hykops.lpd.datamodel.HComposition;
import de.hykops.lpd.ui.Perspective;
import de.hykops.lpd.ui.editor.CompositionMainEditor;
import de.hykops.lpd.ui.support.CompositionUISupport;

/**
 * 
 *
 * @author stoye
 * @since Jun 28, 2016
 */
@SuppressWarnings("restriction")
public class NewWizard extends BasicNewFileResourceWizard {
	private TemplatePage templatePage;

	@Override
	public void addPages() {
		this.templatePage = new TemplatePage("Templates");
		this.templatePage.setTitle("New HYKOPS composition");
		this.templatePage.setTitle("Create a new file of type HYKOPS lofted profile data");
		addPage(this.templatePage);
	}

	@Override
	public boolean performFinish() {
		final HComposition composition = this.templatePage.getComposition();

		AbstractEditorModelUISupport.openEditor(new CompositionUISupport().createEditorModelInput(composition, "Example HYKOPS lofted profile data"), CompositionMainEditor.ID, Perspective.ID);

		return true;
	}

}
