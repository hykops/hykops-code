/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.lpd.ui.wizard;

import java.net.URL;

import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.IConfigurationElement;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.swt.graphics.Image;
import org.osgi.framework.Bundle;

import de.hykops.lpd.datamodel.HComposition;

/**
 * 
 *
 * @author stoye
 * @since Jun 28, 2016
 */
public class CompositionTemplate {
	private static final String		ATT_EWIZARD_TEMPLATES_ENTRY	= "wizardtemplatesentry";
	private static final String		ATT_ICON					= "icon";

	private IConfigurationElement	configurationElement;

	public CompositionTemplate(IConfigurationElement configurationElement_) {
		this.configurationElement = configurationElement_;
	}

	public String getName() {
		return EWizardTemplates.valueOf(this.configurationElement.getAttribute(ATT_EWIZARD_TEMPLATES_ENTRY)).getName();
	}

	public String getDescription() {
		return EWizardTemplates.valueOf(this.configurationElement.getAttribute(ATT_EWIZARD_TEMPLATES_ENTRY)).getName();
	}

	public HComposition getComposition() {
		return EWizardTemplates.valueOf(this.configurationElement.getAttribute(ATT_EWIZARD_TEMPLATES_ENTRY)).getComposition();
	}

	public Image getIcon() {
		String iconPath = this.configurationElement.getAttribute(ATT_ICON);
		if (iconPath != null) {
			Bundle bundle = Platform.getBundle(this.configurationElement.getContributor().getName());
			URL url = FileLocator.find(bundle, new Path(iconPath), null);
			if (url != null)
				return ImageDescriptor.createFromURL(url).createImage();
		}
		return null;
	}

}
