/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.lpd.ui.wizard;

import de.hykops.lpd.core.Activator;
import de.hykops.lpd.core.factory.IHykopsFactory;
import de.hykops.lpd.datamodel.HComposition;
import de.hykops.lpd.datamodel.HCoordinateSystem3DAffine;
import de.hykops.lpd.datamodel.HPoint3D;

/**
 * 
 *
 * @author stoye
 * @since Jun 29, 2016
 */
public enum EWizardTemplates implements IWizardTemplates {

	EMPTY("Empty", "Empty composition with cartesian coordinate system") {
		@Override
		public HComposition getComposition() {
			
			IHykopsFactory factory = Activator.getFactoryService().getFactory();
			HComposition composition = factory.createComposition();
			composition.setName("Example HYKOPS composition");
			composition.setDescription("Example composition");
			HCoordinateSystem3DAffine kos = factory.createCoordinateSystemAffine();
			HPoint3D orig = factory.createPoint3D();
			orig.setX1(0.);
			orig.setX2(0.);
			orig.setX3(0.);
			HPoint3D x1 = factory.createPoint3D();
			x1.setX1(1.);
			x1.setX2(0.);
			x1.setX3(0.);
			HPoint3D x2 = factory.createPoint3D();
			x2.setX1(0.);
			x2.setX2(1.);
			x2.setX3(0.);
			HPoint3D x3 = factory.createPoint3D();
			x3.setX1(0.);
			x3.setX2(0.);
			x3.setX3(1.);
			kos.setOrigin(orig);
			kos.setX1(x1);
			kos.setX2(x2);
			kos.setX3(x3);
			composition.setHCoordinateSystem3D(kos);

			composition.setUnitSystem("SI");
			composition.setVersion(0);
			return composition;
		}
	},
	DEFAULT("Default", "Default") {
		@Override
		public HComposition getComposition() {
			
			IHykopsFactory factory = Activator.getFactoryService().getFactory();
			HComposition composition = factory.createComposition();
			composition.setName("Example HYKOPS composition");
			composition.setDescription("Example composition");
			HCoordinateSystem3DAffine kos = factory.createCoordinateSystemAffine();
			HPoint3D orig = factory.createPoint3D();
			orig.setX1(0.);
			orig.setX2(0.);
			orig.setX3(0.);
			HPoint3D x1 = factory.createPoint3D();
			x1.setX1(1.);
			x1.setX2(0.);
			x1.setX3(0.);
			HPoint3D x2 = factory.createPoint3D();
			x2.setX1(0.);
			x2.setX2(1.);
			x2.setX3(0.);
			HPoint3D x3 = factory.createPoint3D();
			x3.setX1(0.);
			x3.setX2(0.);
			x3.setX3(1.);
			kos.setOrigin(orig);
			kos.setX1(x1);
			kos.setX2(x2);
			kos.setX3(x3);
			composition.setHCoordinateSystem3D(kos);

			composition.setUnitSystem("SI");
			composition.setVersion(0);
			return composition;
		}
	};
	private EWizardTemplates(String name_, String description_) {

		this.name = name_;
		this.description = description_;
	}

	String	name;
	String	description;

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public String getDescription() {
		return this.description;
	}
}
