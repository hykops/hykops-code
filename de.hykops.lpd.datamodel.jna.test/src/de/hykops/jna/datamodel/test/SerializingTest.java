package de.hykops.jna.datamodel.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.net.URL;

import org.junit.Test;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;

import de.hykops.lpd.core.Activator;
import de.hykops.lpd.core.factory.IHykopsFactory;
import de.hykops.lpd.core.serialize.ISerializer;
import de.hykops.lpd.datamodel.HComponent;
import de.hykops.lpd.datamodel.HComposition;
import de.hykops.lpd.datamodel.HCoordinateSystem3D;
import de.hykops.lpd.datamodel.HCoordinateSystem3DAffine;
import de.hykops.lpd.datamodel.HPoint3D;

public class SerializingTest {

	@Test
	public void serialize() {

		IHykopsFactory factory = Activator.getFactoryService().getFactory();

		ISerializer serializer = factory.createSerializer();
		HComposition composition = factory.createComposition();

		String xml = serializer.serialize(composition);
		assertTrue(!xml.isEmpty());
	}

	@Test
	public void deserialize() throws IOException {

		IHykopsFactory factory = Activator.getFactoryService().getFactory();

		URL resource = Resources.getResource("models/Model.hykops");
		String xml = Resources.toString(resource, Charsets.UTF_8);

		HComposition composition = factory.createDeserializer().deserialize(xml);

		assertTrue(composition.getHandle().intValue() != 0);
		assertEquals(composition.getName(), "Propeller import");

		HComponent component1 = composition.getHComponents().get(0);
		assertEquals(component1.getDescription(), "TestDescription");
		HComposition compostionAsParent = (HComposition) component1.getHParent();
		assertTrue(compostionAsParent.getHComponents().size() == 2);
		assertEquals(component1.getHParent(), composition);

		HComposition tmpComp = (HComposition) component1.getHComposition();
		assertEquals(tmpComp.getName(), "Propeller import");
		assertEquals(component1.getHLofts().get(0).getName(), "blade profile loft");

		HCoordinateSystem3D cos = component1.getHCoordinateSystem3D();
		assertTrue(cos instanceof HCoordinateSystem3DAffine);
		assertEquals(cos.getHParent(), component1);

		HCoordinateSystem3DAffine affine = (HCoordinateSystem3DAffine) cos;
		HPoint3D origin = affine.getOrigin();

		assertEquals(cos, origin.getHParent());
		assertEquals(0.6, origin.getX1(), 0);
		assertEquals(4.0, origin.getX2(), 0);
		assertEquals(2.7, origin.getX3(), 0);

		HPoint3D x1 = affine.getX1();
		assertEquals(x1.getHParent(), cos);
		assertEquals(1.0, x1.getX1(), 0);
		assertEquals(0.0, x1.getX2(), 0);
		assertEquals(0.0, x1.getX3(), 0);

		assertEquals("Value1", component1.getParameters().get("Key1"));

	}
}
