package de.hykops.jna.datamodel.test;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

import de.hykops.lpd.core.Activator;
import de.hykops.lpd.core.factory.IHykopsFactory;
import de.hykops.lpd.core.serialize.ISerializer;
import de.hykops.lpd.datamodel.HComponent;
import de.hykops.lpd.datamodel.HComposition;

public class StringSizeTest {
	
	private static final String COMPOSITION_XML_END_TAG = "</de.hykops.lpd.datamodel:HComposition>";

	@Test
	public void endsWithoutSpace() {

		IHykopsFactory factory = Activator.getFactoryService().getFactory();
		HComposition composition = factory.createComposition();
		ISerializer serializer = factory.createSerializer();

		HComponent component = factory.createComponent();
		component.setHParent(composition);
		composition.getHComponents().add(component);

		String xml = serializer.serialize(composition);
		assertTrue(xml.trim().endsWith(COMPOSITION_XML_END_TAG));
	}

}
