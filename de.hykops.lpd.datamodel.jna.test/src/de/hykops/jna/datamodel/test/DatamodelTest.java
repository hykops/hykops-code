package de.hykops.jna.datamodel.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Test;

import de.hykops.jna.datamodel.core.HykopsCore;
import de.hykops.lpd.core.Activator;
import de.hykops.lpd.core.factory.IHykopsFactory;
import de.hykops.lpd.datamodel.HComponent;
import de.hykops.lpd.datamodel.HComposition;
import de.hykops.lpd.datamodel.HCoordinateSystem3D;
import de.hykops.lpd.datamodel.HCoordinateSystem3DAffine;
import de.hykops.lpd.datamodel.HLoft;
import de.hykops.lpd.datamodel.HPoint3D;

public class DatamodelTest {

	private static final String COMPOSITION_NAME = "ExampleCompositionName";
	private static final String PROPELLER_DESCRIPTION = "I'm a propelleeeeer description";

	@Test
	public void assembleComponent() {

		HykopsCore.INSTANCE.init();
		IHykopsFactory factory = Activator.getFactoryService().getFactory();
		
		HComposition composition = factory.createComposition();
		composition.setName(COMPOSITION_NAME);
		assertEquals(COMPOSITION_NAME, composition.getName());

		HComponent component = factory.createComponent();
		component.setHParent(composition);
		assertEquals(composition, component.getHParent());

		component.setHComposition(composition);
		assertEquals(composition, component.getHComposition());

		component.setDescription(PROPELLER_DESCRIPTION);
		assertEquals(PROPELLER_DESCRIPTION, component.getDescription());

		HCoordinateSystem3DAffine cos = factory.createCoordinateSystemAffine();
		component.setHCoordinateSystem3D(cos);
		cos.setHParent(component);

		assertEquals(component.getHCoordinateSystem3D(), cos);
		assertEquals(cos.getHParent(), component);

		HPoint3D origin = factory.createPoint3D();
		origin.setHParent(cos);
		origin.setX1(1.0);
		origin.setX2(1.0);
		origin.setX3(1.0);
		cos.setOrigin(origin);

		HPoint3D x1 = factory.createPoint3D();
		x1.setHParent(cos);
		x1.setX1(1337.);
		x1.setX2(666.);
		x1.setX3(9999.);
		cos.setX1(x1);

		List<HComponent> components = composition.getHComponents();
		components.add(component);
		assertEquals(composition.getHComponents(), components);

		List<HLoft> lofts = component.getHLofts();
		HLoft loft1 = factory.createLoft();
		loft1.setName("LoftOne");
		loft1.setHParent(component);
		lofts.add(loft1);
		HLoft loft2 = factory.createLoft();
		loft2.setName("LoftTwo");
		lofts.add(loft2);
		loft2.setHParent(component);

		HCoordinateSystem3D affine_loft1 = factory.createCoordinateSystemAffine();
		affine_loft1.setHParent(loft1);

		HCoordinateSystem3D polar_loft1 = factory.createCoordinateSystemPolar();
		polar_loft1.setHParent(loft1);

		List<HCoordinateSystem3D> loft_cos = loft1.getHCoordinateSystems3D();
		loft_cos.add(affine_loft1);
		loft_cos.add(polar_loft1);

		component.getParameters().put("Key1", "Value1");
		component.getParameters().put("Key2", "Value2");
		
		
		assertTrue(component.getParameters().containsKey("Key1") && component.getParameters().containsKey("Key2"));
		assertEquals(component.getParameters().get("Key1"), "Value1");
		assertEquals(component.getParameters().get("Key2"), "Value2");


		assertEquals(component.getHLofts().size(), 2);
		assertEquals(component.getHLofts().get(0).getName(), "LoftOne");
		assertEquals(component.getHLofts().get(1).getName(), "LoftTwo");
		
	}

}
