package de.hykops.jna.datamodel.test;

import static org.junit.Assert.assertEquals;
import java.io.IOException;
import java.net.URL;

import org.junit.Test;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;

import de.hykops.jna.datamodel.core.HykopsCore;
import de.hykops.lpd.core.Activator;
import de.hykops.lpd.core.serialize.IDeserializer;
import de.hykops.lpd.datamodel.HComposition;

public class CoordinateTransformationTest {

	@Test
	public void ToGlobalCos() throws IOException {

		IDeserializer dserializer = Activator.getFactoryService().getFactory().createDeserializer();

		URL resource = Resources.getResource("models/Model.hykops");
		String xml = Resources.toString(resource, Charsets.UTF_8);
		HComposition composition = dserializer.deserialize(xml);

		double[] result = new double[3];
		HykopsCore.INSTANCE.toGlobalCoordinate(composition.getHComponents().get(0).getHLofts().get(0).getHLoftElements().get(0).getHandle(), 2, 5, result);

		assertEquals(2.631855010986328, result[0], 0);
		assertEquals(result[1], 3.527759552001953, 0);
		assertEquals(result[2], 3.3281941413879395, 0);
	}
}
