# README #

The objective of the collaborative project is the creation of geometric standards and design methods for hydrodynamic components within the framework of the development of a uniform framework for developers, test institutes and large-scale production in the network. HYKOPS is a collaborative scientific research project that will yield a new open source file format for storing ship propellers. Up until now, all existing formats are not suited for dealing with the specifics of ship propellers, ducts and rudders for various reasons. The file format is basically a C++ library that can be linked against in any modern programming language and hence used in the user's workflow to his or her liking.
The entire project is work in process (very early alpha) and registered under GPL2.

## How to install ##


1. Clone the repository:

    ```
    ?> git clone git@bitbucket.org:hykops/hykops-code.git
    ```

2. Execute the build script on all cores of your machine:

    ```
    ?>  ./buildAll.sh
    ```
    Or define the number of cores you would like to run the build script to use:

    ```
    ?> ./buildAll.sh 1
    ```
    Which would solely use a single core for the compilation.

### Dependencies ###

* It is imporant that you have a C++11 compatible compiler installed on your system!
* QT 4 needs to be installed

## Who do I talk to? ##

* [Jens Hoepken](hoepken@dst-org.de)
* [Thomas Stoye](thomas.stoye@fsg.de)