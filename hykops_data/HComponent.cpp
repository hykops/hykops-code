#include "HComponent.h"

#include "HLoft.h"
#include "HComposition.h"
#include "HParent.h"
#include "HCoordinateSystem3D.h"

HComponent::HComponent()
 : mName(QString()),
   mType(QString()),
   mDescription(QString()),
   mParameters(new QMap<QString,QString>()),
   mHCoordinateSystem3D(0),
   mHLofts(new QList<HLoft*>())
{;}

