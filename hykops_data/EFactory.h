#ifndef __EFACTORY_H
#define __EFACTORY_H

#include <QObject>
#include <QMetaType>
#include <QList>
#include <QVariant>

class EFactory
{
 public:
   static bool initialized;
   static void init();
   static QObject* construct(const QString& className);
   static QVariant toVariant(const QString& className, QObject* obj);
   static QObject* getObject(const QString& className, const QVariant& vObj);
   static void setObject(const QString& className, QVariant& vObj, QObject *object);
   static QList<QObject*>* getList(const QString& className, const QVariant& vList);
   static void registerListTypes();
};

#endif // __EFACTORY_H
