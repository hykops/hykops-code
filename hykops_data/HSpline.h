#ifndef __HSPLINE_H
#define __HSPLINE_H

#include <QObject>
#include <QList>

#include "HParent.h"
#include "HPoint3D.h"


//! Abstract base class for all spline (or piecewise polynomial interpolations).
/*! 
 * Derived classes must only implement the update method, in order to provide
 * a new logic. Nothing else needs to be done.
 */

class HSpline
:
    public HParent
{
    Q_OBJECT

    //! Stores all knots for the piecewise polynomial interpolation
    const QList<HPoint3D> points_;

public:

    HSpline
    (
        const QList<HPoint3D> points
    );
    
    //! Returns the position on the spline, determined by the relative position
    /*!
     * @param const int& position is the relative position on the spline \f$ p \in [0;1] \f$
    */
    virtual void update(const double& position) = 0;

    //! Returns the position on the spline, determined by a point in space
    /*!
     * @param const HPoint3D& position is a point in space which should be in
     * the vicinity of the spline. It is projected onto the spline.
    */
    virtual void update(const HPoint3D& position) = 0;

    //! Provides constant to the knots of the spline
    const QList<HPoint3D>& points() const;

};

Q_DECLARE_METATYPE(HSpline*)

#endif // __HSPLINE_H
