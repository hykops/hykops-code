#include "HCoordinateSystem3DCone.h"

#include "HPoint3D.h"
#include "HCoordinateSystem3D.h"

HCoordinateSystem3DCone::HCoordinateSystem3DCone()
 : mAxialVector(0),
   mRadialVector(0),
   mAngle(0)
{;}

