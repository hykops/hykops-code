#ifndef __HDISCRETECURVE2D_H
#define __HDISCRETECURVE2D_H

#include <QObject>
#include <QDateTime>
#include <QMetaType>
#include <QString>

#include "HCurve2D.h"

class HCurvePoint2D;

class HDiscreteCurve2D : public HCurve2D
{
  Q_OBJECT
  Q_PROPERTY(QString interpolationScheme READ getInterpolationScheme WRITE setInterpolationScheme STORED true)
  Q_PROPERTY(QList<HCurvePoint2D*>* curvePoints READ getCurvePoints STORED true)

  QString mInterpolationScheme;
  QList<HCurvePoint2D*>* mCurvePoints;
#include "HDiscreteCurve2D_user.h"
public:
  HDiscreteCurve2D();
  HDiscreteCurve2D(const HDiscreteCurve2D&) : HCurve2D() {;}
// getter:
  QString getInterpolationScheme() const { return mInterpolationScheme; }
  QList<HCurvePoint2D*>* getCurvePoints() const { return mCurvePoints; }
// setter:
  void setInterpolationScheme(QString val) { mInterpolationScheme=val; }
};

Q_DECLARE_METATYPE(HDiscreteCurve2D*)

#endif // __HDISCRETECURVE2D_H
