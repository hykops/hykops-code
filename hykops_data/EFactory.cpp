#include "EFactory.h"

#include "HParent.h"
#include "HPoint2D.h"
#include "HPairStringDouble.h"
#include "HPoint3D.h"
#include "HSurfaceIdentifier.h"
#include "HDiscreteLoftPathPoint3D.h"
#include "HHinge.h"
#include "HCurve2D.h"
#include "HCurvePoint2D.h"
#include "HCoordinateSystem3D.h"
#include "HSeriesProfileCurve2D.h"
#include "HDiscreteCurve2D.h"
#include "HCoordinateSystem3DPolar.h"
#include "HCoordinateSystem3DAffine.h"
#include "HLoftElement.h"
#include "HCoordinateSystem3DCartesian.h"
#include "HComposition.h"
#include "HLoftPath.h"
#include "HLoft.h"
#include "HDiscreteLoftPath.h"
#include "HCoordinateSystem3DCone.h"
#include "HLinearLoftPath.h"
#include "HComponent.h"

bool EFactory::initialized=false;

void EFactory::init()
{
  if (initialized) return;
  initialized = true;
  registerListTypes();
}


QObject* EFactory::construct(const QString& className)
{
  init();
  if (className=="HParent") return new HParent;
  if (className=="HPoint2D") return new HPoint2D;
  if (className=="HPairStringDouble") return new HPairStringDouble;
  if (className=="HPoint3D") return new HPoint3D;
  if (className=="HSurfaceIdentifier") return new HSurfaceIdentifier;
  if (className=="HDiscreteLoftPathPoint3D") return new HDiscreteLoftPathPoint3D;
  if (className=="HHinge") return new HHinge;
  if (className=="HCurve2D") return new HCurve2D;
  if (className=="HCurvePoint2D") return new HCurvePoint2D;
  if (className=="HCoordinateSystem3D") return new HCoordinateSystem3D;
  if (className=="HSeriesProfileCurve2D") return new HSeriesProfileCurve2D;
  if (className=="HDiscreteCurve2D") return new HDiscreteCurve2D;
  if (className=="HCoordinateSystem3DPolar") return new HCoordinateSystem3DPolar;
  if (className=="HCoordinateSystem3DAffine") return new HCoordinateSystem3DAffine;
  if (className=="HLoftElement") return new HLoftElement;
  if (className=="HCoordinateSystem3DCartesian") return new HCoordinateSystem3DCartesian;
  if (className=="HComposition") return new HComposition;
  if (className=="HLoftPath") return new HLoftPath;
  if (className=="HLoft") return new HLoft;
  if (className=="HDiscreteLoftPath") return new HDiscreteLoftPath;
  if (className=="HCoordinateSystem3DCone") return new HCoordinateSystem3DCone;
  if (className=="HLinearLoftPath") return new HLinearLoftPath;
  if (className=="HComponent") return new HComponent;
  return 0;
}

QVariant EFactory::toVariant(const QString& className, QObject* obj)
{
  if (className=="HParent") return QVariant::fromValue(static_cast<HParent*>(obj));
  if (className=="HPoint2D") return QVariant::fromValue(static_cast<HPoint2D*>(obj));
  if (className=="HPairStringDouble") return QVariant::fromValue(static_cast<HPairStringDouble*>(obj));
  if (className=="HPoint3D") return QVariant::fromValue(static_cast<HPoint3D*>(obj));
  if (className=="HSurfaceIdentifier") return QVariant::fromValue(static_cast<HSurfaceIdentifier*>(obj));
  if (className=="HDiscreteLoftPathPoint3D") return QVariant::fromValue(static_cast<HDiscreteLoftPathPoint3D*>(obj));
  if (className=="HHinge") return QVariant::fromValue(static_cast<HHinge*>(obj));
  if (className=="HCurve2D") return QVariant::fromValue(static_cast<HCurve2D*>(obj));
  if (className=="HCurvePoint2D") return QVariant::fromValue(static_cast<HCurvePoint2D*>(obj));
  if (className=="HCoordinateSystem3D") return QVariant::fromValue(static_cast<HCoordinateSystem3D*>(obj));
  if (className=="HSeriesProfileCurve2D") return QVariant::fromValue(static_cast<HSeriesProfileCurve2D*>(obj));
  if (className=="HDiscreteCurve2D") return QVariant::fromValue(static_cast<HDiscreteCurve2D*>(obj));
  if (className=="HCoordinateSystem3DPolar") return QVariant::fromValue(static_cast<HCoordinateSystem3DPolar*>(obj));
  if (className=="HCoordinateSystem3DAffine") return QVariant::fromValue(static_cast<HCoordinateSystem3DAffine*>(obj));
  if (className=="HLoftElement") return QVariant::fromValue(static_cast<HLoftElement*>(obj));
  if (className=="HCoordinateSystem3DCartesian") return QVariant::fromValue(static_cast<HCoordinateSystem3DCartesian*>(obj));
  if (className=="HComposition") return QVariant::fromValue(static_cast<HComposition*>(obj));
  if (className=="HLoftPath") return QVariant::fromValue(static_cast<HLoftPath*>(obj));
  if (className=="HLoft") return QVariant::fromValue(static_cast<HLoft*>(obj));
  if (className=="HDiscreteLoftPath") return QVariant::fromValue(static_cast<HDiscreteLoftPath*>(obj));
  if (className=="HCoordinateSystem3DCone") return QVariant::fromValue(static_cast<HCoordinateSystem3DCone*>(obj));
  if (className=="HLinearLoftPath") return QVariant::fromValue(static_cast<HLinearLoftPath*>(obj));
  if (className=="HComponent") return QVariant::fromValue(static_cast<HComponent*>(obj));
  return QVariant();
}

QObject* EFactory::getObject(const QString& className, const QVariant& vObj)
{
  if (className=="HPoint3D") return (QObject*) vObj.value<HPoint3D*>();
  if (className=="HCurvePoint2D") return (QObject*) vObj.value<HCurvePoint2D*>();
  if (className=="HCoordinateSystem3DAffine") return (QObject*) vObj.value<HCoordinateSystem3DAffine*>();
  if (className=="HCurve2D") return (QObject*) vObj.value<HCurve2D*>();
  if (className=="HParent") return (QObject*) vObj.value<HParent*>();
  if (className=="HPoint2D") return (QObject*) vObj.value<HPoint2D*>();
  if (className=="HPairStringDouble") return (QObject*) vObj.value<HPairStringDouble*>();
  if (className=="HSurfaceIdentifier") return (QObject*) vObj.value<HSurfaceIdentifier*>();
  if (className=="HDiscreteLoftPathPoint3D") return (QObject*) vObj.value<HDiscreteLoftPathPoint3D*>();
  if (className=="HHinge") return (QObject*) vObj.value<HHinge*>();
  if (className=="HCoordinateSystem3D") return (QObject*) vObj.value<HCoordinateSystem3D*>();
  if (className=="HSeriesProfileCurve2D") return (QObject*) vObj.value<HSeriesProfileCurve2D*>();
  if (className=="HDiscreteCurve2D") return (QObject*) vObj.value<HDiscreteCurve2D*>();
  if (className=="HCoordinateSystem3DPolar") return (QObject*) vObj.value<HCoordinateSystem3DPolar*>();
  if (className=="HLoftElement") return (QObject*) vObj.value<HLoftElement*>();
  if (className=="HCoordinateSystem3DCartesian") return (QObject*) vObj.value<HCoordinateSystem3DCartesian*>();
  if (className=="HComposition") return (QObject*) vObj.value<HComposition*>();
  if (className=="HLoftPath") return (QObject*) vObj.value<HLoftPath*>();
  if (className=="HLoft") return (QObject*) vObj.value<HLoft*>();
  if (className=="HDiscreteLoftPath") return (QObject*) vObj.value<HDiscreteLoftPath*>();
  if (className=="HCoordinateSystem3DCone") return (QObject*) vObj.value<HCoordinateSystem3DCone*>();
  if (className=="HLinearLoftPath") return (QObject*) vObj.value<HLinearLoftPath*>();
  if (className=="HComponent") return (QObject*) vObj.value<HComponent*>();
  return 0;
}

void EFactory::setObject(const QString& className, QVariant& vObj, QObject *object)
{
  if (className=="HParent") vObj.setValue((HParent*)object);
  if (className=="HPoint2D") vObj.setValue((HPoint2D*)object);
  if (className=="HPairStringDouble") vObj.setValue((HPairStringDouble*)object);
  if (className=="HPoint3D") vObj.setValue((HPoint3D*)object);
  if (className=="HSurfaceIdentifier") vObj.setValue((HSurfaceIdentifier*)object);
  if (className=="HDiscreteLoftPathPoint3D") vObj.setValue((HDiscreteLoftPathPoint3D*)object);
  if (className=="HHinge") vObj.setValue((HHinge*)object);
  if (className=="HCurve2D") vObj.setValue((HCurve2D*)object);
  if (className=="HCurvePoint2D") vObj.setValue((HCurvePoint2D*)object);
  if (className=="HCoordinateSystem3D") vObj.setValue((HCoordinateSystem3D*)object);
  if (className=="HSeriesProfileCurve2D") vObj.setValue((HSeriesProfileCurve2D*)object);
  if (className=="HDiscreteCurve2D") vObj.setValue((HDiscreteCurve2D*)object);
  if (className=="HCoordinateSystem3DPolar") vObj.setValue((HCoordinateSystem3DPolar*)object);
  if (className=="HCoordinateSystem3DAffine") vObj.setValue((HCoordinateSystem3DAffine*)object);
  if (className=="HLoftElement") vObj.setValue((HLoftElement*)object);
  if (className=="HCoordinateSystem3DCartesian") vObj.setValue((HCoordinateSystem3DCartesian*)object);
  if (className=="HComposition") vObj.setValue((HComposition*)object);
  if (className=="HLoftPath") vObj.setValue((HLoftPath*)object);
  if (className=="HLoft") vObj.setValue((HLoft*)object);
  if (className=="HDiscreteLoftPath") vObj.setValue((HDiscreteLoftPath*)object);
  if (className=="HCoordinateSystem3DCone") vObj.setValue((HCoordinateSystem3DCone*)object);
  if (className=="HLinearLoftPath") vObj.setValue((HLinearLoftPath*)object);
  if (className=="HComponent") vObj.setValue((HComponent*)object);
 
}

Q_DECLARE_METATYPE(QList<HCurvePoint2D*>*)
Q_DECLARE_METATYPE(QList<HLoft*>*)
Q_DECLARE_METATYPE(QList<HHinge*>*)
Q_DECLARE_METATYPE(QList<HCurve2D*>*)
Q_DECLARE_METATYPE(QList<HSurfaceIdentifier*>*)
Q_DECLARE_METATYPE(QList<HLoftElement*>*)
Q_DECLARE_METATYPE(QList<HPairStringDouble*>*)
Q_DECLARE_METATYPE(QList<HComponent*>*)
Q_DECLARE_METATYPE(QList<HCoordinateSystem3D*>*)
Q_DECLARE_METATYPE(QList<HDiscreteLoftPathPoint3D*>*)
Q_DECLARE_METATYPE(HMapQStringQStringPtr)


void EFactory::registerListTypes()
{
  qRegisterMetaType<QList<HCurvePoint2D*>*>("QList<HCurvePoint2D*>*");
  qRegisterMetaType<QList<HLoft*>*>("QList<HLoft*>*");
  qRegisterMetaType<QList<HHinge*>*>("QList<HHinge*>*");
  qRegisterMetaType<QList<HCurve2D*>*>("QList<HCurve2D*>*");
  qRegisterMetaType<QList<HSurfaceIdentifier*>*>("QList<HSurfaceIdentifier*>*");
  qRegisterMetaType<QList<HLoftElement*>*>("QList<HLoftElement*>*");
  qRegisterMetaType<QList<HPairStringDouble*>*>("QList<HPairStringDouble*>*");
  qRegisterMetaType<QList<HComponent*>*>("QList<HComponent*>*");
  qRegisterMetaType<QList<HCoordinateSystem3D*>*>("QList<HCoordinateSystem3D*>*");
  qRegisterMetaType<QList<HDiscreteLoftPathPoint3D*>*>("QList<HDiscreteLoftPathPoint3D*>*");
  qRegisterMetaType<HMapQStringQStringPtr>("HMapQStringQStringPtr");
  qRegisterMetaType<HParent*>("HParent*");
  qRegisterMetaType<HPoint2D*>("HPoint2D*");
  qRegisterMetaType<HPairStringDouble*>("HPairStringDouble*");
  qRegisterMetaType<HPoint3D*>("HPoint3D*");
  qRegisterMetaType<HSurfaceIdentifier*>("HSurfaceIdentifier*");
  qRegisterMetaType<HDiscreteLoftPathPoint3D*>("HDiscreteLoftPathPoint3D*");
  qRegisterMetaType<HHinge*>("HHinge*");
  qRegisterMetaType<HCurve2D*>("HCurve2D*");
  qRegisterMetaType<HCurvePoint2D*>("HCurvePoint2D*");
  qRegisterMetaType<HCoordinateSystem3D*>("HCoordinateSystem3D*");
  qRegisterMetaType<HSeriesProfileCurve2D*>("HSeriesProfileCurve2D*");
  qRegisterMetaType<HDiscreteCurve2D*>("HDiscreteCurve2D*");
  qRegisterMetaType<HCoordinateSystem3DPolar*>("HCoordinateSystem3DPolar*");
  qRegisterMetaType<HCoordinateSystem3DAffine*>("HCoordinateSystem3DAffine*");
  qRegisterMetaType<HLoftElement*>("HLoftElement*");
  qRegisterMetaType<HCoordinateSystem3DCartesian*>("HCoordinateSystem3DCartesian*");
  qRegisterMetaType<HComposition*>("HComposition*");
  qRegisterMetaType<HLoftPath*>("HLoftPath*");
  qRegisterMetaType<HLoft*>("HLoft*");
  qRegisterMetaType<HDiscreteLoftPath*>("HDiscreteLoftPath*");
  qRegisterMetaType<HCoordinateSystem3DCone*>("HCoordinateSystem3DCone*");
  qRegisterMetaType<HLinearLoftPath*>("HLinearLoftPath*");
  qRegisterMetaType<HComponent*>("HComponent*");
}

QList<QObject*>* EFactory::getList(const QString& className, const QVariant& vList)
{
  if (className=="HCurvePoint2D") return (QList<QObject*>*) vList.value<QList<HCurvePoint2D*>*>();
  if (className=="HLoft") return (QList<QObject*>*) vList.value<QList<HLoft*>*>();
  if (className=="HHinge") return (QList<QObject*>*) vList.value<QList<HHinge*>*>();
  if (className=="HCurve2D") return (QList<QObject*>*) vList.value<QList<HCurve2D*>*>();
  if (className=="HSurfaceIdentifier") return (QList<QObject*>*) vList.value<QList<HSurfaceIdentifier*>*>();
  if (className=="HLoftElement") return (QList<QObject*>*) vList.value<QList<HLoftElement*>*>();
  if (className=="HPairStringDouble") return (QList<QObject*>*) vList.value<QList<HPairStringDouble*>*>();
  if (className=="HComponent") return (QList<QObject*>*) vList.value<QList<HComponent*>*>();
  if (className=="HCoordinateSystem3D") return (QList<QObject*>*) vList.value<QList<HCoordinateSystem3D*>*>();
  if (className=="HDiscreteLoftPathPoint3D") return (QList<QObject*>*) vList.value<QList<HDiscreteLoftPathPoint3D*>*>();
  return 0;
}


