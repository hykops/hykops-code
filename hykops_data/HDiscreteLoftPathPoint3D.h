#ifndef __HDISCRETELOFTPATHPOINT3D_H
#define __HDISCRETELOFTPATHPOINT3D_H

#include <QObject>
#include <QDateTime>
#include <QMetaType>
#include <QString>

#include "HPoint3D.h"


class HDiscreteLoftPathPoint3D : public HPoint3D
{
  Q_OBJECT
  Q_PROPERTY(HPoint3D* loftCurvePoint READ getLoftCurvePoint WRITE setLoftCurvePoint STORED true)
  Q_PROPERTY(double loftCoordinate READ getLoftCoordinate WRITE setLoftCoordinate STORED true)
  Q_PROPERTY(HPoint3D* loftPathVector READ getLoftPathVector WRITE setLoftPathVector STORED true)
  Q_PROPERTY(HPoint3D* loftPathXi READ getLoftPathXi WRITE setLoftPathXi STORED true)
  Q_PROPERTY(HPoint3D* loftPathEta READ getLoftPathEta WRITE setLoftPathEta STORED true)

  HPoint3D* mLoftCurvePoint;
  double mLoftCoordinate;
  HPoint3D* mLoftPathVector;
  HPoint3D* mLoftPathXi;
  HPoint3D* mLoftPathEta;
#include "HDiscreteLoftPathPoint3D_user.h"
public:
  HDiscreteLoftPathPoint3D();
  HDiscreteLoftPathPoint3D(const HDiscreteLoftPathPoint3D&) : HPoint3D() {;}
// getter:
  HPoint3D* getLoftCurvePoint() const { return mLoftCurvePoint; }
  double getLoftCoordinate() const { return mLoftCoordinate; }
  HPoint3D* getLoftPathVector() const { return mLoftPathVector; }
  HPoint3D* getLoftPathXi() const { return mLoftPathXi; }
  HPoint3D* getLoftPathEta() const { return mLoftPathEta; }
// setter:
  void setLoftCurvePoint(HPoint3D* val) { mLoftCurvePoint=val; }
  void setLoftCoordinate(double val) { mLoftCoordinate=val; }
  void setLoftPathVector(HPoint3D* val) { mLoftPathVector=val; }
  void setLoftPathXi(HPoint3D* val) { mLoftPathXi=val; }
  void setLoftPathEta(HPoint3D* val) { mLoftPathEta=val; }
};

Q_DECLARE_METATYPE(HDiscreteLoftPathPoint3D*)

#endif // __HDISCRETELOFTPATHPOINT3D_H
