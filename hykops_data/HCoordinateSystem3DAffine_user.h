// user provided (aka generated non) part of HCoordinateSystem3DAffine class header

bool operator< (const HCoordinateSystem3DAffine &other) const {

    if(this->getHParent() < other.getHParent()) {
        return true;
    }

    if(this->getHHinges() < other.getHHinges()) {
        return true;
    }

    if(this->getOrigin() != 0 && other.getOrigin() != 0) {
        if(this->getOrigin()->getX1() <  other.getOrigin()->getX1()) {
            return true;
        }
        if(this->getOrigin()->getX2() <  other.getOrigin()->getX2()) {
            return true;
        }
        if(this->getOrigin()->getX3() <  other.getOrigin()->getX3()) {
            return true;
        }
    }

    if(this->getX1() != 0 && other.getX1() != 0) {
        if(this->getX1()->getX1() <  other.getX1()->getX1()) {
            return true;
        }
        if(this->getX1()->getX2() <  other.getX1()->getX2()) {
            return true;
        }
        if(this->getX1()->getX3() <  other.getX1()->getX3()) {
            return true;
        }
    }

    if(this->getX2() != 0 && other.getX2() != 0) {
        if(this->getX2()->getX1() <  other.getX2()->getX1()) {
            return true;
        }
        if(this->getX2()->getX2() <  other.getX2()->getX2()) {
            return true;
        }
        if(this->getX2()->getX3() <  other.getX2()->getX3()) {
            return true;
        }
    }

    if(this->getX3() != 0 && other.getX3() != 0) {
        if(this->getX3()->getX1() <  other.getX3()->getX1()) {
            return true;
        }
        if(this->getX3()->getX2() <  other.getX3()->getX2()) {
            return true;
        }
        if(this->getX3()->getX3() <  other.getX3()->getX3()) {
            return true;
        }
    }

    return false;
}
