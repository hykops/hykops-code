#include "HSpline.h"

HSpline::HSpline
(
    const QList<HPoint3D> points
)
:
HParent(),
points_(points)
{}

const QList<HPoint3D>& HSpline::points() const
{
    return points_;
};
