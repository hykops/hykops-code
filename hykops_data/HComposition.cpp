#include "HComposition.h"

#include "HParent.h"
#include "HComponent.h"
#include "HCoordinateSystem3D.h"

HComposition::HComposition()
 : mVersion(0),
   mName(QString()),
   mDescription(QString()),
   mUnitSystem(QString()),
   mHCoordinateSystem3D(0),
   mHComponents(new QList<HComponent*>())
{;}

