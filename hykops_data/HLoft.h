#ifndef __HLOFT_H
#define __HLOFT_H

#include <QObject>
#include <QDateTime>
#include <QMetaType>
#include <QString>

#include <QMap>
typedef QMap<QString,QString>* HMapQStringQStringPtr;

#include "HParent.h"

class HLoftPath;
class HLoftElement;
class HSurfaceIdentifier;
class HComponent;
class HCoordinateSystem3D;

class HLoft : public HParent
{
  Q_OBJECT
  Q_PROPERTY(QString name READ getName WRITE setName STORED true)
  Q_PROPERTY(QString description READ getDescription WRITE setDescription STORED true)
  Q_PROPERTY(HMapQStringQStringPtr parameters READ getParameters STORED true)
  Q_PROPERTY(QList<HCoordinateSystem3D*>* hCoordinateSystems3D READ getHCoordinateSystems3D STORED true)
  Q_PROPERTY(HLoftPath* hLoftPath READ getHLoftPath WRITE setHLoftPath STORED true)
  Q_PROPERTY(QList<HLoftElement*>* hLoftElements READ getHLoftElements STORED true)
  Q_PROPERTY(QList<HSurfaceIdentifier*>* hSurfaceIdentifications READ getHSurfaceIdentifications STORED true)
  Q_PROPERTY(HComponent* hComponent READ getHComponent STORED false)

  QString mName;
  QString mDescription;
  HMapQStringQStringPtr mParameters;
  QList<HCoordinateSystem3D*>* mHCoordinateSystems3D;
  HLoftPath* mHLoftPath;
  QList<HLoftElement*>* mHLoftElements;
  QList<HSurfaceIdentifier*>* mHSurfaceIdentifications;
#include "HLoft_user.h"
public:
  HLoft();
  HLoft(const HLoft&) : HParent() {;}
// getter:
  QString getName() const { return mName; }
  QString getDescription() const { return mDescription; }
  HMapQStringQStringPtr getParameters() const { return mParameters; }
  QList<HCoordinateSystem3D*>* getHCoordinateSystems3D() const { return mHCoordinateSystems3D; }
  HLoftPath* getHLoftPath() const { return mHLoftPath; }
  QList<HLoftElement*>* getHLoftElements() const { return mHLoftElements; }
  QList<HSurfaceIdentifier*>* getHSurfaceIdentifications() const { return mHSurfaceIdentifications; }
  HComponent* getHComponent() const { return (HComponent*) parent(); }
// setter:
  void setName(QString val) { mName=val; }
  void setDescription(QString val) { mDescription=val; }
  void setHLoftPath(HLoftPath* val) { mHLoftPath=val; }
};

Q_DECLARE_METATYPE(HLoft*)

#endif // __HLOFT_H
