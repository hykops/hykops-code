#include "HLinearLoftPath.h"

#include "HLoftPath.h"
#include "HPoint3D.h"

HLinearLoftPath::HLinearLoftPath()
 : mOrigin(0),
   mLoftPathVector(0),
   mLoftPathXi(0),
   mLoftPathEta(0)
{;}

