#include "HLoftElement.h"

#include "HLoft.h"
#include "HCurve2D.h"
#include "HParent.h"
#include "HCoordinateSystem3D.h"

HLoftElement::HLoftElement()
 : mLoftPlaneDefiningCoordinateSystem(0),
   mHCurve2Ds(new QList<HCurve2D*>())
{;}

