#ifndef __HCURVE2D_H
#define __HCURVE2D_H

#include <QObject>
#include <QDateTime>
#include <QMetaType>
#include <QString>

#include "HParent.h"

class HSurfaceIdentifier;
class HLoftElement;

class HCurve2D : public HParent
{
  Q_OBJECT
  Q_PROPERTY(QList<HSurfaceIdentifier*>* hSurfaceIdentifiers READ getHSurfaceIdentifiers STORED true)
  Q_PROPERTY(HLoftElement* hLoftElement READ getHLoftElement STORED false)

  QList<HSurfaceIdentifier*>* mHSurfaceIdentifiers;
#include "HCurve2D_user.h"
public:
  HCurve2D();
  HCurve2D(const HCurve2D&) : HParent() {;}
// getter:
  QList<HSurfaceIdentifier*>* getHSurfaceIdentifiers() const { return mHSurfaceIdentifiers; }
  HLoftElement* getHLoftElement() const { return (HLoftElement*) parent(); }
// setter:
};

Q_DECLARE_METATYPE(HCurve2D*)

#endif // __HCURVE2D_H
