#ifndef __HCOORDINATESYSTEM3DPOLAR_H
#define __HCOORDINATESYSTEM3DPOLAR_H

#include <QObject>
#include <QDateTime>
#include <QMetaType>
#include <QString>

#include "HCoordinateSystem3D.h"

class HPoint3D;

class HCoordinateSystem3DPolar : public HCoordinateSystem3D
{
  Q_OBJECT
  Q_PROPERTY(HPoint3D* axialVector READ getAxialVector WRITE setAxialVector STORED true)
  Q_PROPERTY(HPoint3D* radialVector READ getRadialVector WRITE setRadialVector STORED true)
  Q_PROPERTY(HPoint3D* angle READ getAngle WRITE setAngle STORED true)

  HPoint3D* mAxialVector;
  HPoint3D* mRadialVector;
  HPoint3D* mAngle;
#include "HCoordinateSystem3DPolar_user.h"
public:
  HCoordinateSystem3DPolar();
  HCoordinateSystem3DPolar(const HCoordinateSystem3DPolar&) : HCoordinateSystem3D() {;}
// getter:
  HPoint3D* getAxialVector() const { return mAxialVector; }
  HPoint3D* getRadialVector() const { return mRadialVector; }
  HPoint3D* getAngle() const { return mAngle; }
// setter:
  void setAxialVector(HPoint3D* val) { mAxialVector=val; }
  void setRadialVector(HPoint3D* val) { mRadialVector=val; }
  void setAngle(HPoint3D* val) { mAngle=val; }


};

Q_DECLARE_METATYPE(HCoordinateSystem3DPolar*)

#endif // __HCOORDINATESYSTEM3DPOLAR_H
