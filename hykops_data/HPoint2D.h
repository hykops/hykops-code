#ifndef __HPOINT2D_H
#define __HPOINT2D_H

#include <QObject>
#include <QDateTime>
#include <QMetaType>
#include <QString>

#include "HParent.h"


class HPoint2D : public HParent
{
  Q_OBJECT
  Q_PROPERTY(double x1 READ getX1 WRITE setX1 STORED true)
  Q_PROPERTY(double x2 READ getX2 WRITE setX2 STORED true)

  double mX1;
  double mX2;
#include "HPoint2D_user.h"
public:
  HPoint2D();
  HPoint2D(const HPoint2D&) : HParent() {;}
// getter:
  double getX1() const { return mX1; }
  double getX2() const { return mX2; }
// setter:
  void setX1(double val) { mX1=val; }
  void setX2(double val) { mX2=val; }
};

Q_DECLARE_METATYPE(HPoint2D*)

#endif // __HPOINT2D_H
