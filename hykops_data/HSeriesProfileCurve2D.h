#ifndef __HSERIESPROFILECURVE2D_H
#define __HSERIESPROFILECURVE2D_H

#include <QObject>
#include <QDateTime>
#include <QMetaType>
#include <QString>

#include "HCurve2D.h"


class HSeriesProfileCurve2D : public HCurve2D
{
  Q_OBJECT
  Q_PROPERTY(QString denomination READ getDenomination WRITE setDenomination STORED true)
  Q_PROPERTY(QString side READ getSide WRITE setSide STORED true)
  Q_PROPERTY(QString series READ getSeries WRITE setSeries STORED true)

  QString mDenomination;
  QString mSide;
  QString mSeries;
#include "HSeriesProfileCurve2D_user.h"
public:
  HSeriesProfileCurve2D();
  HSeriesProfileCurve2D(const HSeriesProfileCurve2D&) : HCurve2D() {;}
// getter:
  QString getDenomination() const { return mDenomination; }
  QString getSide() const { return mSide; }
  QString getSeries() const { return mSeries; }
// setter:
  void setDenomination(QString val) { mDenomination=val; }
  void setSide(QString val) { mSide=val; }
  void setSeries(QString val) { mSeries=val; }
};

Q_DECLARE_METATYPE(HSeriesProfileCurve2D*)

#endif // __HSERIESPROFILECURVE2D_H
