#ifndef __HPARENT_H
#define __HPARENT_H

#include <QObject>
#include <QDateTime>
#include <QMetaType>
#include <QString>


class HParent : public QObject
{
  Q_OBJECT
  Q_PROPERTY(HParent* hParent READ getHParent WRITE setHParent STORED true)

  HParent* mHParent;
#include "HParent_user.h"
public:
  HParent();
  HParent(const HParent&) : QObject() {;}
// getter:
  HParent* getHParent() const { return mHParent; }
// setter:
  void setHParent(HParent* val) { mHParent=val; }
};

Q_DECLARE_METATYPE(HParent*)

#endif // __HPARENT_H
