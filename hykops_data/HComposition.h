#ifndef __HCOMPOSITION_H
#define __HCOMPOSITION_H

#include <QObject>
#include <QDateTime>
#include <QMetaType>
#include <QString>

#include "HParent.h"

class HComponent;
class HCoordinateSystem3D;

class HComposition : public HParent
{
  Q_OBJECT
  Q_PROPERTY(int version READ getVersion WRITE setVersion STORED true)
  Q_PROPERTY(QString name READ getName WRITE setName STORED true)
  Q_PROPERTY(QString description READ getDescription WRITE setDescription STORED true)
  Q_PROPERTY(QString unitSystem READ getUnitSystem WRITE setUnitSystem STORED true)
  Q_PROPERTY(HCoordinateSystem3D* hCoordinateSystem3D READ getHCoordinateSystem3D WRITE setHCoordinateSystem3D STORED true)
  Q_PROPERTY(QList<HComponent*>* hComponents READ getHComponents STORED true)

  int mVersion;
  QString mName;
  QString mDescription;
  QString mUnitSystem;
  HCoordinateSystem3D* mHCoordinateSystem3D;
  QList<HComponent*>* mHComponents;
#include "HComposition_user.h"
public:
  HComposition();
  HComposition(const HComposition&) : HParent() {;}
// getter:
  int getVersion() const { return mVersion; }
  QString getName() const { return mName; }
  QString getDescription() const { return mDescription; }
  QString getUnitSystem() const { return mUnitSystem; }
  HCoordinateSystem3D* getHCoordinateSystem3D() const { return mHCoordinateSystem3D; }
  QList<HComponent*>* getHComponents() const { return mHComponents; }
// setter:
  void setVersion(int val) { mVersion=val; }
  void setName(QString val) { mName=val; }
  void setDescription(QString val) { mDescription=val; }
  void setUnitSystem(QString val) { mUnitSystem=val; }
  void setHCoordinateSystem3D(HCoordinateSystem3D* val) { mHCoordinateSystem3D=val; }
};

Q_DECLARE_METATYPE(HComposition*)

#endif // __HCOMPOSITION_H
