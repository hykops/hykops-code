#ifndef __HLINEARLOFTPATH_H
#define __HLINEARLOFTPATH_H

#include <QObject>
#include <QDateTime>
#include <QMetaType>
#include <QString>

#include "HLoftPath.h"

class HPoint3D;

class HLinearLoftPath : public HLoftPath
{
  Q_OBJECT
  Q_PROPERTY(HPoint3D* origin READ getOrigin WRITE setOrigin STORED true)
  Q_PROPERTY(HPoint3D* loftPathVector READ getLoftPathVector WRITE setLoftPathVector STORED true)
  Q_PROPERTY(HPoint3D* loftPathXi READ getLoftPathXi WRITE setLoftPathXi STORED true)
  Q_PROPERTY(HPoint3D* loftPathEta READ getLoftPathEta WRITE setLoftPathEta STORED true)

  HPoint3D* mOrigin;
  HPoint3D* mLoftPathVector;
  HPoint3D* mLoftPathXi;
  HPoint3D* mLoftPathEta;
#include "HLinearLoftPath_user.h"
public:
  HLinearLoftPath();
  HLinearLoftPath(const HLinearLoftPath&) : HLoftPath() {;}
// getter:
  HPoint3D* getOrigin() const { return mOrigin; }
  HPoint3D* getLoftPathVector() const { return mLoftPathVector; }
  HPoint3D* getLoftPathXi() const { return mLoftPathXi; }
  HPoint3D* getLoftPathEta() const { return mLoftPathEta; }
// setter:
  void setOrigin(HPoint3D* val) { mOrigin=val; }
  void setLoftPathVector(HPoint3D* val) { mLoftPathVector=val; }
  void setLoftPathXi(HPoint3D* val) { mLoftPathXi=val; }
  void setLoftPathEta(HPoint3D* val) { mLoftPathEta=val; }
};

Q_DECLARE_METATYPE(HLinearLoftPath*)

#endif // __HLINEARLOFTPATH_H
