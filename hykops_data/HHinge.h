#ifndef __HHINGE_H
#define __HHINGE_H

#include <QObject>
#include <QDateTime>
#include <QMetaType>
#include <QString>

#include "HParent.h"

class HPoint3D;
class HPairStringDouble;
class HCoordinateSystem3D;

class HHinge : public HParent
{
  Q_OBJECT
  Q_PROPERTY(HPoint3D* hingeAxis READ getHingeAxis WRITE setHingeAxis STORED true)
  Q_PROPERTY(QList<HPairStringDouble*>* hingePositions READ getHingePositions STORED true)
  Q_PROPERTY(bool hingeLimited READ getHingeLimited WRITE setHingeLimited STORED true)
  Q_PROPERTY(HCoordinateSystem3D* hCoordinateSystem3D READ getHCoordinateSystem3D STORED false)

  HPoint3D* mHingeAxis;
  QList<HPairStringDouble*>* mHingePositions;
  bool mHingeLimited;
#include "HHinge_user.h"
public:
  HHinge();
  HHinge(const HHinge&) : HParent() {;}
// getter:
  HPoint3D* getHingeAxis() const { return mHingeAxis; }
  QList<HPairStringDouble*>* getHingePositions() const { return mHingePositions; }
  bool getHingeLimited() const { return mHingeLimited; }
  HCoordinateSystem3D* getHCoordinateSystem3D() const { return (HCoordinateSystem3D*) parent(); }
// setter:
  void setHingeAxis(HPoint3D* val) { mHingeAxis=val; }
  void setHingeLimited(bool val) { mHingeLimited=val; }
};

Q_DECLARE_METATYPE(HHinge*)

#endif // __HHINGE_H
