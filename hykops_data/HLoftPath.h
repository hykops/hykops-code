#ifndef __HLOFTPATH_H
#define __HLOFTPATH_H

#include <QObject>
#include <QDateTime>
#include <QMetaType>
#include <QString>

#include "HParent.h"

class HLoft;
class HCoordinateSystem3D;

class HLoftPath : public HParent
{
  Q_OBJECT
  Q_PROPERTY(QString denominator READ getDenominator WRITE setDenominator STORED true)
  Q_PROPERTY(HCoordinateSystem3D* hLoftCoordinateSystem READ getHLoftCoordinateSystem WRITE setHLoftCoordinateSystem STORED true)
  Q_PROPERTY(HLoft* hLoft READ getHLoft STORED false)

  QString mDenominator;
  HCoordinateSystem3D* mHLoftCoordinateSystem;
#include "HLoftPath_user.h"
public:
  HLoftPath();
  HLoftPath(const HLoftPath&) : HParent() {;}
// getter:
  QString getDenominator() const { return mDenominator; }
  HCoordinateSystem3D* getHLoftCoordinateSystem() const { return mHLoftCoordinateSystem; }
  HLoft* getHLoft() const { return (HLoft*) parent(); }
// setter:
  void setDenominator(QString val) { mDenominator=val; }
  void setHLoftCoordinateSystem(HCoordinateSystem3D* val) { mHLoftCoordinateSystem=val; }
};

Q_DECLARE_METATYPE(HLoftPath*)

#endif // __HLOFTPATH_H
