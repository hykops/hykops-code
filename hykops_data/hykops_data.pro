#-------------------------------------------------
#
# Project created by QtCreator 2016-07-07T15:22:50
#
#-------------------------------------------------

TARGET   = hykops_data
QT       -= gui


TEMPLATE = lib

CONFIG += static


DEFINES += LIBHYKOPS_LIBRARY

unix:DESTDIR = /tmp/libs/
win32:DESTDIR = C:\tmp\libs\


unix {
    target.path = /tmp/libs/
    INSTALLS += target
}

SOURCES += \
    EFactory.cpp \
    HSurfaceIdentifier.cpp \
    HSeriesProfileCurve2D.cpp \
    HPoint3D.cpp \
    HPoint2D.cpp \
    HParent.cpp \
    HPairStringDouble.cpp \
    HLoftPath.cpp \
    HLoftElement.cpp \
    HLoft.cpp \
    HLinearLoftPath.cpp \
    HHinge.cpp \
    HDiscreteLoftPathPoint3D.cpp \
    HDiscreteLoftPath.cpp \
    HCoordinateSystem3DCone.cpp \
    HCoordinateSystem3DCartesian.cpp \
    HCoordinateSystem3DAffine.cpp \
    HDiscreteCurve2D.cpp \
    HCurvePoint2D.cpp \
    HCurve2D.cpp \
    HCoordinateSystem3DPolar.cpp \
    HCoordinateSystem3D.cpp \
    HComposition.cpp \
    HComponent.cpp \
    HSpline.cpp

HEADERS +=\
    HSurfaceIdentifier_user.h \
    HSurfaceIdentifier.h \
    HSeriesProfileCurve2D_user.h \
    HSeriesProfileCurve2D.h \
    HPoint3D_user.h \
    HPoint3D.h \
    HPoint2D_user.h \
    HPoint2D.h \
    HParent_user.h \
    HParent.h \
    HPairStringDouble_user.h \
    HPairStringDouble.h \
    HLoftPath_user.h \
    HLoftPath.h \
    HLoftElement_user.h \
    HLoftElement.h \
    HLoft_user.h \
    HLoft.h \
    HLinearLoftPath_user.h \
    HLinearLoftPath.h \
    HHinge_user.h \
    HHinge.h \
    HDiscreteLoftPathPoint3D_user.h \
    HDiscreteLoftPathPoint3D.h \
    HDiscreteLoftPath_user.h \
    HDiscreteLoftPath.h \
    HDiscreteCurve2D_user.h \
    HDiscreteCurve2D.h \
    HCoordinateSystem3DCone.h \
    HCoordinateSystem3DCartesian_user.h \
    HCoordinateSystem3DCartesian.h \
    HCoordinateSystem3DAffine_user.h \
    HCoordinateSystem3DAffine.h \
    HCoordinateSystem3D_user.h \
    HCoordinateSystem3D.h \
    HCurvePoint2D_user.h \
    HCurvePoint2D.h \
    HCurve2D_user.h \
    HCurve2D.h \
    HCoordinateSystem3DPolar_user.h \
    HCoordinateSystem3DPolar.h \
    HCoordinateSystem3DCone_user.h \
    HComposition_user.h \
    HComposition.h \
    HComponent_user.h \
    HComponent.h \
    HSpline.h \
    EFactory.h


