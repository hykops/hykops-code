#ifndef __HCOORDINATESYSTEM3DAFFINE_H
#define __HCOORDINATESYSTEM3DAFFINE_H

#include <QObject>
#include <QDateTime>
#include <QMetaType>
#include <QString>

#include "HCoordinateSystem3D.h"

class HPoint3D;

class HCoordinateSystem3DAffine : public HCoordinateSystem3D
{
  Q_OBJECT
  Q_PROPERTY(HPoint3D* x1 READ getX1 WRITE setX1 STORED true)
  Q_PROPERTY(HPoint3D* x2 READ getX2 WRITE setX2 STORED true)
  Q_PROPERTY(HPoint3D* x3 READ getX3 WRITE setX3 STORED true)

  HPoint3D* mX1;
  HPoint3D* mX2;
  HPoint3D* mX3;
#include "HCoordinateSystem3DAffine_user.h"
public:
  HCoordinateSystem3DAffine();
  HCoordinateSystem3DAffine(const HCoordinateSystem3DAffine&) : HCoordinateSystem3D() {;}
// getter:
  HPoint3D* getX1() const { return mX1; }
  HPoint3D* getX2() const { return mX2; }
  HPoint3D* getX3() const { return mX3; }
// setter:
  void setX1(HPoint3D* val) { mX1=val; }
  void setX2(HPoint3D* val) { mX2=val; }
  void setX3(HPoint3D* val) { mX3=val; }
};

Q_DECLARE_METATYPE(HCoordinateSystem3DAffine*)

#endif // __HCOORDINATESYSTEM3DAFFINE_H
