#include "HDiscreteLoftPath.h"

#include "HLoftPath.h"
#include "HDiscreteLoftPathPoint3D.h"

HDiscreteLoftPath::HDiscreteLoftPath()
 : mLoftCurvePoints(new QList<HDiscreteLoftPathPoint3D*>()),
   mInterpolationScheme(QString())
{;}

