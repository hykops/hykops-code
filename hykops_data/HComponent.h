#ifndef __HCOMPONENT_H
#define __HCOMPONENT_H

#include <QObject>
#include <QDateTime>
#include <QMetaType>
#include <QString>

#include <QMap>
typedef QMap<QString,QString>* HMapQStringQStringPtr;

#include "HParent.h"

class HLoft;
class HComposition;
class HCoordinateSystem3D;

class HComponent : public HParent
{
  Q_OBJECT
  Q_PROPERTY(QString name READ getName WRITE setName STORED true)
  Q_PROPERTY(QString type READ getType WRITE setType STORED true)
  Q_PROPERTY(QString description READ getDescription WRITE setDescription STORED true)
  Q_PROPERTY(HMapQStringQStringPtr parameters READ getParameters STORED true)
  Q_PROPERTY(HCoordinateSystem3D* hCoordinateSystem3D READ getHCoordinateSystem3D WRITE setHCoordinateSystem3D STORED true)
  Q_PROPERTY(QList<HLoft*>* hLofts READ getHLofts STORED true)
  Q_PROPERTY(HComposition* hComposition READ getHComposition STORED false)

  QString mName;
  QString mType;
  QString mDescription;
  HMapQStringQStringPtr mParameters;
  HCoordinateSystem3D* mHCoordinateSystem3D;
  QList<HLoft*>* mHLofts;
#include "HComponent_user.h"
public:
  HComponent();
  HComponent(const HComponent&) : HParent() {;}
// getter:
  QString getName() const { return mName; }
  QString getType() const { return mType; }
  QString getDescription() const { return mDescription; }
  HMapQStringQStringPtr getParameters() const { return mParameters; }
  HCoordinateSystem3D* getHCoordinateSystem3D() const { return mHCoordinateSystem3D; }
  QList<HLoft*>* getHLofts() const { return mHLofts; }
  HComposition* getHComposition() const { return (HComposition*) parent(); }
// setter:
  void setName(QString val) { mName=val; }
  void setType(QString val) { mType=val; }
  void setDescription(QString val) { mDescription=val; }
  void setHCoordinateSystem3D(HCoordinateSystem3D* val) { mHCoordinateSystem3D=val; }
};

Q_DECLARE_METATYPE(HComponent*)

#endif // __HCOMPONENT_H
