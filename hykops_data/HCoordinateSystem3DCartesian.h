#ifndef __HCOORDINATESYSTEM3DCARTESIAN_H
#define __HCOORDINATESYSTEM3DCARTESIAN_H

#include <QObject>
#include <QDateTime>
#include <QMetaType>
#include <QString>

#include "HCoordinateSystem3D.h"

class HPoint3D;

class HCoordinateSystem3DCartesian : public HCoordinateSystem3D
{
  Q_OBJECT
  Q_PROPERTY(HPoint3D* x1 READ getX1 WRITE setX1 STORED true)
  Q_PROPERTY(HPoint3D* e1 READ getE1 WRITE setE1 STORED true)

  HPoint3D* mX1;
  HPoint3D* mE1;

#include "HCoordinateSystem3DCartesian_user.h"
public:
  HCoordinateSystem3DCartesian();
  HCoordinateSystem3DCartesian(const HCoordinateSystem3DCartesian&) : HCoordinateSystem3D() {;}
// getter:
  HPoint3D* getX1() const { return mX1; }
  HPoint3D* getE1() const { return mE1; }
// setter:
  void setX1(HPoint3D* val) { mX1=val; }
  void setE1(HPoint3D* val) { mE1=val; }
};

Q_DECLARE_METATYPE(HCoordinateSystem3DCartesian*)

#endif // __HCOORDINATESYSTEM3DCARTESIAN_H
