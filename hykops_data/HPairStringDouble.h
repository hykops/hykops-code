#ifndef __HPAIRSTRINGDOUBLE_H
#define __HPAIRSTRINGDOUBLE_H

#include <QObject>
#include <QDateTime>
#include <QMetaType>
#include <QString>

#include "HParent.h"


class HPairStringDouble : public HParent
{
  Q_OBJECT
  Q_PROPERTY(QString a READ getA WRITE setA STORED true)
  Q_PROPERTY(double b READ getB WRITE setB STORED true)

  QString mA;
  double mB;
#include "HPairStringDouble_user.h"
public:
  HPairStringDouble();
  HPairStringDouble(const HPairStringDouble&) : HParent() {;}
// getter:
  QString getA() const { return mA; }
  double getB() const { return mB; }
// setter:
  void setA(QString val) { mA=val; }
  void setB(double val) { mB=val; }
};

Q_DECLARE_METATYPE(HPairStringDouble*)

#endif // __HPAIRSTRINGDOUBLE_H
