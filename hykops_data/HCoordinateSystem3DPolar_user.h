// user provided (aka generated non) part of HCoordinateSystem3DPolar class header
bool operator< (const HCoordinateSystem3DPolar &other) const {
    if(this->getHParent() < other.getHParent()) {
        return true;
    }

    if(this->getHHinges() < other.getHHinges()) {
        return true;
    }

    if(this->getOrigin() != 0 && other.getOrigin() != 0) {
        if(this->getOrigin()->getX1() <  other.getOrigin()->getX1()) {
            return true;
        }
        if(this->getOrigin()->getX2() <  other.getOrigin()->getX2()) {
            return true;
        }
        if(this->getOrigin()->getX3() <  other.getOrigin()->getX3()) {
            return true;
        }
    }

    if(this->getAngle() != 0 && other.getAngle() != 0) {
        if(this->getAngle()->getX1() <  other.getAngle()->getX1()) {
            return true;
        }
        if(this->getAngle()->getX2() <  other.getAngle()->getX2()) {
            return true;
        }
        if(this->getAngle()->getX3() <  other.getAngle()->getX3()) {
            return true;
        }
    }

    if(this->getAxialVector() != 0 && other.getAxialVector() != 0) {
        if(this->getAxialVector()->getX1() <  other.getAxialVector()->getX1()) {
            return true;
        }
        if(this->getAxialVector()->getX2() <  other.getAxialVector()->getX2()) {
            return true;
        }
        if(this->getAxialVector()->getX3() <  other.getAxialVector()->getX3()) {
            return true;
        }
    }

    if(this->getRadialVector() != 0 && other.getRadialVector() != 0) {
        if(this->getRadialVector()->getX1() <  other.getRadialVector()->getX1()) {
            return true;
        }
        if(this->getRadialVector()->getX2() <  other.getRadialVector()->getX2()) {
            return true;
        }
        if(this->getRadialVector()->getX3() <  other.getRadialVector()->getX3()) {
            return true;
        }
    }

    return false;
}
