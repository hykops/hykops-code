#ifndef __HCOORDINATESYSTEM3D_H
#define __HCOORDINATESYSTEM3D_H

#include <QObject>
#include <QDateTime>
#include <QMetaType>
#include <QString>

#include "HParent.h"
#include "HPoint3D.h"

class HPoint3D;
class HHinge;

class HCoordinateSystem3D : public HParent
{
  Q_OBJECT
  Q_PROPERTY(HPoint3D* origin READ getOrigin WRITE setOrigin STORED true)
  Q_PROPERTY(QList<HHinge*>* hHinges READ getHHinges STORED true)

  HPoint3D* mOrigin;
  QList<HHinge*>* mHHinges;
#include "HCoordinateSystem3D_user.h"
public:
  HCoordinateSystem3D();
  HCoordinateSystem3D(const HCoordinateSystem3D&) : HParent() {;}
// getter:
  HPoint3D* getOrigin() const { return mOrigin; }
  QList<HHinge*>* getHHinges() const { return mHHinges; }
// setter:
  void setOrigin(HPoint3D* val) { mOrigin=val; }
};

Q_DECLARE_METATYPE(HCoordinateSystem3D*)

#endif // __HCOORDINATESYSTEM3D_H
