#include "HLoft.h"

#include "HLoftPath.h"
#include "HLoftElement.h"
#include "HSurfaceIdentifier.h"
#include "HParent.h"
#include "HComponent.h"
#include "HCoordinateSystem3D.h"

HLoft::HLoft()
 : mName(QString()),
   mDescription(QString()),
   mParameters(new QMap<QString,QString>()),
   mHCoordinateSystems3D(new QList<HCoordinateSystem3D*>()),
   mHLoftPath(0),
   mHLoftElements(new QList<HLoftElement*>()),
   mHSurfaceIdentifications(new QList<HSurfaceIdentifier*>())
{;}

