#ifndef __HLOFTELEMENT_H
#define __HLOFTELEMENT_H

#include <QObject>
#include <QDateTime>
#include <QMetaType>
#include <QString>

#include "HParent.h"

class HLoft;
class HCurve2D;
class HCoordinateSystem3D;

class HLoftElement : public HParent
{
  Q_OBJECT
  Q_PROPERTY(HCoordinateSystem3D* loftPlaneDefiningCoordinateSystem READ getLoftPlaneDefiningCoordinateSystem WRITE setLoftPlaneDefiningCoordinateSystem STORED true)
  Q_PROPERTY(QList<HCurve2D*>* hCurve2Ds READ getHCurve2Ds STORED true)
  Q_PROPERTY(HLoft* hLoft READ getHLoft STORED false)

  HCoordinateSystem3D* mLoftPlaneDefiningCoordinateSystem;
  QList<HCurve2D*>* mHCurve2Ds;
#include "HLoftElement_user.h"
public:
  HLoftElement();
  HLoftElement(const HLoftElement&) : HParent() {;}
// getter:
  HCoordinateSystem3D* getLoftPlaneDefiningCoordinateSystem() const { return mLoftPlaneDefiningCoordinateSystem; }
  QList<HCurve2D*>* getHCurve2Ds() const { return mHCurve2Ds; }
  HLoft* getHLoft() const { return (HLoft*) parent(); }
// setter:
  void setLoftPlaneDefiningCoordinateSystem(HCoordinateSystem3D* val) { mLoftPlaneDefiningCoordinateSystem=val; }
};

Q_DECLARE_METATYPE(HLoftElement*)

#endif // __HLOFTELEMENT_H
