#ifndef __HSURFACEIDENTIFIER_H
#define __HSURFACEIDENTIFIER_H

#include <QObject>
#include <QDateTime>
#include <QMetaType>
#include <QString>

#include "HParent.h"


class HSurfaceIdentifier : public HParent
{
  Q_OBJECT
  Q_PROPERTY(QString name READ getName WRITE setName STORED true)

  QString mName;
#include "HSurfaceIdentifier_user.h"
public:
  HSurfaceIdentifier();
  HSurfaceIdentifier(const HSurfaceIdentifier&) : HParent() {;}
// getter:
  QString getName() const { return mName; }
// setter:
  void setName(QString val) { mName=val; }
};

Q_DECLARE_METATYPE(HSurfaceIdentifier*)

#endif // __HSURFACEIDENTIFIER_H
