#include "HDiscreteLoftPathPoint3D.h"

#include "HPoint3D.h"

HDiscreteLoftPathPoint3D::HDiscreteLoftPathPoint3D()
 : mLoftCurvePoint(0),
   mLoftCoordinate(0.),
   mLoftPathVector(0),
   mLoftPathXi(0),
   mLoftPathEta(0)
{;}

