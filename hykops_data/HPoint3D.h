#ifndef __HPOINT3D_H
#define __HPOINT3D_H

#include <QObject>

#include "HParent.h"


class HPoint3D : public HParent
{
  Q_OBJECT
  Q_PROPERTY(double x1 READ getX1 WRITE setX1 STORED true)
  Q_PROPERTY(double x2 READ getX2 WRITE setX2 STORED true)
  Q_PROPERTY(double x3 READ getX3 WRITE setX3 STORED true)

  double mX1;
  double mX2;
  double mX3;
#include "HPoint3D_user.h"
public:
  HPoint3D();

  HPoint3D(const HPoint3D&) : HParent() {;}

  HPoint3D
  (
    const double& x,
    const double& y,
    const double& z
  );

// getter:
  double getX1() const { return mX1; }
  double getX2() const { return mX2; }
  double getX3() const { return mX3; }
// setter:
  void setX1(double val) { mX1=val; }
  void setX2(double val) { mX2=val; }
  void setX3(double val) { mX3=val; }
};

Q_DECLARE_METATYPE(HPoint3D*)

#endif // __HPOINT3D_H
