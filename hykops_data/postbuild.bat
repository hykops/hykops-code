REM Author: Timo Lange 
REM [Flensburger Schiffbau-Gesellschaft mbH. & Co. KG]

@ECHO off
SETLOCAL EnableExtensions
SETLOCAL EnableDelayedExpansion

REM [optional] path to your eclipse workspace containing the Hykops JNA Plugin
SET workspace=%1

REM [optional] the platform e.g.:
REM linux-x86 / linux-x86-64 / win32-x86 / win32-x86_64 / darwin [OSX]
SET platform=%2
SET jnaPluginName=de.hykops.lpd.datamodel.jna

CALL :get_pro_var "TARGET"
SET target=%res: =%

CALL :get_pro_var "win32:DESTDIR"
SET destdir=%res: =%

SET basicLibPath="%destdir%%target%.dll"

IF DEFINED workspace (
	IF DEFINED platform (
		SET destination=%workspace%\%jnaPluginName%\%platform%
		copy !basicLibPath! !destination!
	)
)

ECHO "done"
EXIT /B



REM Function to retrieve the value from key in the *.pro definition file
REM First Parameter: Key
REM Result: Value written to %res%
:get_pro_var
	FOR /F "delims=" %%i in ('dir /B *.pro') DO SET file=%%i
	FOR /F "tokens=1,2,3 delims==" %%i IN (%file%) DO (
		
	ECHO %%i | findstr %1>nul && (
  			SET res=%%j
	)
)



