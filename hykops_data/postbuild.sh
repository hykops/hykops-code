#!/bin/bash

echo $()

function get_pro_var {
	echo `cat *.pro | grep $1 | cut --delimiter='=' -f 2 | tr -d ' '`
}


libName=$(get_pro_var 'TARGET')
libDir=$(get_pro_var 'unix:DESTDIR')

# name of the Hykops JNA Plugin
jnaPluginName="de.hykops.lpd.datamodel.jna"

# (optional) path to your eclipse workspace containing the Hykops JNA Plugin
workspace=$1

# (optional) the platform e.g.:
# linux-x86 / linux-x86-64 / win32-x86 / win32-x86_64 / darwin (OSX)
platform=$2



basicLibPath=${libDir}lib${libName}.so


basicLibPath_L1=${basicLibPath}.1
basicLibPath_L2=${basicLibPath}.1.0

realLibPath=${basicLibPath}.1.0.0


echo "Removing links"
rm $basicLibPath
rm $basicLibPath_L1
rm $basicLibPath_L2


echo "Renaming to correct lib name"
mv $realLibPath $basicLibPath_L1


echo "Create link for other builds"
ln -s $basicLibPath_L1 $basicLibPath


if [ -n ${workspace+x} ]; then
	if [ -n ${platform+x} ]; then
		destination=${workspace}/${jnaPluginName}/${platform}
		echo "Copying library into:" $destination
		cp $basicLibPath_L1 $destination
	fi
fi


echo "done"
exit 0
