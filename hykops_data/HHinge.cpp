#include "HHinge.h"

#include "HPoint3D.h"
#include "HParent.h"
#include "HPairStringDouble.h"
#include "HCoordinateSystem3D.h"

HHinge::HHinge()
 : mHingeAxis(0),
   mHingePositions(new QList<HPairStringDouble*>()),
   mHingeLimited(false)
{;}

