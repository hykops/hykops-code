#ifndef __HCURVEPOINT2D_H
#define __HCURVEPOINT2D_H

#include <QObject>
#include <QDateTime>
#include <QMetaType>
#include <QString>

#include "HPoint2D.h"


class HCurvePoint2D : public HPoint2D
{
  Q_OBJECT
  Q_PROPERTY(double curveCoordinate READ getCurveCoordinate WRITE setCurveCoordinate STORED true)

  double mCurveCoordinate;
#include "HCurvePoint2D_user.h"
public:
  HCurvePoint2D();
  HCurvePoint2D(const HCurvePoint2D&) : HPoint2D() {;}
// getter:
  double getCurveCoordinate() const { return mCurveCoordinate; }
// setter:
  void setCurveCoordinate(double val) { mCurveCoordinate=val; }
};

Q_DECLARE_METATYPE(HCurvePoint2D*)

#endif // __HCURVEPOINT2D_H
