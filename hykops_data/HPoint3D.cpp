#include "HPoint3D.h"

#include "HParent.h"

HPoint3D::HPoint3D()
 : mX1(0.),
   mX2(0.),
   mX3(0.)
{;}

HPoint3D::HPoint3D
(
    const double& x,
    const double& y,
    const double& z
)
:
    HParent(),
    mX1(x),
    mX2(y),
    mX3(z)
{}
