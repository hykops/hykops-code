#include "HCoordinateSystem3DPolar.h"

#include "HPoint3D.h"
#include "HCoordinateSystem3D.h"

HCoordinateSystem3DPolar::HCoordinateSystem3DPolar()
 : mAxialVector(0),
   mRadialVector(0),
   mAngle(0)
{;}

