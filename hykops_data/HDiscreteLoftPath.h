#ifndef __HDISCRETELOFTPATH_H
#define __HDISCRETELOFTPATH_H

#include <QObject>
#include <QDateTime>
#include <QMetaType>
#include <QString>

#include "HLoftPath.h"

class HDiscreteLoftPathPoint3D;

class HDiscreteLoftPath : public HLoftPath
{
  Q_OBJECT
  Q_PROPERTY(QList<HDiscreteLoftPathPoint3D*>* loftCurvePoints READ getLoftCurvePoints STORED true)
  Q_PROPERTY(QString interpolationScheme READ getInterpolationScheme WRITE setInterpolationScheme STORED true)

  QList<HDiscreteLoftPathPoint3D*>* mLoftCurvePoints;
  QString mInterpolationScheme;
#include "HDiscreteLoftPath_user.h"
public:
  HDiscreteLoftPath();
  HDiscreteLoftPath(const HDiscreteLoftPath&) : HLoftPath() {;}
// getter:
  QList<HDiscreteLoftPathPoint3D*>* getLoftCurvePoints() const { return mLoftCurvePoints; }
  QString getInterpolationScheme() const { return mInterpolationScheme; }
// setter:
  void setInterpolationScheme(QString val) { mInterpolationScheme=val; }
};

Q_DECLARE_METATYPE(HDiscreteLoftPath*)

#endif // __HDISCRETELOFTPATH_H
