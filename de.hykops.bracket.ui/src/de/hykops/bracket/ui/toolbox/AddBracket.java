/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.bracket.ui.toolbox;

import org.eclipse.swt.SWT;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import de.fsg.tk.ui.views.operationToolBox.ConfigurationPage;
import de.fsg.tk.ui.views.operationToolBox.DefaultConfigurationPage;
import de.hykops.bracket.ui.sdk.AddExampleBracket;
import de.hykops.lpd.datamodel.HComposition;
import de.hykops.lpd.ui.editor.transformations.HYKOPSOperationProvider;
import de.hykops.profile.series.impl.NACA_ONE;

/**
 * 
 *
 * @author stoye
 * @since Nov 16, 2016
 */
public class AddBracket extends HYKOPSOperationProvider {
	private class OperationConfigurationPage extends DefaultConfigurationPage {
		public OperationConfigurationPage() {
		}

		public void createControl(Composite parent) {
			Composite composite = new Composite(parent, SWT.NONE);
			composite.setLayout(new GridLayout(1, false));
			composite.layout();
			refresh();
		}

		public void refresh() {

		}

		private OperationConfigurationPage page;

		public ConfigurationPage getConfigurationPage() {
			if (this.page == null) {
				this.page = new OperationConfigurationPage();
			}
			return this.page;
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see de.hykops.lpd.ui.editor.transformations.HYKOPSOperationProvider#
	 * performImpl(de.hykops.lpd.datamodel.HComposition)
	 */
	@Override
	public void performImpl(HComposition composition) {
		double xref = 0.;
		double yref = 0.;
		double zref = 0.;
		double length = 2.;
		double chord = 1.;
		double circumAngle = Math.toRadians(45.);
		double transversalAngle = Math.toRadians(20.);
		double radialAngle = Math.toRadians(5.);
		String profileSeries = NACA_ONE.SERIESNAME;
		String profileName = "NACA 16-020";
		AddExampleBracket.createExampleBracket(composition, xref, yref, zref, length, circumAngle, transversalAngle, radialAngle, chord, profileSeries, profileName);

	}

}
