/********************************************************************************************
 * Copyright (c) 2012 Flensburger Schiffbau-Gesellschaft mbH & Co. KG
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Flensburger Schiffbau-Gesellschaft License v1.0
 * which accompanies this distribution, and is available at
 * http://rde.fsg-ship.de/legal/fsgl-v10.html
 *
 * Contributors:
 *     Flensburger Schiffbau-Gesellschaft mbH & Co. KG - initial API and implementation
 ********************************************************************************************/
package de.hykops.bracket.ui.editor;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.impl.NotificationImpl;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.edit.domain.EditingDomain;
import org.eclipse.emf.transaction.RecordingCommand;
import org.eclipse.emf.transaction.TransactionalEditingDomain;
import org.eclipse.swt.SWT;
import org.eclipse.swt.graphics.Color;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Composite;

import de.fsg.tk.sdk.values.AbstractPhysicalDoubleValue;
import de.fsg.tk.ui.basic.ColorChooser;
import de.fsg.tk.ui.basic.UniqueColorProvider;
import de.fsg.tk.ui.widgets.BooleanValueWidget;
import de.fsg.tk.ui.widgets.IValueWidget;
import de.fsg.tk.ui.widgets.PhysicalValueWidget;
import de.fsg.tk.ui.widgets.ValueWidgetManager;
import de.hykops.bracket.core.sdk.BracketDataSupport;
import de.hykops.lpd.datamodel.HComponent;
import de.hykops.lpd.ui.editor.component.ComponentMainParameterUIInterface;
import de.hykops.lpd.ui.editor.transformations.TransformationHelper;

/**
 * 
 *
 * @author stoye
 * @since Nov 17, 2016
 */
@SuppressWarnings("restriction")
public class BracketMainParameterInterface extends ComponentMainParameterUIInterface {

	de.fsg.tk.sdk.values.IPhysicalDoubleValue<HComponent>	lengthValue;
	PhysicalValueWidget<HComponent>							lengthWidget;
	de.fsg.tk.sdk.values.IPhysicalDoubleValue<HComponent>	chordValue;
	PhysicalValueWidget<HComponent>							chordWidget;

	de.fsg.tk.sdk.values.IPhysicalDoubleValue<HComponent>	circumValue;
	PhysicalValueWidget<HComponent>							circumWidget;
	de.fsg.tk.sdk.values.IPhysicalDoubleValue<HComponent>	rakeValue;
	PhysicalValueWidget<HComponent>							rakeWidget;
	de.fsg.tk.sdk.values.IPhysicalDoubleValue<HComponent>	attackValue;
	PhysicalValueWidget<HComponent>							attackWidget;

	de.fsg.tk.sdk.values.IBooleanValue<HComponent> mirrorYValue;
	BooleanValueWidget<HComponent> mirrorYWidget;
	
	private ColorChooser									colorChooser;


	public BracketMainParameterInterface (Composite parent_){
		super(parent_);
	}

	/* (non-Javadoc)
	 * @see de.hykops.lpd.ui.editor.component.IComponentMainParameterUIInterface#getHRComponentType()
	 */
	@Override
	public String getHRComponentType() {
		return de.hykops.bracket.core.TypeID.TYPE;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * de.hykops.lpd.ui.editor.component.IComponentMainParameterUIInterface#createControl()
	 */
	@Override
	public void createMainParameterControl() {
		Composite bracketComposite = new Composite(this.mainParameterComposite, SWT.BORDER);
		bracketComposite.setLayout(new GridLayout(2, true));
		bracketComposite.setLayoutData(new GridData(SWT.FILL, SWT.FILL, true, true));

		this.lengthValue = new AbstractPhysicalDoubleValue<HComponent>(de.fsg.tk.sdk.units.Length.meter, "L", "bracket length",
				"bracket length", true) {
			@Override
			public double getValue(HComponent model) {
				return BracketDataSupport.getBracketLength(model);
			}

			@Override
			public void setValueImpl(HComponent model, double value) {
				EditingDomain eD = TransformationHelper.getEditingDomain();
				eD.getCommandStack().execute(new RecordingCommand((TransactionalEditingDomain) eD, "Set bracket height") {

					@Override
					protected void doExecute() {
						BracketDataSupport.setBracketLength(model, value);
						refreshMainParameterComposite();
					}
				});
			}
		};
		this.lengthWidget = new PhysicalValueWidget<HComponent>(bracketComposite, this.lengthValue, null);
		this.mainParameterWidgets.add(this.lengthWidget);

		this.chordValue = new AbstractPhysicalDoubleValue<HComponent>(de.fsg.tk.sdk.units.Length.meter, "C", "chord length",
				"chord length", true) {
			@Override
			public double getValue(HComponent model) {
				return BracketDataSupport.getChordLength(model);
			}

			@Override
			public void setValueImpl(HComponent model, double value) {
				EditingDomain eD = TransformationHelper.getEditingDomain();
				eD.getCommandStack().execute(new RecordingCommand((TransactionalEditingDomain) eD, "Set chord length") {

					@Override
					protected void doExecute() {
						BracketDataSupport.setChordLength(model, value);
						refreshMainParameterComposite();
					}
				});
			}
		};
		this.chordWidget = new PhysicalValueWidget<HComponent>(bracketComposite, this.chordValue, null);
		this.mainParameterWidgets.add(this.chordWidget);

		
		this.circumValue = new AbstractPhysicalDoubleValue<HComponent>(de.fsg.tk.sdk.units.Angle.radian, "beta", "circumferential angle",
				"circumferential angle", true) {
			@Override
			public double getValue(HComponent model) {
				return BracketDataSupport.getCircumAngle(model);
			}

			@Override
			public void setValueImpl(HComponent model, double value) {
				EditingDomain eD = TransformationHelper.getEditingDomain();
				eD.getCommandStack().execute(new RecordingCommand((TransactionalEditingDomain) eD, "Set circumferential angle") {

					@Override
					protected void doExecute() {
						BracketDataSupport.setCircumAngle(model, value);
						refreshMainParameterComposite();
					}
				});
			}
		};
		this.circumWidget = new PhysicalValueWidget<HComponent>(bracketComposite, this.circumValue, null);
		this.circumWidget.setUnit(de.fsg.tk.sdk.units.Angle.degree);
		this.mainParameterWidgets.add(this.circumWidget);

		
		this.rakeValue = new AbstractPhysicalDoubleValue<HComponent>(de.fsg.tk.sdk.units.Angle.radian, "gamma", "rake angle",
				"rake angle", true) {
			@Override
			public double getValue(HComponent model) {
				return BracketDataSupport.getRakeAngle(model);
			}

			@Override
			public void setValueImpl(HComponent model, double value) {
				EditingDomain eD = TransformationHelper.getEditingDomain();
				eD.getCommandStack().execute(new RecordingCommand((TransactionalEditingDomain) eD, "Set rake angle") {

					@Override
					protected void doExecute() {
						BracketDataSupport.setRakeAngle(model, value);
						refreshMainParameterComposite();
					}
				});
			}
		};
		this.rakeWidget = new PhysicalValueWidget<HComponent>(bracketComposite, this.rakeValue, null);
		this.rakeWidget.setUnit(de.fsg.tk.sdk.units.Angle.degree);
		this.mainParameterWidgets.add(this.rakeWidget);
		
		this.attackValue = new AbstractPhysicalDoubleValue<HComponent>(de.fsg.tk.sdk.units.Angle.radian, "alpha", "angle of attack",
				"angle of attack", true) {
			@Override
			public double getValue(HComponent model) {
				return BracketDataSupport.getAttackAngle(model);
			}

			@Override
			public void setValueImpl(HComponent model, double value) {
				EditingDomain eD = TransformationHelper.getEditingDomain();
				eD.getCommandStack().execute(new RecordingCommand((TransactionalEditingDomain) eD, "Set attack angle") {

					@Override
					protected void doExecute() {
						BracketDataSupport.setAttackAngle(model, value);
						refreshMainParameterComposite();
					}
				});
			}
		};
		this.attackWidget = new PhysicalValueWidget<HComponent>(bracketComposite, this.attackValue, null);
		this.attackWidget.setUnit(de.fsg.tk.sdk.units.Angle.degree);
		this.mainParameterWidgets.add(this.attackWidget);
		
		this.mirrorYValue = de.hykops.lpd.ui.dataaccess.ComponentDataAdaptor.getMirrorY();
		this.mirrorYWidget = new BooleanValueWidget<HComponent>(bracketComposite, this.mirrorYValue, null);
		this.mainParameterWidgets.add(this.mirrorYWidget);
		
		this.colorChooser = new ColorChooser(bracketComposite, HComponent.class.toString()) {
			@Override
			protected void refreshAllViews() {
				((EObject) getReference()).eNotify(new NotificationImpl(Notification.NO_FEATURE_ID, 0, 0));
			}
		};

		ValueWidgetManager.align(this.mainParameterWidgets);

		this.mainParameterComposite.pack();
		this.mainParameterComposite.layout();
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public void refreshMainParameterComposite() {
		super.refreshMainParameterComposite();
		HComponent model = getModel();
		Color col = UniqueColorProvider.getSWTColor(model, HComponent.class.toString());
		this.colorChooser.setColor(col, model);

		for (IValueWidget<?> w : this.mainParameterWidgets) {
			IValueWidget w2 = w;
			w2.setModel(model);
			w2.refresh();
		}
	}

}
